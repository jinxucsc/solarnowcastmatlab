function undistma1_filled=TO_FillBlack(undistma1,undistma2,MV)
mtsi_startup;

mv_x=MV(1);
mv_y=MV(2);
r=undistma1(:,:,1);
g=undistma1(:,:,2);
b=undistma1(:,:,3);
r_ref=undistma2(:,:,1);
g_ref=undistma2(:,:,2);
b_ref=undistma2(:,:,3);


invalidindma=(r<=10 & g<=10 & b<=10);

for y = 1:UNDISTH
    for x = 1:UNDISTW
        if true==invalidindma(y,x)
            newy=y+mv_y;
            newx=x+mv_x;
            if newy>UNDISTH || newx>UNDISTW || newy<1 || newx<1
                continue;
            end
            if r_ref(newy,newx)<=10 && g_ref(newy,newx)<=10 && b_ref(newy,newx)<=10
                continue;
            end
            r(y,x)=r_ref(newy,newx);
            g(y,x)=g_ref(newy,newx);
            b(y,x)=b_ref(newy,newx);
        end
    end
end
undistma1_filled=undistma1;
undistma1_filled(:,:,1)=r(:,:);
undistma1_filled(:,:,2)=g(:,:);
undistma1_filled(:,:,3)=b(:,:);


%   Generate MV2H Matrix
%TO_MV2HMatrix('tsi2','tsi3');


end