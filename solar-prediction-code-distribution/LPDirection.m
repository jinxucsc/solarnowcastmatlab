function LPDirect=LPDirection(Azumith)

if(Azumith==pi/2||Azumith==pi*3/2)
    LPDirect='x';
end

if(abs(tan(Azumith))>1)
    LPDirect='x';
else
    LPDirect='y';
end


end