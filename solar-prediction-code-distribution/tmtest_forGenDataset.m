% tmtest_forGenDataset is to test the realtime_mtsipipe_GenDataset since all 25 solar blocks have
% the same radiation output

% check the filled image and use filled image to do prediction
FilledTSI3Dir='./newfilled/';
preN=0;
timerange=60:60:600;
tr_str='60:60:600';
cldmodelmatf='model_rbr.mat';
configfpath='mtsi_startup';
datasetDir='./data/';
DNDIFF = datenum([0 0 0 0 10 0]); % consider only 15 minutes ahead
set_dn=java.util.HashSet;



files=dir(FilledTSI3Dir);
N=size(files,1);
utc_time = java.lang.System.currentTimeMillis;
a=utc_time/1000; % conver to UNIX TIMESTAMP
nowdn=(a/86400 + datenum(1970,1,1)); % UTC now datenum
startdn=nowdn-DNDIFF;
tmdns=[];
indice=[];
for i=1:N
    filename=files(i).name;
    tmdv=GetDVFromImg(filename);
    if tmdv==0
        continue;
    end
    tmdn=datenum(tmdv);
    if tmdn<startdn || set_dn.contains(tmdn)
        continue; % in range then start the
    end
    tmdns=[tmdns tmdn];
    indice=[indice i];
end
l=length(tmdns);
for i=1:l
    tmdn=tmdns(i);
    tmdv=datevec(tmdn);
    if isempty(find(tmdv~=[2014 03 10 18 03 13],1))
        realtime_mtsipipe_GenDataset(tmdv,timerange,cldmodelmatf,configfpath,FilledTSI3Dir,datasetDir);
    end
end
