function [angles,datenums]=FindAllSpecialTSonSunPos(IMP_PARAS)
    %   function is designed to return angles and time related stamps that 
    %   the Azumith are closed to 0 45 90 135 180 225 270 315 degree.
    %   OUT:
    %       angles          nearest to angles(in radius) to 0 45 90 135 180
    %       225 270 315
    %       datenums        related timstamps in date number form
    
    %   SGP site
    location.longitude = -97.4850; 
    location.latitude = 36.6060; 
    location.altitude = 316;        %metre
   
    THRESH=zeros(8,1);
    angles=zeros(8,1);
    datenums=zeros(8,1);
    tmpmax=zeros(8,1);
    tmpts=zeros(8,1);
    tmpangles=zeros(8,1);
    
    for i=1:8
        THRESH(i)=(i-1)*45;
        THRESH(i)=THRESH(i)*1/180*pi;
        angles(i)=-1;
        tmpmax(i)=999;
        datenums(i)=-1;
    end
    DateNum=datenum(2010,7,13,0,0,0);    %   set as default
    DateVector=datevec(DateNum);        %   Date start time
    DateNum1=datenum(2010,7,14,0,0,0);   %   Date ends time
    DateVector_final=datevec(DateNum1); %
    SPAN=datenum(0,0,0,0,1,0);          %   sampling rate defalult is 60 seconds
    TSISPAN=datenum(0,0,0,0,0,30);      %   TSI Image sampling rate

    dn_r=DateNum;
    i=1;
    while(1)
        datevector=datevec(dn_r);
        
        time.year    =  datevector(1);
        time.month   =  datevector(2);
        time.day     =  datevector(3);
        time.hour    =  datevector(4);
        time.min	=   datevector(5);
        time.sec    =   datevector(6);
        time.UTC    =   0;
        sun = sun_position(time, location);  % compute the sun position
        ZenithAngle=sun.zenith;
        Azumith=sun.azimuth;
        ZenithAngle=ZenithAngle/180*pi;
        Azumith=Azumith/180*pi;
        
        for tmpi=1:8
            if(Azumith>THRESH(tmpi))
                continue;
            else
                break;
            end
        end
        if(Azumith>THRESH(tmpi))
            % >315, i=8
            [tmpmax,tmpangles,tmpts]=IsNear(Azumith,dn_r,tmpmax,tmpangles,tmpts,THRESH,8,9);
        else
            %i-1 --- i
            [tmpmax,tmpangles,tmpts]=IsNear(Azumith,dn_r,tmpmax,tmpangles,tmpts,THRESH,tmpi-1,tmpi);
        end
        dn_r=DateNum;
        dn_r=DateNum+i*TSISPAN;
        i=i+1;
        if(dn_r>=DateNum1)
            break;
        end
    end
    angles=tmpangles;
    datenums=tmpts;
    
    function [newtmpmax,newtmpangles,newtmpts]=IsNear(Azumith,TS,tmpmax,tmpangles,tmpts,THRESH,starti,endi)
        newtmpmax=tmpmax;
        newtmpts=tmpts;
        newtmpangles=tmpangles;
        if(abs(Azumith-THRESH(starti))<=tmpmax(starti))
            newtmpmax(starti)=abs(Azumith-THRESH(starti));
            newtmpangles(starti)=Azumith;
            newtmpts(starti)=TS;
        end
        if(endi==9)
            endii=1;
            if(abs(Azumith-2*pi-THRESH(endii))<=tmpmax(endii))
                newtmpmax(endii)=abs(Azumith-2*pi-THRESH(endii));
                newtmpangles(endii)=Azumith;
                newtmpts(endii)=TS;
            end
        else
            endii=endi;
            if(abs(Azumith-2*pi-THRESH(endii))<=tmpmax(endii))
                newtmpmax(endii)=abs(Azumith-2*pi-THRESH(endii));
                newtmpangles(endii)=Azumith;
                newtmpts(endii)=TS;
            end
        end
    end
end