function H_n=tmp_FindMostSimHRan(Hs,H_range)
tml=length(Hs);
minH=min(H_range);  maxH=max(H_range);
H_n=Hs;
for i=1:tml
    if Hs(i)<minH || Hs(i)>maxH
        dispstr=sprintf('[WARNING]: H = %d is not in the H_range from %d to %d! Choose min or max.',Hs(i),minH,maxH);
        disp(dispstr);
        if Hs(i)<minH 
            H_n(i)=minH;
        else
            H_n(i)=maxH;
        end
    else
        diffvals=abs(H_range-Hs(i));
        [~,mini]=min(diffvals);
        H_n(i)=H_range(mini);
    end
    
end


end