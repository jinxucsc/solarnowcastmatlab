function [y,x]=normxcorr2_mv2coor(mv_vec,templateSize,ASize)
SearchWinSizeH=round((ASize(1)-templateSize(1))/2);
SearchWinSizeW=round((ASize(2)-templateSize(2))/2);
y=mv_vec(1)+templateSize(1)+SearchWinSizeH;
x=mv_vec(2)+templateSize(2)+SearchWinSizeW;

end
