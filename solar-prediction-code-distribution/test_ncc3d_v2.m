%   Version 2.0(obsolete):
%       1. Consider only frame t/time t
%       2. But with 3 TSIs involved
%       Please refer to ncc3d_HighCld later

function [H,maxcc,HMV_x,HMV_y]=test_ncc3d_v2(imma11,imma21,imma31,undistp31,recSize,H_range)
mtsi_startup;

t=load('hma.mat');
hma31=t.Hma.hma31;
hma32=t.Hma.hma32;

H_start=min(H_range);
H_end=max(H_range);
H_l=length(H_range);

hma31_valid=hma31;
hma32_valid=hma32;
hma31_invalidma=(hma31<H_start|hma31>=H_end|hma31==inf);
hma32_invalidma=(hma32<H_start|hma32>=H_end|hma32==inf);
hma31_valid(hma31_invalidma)=0;
hma32_valid(hma32_invalidma)=0;


H_ran31=zeros(H_l,1);
H_ran32=zeros(H_l,1);
HMV31_x=zeros(H_l,1);
HMV31_y=zeros(H_l,1);
HMV32_x=zeros(H_l,1);
HMV32_y=zeros(H_l,1);


recH=recSize(1);
recW=recSize(2);
uy31=undistp31(1);
ux31=undistp31(2);

blk31=single(TO_GetImgVal_Pad([uy31 ux31],[recH recW],imma31,20));

% 1. Get the most closest value of height
for i=1:H_l
    H=H_range(i);
    tmma=abs(hma31_valid-H);
    ind=find(tmma==min(min(tmma)));
    if length(ind)>1
        ind=ind(1);
    end
    [tmy,tmx]=ind2sub(size(tmma),ind);
    HMV31_x(i)=tmx-241;
    HMV31_y(i)=tmy-241;
    H_ran31(i)=hma31_valid(ind);
    
    tmma=abs(hma32_valid-H);
    ind=find(tmma==min(min(tmma)));
    if length(ind)>1
        ind=ind(1);
    end
    [tmy,tmx]=ind2sub(size(tmma),ind);
    HMV32_x(i)=tmx-241;
    HMV32_y(i)=tmy-241;
    H_ran32(i)=hma32_valid(ind);
    
    %     [ux_new,uy_new]=TO_Undist2Undist(ux31,uy31,H,TSI3);
    %     ux11=ux_new(1);
    %     uy11=uy_new(1);
    %     ux21=ux_new(2);
    %     uy21=uy_new(2);
    %     HMV31_x(i)=ux11-ux31;
    %     HMV31_y(i)=uy11-uy31;
    %     HMV32_x(i)=ux21-ux31;
    %     HMV32_y(i)=uy21-uy31;
    
end



% 2. For each height in range do similarity check
sim31=zeros(H_l,1);
sim32=zeros(H_l,1);
for i=1:H_l
    H=H_range(i);
    H31=H_ran31(i);
    H32=H_ran32(i);
    %     if abs(H31-H)/H>0.1 || abs(H32-H)/H>0.1
    %         continue;
    %     end
    tmHMV_x=HMV31_x(i);
    tmHMV_y=HMV31_y(i);
    ux11=round(ux31+tmHMV_x);
    uy11=round(uy31+tmHMV_y);
    blk11=single(TO_GetImgVal_Pad([uy11 ux11],[recH recW],imma11,20));
    tmHMV_x=HMV32_x(i);
    tmHMV_y=HMV32_y(i);
    ux21=round(ux31+tmHMV_x);
    uy21=round(uy31+tmHMV_y);
    blk21=single(TO_GetImgVal_Pad([uy21 ux21],[recH recW],imma21,20));
    
    tmcc=normxcorr2_blk(blk31,blk11);
    sim31(i)=tmcc(recH,recW);
    if sim31(i)<0.6
        sim31(i)=0;
        sim32(i)=0;
        continue;
    end
    tmcc=normxcorr2_blk(blk31,blk21);
    sim32(i)=tmcc(recH,recW);
    if sim32(i)<0.6
        sim31(i)=0;
        sim32(i)=0;
        continue;
    end
    %     d1=norm(blk31-blk11,2);
    %     d2=norm(blk31-blk21,2);
    %     sim31(i)=d1;
    %     sim32(i)=d2;
end
sim_all=sim31+sim32;



% 3. Get most similar Height from similiar matrix
[maxcc, ind]=max(sim_all);
% [maxcc, ind]=min(sim_all);
H=H_range(ind);
HMV_x=[HMV31_x(ind) HMV32_x(ind)];
HMV_y=[HMV31_y(ind) HMV32_y(ind)];






end