function [ranx,rany]=ReGenVertiArrayWithDim(vertiranx,vertirany,minidim)
vertiranx=round(vertiranx);
vertirany=round(vertirany);
index1=(vertiranx<=minidim)&(vertiranx>0)&(vertirany<=minidim)&(vertirany>0);
ranx=vertiranx(index1);
rany=vertirany(index1);

end