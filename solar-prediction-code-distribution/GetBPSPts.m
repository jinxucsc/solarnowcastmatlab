function [bpsx,bpsy]=GetBPSPts(tmdv,site,H)
mtsi_startup_mini;
lisfpbs_startup;

if strcmpi(TSI3,site)
    location=location3;
elseif strcmpi(TSI2,site)
    location=location2;
elseif strcmpi(TSI1,site)
    location=location1;
end


for i=1:NPBS
    % 1. Get SUN Position, then cloud should be (az,ze,H)
    time.year    =  tmdv(1);
    time.month   =  tmdv(2);
    time.day     =  tmdv(3);
    time.hour    =  tmdv(4);
    time.min    =   tmdv(5);
    time.sec    =   tmdv(6);
    time.UTC    =   0;
    tmloc.longitude=PBS_LOCATIONTABLE(i,2);
    tmloc.latitude=PBS_LOCATIONTABLE(i,1);
    tmloc.altitude=0;
    sun = sun_position(time, tmloc);
    az=sun.azimuth;
    ze=sun.zenith;
    az=az/180*pi;   ze=ze/180*pi;
    % 2. AZ to relative coordinate to PBS# (az,ze,H)->(rx_pbs,ry_pbs,rz_pbs)
    [rx_pbs,ry_pbs,rz_pbs]=AZH2SpaceCoor(az,ze,H);
    
    
    % 3. (rx_pbs,ry_pbs,rz_pbs) -> (rx,ry,rz)
    [rx,ry,rz]=TO_RelativeCoor2RelativeCoor(rx_pbs,ry_pbs,rz_pbs,tmloc,location);
    
    % 4. Relative coor to undist coor (rx,ry,rz) -> (uy,ux)
    [ux,uy,~]=TO_RelativeCoor2Undist(rx,ry,rz);
    bpsx(i)=ux;
    bpsy(i)=uy;
end



end