function bmpf=tmp_IsImgBMPInDataDir(imgObj,site,dataDir)
%label of site
TSI1='tsi1';
TSI2='tsi2';
TSI3='tsi3';

if nargin==1
    imgf=GetImgFromDV_BNL(imgObj,site);
    site=TO_isTSIImg(imgObj);
    dataDir='/data/workdata/mtsi_stich/';
elseif nargin==2
    dataDir='/data/workdata/mtsi_stich/';
end
if ischar(imgObj)
    imgf=imgObj;
elseif isfloat(imgObj) && 6==length(imgObj)
    imgf=GetImgFromDV_BNL(imgObj,site);
else
    disp('Unknown type of input! Please check your input');
end


filename=GetFilenameFromAbsPath(imgf);
fname=[filename(1:end-4) '.bmp'];


if strcmp(site,TSI1)
    tsi1D=[dataDir 'tsi1_undist_mat/'];
    bmpf=[tsi1D fname];
elseif strcmp(site,TSI2)
    tsi2D=[dataDir 'tsi2_undist_mat/'];
    bmpf=[tsi2D fname];
elseif strcmp(site,TSI3)
    tsi3D=[dataDir 'tsi3_undist_mat/'];
    bmpf=[tsi3D fname];
else
    disp('Unknown site. site should be tsi1, tsi2, tsi3');
end

if ~exist(bmpf,'file')
    bmpf=[];    
end


end