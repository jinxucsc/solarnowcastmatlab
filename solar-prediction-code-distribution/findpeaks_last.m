function [peaks locs]=findpeaks_last(vec_a)
[m,n]=size(vec_a);    
if vec_a(end-1)>vec_a(end)
    [peaks locs]=findpeaks(vec_a);
else
    [peaks locs]=findpeaks(vec_a);
    if m==1
        peaks=[peaks vec_a(end)];
        locs=[locs length(vec_a)];
    elseif n==1
        peaks=[peaks;vec_a(end)];
        locs=[locs;length(vec_a)];
    end
    
    
end


end