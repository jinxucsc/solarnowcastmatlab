function sun=INNER_GETAandZ(tmvec,lat,lon,alt)
    time.year    =  tmvec(1);
    time.month   =  tmvec(2);
    time.day     =  tmvec(3);
    time.hour    =  tmvec(4);
    time.min    =   tmvec(5);
    time.sec    =   tmvec(6);
    time.UTC    =   0;
    location.longitude=lon;
    location.latitude=lat;
    location.altitude=alt;
    sun = sun_position(time, location);
end