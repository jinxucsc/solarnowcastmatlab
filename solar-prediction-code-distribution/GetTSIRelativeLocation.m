function [rx ry rz]=GetTSIRelativeLocation(lon,lat,alt,site)
startup;

if TSI1==site
    lon_ori=location1.longitude;
    lat_ori=location1.latitude;
    alt_ori=location1.altitude;
elseif TSI2==site
    lon_ori=location2.longitude;
    lat_ori=location2.latitude;
    alt_ori=location2.altitude;
elseif TSI3==site
    lon_ori=location3.longitude;
    lat_ori=location3.latitude;
    alt_ori=location3.altitude;
end


%   Convert real coordinate to relative coordinate to origin
[rx ry rz]=TO_RealCoor2RelativeCoor(lon_ori,lat_ori,alt_ori,lon,lat,alt);
% [az ze]=TO_RelativeCoor2AZ(rx,ry,rz);
% 
% tsiaz=az;
% tsize=ze;

end