 #include "mex.h"
#include "matrix.h"
#include <math.h>


/*void matrixstd(double *imma, double *imma_std, double *imma_std_invalid, int M, int N, int stdwidth){
	int i,j=0;
	int tmi,tmj;
	double tmsum,tmsumsqr,tmavg;
	int n=(2*stdwidth+1)*(2*stdwidth+1);

	for(i=0+stdwidth; i<M-stdwidth; i++){
		for(j=0+stdwidth; j<N-stdwidth; j++){
			tmsum=0;	tmsumsqr=0;
			imma_std_invalid[i*N+j]=0;
			for (tmi=i-stdwidth;tmi<=i+stdwidth;tmi++){
				for (tmj=j-stdwidth;tmj<=j+stdwidth;tmj++){
					if (0==imma_std_invalid[i*N+j] && 0==imma[tmi*N+tmj]){
						imma_std_invalid[i*N+j]=1;
					}
					tmsum+=imma[tmi*N+tmj];
				}
			}			
			if (0==tmsum){
				imma_std[i*N+j]=0;

				char c[80];
				sprintf(c,"i=%d,j=%d,imma_std_invalid=%f,imma=%f",i,j,imma_std_invalid[i*N+j],imma[i*N+j]);
				mexWarnMsgTxt(c);
				continue;
			}

			tmavg=tmsum/n;
			for (tmi=i-stdwidth;tmi<=i+stdwidth;tmi++){
				for (tmj=j-stdwidth;tmj<=j+stdwidth;tmj++){
					tmsumsqr+=(imma[tmi*N+tmj]-tmavg)*(imma[tmi*N+tmj]-tmavg);
				}
			}
			imma_std[i*N+j]=sqrt(tmsumsqr/n);
          }
     }
	return;
}*/

void std(unsigned char * t1, int * coordinates, double *meanval, double * stdval){
	int sH=coordinates[0];
	int sW=coordinates[1];
	int eH=coordinates[2];
	int eW=coordinates[3];
	int H=coordinates[4];
	int W=coordinates[5];
	int i,j;
	double meansum=0,sumsqr=0;
	int n=(eH-sH+1)*(eW-sW+1);
	for (i=sH;i<=eH;i++){
		for (j=sW;j<=eW;j++){
			meansum+=(double)(t1[j*H+i]);
		}
	}
	meansum=meansum/n;

	for (i=sH;i<=eH;i++){
		for (j=sW;j<=eW;j++){
			sumsqr+=((double)(t1[j*H+i])-meansum)*((double)(t1[j*H+i])-meansum);
		}
	}
	*stdval=sqrt(sumsqr/n);
	*meanval=meansum;
}


void nanstd(unsigned char * t1, char * t1_mask, int * coordinates,double * meanval,double * stdval){
	int sH=coordinates[0];
	int sW=coordinates[1];
	int eH=coordinates[2];
	int eW=coordinates[3];
	int H=coordinates[4];
	int W=coordinates[5];
	int i,j;
	double meansum=0,sumsqr=0;
	/*int n=(eH-sH+1)*(eW-sW+1);*/
	int n=0;

	for (i=sH;i<=eH;i++){
		for (j=sW;j<=eW;j++){
			if (1==t1_mask[j*H+i]){
				meansum+=(double)(t1[j*H+i]);
				n++;
			}
		}
	}
	meansum=meansum/n;

	for (i=sH;i<=eH;i++){
		for (j=sW;j<=eW;j++){
			if (1==t1_mask[j*H+i]){
				sumsqr+=((double)(t1[j*H+i])-meansum)*((double)(t1[j*H+i])-meansum);}
		}
	}
	*stdval=sqrt(sumsqr/n);
	*meanval=meansum;
}






/*****************************************************************
 * INPUT:
 *	imma1 		---	[M,N], double
 *	stdwidth	---	standard deviation of [y-std y+std, x-std x+std]
 * OUTPUT:
 *	imma1_std	---	[M,N] double
 ******************************************************************/

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	/* Declare variables. */ 
	unsigned char *template, *A;
	double *cc;
	size_t M,N;

	/* Check for proper number of input and output arguments. */    
	if (nrhs != 2) {
		mexErrMsgTxt("Two input arguments required.");
	} 
	if (nlhs > 1) {
		mexErrMsgTxt("Too many output arguments.");
	}

	/* Check data type of input argument. */
	/*  if (!(mxIsDouble(prhs[0]))) {
        mexErrMsgTxt("Input array must be of type double.");
    }*/


	/* Create a pointer to every input matrix. */
	template  = (unsigned char *)mxGetData(prhs[0]);
	A  = (unsigned char *)mxGetData(prhs[1]);

	/* Get the dimensions of the input matrices. */
	int Ht = mxGetM(prhs[0]);
	int Wt = mxGetN(prhs[0]);
	int H = mxGetM(prhs[1]);
	int W = mxGetN(prhs[1]);
	int Hc=H+Ht-1;	int Wc=W+Wt-1;
	int i,j,ti,tj,ci,cj,ai,aj=0;

	/* Set the output pointer to the output matrix. */
	plhs[0] = mxCreateDoubleMatrix(Hc,Wc, mxREAL);
	cc= mxGetPr(plhs[0]);
	for (i=0;i<Hc;i++){
		for (j=0;j<Wc;j++){
			cc[j*Hc+i]=0;
		}
	}


	int bTempEmpty=0,bAEmpty=0;
	for (i=0;i<Ht;i++){
		for (j=0;j<Wt;j++){
			if (0==template[j*Ht+i]){
				bTempEmpty=true;
				break;	
			}		
		}
	}

	for (i=0;i<H;i++){
		for (j=0;j<W;j++){
			if (0==A[j*H+i]){
				bAEmpty=true;
				break;	
			}		
		}
	}

	int tempcoors[6],	Acoors[6];
	int n=Ht*Wt;
	double tmmean_t,tmstd_t,tmmean_a,tmstd_a,tmsumval;
	tempcoors[0]=0;
	tempcoors[1]=0;
	tempcoors[2]=Ht-1;
	tempcoors[3]=Wt-1;
	tempcoors[4]=Ht;
	tempcoors[5]=Wt;
	Acoors[4]=H;
	Acoors[5]=W;
	if (!bAEmpty && !bTempEmpty){
		/*Mode 1: norm search*/
		std(template,tempcoors,&tmmean_t,&tmstd_t);
		for (i=0;i<=H-Ht;i++){
			for (j=0;j<=W-Wt;j++){
				Acoors[0]=i;
				Acoors[1]=j;
				Acoors[2]=i+Ht-1;
				Acoors[3]=j+Wt-1;
				std(A,Acoors,&tmmean_a,&tmstd_a);
				tmsumval=0;
				for (ti=0;ti<Ht;ti++){
					ai=i+ti;
					for (tj=0;tj<Wt;tj++){
						aj=j+tj;
						tmsumval+=((double)(A[aj*H+ai])-tmmean_a)*((double)(template[tj*Ht+ti])-tmmean_t);
					}
				}
				tmsumval=tmsumval/tmstd_a/tmstd_t/n;
				ci=i-1+Ht;	cj=j-1+Wt;
				cc[cj*Hc+ci]=tmsumval;
			}
		}
	}
	else if (bAEmpty && !bTempEmpty){
		/*Mode 2: template is full while A is not*/
		/*std(template,tempcoors,&tmmean_t,&tmstd_t);*/
		char * AValidMask = malloc(H*W* sizeof(char));
		char * TValidMask = malloc(Ht*Wt* sizeof(char));
		/*Generate Valid Mask of A */
		for (i=0;i<H;i++){
			for (j=0;j<W;j++){
				if (0==A[j*H+i]){
					AValidMask[j*H+i]=0;}
				else{AValidMask[j*H+i]=1;}
			}
		}
		for (ti=0;ti<Ht;ti++){
			for (tj=0;tj<Wt;tj++){
				TValidMask[tj*Ht+ti]=0;
			}
		}


		for (i=0;i<=H-Ht;i++){
			for (j=0;j<=W-Wt;j++){
				Acoors[0]=i;
				Acoors[1]=j;
				Acoors[2]=i+Ht-1;
				Acoors[3]=j+Wt-1;
				nanstd(A,AValidMask,Acoors,&tmmean_a,&tmstd_a);
				tmsumval=0;
				n=0;
				for (ti=0;ti<Ht;ti++){
					ai=ti+i;
					for (tj=0;tj<Wt;tj++){
						aj=tj+j;
						if (1==AValidMask[aj*H+ai]){
							TValidMask[tj*Ht+ti]=1;
							n++;
						}else{TValidMask[tj*Ht+ti]=0;}
					}
				}
				nanstd(template,TValidMask,tempcoors,&tmmean_t,&tmstd_t);

				for (ti=0;ti<Ht;ti++){
					ai=ti+i;
					for (tj=0;tj<Wt;tj++){
						aj=tj+j;
						if (1==AValidMask[aj*H+ai]){
							tmsumval+=((double)(A[aj*H+ai])-tmmean_a)*((double)(template[tj*Ht+ti])-tmmean_t);
						}
					}
				}

				tmsumval=tmsumval/tmstd_a/tmstd_t/n;
				ci=i-1+Ht;	cj=j-1+Wt;
				cc[cj*Hc+ci]=tmsumval;
			}
		}

		free(TValidMask);
		free(AValidMask);
	}



	else if (!bAEmpty && bTempEmpty){
		/*Mode 3: A is full while template is not*/
		char * AValidMask = malloc(H*W* sizeof(char));
		char * TValidMask = malloc(Ht*Wt* sizeof(char));
		for (i=0;i<H;i++){
			for (j=0;j<W;j++){
				AValidMask[j*H+i]=0;
			}
		}



		/*Generate Valid Mask of T */
		n=0;
		for (i=0;i<Ht;i++){
			for (j=0;j<Wt;j++){
				if (0==template[j*Ht+i]){
					TValidMask[j*Ht+i]=0;}
				else{
					TValidMask[j*Ht+i]=1;
					n++;
				}
			}
		}


		nanstd(template,TValidMask,tempcoors,&tmmean_t,&tmstd_t);


		for (i=0;i<=H-Ht;i++){
			for (j=0;j<=W-Wt;j++){
				Acoors[0]=i;
				Acoors[1]=j;
				Acoors[2]=i+Ht-1;
				Acoors[3]=j+Wt-1;
				for (ti=0;ti<Ht;ti++){
					ai=ti+i;
					for (tj=0;tj<Wt;tj++){
						aj=tj+j;
						if (1==TValidMask[tj*Ht+ti]){
							AValidMask[aj*H+ai]=1;
						}else{AValidMask[aj*H+ai]=0;}
					}
				}
				nanstd(A,AValidMask,Acoors,&tmmean_a,&tmstd_a);

				tmsumval=0;
				for (ti=0;ti<Ht;ti++){
					ai=ti+i;
					for (tj=0;tj<Wt;tj++){
						aj=tj+j;
						if (1==TValidMask[tj*Ht+ti]){
							tmsumval+=((double)(A[aj*H+ai]-tmmean_a))*((double)(template[tj*Ht+ti])-tmmean_t);}
					}
				}
				tmsumval=tmsumval/tmstd_a/tmstd_t/n;
				ci=i-1+Ht;	cj=j-1+Wt;
				cc[cj*Hc+ci]=tmsumval;
			}
		}

		free(TValidMask);
		free(AValidMask);
	}	
	else if (bAEmpty && bTempEmpty){
		/*Mode 4: template and A are not full*/
		char * TValidMask = malloc(Ht*Wt* sizeof(char));
		char * AValidMask = malloc(H*W* sizeof(char));
		char * TValidMask_search = malloc(Ht*Wt* sizeof(char));
		char * AValidMask_search = malloc(H*W* sizeof(char));
		int testct,testca,testcall,ai,aj=0;
		int testshow=1;
		for (i=0;i<H;i++){
			for (j=0;j<W;j++){
				AValidMask_search[j*H+i]=0;
			}
		}

		/*	Generate Valid Mask of A */
		char c[100];

		for (i=0;i<H;i++){
			for (j=0;j<W;j++){
				if (0==A[j*H+i]){
					AValidMask[j*H+i]=0;}
				else{AValidMask[j*H+i]=1;}
			}
		}

		/*Generate Valid Mask of T */
		for (i=0;i<Ht;i++){
			for (j=0;j<Wt;j++){
				if (0==template[j*Ht+i]){
					TValidMask[j*Ht+i]=0;}
				else{TValidMask[j*Ht+i]=1;}
			}
		}
		nanstd(template,TValidMask,tempcoors,&tmmean_t,&tmstd_t);
		for (i=0;i<=H-Ht;i++){
			for (j=0;j<=W-Wt;j++){
				Acoors[0]=i;
				Acoors[1]=j;
				Acoors[2]=i+Ht-1;
				Acoors[3]=j+Wt-1;

				tmsumval=0;
				n=0;
				
				if (i==0 && j==0){
					testshow=1;
				}else{testshow=0;}
				
				for (ti=0;ti<Ht;ti++){
					for (tj=0;tj<Wt;tj++){
						ai=i+ti;
						aj=j+tj;
/*						if (1==TValidMask[ti*Wt+tj] && 0==AValidMask[(i+ti)*W+j+tj] && 1==testshow){
							char c[100];
							sprintf(c,"i=%d,j=%d,ai=%d,aj=%d,t=%d,A=%d",i,j,ai,aj,template[ti*Wt+tj],A[ai*W+aj]);
							mexWarnMsgTxt(c);
							testshow=0;
						}*/
/*						if (ti<=2 && tj<=2 && 1==testshow){
							sprintf(c,"i=%d,j=%d,ai=%d,aj=%d,t=%d,A=%d",i,j,ai,aj,template[tj*Ht+ti],A[aj*H+ai]);
							mexWarnMsgTxt(c);
						}*/
						
						if (1==TValidMask[tj*Ht+ti] && 1==AValidMask[aj*H+ai]){
							TValidMask_search[tj*Ht+ti]=1;
							AValidMask_search[aj*H+ai]=1;
							n++;
						}else{
							TValidMask_search[tj*Ht+ti]=0;
							AValidMask_search[aj*H+ai]=0;
						}
					}
				}
				
				nanstd(template,TValidMask_search,tempcoors,&tmmean_t,&tmstd_t);
				nanstd(A,AValidMask_search,Acoors,&tmmean_a,&tmstd_a);

				for (ti=0;ti<Ht;ti++){
					ai=i+ti;
					for (tj=0;tj<Wt;tj++){
						aj=j+tj;
						if (1==TValidMask[tj*Ht+ti] && 1==AValidMask[aj*H+ai]){
							tmsumval+=((double)(A[aj*H+ai]-tmmean_a))*((double)(template[tj*Ht+ti])-tmmean_t);
						}
					}
				}

				tmsumval=tmsumval/tmstd_a/tmstd_t/n;
				ci=i-1+Ht;	cj=j-1+Wt;
				cc[cj*Hc+ci]=tmsumval;
/*				if (ci==87 && cj==84){
					sprintf(c,"i=%d,j=%d,tmean=%f,tstd=%f,n=%d,testct=%d",i,j,tmmean_t,tmstd_t,n,testct);
					mexWarnMsgTxt(c);
					sprintf(c,"Amean=%f,Astd=%f,cc=%f,n=%d,testca=%d",tmmean_a,tmstd_a,tmsumval,n,testca);
					mexWarnMsgTxt(c);
				}*/
				
			}
		}
		free(TValidMask);
		free(AValidMask);
		free(TValidMask_search);
		free(AValidMask_search);
	}

}

