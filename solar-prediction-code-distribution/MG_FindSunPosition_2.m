function spcoor=MG_FindSunPosition_2(undistDirNew)
%   Find sun position within undist/ folder directly:
%   1. Dir all undist directory and do each file individually
%   2. for each file call CalImageCoorFrom_ALL to get original coor
%   3. Calculate undist coor from az and ze
%   IN:
%       undistDir(in) = 'undist/'   --  input image directory
%   OUTPUT:
%       spcoor  --  Nx2 matrix, N is the total number of images.(x,y)->(spcoor(i,1),spcoor(i,2))
%   VERSION:    1.2     2012/04/05
%           
load('UndistortionParameters.mat');



dirfiles=dir(undistDirNew);
[m,~]=size(dirfiles);
spcoor=zeros(m,2);

undistcen.x=Wu/2;
undistcen.y=Hu/2;

iter_i=1;
for i=1:m;
    if(dirfiles(i).isdir~=1)
        filename=dirfiles(i).name;
        tmdv=GetDVFromImg(filename);
        [Azimuth,Zenith]=GetAandZ(tmdv,location);   % loca from Mat file
%         [x,y]=CalImageCoorFrom_ALL(Azumith,Zenith);
        ruu=Ru*tan(Zenith)/tan(ViewAngle);
        uix=ruu*sin(Azimuth);
        uiy=ruu*cos(Azimuth);
        ux=uix+undistcen.x;
        uy=undistcen.y-uiy;
        
        spcoor(i,1)=ux;
        spcoor(i,2)=uy;
        
        ux=round(ux);
        uy=round(uy);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   test SP
        %   Draw out sp on undist image(uncoment to use)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         testoutdir='test/';
%         if(exist(testoutdir,'dir')==0)
%             mkdir(testoutdir);
%         end
%         outpath=sprintf('%s%s',testoutdir,filename);
%         undistpath=sprintf('%s%s',undistDir,filename);
%         if(ux<500&&uy<500&&ux>0&&uy>0)
%             tmimg=imread(undistpath);
%             tmimg(uy,ux,1)=255;
%             tmimg(uy,ux,2)=0;
%             tmimg(uy,ux,3)=0;
%             tmimg(uy,ux+1,1)=255;
%             tmimg(uy,ux+1,2)=0;
%             tmimg(uy,ux+1,3)=0;
%             tmimg(uy,ux-1,1)=255;
%             tmimg(uy,ux-1,2)=0;
%             tmimg(uy,ux-1,3)=0;
%             tmimg(uy+1,ux,1)=255;
%             tmimg(uy+1,ux,2)=0;
%             tmimg(uy+1,ux,3)=0;
%             tmimg(uy-1,ux,1)=255;
%             tmimg(uy-1,ux,2)=0;
%             tmimg(uy-1,ux,3)=0;
%             imwrite(tmimg,outpath,'jpg');
%         end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    else
        iter_i=iter_i+1;
    end
end
spcoor=[spcoor(iter_i:end,1),spcoor(iter_i:end,2)];
%save('spcoor.mat','spcoor');
end