tml=length(Cldc_arr);
tmcs=round(Cldc_arr./100);
tmHs=[];
for li=1:tml
    tmH=zeros(tmcs(li),1);
    tmH(:)=H_arr(li);
    tmHs=[tmHs;tmH];
end

bClusterd=false;
while false==bClusterd
    try
        [~,C]=kmeans(tmHs,MAXNUMOFLAYER);
        bClusterd=true;
    catch e
        bClusterd=false;
    end
end
if abs(C(1)-C(2))<1000
    bSingleLayered=true;
    HRef=[0 mean(tmHs)];
else
    bSingleLayered=false;
    HRef=[C(1) C(2)];
end

