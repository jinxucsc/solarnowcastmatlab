%   TO_imread is to read image into matlab. Current version support the format of *.jpg, *.bmp and
%   *.mat. 
%   INPUT:
%       imagedv             --- Datevector, image datevector
%       tsiDir              --- String, directory containing the images
%       site(optional)      --- String, 'tsi1','tsi2','tsi3'. If no site as input, the program will
%                               scan the files in tsiDir and get site information out
%
%   OUTPUT:
%       imma                --- Matrix, image matrix, default = 0 when READ fails.
%   VERSION 1.0     2014-05-07
function [imma,inputf]=TO_imread(imagedv,tsiDir,site)
imma=0;
inputf='';
if nargin==2
    TSIFileExpression='bnltsiskyimageC[123].a[1].\d{8}.\d{6}.jpg.\d{14}';
    files = dir(tsiDir);
    [m,~]=size(files);
    site = 'nan';
    for i=1:m
        if ~isempty(regexp( files(i).name,TSIFileExpression, 'once'))
            fname=files(i).name;
            switch fname(16)
                case '1'
                    site='tsi1';
                    break;
                case '2'
                    site='tsi1';
                    break;
                case '3'
                    site='tsi1';
                    break;
                otherwise
                    assert(false,['Not supported site: ' fname(16)]);
            end
        end
    end
    if strcmp(site,'nan')
        disp(['tsiDir is empty or contains no regular file matches pattern = ' TSIFileExpression]);
        return;
    end
end
imgf=GetImgFromDV_BNL(imagedv,site);
f=imgf(1:end-4);
inputf=sprintf('%s%s',tsiDir,imgf);

% bnltsiskyimageC3.a1.20130625.153650.jpg.20130625153650.jpg
if exist(inputf,'file')
    try
        imma = imread(inputf);
    catch err
        disp(err.message);
    end
    return;
end

% bnltsiskyimageC3.a1.20130625.153650.jpg.20130625153650.mat
inputf=sprintf('%s%s.mat',tsiDir,f);
if exist(inputf,'file')
    try
        t = load(inputf);
        imma=t.imma;    % default name is imma
    catch err
        disp(err.message);
    end
    return;
end


% bnltsiskyimageC3.a1.20130625.153650.jpg.20130625153650.bmp
inputf=sprintf('%s%s.bmp',tsiDir,f);
if exist(inputf,'file')
    try
        imma=imread(inputf);
    catch err
        disp(err.message);
    end
    return;
end

assert(false,'Cannot extract image matrix! Please check your inputs.');

end