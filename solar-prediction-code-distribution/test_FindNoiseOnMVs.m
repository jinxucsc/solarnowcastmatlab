function noisemvind=test_FindNoiseOnMVs(mv_x,mv_y,nmv_x,nmv_y,DISTR)

mvdist=sqrt((mv_x-nmv_x).^2+(mv_y-nmv_y).^2);
noisemvind=mvdist>DISTR;




end