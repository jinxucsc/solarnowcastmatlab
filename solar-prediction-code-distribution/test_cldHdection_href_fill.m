function test_cldHdection_href_fill(mode,cldmodelmatf,outputDir,configfpath)
%mtsi_startup;
run(configfpath);

%outputDir='tsi3_fill_3700';
outputDir=FormatDirName(outputDir);
if ~exist(outputDir,'dir')
    mkdir(outputDir);
end


startdv=[2013 05 14 14 0 0];
enddv=[2013 05 15 0 0 0];
startdn=datenum(startdv);
enddn=datenum(enddv);
InnerBlockSize=10;    
StdBlockSize=11;
StdBlockHalfSize=floor(InnerBlockSize/2);
ConnMinSize=10;
ConnMaxSize=100;
NextFrameMVSearchWinSize=15;
H_sran_low=1000:100:5000;
H_sran_high=5500:500:20000;
H_sran_simple=[2000:500:5000 5500:1000:20000];
H_sran=[H_sran_low H_sran_high];
ncc_thres=0.5;
ncc_thres_tsi32=2.1;        % ncc31,32 + ncc31,32 + ncc21,22
ncc_cloudfield_trust=0.7;   % ncc value which consider the movement is reasonable for valid cloud field
NCCFSOutMatDir='ncc_fs_all';
LOWCLDHTHRES = 5000;        % Treat as low altittude




NCCFSOutMatDir=FormatDirName(NCCFSOutMatDir);
if ~exist(NCCFSOutMatDir,'dir')
    mkdir(NCCFSOutMatDir);
end

set(0,'DefaultFigureVisible', 'on');
% set(0,'DefaultFigureVisible', 'off');
assert((exist(cldmodelmatf,'file')~=0),'Cloud Model mat file doen''s exist!');
t=load(cldmodelmatf);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
model=t.model;
x1_min=t.x1_min;
x1_max=t.x1_max;
x2_min=t.x2_min;
x2_max=t.x2_max;
x3_min=t.x3_min;
x3_max=t.x3_max;
x4_min=t.x4_min;
x4_max=t.x4_max;
h=t.h;
w_1=model.SVs'*model.sv_coef;b_1=-model.rho;
% predicted_labels=svmpredict_li(x_valid,w_1,b_1);
%     predicted_labels(predicted_labels==1)=0;        % cld
%     predicted_labels(predicted_labels==-1)=1;       % sky
x_min=[x1_min x2_min x3_min x4_min];
x_max=[x1_max x2_max x3_max x4_max];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bMulti=modepaser(mode,'multicore');
bServer=modepaser(mode,'server');
bPlot=modepaser(mode,'plot');
bRun=modepaser(mode,'run');

ONEMIN_DN=datenum([0 0 0 0 1 0]);
TENSECS_DN=datenum([0 0 0 0 0 10]);
tmdn=startdn;
oddns=[];
i=0;
while(tmdn<enddn)
    tmdn=startdn+i*TENSECS_DN;
    oddns=[oddns;tmdn];
    i=i+1;
end
oddvs=datevec(oddns);
oddns_n=length(oddns);


if bServer && bMulti
    nMatlabinst=16;
    severtype=64;
elseif  ~bServer && bMulti
    nMatlabinst=8;
    severtype=64;
end
if bServer
    dataDir='/cewit/home/zpeng/workdata/mtsi_stich/';
else
    dataDir='/data/workdata/mtsi_stich/';
end
dataDir=FormatDirName(dataDir);


OutputPath='./tmptestme.mat';


StdCldThres=5;
BlockSize=50;
SearchWinSize=50;
CorThreshold=0.5;
BlackThreshold=0;


SkyColorTestOutDir='SkyColorTestOut_histeq';
SkyColorTestOutDir=FormatDirName(SkyColorTestOutDir);
if ~exist(SkyColorTestOutDir,'dir')
    mkdir(SkyColorTestOutDir);
end

hist_predns=[];
hist_preR=[];
hist_preG=[];
hist_preB=[];

NLOWREF=1;NHIGHREF=1;
dns_ref=zeros(oddns_n,1);
H_ref_l=zeros(oddns_n,NLOWREF);
H_ref_h=zeros(oddns_n,NHIGHREF);

% ConnMap_ref;
% nConn_ref;
% Conn_H_ref;
% HMV
HRef=1800;

%   Fill the sky
skyfillfpath='skyfill.bmp';
skyfillma=imread(skyfillfpath);
    

for i =1:oddns_n-1
    tmdn=oddns(i);
    tmdv=oddvs(i,:);
    tmdv_1faf=oddvs(i+1,:);
    tmdn_1faf=oddns(i+1);
%     if tmdn<datenum([2013 06 17 14 00 00])
%         continue;
%     end
%     if rem(i,6)~=0
%         continue;
%     end
    
    try
        imma1_ori=tmp_GetImgMatFromDataDir(tmdv,TSI1,dataDir);
        imma2_ori=tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir);
        imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
        Red1 = imma3(:, :, 1);
        Green1 = imma3(:, :, 2);
        Blue1 = imma3(:, :, 3);
        HnRed1=imhist(Red1);
        HnGreen1=imhist(Green1);
        HnBlue1=imhist(Blue1);
        HnRed1(1)=0;HnGreen1(1)=0;HnBlue1(1)=0;     % 0 means not valid
        ref_sky_hist=[HnRed1 HnGreen1 HnBlue1];
        
        %         imma1_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI1,dataDir);
        %         imma2_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI2,dataDir);
        %         imma3_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI3,dataDir);
    catch
        continue;
    end
    imma1=TO_histeq_TSISky(imma1_ori,ref_sky_hist);
    imma2=TO_histeq_TSISky(imma2_ori,ref_sky_hist);
    [tmHMV_x,tmHMV_y]=TO_H2HMV(HRef,TSI3);
    tmHMV_x31=tmHMV_x(1);tmHMV_x32=tmHMV_x(2);
    tmHMV_y31=tmHMV_y(1);tmHMV_y32=tmHMV_y(2);
    imma1_lum=single(TO_rgb2lum(imma1));
    imma2_lum=single(TO_rgb2lum(imma2));
    imma3_lum=single(TO_rgb2lum(imma3));
    
    
    % Generate imma3_fill image using low alttitude cloud
    imma3_fill=imma3;
    imma3_fill=single(imma3_fill);
    imma3_fill_indma=imma3_lum==0;
    imma1_s=TO_imshift(imma1,[-tmHMV_y31 -tmHMV_x31]);
    imma2_s=TO_imshift(imma2,[-tmHMV_y32 -tmHMV_x32]);
    tmc1_ma=TO_rgb2lum(imma1_s)==0;
    tmc2_ma=TO_rgb2lum(imma2_s)==0;
    tmc1_ma_3d=logical(TO_3DMaskfrom2D(tmc1_ma));
    tmc2_ma_3d=logical(TO_3DMaskfrom2D(tmc2_ma));
    imma1_s(tmc1_ma_3d)=imma2_s(tmc1_ma_3d);
    imma2_s(tmc2_ma_3d)=imma1_s(tmc2_ma_3d);
    imma3_fill_indma_3d=logical(TO_3DMaskfrom2D(imma3_fill_indma));
    imma3_fill(imma3_fill_indma_3d)=(single(imma1_s(imma3_fill_indma_3d))+single(imma2_s(imma3_fill_indma_3d)))./2;
    imma3_fill=uint8(imma3_fill);
    invalidma=TO_rgb2lum(imma3_fill)==0;
    imma3_fill(TO_3DMaskfrom2D(invalidma))=skyfillma(TO_3DMaskfrom2D(invalidma));
    imma3_fill_l=uint8(imma3_fill);
    
    %imma1_s_l=imma1_s;
    %imma2_s_l=imma2_s;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show two filling matrix of imma3 for debug
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %     figure();
    %     subplot(2,2,1);imshow(imma3);
    %     subplot(2,2,2);imshow(uint8(imma3_fill_l));
    %     subplot(2,2,3);imshow(uint8(imma1_s_l));
    %     subplot(2,2,4);imshow(uint8(imma2_s_l));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    filename=GetImgFromDV_BNL(tmdv,TSI3);
    outputf=sprintf('%s%s_fill.bmp',outputDir,filename(1:end-4));
    imwrite(imma3_fill_l,outputf,'bmp');
    
    
        
        
end
