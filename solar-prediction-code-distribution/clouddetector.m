% this is function is the module of cloud detector
%   VERSION 1.0     2013/10/02
%       imagedatevec    --- Datevector of input image
%       cldmodematf     --- SVM trained mat file
%       dataDir         --- mtsi data directory
%   VERSION 1.1 Use realtime_getimgstd instead to extract img std
%   VERSION 1.2 2014/04/29  Change the default settings and input/output format
function [cldmask1,cldmask2,cldmask3]=clouddetector(imagedatevec,cldmodelmatf,configfpath)
run(configfpath);
% mtsi_startup;
% StdCldThres=5;                          % Std deviation compenstaion threshold
% StdBlockSize=11;                        % Std calculation block size 11x11  
% StdBlockHalfSize=floor(StdBlockSize/2);
% cldtsi1Dir=[RealtimePipeDataDir 'tsi1_cld_mat/'];
% cldtsi2Dir=[RealtimePipeDataDir 'tsi2_cld_mat/'];
% cldtsi3Dir=[RealtimePipeDataDir 'tsi3_cld_mat/'];

% pre-settings for cloud directories
if ~exist(cldtsi1Dir,'dir')
    mkdir(cldtsi1Dir);
end
if ~exist(cldtsi2Dir,'dir')
    mkdir(cldtsi2Dir);
end
if ~exist(cldtsi3Dir,'dir')
    mkdir(cldtsi3Dir);
end
cldtsi1Dir=FormatDirName(cldtsi1Dir);
cldtsi2Dir=FormatDirName(cldtsi2Dir);
cldtsi3Dir=FormatDirName(cldtsi3Dir);

tmdv=imagedatevec;
f1=GetImgFromDV_BNL(tmdv,TSI1);
f2=GetImgFromDV_BNL(tmdv,TSI2);
f3=GetImgFromDV_BNL(tmdv,TSI3);
f1=[f1(1:end-4) '.mat'];
f2=[f2(1:end-4) '.mat'];
f3=[f3(1:end-4) '.mat'];
outputf1=sprintf('%s%s',cldtsi1Dir,f1);
outputf2=sprintf('%s%s',cldtsi2Dir,f2);
outputf3=sprintf('%s%s',cldtsi3Dir,f3);

%if 0
if exist(outputf1,'file')&&exist(outputf2,'file')&&exist(outputf3,'file')
    t1=load(outputf1,'cldmask');
    t2=load(outputf2,'cldmask');
    t3=load(outputf3,'cldmask');
    cldmask1=t1.cldmask;
    cldmask2=t2.cldmask;
    cldmask3=t3.cldmask;
    return;
end


% load model file
assert((exist(cldmodelmatf,'file')~=0),'Cloud Model mat file doen''s exist!');
t=load(cldmodelmatf);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
model=t.model;
x1_min=t.x1_min;
x1_max=t.x1_max;
x2_min=t.x2_min;
x2_max=t.x2_max;
x3_min=t.x3_min;
x3_max=t.x3_max;
x4_min=t.x4_min;
x4_max=t.x4_max;
h=t.h;
w_1=model.SVs'*model.sv_coef;b_1=-model.rho;
% predicted_labels=svmpredict_li(x_valid,w_1,b_1);
%     predicted_labels(predicted_labels==1)=0;        % cld
%     predicted_labels(predicted_labels==-1)=1;       % sky
x_min=[x1_min x2_min x3_min x4_min];
x_max=[x1_max x2_max x3_max x4_max];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get site and TSI1,2,3 images
%site=TO_isTSIImg(immapath);
%assert(strcmp(site,TSI1)||strcmp(site,TSI2)||strcmp(site,TSI3),'Not valid image!Image path must contains c1,c2 or c3!');
%filename=GetFilenameFromAbsPath(immapath);
[imma1_ori,inputf1]= TO_imread(tmdv,utsi1Dir,TSI1);
[imma2_ori,inputf2]= TO_imread(tmdv,utsi2Dir,TSI2);
[imma3_ori,inputf3]= TO_imread(tmdv,utsi3Dir,TSI3);
% previous version (obsolete)
%imma1_ori=tmp_GetImgMatFromDataDir(tmdv,TSI1,RealtimePipeDataDir);
%imma2_ori=tmp_GetImgMatFromDataDir(tmdv,TSI2,RealtimePipeDataDir);
%imma3_ori=tmp_GetImgMatFromDataDir(tmdv,TSI3,RealtimePipeDataDir);

Red1 = imma3_ori(:, :, 1);
Green1 = imma3_ori(:, :, 2);
Blue1 = imma3_ori(:, :, 3);
HnRed1=imhist(Red1);
HnGreen1=imhist(Green1);
HnBlue1=imhist(Blue1);
HnRed1(1)=0;HnGreen1(1)=0;HnBlue1(1)=0;     % 0 means not valid
ref_sky_hist=[HnRed1 HnGreen1 HnBlue1];
imma1=TO_histeq_TSISky(imma1_ori,ref_sky_hist);
imma2=TO_histeq_TSISky(imma2_ori,ref_sky_hist);

% SVM classifier
xtrain1=tmp_img2traindata_rbr(imma1,h,x_min,x_max);
xtrain2=tmp_img2traindata_rbr(imma2,h,x_min,x_max);
xtrain3=tmp_img2traindata_rbr(imma3_ori,h,x_min,x_max);
pl1=svmpredict_li(xtrain1,w_1,b_1);
pl1(pl1==1)=0;        % cld
pl1(pl1==-1)=1;       % sky
pl2=svmpredict_li(xtrain2,w_1,b_1);
pl2(pl2==1)=0;        % cld
pl2(pl2==-1)=1;       % sky
pl3=svmpredict_li(xtrain3,w_1,b_1);
pl3(pl3==1)=0;        % cld
pl3(pl3==-1)=1;       % sky
imma1_cldma=false(UNDISTH,UNDISTW);
imma2_cldma=false(UNDISTH,UNDISTW);
imma3_cldma=false(UNDISTH,UNDISTW);
imma1_cldma(:)=~pl1;
imma2_cldma(:)=~pl2;
imma3_cldma(:)=~pl3;


% New version integrate std part together
% %OLD Version(obsolete)
%[imma1_std,imma2_std,imma3_std,imma1_std_invalid,imma2_std_invalid,imma3_std_invalid]...
%    =realtime_getimgstd(tmdv,StdBlockSize,RealtimePipeDataDir);
%[imma1_std,imma1_std_invalid]=tmp_GetImgStdMatFromDataDir(tmdv,TSI1,StdBlockSize,dataDir,configfpath);
%[imma2_std,imma2_std_invalid]=tmp_GetImgStdMatFromDataDir(tmdv,TSI2,StdBlockSize,dataDir,configfpath);
%[imma3_std,imma3_std_invalid]=tmp_GetImgStdMatFromDataDir(tmdv,TSI3,StdBlockSize,dataDir,configfpath);
stdstr=int2str(StdBlockSize);
tsi1Dir_std=[RealtimePipeDataDir 'tsi1_undist_std_' stdstr '/'];
tsi2Dir_std=[RealtimePipeDataDir 'tsi2_undist_std_' stdstr '/'];
tsi3Dir_std=[RealtimePipeDataDir 'tsi3_undist_std_' stdstr '/'];
outputf1=sprintf('%s%s.std.mat',tsi1Dir_std,f1);
outputf2=sprintf('%s%s.std.mat',tsi2Dir_std,f2);
outputf3=sprintf('%s%s.std.mat',tsi3Dir_std,f3);

if exist(outputf1,'file') && exist(outputf2,'file') && exist(outputf3,'file')
    t1=load(outputf1);
    t2=load(outputf2);
    t3=load(outputf3);
    imma1_std=t1.imma_std;
    imma2_std=t2.imma_std;
    imma3_std=t3.imma_std;
    imma1_std_invalid=t1.imma_std_invalid;
    imma2_std_invalid=t1.imma_std_invalid;
    imma3_std_invalid=t1.imma_std_invalid;
else
    imma_lum1=single(TO_rgb2lum(imma1_ori));
    imma_lum2=single(TO_rgb2lum(imma2_ori));
    imma_lum3=single(TO_rgb2lum(imma3_ori));
    [imma1_std,imma1_std_invalid]=imgstd(double(imma_lum1),StdBlockHalfSize);
    [imma2_std,imma2_std_invalid]=imgstd(double(imma_lum2),StdBlockHalfSize);
    [imma3_std,imma3_std_invalid]=imgstd(double(imma_lum3),StdBlockHalfSize);
    imma1_std=uint8(round(imma1_std));
    imma2_std=uint8(round(imma2_std));
    imma3_std=uint8(round(imma3_std));
    
    tmdv=imagedatevec;
    imma_std=imma1_std;
    imma_std_invalid=imma1_std_invalid;
    originalf=inputf1;
    save(outputf1,'StdBlockSize','StdBlockHalfSize','imma_std','tmdv','originalf','imma_std_invalid')
    
    imma_std=imma1_std;
    imma_std_invalid=imma2_std_invalid;
    originalf=inputf2;
    save(outputf2,'StdBlockSize','StdBlockHalfSize','imma_std','tmdv','originalf','imma_std_invalid')
    
    imma_std=imma3_std;
    imma_std_invalid=imma3_std_invalid;
    originalf=inputf3;
    save(outputf3,'StdBlockSize','StdBlockHalfSize','imma_std','tmdv','originalf','imma_std_invalid')
end


imma1_std(~imma1_std_invalid)=0;
imma2_std(~imma2_std_invalid)=0;
imma3_std(~imma3_std_invalid)=0;
cldmask1=(imma1_std>StdCldThres | imma1_cldma) & ~imma1_std_invalid;
cldmask2=(imma2_std>StdCldThres | imma2_cldma) & ~imma2_std_invalid;
cldmask3=(imma3_std>StdCldThres | imma3_cldma) & ~imma3_std_invalid;

outputf1=sprintf('%s%s',cldtsi1Dir,f1);
outputf2=sprintf('%s%s',cldtsi2Dir,f2);
outputf3=sprintf('%s%s',cldtsi3Dir,f3);
cldmask=cldmask1;
save(outputf1,'cldmask');
cldmask=cldmask2;
save(outputf2,'cldmask');
cldmask=cldmask3;
save(outputf3,'cldmask');


end