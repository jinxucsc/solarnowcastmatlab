% mtsipipe_TrainTestDataset.m
%   This function is JUST TEST VERSION! It is used for verification of mtsipipe_GenDataset.m.
%   This is a script which contains all presettings in the head.
%
% PREREQUISITE:
%       mtsipipe_GenDataset ---  Generate all datasets for training and modeling
%
% VERSION: 1.0      2014/01/14
%   Previously paperuse_multicldli_2
% VERSION: 1.1      2014/01/20
%   Follow the mtsipipe_GenDataset to use 23 features;
%
% USAGE:
%     Run script mtsipipe_GenDataset to
%
%function paperuse_multicldli_2
%set(0,'DefaultFigureVisible', 'on');
set(0,'DefaultFigureVisible', 'off');
bPlot=false;    % This is the swith to open plot for debug
bSingleStationMode = false; % Mode for single station; otherwise put all 25 stations as training data
%bModeling=false; % generate and save sfmodels.mat
BPS_N=25;
lf_para=zeros(BPS_N,7);
MAE=zeros(BPS_N,3);
RMSE=zeros(BPS_N,3);
% plot out

%datasetDir='./dataset_20131230';    %dataset direction, refer to the settings of mtsipipe_GenDataset
%outputDir='sftestplot_20131230/';       % refer to the settings of mtsipipe_GenDataset
datasetDir='./dataset_20140120';
outputDir='sftestplot_20140120/';
%modelsDir='./models_20131230';
rbftuningDir='models_rbf/';         % svr-rbf tuning results
lituningDir='models_li/';           % svr-li tuning results

nfolds=5;   % 5-folds CV
rbftuningDir=FormatDirName(rbftuningDir);
lituningDir=FormatDirName(lituningDir);
traintimerange=60:60:600;   % time range for generation of model file
meanwinsize=3;              % mean window size
startdv=[2013 06 17 0 0 0]; % startdv
enddv=[2013 06 18 0 0 0];   % enddv
str1=datestr(startdv,'yyyymmdd.HHMMSS');
str2=datestr(enddv,'yyyymmdd.HHMMSS');
tmfilename=[str1 '-' str2];

datasetDir=FormatDirName(datasetDir);
outputDir=FormatDirName(outputDir);
if ~exist(outputDir,'dir')
    mkdir(outputDir);
end
% modelsDir=FormatDirName(modelsDir);
% if ~exist(modelsDir,'dir')
%     mkdir(modelsDir);
% end
modelfile=sprintf('%s%s_models_no19.mat',datasetDir,tmfilename);     % OUTPUT model file
if ~exist(modelfile,'file')
    bModeling=true;
else
    bModeling=false;
end
% bModeling=true;   %debug for stats of total counts of training/testing data
% stats_all=[];
% stats_train=[];

% bModeling=true;   %debug for stats of total counts of testing data when using 19
% stats_test=[];

bModeling=true;         %debug for using BPS19 only
bSingleStationMode = false;
bPlot=false;
% MAE_only19=[];
% RMSE_only19=[];
% traincount_19=[];
% testcount_19=[];

rbr_ind=20;
prerbr_ind=19;
opt1='-s 3 -t 0';
opt2='-s 3 -t 2';
tl=length(traintimerange);


if bModeling
    models_li=cell(tl,BPS_N,5);
    models_rbf=cell(tl,BPS_N,5);
    models_rbr=cell(tl,BPS_N,1);
    models_a=cell(tl,BPS_N,1);
    
    models_li_allbps=cell(tl,5);
    models_rbf_allbps=cell(tl,5);
    models_rbr_allbps=cell(tl,1);
    models_a_allbps=cell(tl,1);
    
    testrmse=zeros(tl,BPS_N,5);
    testmae=zeros(tl,BPS_N,5);
    
    MAE_svrli_allbps = zeros(tl,5);
    RMSE_svrli_allbps = zeros(tl,5);
    MAE_svrrbf_allbps = zeros(tl,5);
    RMSE_svrrbf_allbps = zeros(tl,5);
    MAE_alr_allbps = zeros(tl,5);
    RMSE_alr_allbps = zeros(tl,5);
    MAE_rbr_allbps = zeros(tl,5);
    RMSE_rbr_allbps = zeros(tl,5);
    MAE_rs_allbps = zeros(tl,5);
    RMSE_rs_allbps = zeros(tl,5);
    
    for traini=1:tl
        traintime=traintimerange(traini);
        %datafile=sprintf('%s%s_%d_%d_all.mat',outputDir,tmfilename,timeahead,meanwinsize);
        modelmatf=sprintf('%s%s_%d_%d_all.mat',datasetDir,tmfilename,traintime,meanwinsize);
        if (~exist(modelmatf,'file'))
            disp(['[ERROR]: There is no such dataset file:' modelmatf]);
        end
        
        t1=load(modelmatf);
        showstarti=1;
        showendi=t1.NRECS;
        od_r_n=t1.od_r_n;   od_g_n=t1.od_g_n;   od_b_n=t1.od_b_n;
        od_prer_n=t1.od_prer_n; od_preg_n=t1.od_preg_n; od_preb_n=t1.od_preb_n;
        od_rbr_n=t1.od_rbr_n;
        od_prerbr_n=t1.od_prerbr_n;
        od_cf=t1.od_cf;
        od_rad_n=t1.od_rad_n;
        od_prerad_n=t1.od_prerad_n;
        od_arad_n=t1.od_arad_n;
        od_Href=t1.od_Href;
        od_Href(od_Href<5000)=1;
        od_Href(od_Href>=5000)=0;
        
        %svmoptions_rbf=sprintf('%s -c %d -n %f',opt2,2^ci,ui);
        rbfparas=zeros(BPS_N,3);
        liparas=zeros(BPS_N,2);
        xtrain_norm_all=[];
        ytrain_norm_all=[];
        for i=1:BPS_N
            if(i==19)
                continue;
            end
            rbfmodelf=sprintf('%srbfo%d.mat',rbftuningDir,i);
            t=load(rbfmodelf);
            rbf_paraperm=t.poly_paraperm;
            [minval,mini]=min(t.tmmae+t.tmrmse);    %mae + tmrmse
            %[minval,mini]=min(t.tmmae);
            c_rbf=rbf_paraperm(mini,1);
            g_rbf=rbf_paraperm(mini,2);
            ep_rbf=rbf_paraperm(mini,3);
            rbfparas(i,1)=c_rbf;
            rbfparas(i,2)=g_rbf;
            rbfparas(i,3)=ep_rbf;
            svmoptions_rbf=sprintf('%s -c %d -g %f -p %f -h 0',opt2,10^c_rbf,10^g_rbf,ep_rbf);
            
            limodelf=sprintf('%slio%d.mat',lituningDir,i);
            t=load(limodelf);
            li_paraperm=t.poly_paraperm;
            %[minval,mini]=min(t.tmmae+t.tmrmse); %mae + tmrmse
            [minval,mini]=min(t.tmmae); %mae
            c_li=li_paraperm(mini,1);
            %g_rbf=rbf_paraperm(mini,2);
            ep_li=li_paraperm(mini,2);
            liparas(i,1)=c_li;
            liparas(i,2)=ep_li;
            svmoptions_li=sprintf('%s -c %d -p %f -h 0',opt1,2^c_li,ep_li);
            
            outputf=sprintf('%ssfmodel_bps%d.png',outputDir,i);
            r_m=od_r_n(showstarti:showendi,i,3);
            g_m=od_g_n(showstarti:showendi,i,3);
            b_m=od_b_n(showstarti:showendi,i,3);
            r_min=od_r_n(showstarti:showendi,i,1);
            g_min=od_g_n(showstarti:showendi,i,1);
            b_min=od_b_n(showstarti:showendi,i,1);
            r_max=od_r_n(showstarti:showendi,i,2);
            g_max=od_g_n(showstarti:showendi,i,2);
            b_max=od_b_n(showstarti:showendi,i,2);
            arad = od_arad_n(showstarti:showendi,i);
            rbr=od_rbr_n(showstarti:showendi,i);
            
            prer_m=od_prer_n(showstarti:showendi,i,3);
            preg_m=od_preg_n(showstarti:showendi,i,3);
            preb_m=od_preb_n(showstarti:showendi,i,3);
            prer_min=od_prer_n(showstarti:showendi,i,1);
            preg_min=od_preg_n(showstarti:showendi,i,1);
            preb_min=od_preb_n(showstarti:showendi,i,1);
            prer_max=od_prer_n(showstarti:showendi,i,2);
            preg_max=od_preg_n(showstarti:showendi,i,2);
            preb_max=od_preb_n(showstarti:showendi,i,2);
            
            
            
            prerad=od_prerad_n(showstarti:showendi,i);
            prerbr=od_prerbr_n(showstarti:showendi,i);
            cldf=od_cf(showstarti:showendi);
            fit_y = od_rad_n(showstarti:showendi,i);
            % 17 features -> 23 features
            xtrain_norm=...
                [r_m r_min r_max g_m g_min g_max b_m b_min b_max ...
                prer_m prer_min prer_max preg_m preg_min preg_max preb_m preb_min preb_max ...
                prerbr rbr cldf arad prerad];
            %xtrain_norm=[r g b prer preg preb prerbr rbr cldf prerad];
            ytrain_norm=fit_y;
            
            invalidma=markInvalid(xtrain_norm,'nan+0');
            if ~isempty(find(invalidma,1))
                disp('[WARNING]: There is invalid value in training dataset, remove it');
                tmrecinvalid=false(size(xtrain_norm,1),1);
                for atti=1:size(xtrain_norm,2)
                    tmrecinvalid=tmrecinvalid|invalidma(:,atti);
                end
            else
                tmrecinvalid=false(size(xtrain_norm,1),1);
            end
            ytrain_norm=ytrain_norm(~tmrecinvalid);
            xtrain_norm=xtrain_norm(~tmrecinvalid,:);
            xtrain_norm_all=[xtrain_norm_all;xtrain_norm];
            ytrain_norm_all=[ytrain_norm_all;ytrain_norm];
            
            if bSingleStationMode
                % This block is to do 5-fold cv for each BPS
                [xtrain,ytrain,xtest,ytest]=cvGenFolds(xtrain_norm,ytrain_norm,nfolds);
                tmli_a=[];tmrbr_a=[];
                MAE_svrli=zeros(nfolds,1);
                RMSE_svrli=zeros(nfolds,1);
                MAE_svrrbf=zeros(nfolds,1);
                RMSE_svrrbf=zeros(nfolds,1);
                MAE_alr=zeros(nfolds,1);
                RMSE_alr=zeros(nfolds,1);
                MAE_rbr=zeros(nfolds,1);
                RMSE_rbr=zeros(nfolds,1);
                MAE_shift=zeros(nfolds,1);
                RMSE_shift=zeros(nfolds,1);
                alr_a_folds=[];
                rbr_a_folds=[];
                for fi=1:nfolds
                    tmytrain=ytrain{fi};    tmytest = ytest{fi};
                    tmxtrain=xtrain{fi};    tmxtest = xtest{fi};
                    model_li=svmtrain(tmytrain,tmxtrain,svmoptions_li);
                    model_rbf=svmtrain(tmytrain,tmxtrain,svmoptions_rbf);
                    assert(~isnan(model_li.rho));
                    [predicted_label, accuracy, y1]=svmpredict(tmytest,tmxtest,model_li);
                    [predicted_label, accuracy, y2]=svmpredict(tmytest,tmxtest,model_rbf);
                    models_li{traini,i,fi}=model_li;
                    models_rbf{traini,i,fi}=model_rbf;
                    y1(y1>1)=1; y1(y1<0)=0;
                    y2(y2>1)=1; y2(y2<0)=0;
                    MAE_svrli(i,fi)=mean(abs(y1-tmytest));
                    RMSE_svrli(i,fi)=sqrt(mean((y1-tmytest).^2));
                    MAE_svrrbf(i,fi)=mean(abs(y2-tmytest));
                    RMSE_svrrbf(i,fi)=sqrt(mean((y2-tmytest).^2));
                    tmlix=[ones(size(tmxtrain,1),1) tmxtrain];
                    tmliy=tmytrain;
                    tm_alr=tmlix\tmliy;
                    pred_y=[ones(size(tmxtest,1),1) tmxtest]*tm_alr;
                    MAE_alr(fi)=mean(abs(pred_y-tmytest));
                    RMSE_alr(fi)=sqrt(mean((pred_y-tmytest).^2));
                    tm_alr=tm_alr';
                    alr_a_folds=[alr_a_folds;tm_alr];
                    tmrbrx=tmxtrain(:,rbr_ind)-tmxtrain(:,prerbr_ind); %rbr-prerbr
                    tmrbrx_test=tmxtest(:,rbr_ind)-tmxtest(:,prerbr_ind); %rbr-prerbr
                    tmrbry=tmytrain-tmxtrain(:,end);    %rad - prerad
                    tmrbry_test=tmytest-tmxtest(:,end);    %rad - prerad
                    tmrbr_a=tmrbrx\tmrbry;  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
                    pred_y=tmrbrx_test*tmrbr_a;
                    pred_y=pred_y+tmxtest(:,end);       %predy = predy + prerad
                    MAE_rbr(fi)=mean(abs(pred_y-tmytest));
                    RMSE_rbr(fi)=sqrt(mean((pred_y-tmytest).^2));
                    rbr_a_folds=[rbr_a_folds;tmrbr_a];
                    
                    pred_y=tmxtest(:,end);
                    MAE_shift(fi)=mean(abs(pred_y-tmytest));
                    RMSE_shift(fi)=sqrt(mean((pred_y-tmytest).^2));

                end
                a=mean(alr_a_folds);
                a1=mean(rbr_a_folds);
                models_a{traini,i}=a;
                models_rbr{traini,i}=a1;
                
                % debug for BPS19 only
                MAEs=zeros(1,5);
                RMSEs=zeros(1,5);
                MAEs(1) = mean(MAE_alr(:));
                MAEs(2) = mean(MAE_rbr(:));
                MAEs(3) = mean(MAE_svrli(i,:));
                MAEs(4) = mean(MAE_svrrbf(i,:));
                MAEs(5) = mean(MAE_shift);
                MAE_only19=[MAE_only19;MAEs];
                RMSEs(1) = mean(RMSE_alr(:));
                RMSEs(2) = mean(RMSE_rbr(:));
                RMSEs(3) = mean(RMSE_svrli(i,:));
                RMSEs(4) = mean(RMSE_svrrbf(i,:));
                RMSEs(5) = mean(RMSE_shift(:));   % we didn't add rshift here
                RMSE_only19=[RMSE_only19;RMSEs];
                
                traincount_19=[traincount_19;size(ytrain{1},1)];
                testcount_19=[testcount_19;size(ytest{1},1)];
                
                % Test of 5 folds CV MSE and MAE
                if(bPlot)
                    MAEs=zeros(5,1);
                    RMSEs=zeros(5,1);
                    MAEs(1) = mean(MAE_alr(:));
                    MAEs(2) = mean(MAE_svrli(i,:));
                    MAEs(3) = mean(MAE_svrrbf(i,:));
                    MAEs(4) = mean(MAE_rbr(:));
                    MAEs(5) = mean(MAE_shift);
                    RMSEs(1) = mean(RMSE_alr(:));
                    RMSEs(2) = mean(RMSE_svrli(i,:));
                    RMSEs(3) = mean(RMSE_svrrbf(i,:));
                    RMSEs(4) = mean(RMSE_rbr(:));
                    RMSEs(5) = mean(RMSE_shift(:));   % we didn't add rshift here
                    f1=figure();
                    bar(MAEs);
                    f2=figure();
                    bar(RMSEs);
                end
            end
        end
        
        
        
        if bSingleStationMode==false
            %   This block is to do 5-fold cv for ALL BPS
            [xtrain,ytrain,xtest,ytest]=cvGenFolds(xtrain_norm_all,ytrain_norm_all,nfolds);
%             stats_all=[stats_all;size(ytrain_norm_all,1)];
%             stats_train=[stats_train;size(ytrain{1},1)];
%             continue; %debug only
            alr_a_folds=[];
            rbr_a_folds=[];
            ti=traini;
            for fi=1:nfolds
                tmytrain=ytrain{fi};    tmytest = ytest{fi};
                tmxtrain=xtrain{fi};    tmxtest = xtest{fi};
                model_li=svmtrain(tmytrain,tmxtrain,svmoptions_li);
                model_rbf=svmtrain(tmytrain,tmxtrain,svmoptions_rbf);
                assert(~isnan(model_li.rho));
                [predicted_label, accuracy, y1]=svmpredict(tmytest,tmxtest,model_li);
                [predicted_label, accuracy, y2]=svmpredict(tmytest,tmxtest,model_rbf);
                models_li_allbps{traini,fi}=model_li;
                models_rbf_allbps{traini,fi}=model_rbf;
                y1(y1>1)=1; y1(y1<0)=0;
                y2(y2>1)=1; y2(y2<0)=0;
                MAE_svrli_allbps(ti,fi)=mean(abs(y1-tmytest));
                RMSE_svrli_allbps(ti,fi)=sqrt(mean((y1-tmytest).^2));
                MAE_svrrbf_allbps(ti,fi)=mean(abs(y2-tmytest));
                RMSE_svrrbf_allbps(ti,fi)=sqrt(mean((y2-tmytest).^2));
                tmlix=[ones(size(tmxtrain,1),1) tmxtrain];
                tmliy=tmytrain;
                tm_alr=tmlix\tmliy;
                pred_y=[ones(size(tmxtest,1),1) tmxtest]*tm_alr;
                MAE_alr_allbps(ti,fi)=mean(abs(pred_y-tmytest));
                RMSE_alr_allbps(ti,fi)=sqrt(mean((pred_y-tmytest).^2));
                tm_alr=tm_alr';
                alr_a_folds=[alr_a_folds;tm_alr];
                tmrbrx=tmxtrain(:,rbr_ind)-tmxtrain(:,prerbr_ind); %rbr-prerbr
                tmrbrx_test=tmxtest(:,rbr_ind)-tmxtest(:,prerbr_ind); %rbr-prerbr
                tmrbry=tmytrain-tmxtrain(:,end);    %rad - prerad
                tmrbry_test=tmytest-tmxtest(:,end);    %rad - prerad
                tmrbr_a=tmrbrx\tmrbry;  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
                pred_y=tmrbrx_test*tmrbr_a;
                pred_y=pred_y+tmxtest(:,end);       %predy = predy + prerad
                MAE_rbr_allbps(ti,fi)=mean(abs(pred_y-tmytest));
                RMSE_rbr_allbps(ti,fi)=sqrt(mean((pred_y-tmytest).^2));
                rbr_a_folds=[rbr_a_folds;tmrbr_a];
                pred_y=tmxtest(:,end);
                MAE_rs_allbps(ti,fi)=mean(abs(pred_y-tmytest));
                RMSE_rs_allbps(ti,fi)=sqrt(mean((pred_y-tmytest).^2));
            end
            a=mean(alr_a_folds);
            a1=mean(rbr_a_folds);
            models_a_allbps{traini}=a;
            models_rbr_allbps{traini}=a1;
        end
        
    end
    
    if bSingleStationMode==true
        save(modelfile,'models_li','models_rbf','models_a','models_rbr','traintimerange','bSingleStationMode');
    else
        save(modelfile,'models_li_allbps','models_rbf_allbps','models_a_allbps','models_rbr_allbps','traintimerange','bSingleStationMode');
    end
    return;
end
%
% timeran=0:60:300;
% tl=length(timeran);
% nfolds=5;
% meanwinsize=3;

if ~bModeling
    t=load(modelfile);
    bSingleStationMode=t.bSingleStationMode;
    
    % RMSE and MAE test
    % li,svr_li,svr_rbf,rbrt,rshift
    meanmae=zeros(tl,5);
    meanrmse=zeros(tl,5);
    MAE_li=zeros(BPS_N,nfolds,tl);
    MAE_rbf=zeros(BPS_N,nfolds,tl);
    RMSE_li=zeros(BPS_N,nfolds,tl);
    RMSE_rbf=zeros(BPS_N,nfolds,tl);
    MAE_alr=zeros(BPS_N,tl);
    RMSE_alr=zeros(BPS_N,tl);
    MAE_rbr=zeros(BPS_N,tl);
    RMSE_rbr=zeros(BPS_N,tl);
    MAE_rs=zeros(BPS_N,tl);
    RMSE_rs=zeros(BPS_N,tl);
    MAE_li_avg=zeros(BPS_N,tl);
    MAE_rbf_avg=zeros(BPS_N,tl);
    RMSE_li_avg=zeros(BPS_N,tl);
    RMSE_rbf_avg=zeros(BPS_N,tl);
    datacounts = zeros(BPS_N,tl);
    for ti=1:tl
        MAE=zeros(BPS_N,5);
        RMSE=zeros(BPS_N,5);
        traintime=traintimerange(ti);
        predmatf=sprintf('%s%s_%d_%d_all.mat',datasetDir,tmfilename,traintime,meanwinsize);
        if (~exist(predmatf,'file'))
            disp(['[ERROR]: There is no such dataset file:' modelmatf]);
        end
        t1=load(predmatf);
        showstarti=1;
        showendi=t1.NRECS;
        od_r_n=t1.od_r_n;   od_g_n=t1.od_g_n;   od_b_n=t1.od_b_n;
        od_prer_n=t1.od_prer_n; od_preg_n=t1.od_preg_n; od_preb_n=t1.od_preb_n;
        od_rbr_n=t1.od_rbr_n;
        od_prerbr_n=t1.od_prerbr_n;
        od_cf=t1.od_cf;
        od_rad_n=t1.od_rad_n;
        od_prerad_n=t1.od_prerad_n;
        od_rad=t1.od_rad;
        od_arad_n=t1.od_arad_n;
        od_Href=t1.od_Href;
        od_Href(od_Href<5000)=1;
        od_Href(od_Href>=5000)=0;
        
        
        for i=1:BPS_N
            % for comparison to one TSI. We use BPS19 ONLY
            if i~=19
                continue
            end
            r_m=od_r_n(showstarti:showendi,i,3);
            g_m=od_g_n(showstarti:showendi,i,3);
            b_m=od_b_n(showstarti:showendi,i,3);
            r_min=od_r_n(showstarti:showendi,i,1);
            g_min=od_g_n(showstarti:showendi,i,1);
            b_min=od_b_n(showstarti:showendi,i,1);
            r_max=od_r_n(showstarti:showendi,i,2);
            g_max=od_g_n(showstarti:showendi,i,2);
            b_max=od_b_n(showstarti:showendi,i,2);
            arad = od_arad_n(showstarti:showendi,i);
            rbr=od_rbr_n(showstarti:showendi,i);
            prer_m=od_prer_n(showstarti:showendi,i,3);
            preg_m=od_preg_n(showstarti:showendi,i,3);
            preb_m=od_preb_n(showstarti:showendi,i,3);
            prer_min=od_prer_n(showstarti:showendi,i,1);
            preg_min=od_preg_n(showstarti:showendi,i,1);
            preb_min=od_preb_n(showstarti:showendi,i,1);
            prer_max=od_prer_n(showstarti:showendi,i,2);
            preg_max=od_preg_n(showstarti:showendi,i,2);
            preb_max=od_preb_n(showstarti:showendi,i,2);
            
            prerad=od_prerad_n(showstarti:showendi,i);
            prerbr=od_prerbr_n(showstarti:showendi,i);
            cldf=od_cf(showstarti:showendi);
            fit_y = od_rad_n(showstarti:showendi,i);
            rad_non=od_rad(showstarti:showendi,i);
            rad_shift=prerad;
            
            % 17 features -> 23 features
            xtrain_norm=...
                [r_m r_min r_max g_m g_min g_max b_m b_min b_max ...
                prer_m prer_min prer_max preg_m preg_min preg_max preb_m preb_min preb_max ...
                prerbr rbr cldf arad prerad];
            %xtrain_norm=[r g b prer preg preb prerbr rbr cldf prerad];
            ytrain_norm=fit_y;
            
            invalidma=markInvalid(xtrain_norm,'nan+0');
            if ~isempty(find(invalidma,1))
                disp('[WARNING]: There is invalid value in training dataset, remove it');
                tmrecinvalid=false(size(xtrain_norm,1),1);
                for atti=1:size(xtrain_norm,2)
                    tmrecinvalid=tmrecinvalid|invalidma(:,atti);
                end
            else
                tmrecinvalid=false(size(xtrain_norm,1),1);
            end
            datacounts(i,ti)=length(find(~tmrecinvalid));
            default_y=ytrain_norm(tmrecinvalid);
            default_closest=xtrain_norm(tmrecinvalid,end-1);
            ytrain_norm=ytrain_norm(~tmrecinvalid);
            xtrain_norm=xtrain_norm(~tmrecinvalid,:);
            fit_y1=ytrain_norm;
            fit_y1_default=[ytrain_norm;default_y];
            rad_shift1=rad_shift(~tmrecinvalid);
            rad_shift1_truth=fit_y1;
            
            
            if bSingleStationMode
                a=t.models_a{ti,i};  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
                a=a';
                a_rbr=t.models_rbr{ti,i};
            else
                a=t.models_a_allbps{ti};  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
                a=a';
                a_rbr=t.models_rbr_allbps{ti};
            end
            
            
            % ALR
            tmli_x=[ones(size(xtrain_norm,1),1) xtrain_norm];
            pre_y=tmli_x*a;
            pre_y(pre_y<0)=0;   pre_y(pre_y>1)=1;
            pre_y=[pre_y;default_closest];
            
            MAE_alr(i,ti)=mean(abs(pre_y-fit_y1_default));
            RMSE_alr(i,ti)=sqrt(mean((pre_y-fit_y1_default).^2));
            
            %RBR
            fit_rbr_x=xtrain_norm(:,rbr_ind)-xtrain_norm(:,prerbr_ind); % prerbr - rbr
            pre_y_rbr=fit_rbr_x*a_rbr+xtrain_norm(:,end);
            pre_y_rbr(pre_y_rbr<0)=0;   pre_y_rbr(pre_y_rbr>1)=1;
            pre_y_rbr=[pre_y_rbr;default_closest];
            MAE_rbr(i,ti)=mean(abs(pre_y_rbr-fit_y1_default));
            RMSE_rbr(i,ti)=sqrt(mean((pre_y_rbr-fit_y1_default).^2));
            
            for fi=1:nfolds
                if bSingleStationMode
                    model_li=t.models_li{ti,i,fi};
                    model_rbf=t.models_rbf{ti,i,fi};
                else
                    model_li=t.models_li_allbps{ti,fi};
                    model_rbf=t.models_rbf_allbps{ti,fi};
                end
                [predicted_label, accuracy, y1]=svmpredict(fit_y1,xtrain_norm,model_li);
                [predicted_label, accuracy, y2]=svmpredict(fit_y1,xtrain_norm,model_rbf);
                y1(y1>1)=1; y1(y1<0)=0;
                y2(y2>1)=1; y2(y2<0)=0;
                y1=[y1;default_closest];
                y2=[y2;default_closest];
                MAE_li(i,fi,ti)=mean(abs(y1-fit_y1_default));
                RMSE_li(i,fi,ti)=sqrt(mean((y1-fit_y1_default).^2));
                MAE_rbf(i,fi,ti)=mean(abs(y2-fit_y1_default));
                RMSE_rbf(i,fi,ti)=sqrt(mean((y2-fit_y1_default).^2));

            end
            tmvals=MAE_li(i,:,ti);
            MAE_li_avg(i,ti)=mean(tmvals(:));
            tmvals=MAE_rbf(i,:,ti);
            MAE_rbf_avg(i,ti)=mean(tmvals(:));
            tmvals=RMSE_li(i,:,ti);
            RMSE_li_avg(i,ti)=mean(tmvals(:));
            tmvals=RMSE_rbf(i,:,ti);
            RMSE_rbf_avg(i,ti)=mean(tmvals(:));
            stats_test=[stats_test;size(y1,1)];
            
            if ~isempty(find(isnan(pre_y),1)) || ~isempty(find(isnan(fit_y1),1))
                pre_y
            end
            %RShift
            % Use arad value which is the most CURRENT radiation to predict fit_y1
            pred_y=arad;
            MAE_rs(i,ti)=mean(abs(pred_y-fit_y));
            RMSE_rs(i,ti)=sqrt(mean((pred_y-fit_y).^2));
            %MAE_rs(i,ti)=mean(abs(xtrain_norm(:,end-1)-fit_y1));
            %RMSE_rs(i,ti)=sqrt(mean((xtrain_norm(:,end-1)-fit_y1).^2));
            %MAE(i,5)=mean(abs(rad_shift1-rad_non1)./950);
            %RMSE(i,5)=sqrt(mean(((rad_shift1-rad_non1)./950).^2));
            assert(isempty(find(isnan(RMSE_rs(i,5)))),'error');
        end
        %for li
        bpsmae_li=zeros(BPS_N,1);
        bpsrmse_li=zeros(BPS_N,1);
        bpsmae_rbf=zeros(BPS_N,1);
        bpsrmse_rbf=zeros(BPS_N,1);
        for i=1:BPS_N
            tmmae=MAE_li(i,:,ti);
            maeval_li=mean(tmmae(:));
            tmmae=MAE_rbf(i,:,ti);
            maeval_rbf=mean(tmmae(:));
            tmrmse=RMSE_li(i,:,ti);
            rmseval_li=mean(tmrmse(:));
            tmrmse=RMSE_rbf(i,:,ti);
            rmseval_rbf=mean(tmrmse(:));
            bpsmae_li(i)=maeval_li;
            bpsmae_rbf(i)=maeval_rbf;
            bpsrmse_li(i)=rmseval_li;
            bpsrmse_rbf(i)=rmseval_rbf;
        end
        meanmae(ti,2)=mean(bpsmae_li);
        meanrmse(ti,2)=mean(bpsrmse_li);
        meanmae(ti,3)=mean(bpsmae_rbf);
        meanrmse(ti,3)=mean(bpsrmse_rbf);
        
        meanmae(ti,1)=mean(MAE(:,1));
        meanmae(ti,4)=mean(MAE(:,4));
        meanmae(ti,5)=mean(MAE(:,5));
        meanrmse(ti,1)=mean(RMSE(:,1));
        meanrmse(ti,4)=mean(RMSE(:,4));
        meanrmse(ti,5)=mean(RMSE(:,5));
    end
    meanmae(1,5)=0;    %RShift has no error if no shif
    meanrmse(1,5)=0;
end
%return;
%Generate bar graph for prediction
%indexiplot=[1 3 5 7 9 11 13]; %0,10,20,40,60
%% lineplot out prediction results
% for one TSI comparison, use 19 only.
compMAE=[MAE_alr(19,:)' MAE_rbr(19,:)' MAE_li_avg(19,:)' MAE_rbf_avg(19,:)' MAE_rs(19,:)'];
compRMSE=[RMSE_alr(19,:)' RMSE_rbr(19,:)' RMSE_li_avg(19,:)' RMSE_rbf_avg(19,:)' RMSE_rs(19,:)'];


for stationn = 1:BPS_N
    if(stationn~=19)
        continue;
    end
    indexiplot=[2 3 4 5 6 7]; %0,10,20,40,60
    f_b=figure();
    hold on;
    grid on;
    x=1:10;
    plot(x,MAE_rs(stationn,:),'-+g','linewidth', 2,'MarkerSize',10);
    plot(x,MAE_alr(stationn,:),'-sb','linewidth',2,'MarkerSize',10);
    plot(x,MAE_li_avg(stationn,:),'-dm','linewidth', 2,'MarkerSize',10);
    plot(x,MAE_rbf_avg(stationn,:),'-xr','linewidth', 2,'MarkerSize',10);
    plot(x,MAE_rbr(stationn,:),'-oc','linewidth', 2,'MarkerSize',10);
    
    legStr={'RShift','linear_{all}','SVR_{linear}','SVR_{rbf}','linear_{\delta}'};
    set(gca,'Fontsize', 20);
    %xlabels={'0','1min','2min','3min'};
    xlabels=x;
    ylabel('Average MAE');
    %axis([x(1) x(end) 0 0.2]);
    %set(gca,'YTick',0.02:0.02:0.11);
    set(gca,'Fontsize', 20);
    set(gca,'XTick',x);
    set(gca,'XTickLabel',xlabels);
    legend(legStr,'location','North','Orientation','horizontal');
    legend boxoff;
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
    hold off;
    outputf=[datasetDir datestr(startdv,'yyyymmdd') '_' datestr(enddv,'yyyymmdd') '_' int2str(stationn) '_predmae.jpg'];
    print(f_b,'-djpeg ',outputf);
    %print(f_b,'-depsc','-r300',[outputf(1:end-4) '.eps']);
    
    
    f_b=figure();
    hold on;
    grid on;
    tmindex=1:30;
    plot(x,RMSE_rs(stationn,:),'-+g','linewidth', 2,'MarkerSize',10);
    plot(x,RMSE_alr(stationn,:),'-sb','linewidth',2,'MarkerSize',10);
    plot(x,RMSE_li_avg(stationn,:),'-dm','linewidth', 2,'MarkerSize',10);
    plot(x,RMSE_rbf_avg(stationn,:),'-xr','linewidth', 2,'MarkerSize',10);
    plot(x,RMSE_rbr(stationn,:),'-oc','linewidth', 2,'MarkerSize',10);
    
    legStr={'RShift','linear_{all}','SVR_{linear}','SVR_{rbf}','linear_{\delta}'};
    set(gca,'Fontsize', 20);
    %xlabels={'0','1min','2min','3min'};
    %xlabels={'1min','2min','3min'};
    ylabel('Average RMSE');
    %axis([x(1) x(end) 0 max(RMSE_rs(:))+0.05]);
    %set(gca,'YTick',0.07:0.03:0.2);
    set(gca,'Fontsize', 20);
    set(gca,'XTick',x);
    set(gca,'XTickLabel',xlabels);
    legend(legStr,'location','North','Orientation','horizontal');
    legend boxoff;
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
    hold off;
    outputf=[datasetDir datestr(startdv,'yyyymmdd') '_' datestr(enddv,'yyyymmdd') '_' int2str(stationn) '_predrmse.jpg'];
    print(f_b,'-djpeg ',outputf);
    %print(f_b,'-depsc','-r300',[outputf(1:end-4) '.eps']);
end
return;


%% plot out prediction results
indexiplot=[1 2 3 4 5 6]; %0,10,20,40,60
f_b=figure();
hold on;
grid on;
bardata1=zeros(30,1);   bardata2=zeros(30,1); bardata3=zeros(30,1);
bardata4=zeros(30,1);   bardata5=zeros(30,1);
tmindex=1:30;
for tmi=1:30
    tmr=rem(tmi,5);
    if tmr==0
        tmindexi=indexiplot(floor(tmi/5));
    else
        tmindexi=indexiplot(floor(tmi/5)+1);
    end
    if tmr==1
        bardata1(tmi)=meanmae(tmindexi,1);
    elseif tmr==2
        bardata2(tmi)=meanmae(tmindexi,2);
    elseif tmr==3
        bardata3(tmi)=meanmae(tmindexi,3);
    elseif tmr==4
        bardata4(tmi)=meanmae(tmindexi,4);
    elseif tmr==0
        bardata5(tmi)=meanmae(tmindexi,5);
    end
end
h1=bar(bardata1,'r');
h2=bar(bardata2,'g');
h3=bar(bardata3,'b');
h4=bar(bardata4,'c');
h5=bar(bardata5,'y');

legStr={'li','SVR_{li}','SVR_{rbf}','RBR','RShift'};
set(gca,'Fontsize', 20);
%xlabels={'0','10s','20s','40s','1min'};
xlabels={'0','1min','2min','3min','4min','5min'};
%xlabel(xStr);
ylabel('Average MAE');
axis([0 31 0.02 0.12]);
set(gca,'Fontsize', 16);
set(gca,'XTick',3:5:28);
set(gca,'XTickLabel',xlabels);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='predmae.jpg';
print(f_b,'-djpeg ',outputf);
print(f_b,'-depsc','-r300',[outputf(1:end-4) '.eps']);




indexiplot=[1 2 3 4 5 6]; %0,10,20,40,60
f_b=figure();
hold on;
grid on;
bardata1=zeros(30,1);   bardata2=zeros(30,1); bardata3=zeros(30,1);
bardata4=zeros(30,1);   bardata5=zeros(30,1);
tmindex=1:30;
for tmi=1:30
    tmr=rem(tmi,5);
    if tmr==0
        tmindexi=indexiplot(floor(tmi/5));
    else
        tmindexi=indexiplot(floor(tmi/5)+1);
    end
    if tmr==1
        bardata1(tmi)=meanrmse(tmindexi,1);
    elseif tmr==2
        bardata2(tmi)=meanrmse(tmindexi,2);
    elseif tmr==3
        bardata3(tmi)=meanrmse(tmindexi,3);
    elseif tmr==4
        bardata4(tmi)=meanrmse(tmindexi,4);
    elseif tmr==0
        bardata5(tmi)=meanrmse(tmindexi,5);
    end
end
h1=bar(bardata1,'r');
h2=bar(bardata2,'g');
h3=bar(bardata3,'b');
h4=bar(bardata4,'c');
h5=bar(bardata5,'y');

legStr={'li','SVR_{li}','SVR_{rbf}','RBR','RShift'};
set(gca,'Fontsize', 16);
%xlabels={'0','10s','20s','40s','1min'};
xlabels={'0','1min','2min','3min','4min','5min'};
%xlabel(xStr);
ylabel('Average RMSE');
axis([0 31 0.07 0.22]);
set(gca,'Fontsize', 16);
set(gca,'XTick',3:5:28);
set(gca,'XTickLabel',xlabels);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='predrmse.jpg';
print(f_b,'-djpeg ',outputf);
print(f_b,'-depsc','-r300',[outputf(1:end-4) '.eps']);

return;

%% previous code
f1=figure();
legStr={'li','SVR_{li}','SVR_{rbf}'};
xStr='Solar Panel #';
hold on;
grid on;
xlabel(xStr,'Fontsize', 16);
ylabel('Mean Absolute Error','Fontsize', 16);
plot(1:BPS_N,MAE(:,1),'--xr','linewidth', 2);
plot(1:BPS_N,MAE(:,2),'--+g','linewidth', 2);
plot(1:BPS_N,MAE(:,3),'--ob','linewidth', 2);

set(gca,'Fontsize', 16);
axis([1 25 0.055 0.1]);
set(gca,'XTick',1:25);
set(gca,'XTickLabel',1:25);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='modelsmae.jpg';
print(f1,'-djpeg ',outputf);
print(f1,'-depsc','-r300',[outputf(1:end-4) '.eps']);


f2=figure();
legStr={'li','SVR_{li}','SVR_{rbf}'};
xStr='Solar Panel #';
hold on;
grid on;
xlabel(xStr,'Fontsize', 16);
ylabel('Root Mean Squre Error','Fontsize', 16);
plot(1:BPS_N,RMSE(:,1),'--xr','linewidth', 2);
plot(1:BPS_N,RMSE(:,2),'--+g','linewidth', 2);
plot(1:BPS_N,RMSE(:,3),'--ob','linewidth', 2);

set(gca,'Fontsize', 16);
axis([1 25 0.09 0.15]);
set(gca,'XTick',1:25);
set(gca,'XTickLabel',1:25);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='modelsrmse.jpg';
print(f2,'-djpeg ',outputf);
print(f2,'-depsc','-r300',[outputf(1:end-4) '.eps']);


sec1=1:10;
sec2=11:20;
sec3=21:25;
secs={sec1,sec2,sec3};
maes=zeros(3,3);
rmses=zeros(3,3);
for seci=1:3
    tmsec=secs{seci};
    maes(seci,1)=mean(MAE(tmsec,1));
    maes(seci,2)=mean(MAE(tmsec,2));
    maes(seci,3)=mean(MAE(tmsec,3));
    rmses(seci,1)=mean(RMSE(tmsec,1));
    rmses(seci,2)=mean(RMSE(tmsec,2));
    rmses(seci,3)=mean(RMSE(tmsec,3));
end
f3=figure();
hold on;
legStr={'li','SVR_{li}','SVR_{rbf}'};
set(gca,'Fontsize', 16);
h1=bar([maes(1,1) 0 0 maes(2,1) 0 0  maes(3,1) 0 0],'r');
h2=bar([0 maes(1,2) 0 0 maes(2,2) 0 0 maes(3,2) 0],'b');
h3=bar([0 0 maes(1,3) 0 0 maes(2,3) 0 0 maes(3,3)],'y');
xlabels={'Panel 1~10','Panel 11~20','Panel 21~25'};
%xlabel(xStr);
ylabel('Average of MAE');
axis([0 10 0.05 0.095]);
set(gca,'Fontsize', 16);
set(gca,'XTick',2:3:8);
set(gca,'XTickLabel',xlabels);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='secmae.jpg';
print(f3,'-djpeg ',outputf);
print(f3,'-depsc','-r300',[outputf(1:end-4) '.eps']);




MAE