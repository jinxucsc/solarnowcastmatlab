function cos_ma=sunlight_angle(datevector,site)
mtsi_startup;
t=load(u2azmatf);
u2az=t.u2az;
u2ze=t.u2ze;
z_u=1;
x_u=z_u*tan(u2ze).*sin(u2az);
y_u=z_u*tan(u2ze).*cos(u2az);
z_u=ones(size(x_u));

vec_u=[x_u(:) y_u(:) z_u(:)];
%vec_u_n=norm(vec_u);
%tml=UNDISTH*UNDISTW;
vec_u_n=sqrt(vec_u(:,1).^2+vec_u(:,2).^2+vec_u(:,3).^2);
% vec_u_n=zeros(tml,1);
% for i=1:tml
%     vec_u_n(i)=norm(vec_u(i,:));
% end
% 
% indexi=find(abs(vec_u_n_1-vec_u_n)>0.01)



if site==TSI1
    location=location1;
elseif site==TSI2
    location=location2;
elseif site==TSI3
    location=location3;
end

% Calculate Zenith here
tmdv=datevector;
time.year    =  tmdv(1);
time.month   =  tmdv(2);
time.day     =  tmdv(3);
time.hour    =  tmdv(4);
time.min    =   tmdv(5);
%             time.sec    =   tmdv(6);
time.sec    =   0;    %should be the same as 0
time.UTC    =   0;
sun = sun_position(time, location);
az_s=sun.azimuth;
ze_s=sun.zenith;

z_s=1;
x_s=z_s*tan(ze_s)*sin(az_s);
y_s=z_s*tan(ze_s)*cos(az_s);

vec_s=[x_s y_s z_s];
vec_s=vec_s./norm(vec_s);
cos_ma=zeros(UNDISTH,UNDISTW);


cosvals=vec_s(1)*vec_u(:,1)+vec_s(2)*vec_u(:,2)+vec_s(3)*vec_u(:,3);
cosvals=cosvals./vec_u_n;
cos_ma(:)=cosvals;
% for i=1:UNDISTH
%     for j=1:UNDISTW
%         % (i,j) -> az_u,ze_u
%         % az_u,ze_u -> (x_u,y_u,z_u)
%         vec_u=[x_u(i,j) y_u(i,j) z_u(i,j)];
%         cos_ma(i,j)=dot(vec_s,vec_u)/norm(vec_u);
%     end
% end


end