% tmp_ChooseHFromHRegfiles
%   This function is to choose reference H from HRefDir which is the regulated one that has been
%   calculated by mtsipipe2. This one is used in mtsi_HPlot for picking reasonal H to be current H
%   base on history reference. mtsi_HPlot will plot them out for visualization.
%   The default frame span is 10s.
%   look up to nframes_lookup ahead of tmdv to get reference H
%   IN:
%
%   OUT:
%
%   VERSION 1.0     2013-11-26
%   USAGE:
%       mtsi_HPlot.m
%   PREREQUISITE:
%       mtsipipe2.m or related
function [HRef, MVRef]=tmp_ChooseHFromHRegfiles(HRefDir,tmdv,nframes_lookup)
%% PRESETTINGS
TENSECS_DN=datenum([0 0 0 0 0 10]);
ONEMIN_DN=datenum([0 0 0 0 1 0]);
site='tsi3';
%%  MAIN
HRefDir=FormatDirName(HRefDir);
tmdn=datenum(tmdv);
NFRAMES=nframes_lookup+1;   % including current frame
dns=zeros(1,NFRAMES);
dns(:)=tmdn;
diffdn=nframes_lookup:-1:0;
%diffdn=diffdn*TENSECS_DN;
diffdn=diffdn*ONEMIN_DN;
dns=dns-diffdn;

hcounts=zeros(NFRAMES,2);
hvals=zeros(NFRAMES,2);
mvxvals=zeros(NFRAMES,2);
mvyvals=zeros(NFRAMES,2);

for i=1:NFRAMES
    tmdn=dns(i);
    tmdv=datevec(tmdn);
    tmf=GetImgFromDV_BNL(tmdv,site);
    cldtrkmatoutf_reg=sprintf('%s%s.cldtrkreg.mat',HRefDir,tmf(1:end-4));
    if ~exist(cldtrkmatoutf_reg,'file')
        continue;
    end
    t=load(cldtrkmatoutf_reg);
    H_Reg=t.H_Reg;  Conn_Reg=t.Conn_Reg;    CldC_Reg=t.CldC_Reg;
    MV_x_Reg=t.MV_x_Reg;    MV_y_Reg=t.MV_y_Reg;
    
    %validind=find(H_Reg~=0&CldC_Reg>900&(MV_x_Reg~=0|MV_y_Reg~=0));
    validind=find(H_Reg~=0&CldC_Reg>=2500);
    if isempty(validind)
        %dispstr=sprintf('[WARNING]: No cloud are detected in this case, timestamp = %s',datestr(tmdn,'yyyy-mm-dd HH:MM:SS'));
        %disp(dispstr);
        continue;
    elseif 1==length(validind)
        % single layer
        hvals(i,validind)=H_Reg(validind);
        mvxvals(i,validind)=MV_x_Reg(validind);
        mvyvals(i,validind)=MV_y_Reg(validind);
        hcounts(i,validind)=CldC_Reg(validind);
    elseif 2==length(validind)
        % 2 layers
        hvals(i,:)=H_Reg(:);
        mvxvals(i,:)=MV_x_Reg(:);
        mvyvals(i,:)=MV_y_Reg(:);
        hcounts(i,:)=CldC_Reg(:);
    end
end

hc_arr=hcounts(:);
h_arr=hvals(:);
x_arr=mvxvals(:);
y_arr=mvyvals(:);

tml=length(hc_arr);
tmcs=round(hc_arr./100);
tmHs=[];
for li=1:tml
    tmH=zeros(tmcs(li),1);
    tmH(:)=h_arr(li);
    tmHs=[tmHs;tmH];
end

bClusterd=false;
bSingleLayered=false;
MAXNUMOFLAYER=2;
if isempty(tmHs)
    HRef=[0 0];
    MVRef=[0 0;0 0];
    return;
end
if 1==length(unique(tmHs))
    bSingleLayered=true;
else
    while false==bClusterd
        try
            [~,C]=kmeans(tmHs,MAXNUMOFLAYER);
            C=sort(C);
            bClusterd=true;
        catch e
            bClusterd=false;
        end
    end
    if abs(C(1)-C(2))<1500
        bSingleLayered=true;
    end
end

if bSingleLayered
    HRef=[0 mean(tmHs)];
    tmmvx2=round(sum(x_arr(:).*hc_arr(:))./sum(hc_arr(:)));
    tmmvy2=round(sum(y_arr(:).*hc_arr(:))./sum(hc_arr(:)));
    MVRef=[0 0;tmmvx2 tmmvy2];
else
    HRef=[C(1) C(2)];
    slipt_H=(C(1)+C(2))/2;
    ind1=(h_arr<=slipt_H);
    ind2=(h_arr>slipt_H);
    tmmvx1=round(sum(x_arr(ind1).*hc_arr(ind1))./sum(hc_arr(ind1)));
    tmmvy1=round(sum(y_arr(ind1).*hc_arr(ind1))./sum(hc_arr(ind1)));
    tmmvx2=round(sum(x_arr(ind2).*hc_arr(ind2))./sum(hc_arr(ind2)));
    tmmvy2=round(sum(y_arr(ind2).*hc_arr(ind2))./sum(hc_arr(ind2)));
    MVRef=[tmmvx1 tmmvy1;tmmvx2 tmmvy2];
end
HRef=round(HRef);



end