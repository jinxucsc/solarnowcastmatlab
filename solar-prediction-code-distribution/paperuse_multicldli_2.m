%function paperuse_multicldli_2
% set(0,'DefaultFigureVisible', 'on');
set(0,'DefaultFigureVisible', 'off');
BPS_N=25;
lf_para=zeros(BPS_N,7);
MAE=zeros(BPS_N,3);
RMSE=zeros(BPS_N,3);
% plot out
outputDir='sftestplot_20131203/';
outputDir=FormatDirName(outputDir);
if ~exist(outputDir,'dir')
    mkdir(outputDir);
end

datasetDir='./dataset_20131203';
datasetDir=FormatDirName(datasetDir);

opt1='-s 3 -t 0';
opt2='-s 3 -t 2';
% cran=1:10;
% gran=0.1:0.05:1;
% epran=0.01:0.02:0.3;
% cran=-3:1:3;
% gran=-3:1:3;
% epran=0.001:0.05:0.2;
% rbf_paraperm=GenPermutation(cran,gran,epran);
% cran=1:10;
% epran=0.01:0.02:0.6;
% li_paraperm=GenPermutation(cran,epran);




bModeling=false; % generate and save sfmodels.mat
%modelfile='sfmodels_rgb';   % rgb
%modelfile='sfmodels_rbr';   % rgb_rbf
modelfile=sprintf('%ssfmodels_all.mat',datasetDir); % rgb+rbf+cf
nfolds=5;
rbftuningDir='models_rbf/';
lituningDir='models_li/';
rbftuningDir=FormatDirName(rbftuningDir);
lituningDir=FormatDirName(lituningDir);

if bModeling
    %     if exist('parapairs.mat','file')
    %         tmt=load('parapairs.mat');
    %         parapairs=tmt.parapairs;
    %     end
    %model_li=svmtrain(ytrain,xtrain_norm,svmoptions_li);
    traintimerange=0:60:300;
    meanwinsize=3;
    tl=length(traintimerange);
    models_li=cell(tl,BPS_N,5);
    models_rbf=cell(tl,BPS_N,5);
    models_rbr=cell(tl,BPS_N,1);
    models_a=cell(tl,BPS_N,1);
    testrmse=zeros(tl,BPS_N,5);
    testmae=zeros(tl,BPS_N,5);
    
    for traini=1:tl
%         if traini~=4
%             continue
%         end
        traintime=traintimerange(traini);
        modelmatf=sprintf('%ssftmp_%d_%d.mat',datasetDir,traintime,meanwinsize);
        
        t1=load(modelmatf);
        od_r_n=t1.od_r_n;   od_g_n=t1.od_g_n;   od_b_n=t1.od_b_n;
        od_prer_n=t1.od_prer_n; od_preg_n=t1.od_preg_n; od_preb_n=t1.od_preb_n;
        od_rbr_n=t1.od_rbr_n;
        od_prerbr_n=t1.od_prerbr_n;
        od_cf=t1.od_cf;
        od_rad_norm=t1.od_rad_norm;
        od_prerad_norm=t1.od_prerad_norm;
        showstarti=t1.showstarti;
        showendi=t1.showendi;
        timelabel=t1.timelabel;
        NinHour=t1.NinHour;
        %ci=6;ui=0.5;
        %svmoptions_rbf=sprintf('%s -c %d -n %f',opt2,2^ci,ui);
        rbfparas=zeros(BPS_N,3);
        liparas=zeros(BPS_N,2);
        for i=1:BPS_N
            %         if i~=6
            %             continue
            %         end
            rbfmodelf=sprintf('%srbfo%d.mat',rbftuningDir,i);
            t=load(rbfmodelf);
            rbf_paraperm=t.poly_paraperm;
            [minval,mini]=min(t.tmmae+t.tmrmse);    %mae + tmrmse
            %[minval,mini]=min(t.tmmae);
            c_rbf=rbf_paraperm(mini,1);
            g_rbf=rbf_paraperm(mini,2);
            ep_rbf=rbf_paraperm(mini,3);
            rbfparas(i,1)=c_rbf;
            rbfparas(i,2)=g_rbf;
            rbfparas(i,3)=ep_rbf;
            svmoptions_rbf=sprintf('%s -c %d -g %f -p %f',opt2,10^c_rbf,10^g_rbf,ep_rbf);
            
            limodelf=sprintf('%slio%d.mat',lituningDir,i);
            t=load(limodelf);
            li_paraperm=t.poly_paraperm;
            %[minval,mini]=min(t.tmmae+t.tmrmse); %mae + tmrmse
            [minval,mini]=min(t.tmmae); %mae
            c_li=li_paraperm(mini,1);
            %g_rbf=rbf_paraperm(mini,2);
            ep_li=li_paraperm(mini,2);
            liparas(i,1)=c_li;
            liparas(i,2)=ep_li;
            svmoptions_li=sprintf('%s -c %d -p %f',opt1,2^c_li,ep_li);

            outputf=sprintf('%ssfmodel_bps%d.png',outputDir,i);
            r_m=od_r_n(showstarti:showendi,i,3);
            g_m=od_g_n(showstarti:showendi,i,3);
            b_m=od_b_n(showstarti:showendi,i,3);
            r_min=od_r_n(showstarti:showendi,i,1);
            g_min=od_g_n(showstarti:showendi,i,1);
            b_min=od_b_n(showstarti:showendi,i,1);
            r_max=od_r_n(showstarti:showendi,i,2);
            g_max=od_g_n(showstarti:showendi,i,2);
            b_max=od_b_n(showstarti:showendi,i,2);
            
            prer=od_prer_n(showstarti:showendi,i);
            preg=od_preg_n(showstarti:showendi,i);
            preb=od_preb_n(showstarti:showendi,i);
            prerad=od_prerad_norm(showstarti:showendi,i);
            rbr=od_rbr_n(showstarti:showendi,i);
            prerbr=od_prerbr_n(showstarti:showendi,i);
            cldf=od_cf(showstarti:showendi);
            fit_y=od_rad_norm(showstarti:showendi,i);
            fit_x=[ones(size(r_m)) r_m g_m b_m prer preg preb prerbr rbr cldf prerad];
            fit_rbr_x=rbr-prerbr;
            fit_rbr_y=(fit_y-prerad);
            a=fit_x\fit_y;  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
            a_rbr=fit_rbr_x\fit_rbr_y;  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
            %lf_para(i,:)=a';
            pre_y=fit_x*a;
            pre_y(pre_y>1)=1;
            pre_y(pre_y<0)=0;
            
            xtrain_norm=[r_m r_min r_max g_m g_min g_max b_m b_min b_max prer preg preb prerbr rbr cldf prerad];
            %xtrain_norm=[r g b prer preg preb prerbr rbr cldf prerad];
            ytrain_norm=fit_y;
            
            invalidma=markInvalid(xtrain_norm,'nan+0');
            if ~isempty(find(invalidma,1))
                disp('[WARNING]: There is invalid value in training dataset, remove it');
                tmrecinvalid=false(size(xtrain_norm,1),1);
                for atti=1:size(xtrain_norm,2)
                    tmrecinvalid=tmrecinvalid|invalidma(:,atti);
                end
            else
                tmrecinvalid=false(size(xtrain_norm,1),1);
            end
            ytrain_norm=ytrain_norm(~tmrecinvalid);
            xtrain_norm=xtrain_norm(~tmrecinvalid,:);
            [xtrain,ytrain,xtest,ytest]=cvGenFolds(xtrain_norm,ytrain_norm,nfolds);
            tmli_a=[];tmrbr_a=[];
            MAE_svrli=zeros(nfolds,1);
            RMSE_svrli=zeros(nfolds,1);
            MAE_svrrbf=zeros(nfolds,1);
            RMSE_svrrbf=zeros(nfolds,1);
            MAE_li=zeros(nfolds,1);
            RMSE_li=zeros(nfolds,1);
            MAE_rbr=zeros(nfolds,1);
            RMSE_rbr=zeros(nfolds,1);
            MAE_shift=zeros(nfolds,1);
            RMSE_shift=zeros(nfolds,1);
            
            for fi=1:nfolds
                tmytrain=ytrain{fi};
                tmxtrain=xtrain{fi};
                model_li=svmtrain(tmytrain,tmxtrain,svmoptions_li);
                model_rbf=svmtrain(ytrain{fi},xtrain{fi},svmoptions_rbf);
                assert(~isnan(model_li.rho));
                [predicted_label, accuracy, y1]=svmpredict(ytest{fi},xtest{fi},model_li);
                [predicted_label, accuracy, y2]=svmpredict(ytest{fi},xtest{fi},model_rbf);
                models_li{traini,i,fi}=model_li;
                models_rbf{traini,i,fi}=model_rbf;
                y1(y1>1)=1; y1(y1<0)=0;
                y2(y2>1)=1; y2(y2<0)=0;
                MAE_svrli(i,fi)=mean(abs(y1-ytest{fi}));
                RMSE_svrli(i,fi)=sqrt(mean((y1-ytest{fi}).^2));
                MAE_svrrbf(i,fi)=mean(abs(y2-ytest{fi}));
                RMSE_svrrbf(i,fi)=sqrt(mean((y2-ytest{fi}).^2));
                tmlix=[ones(size(tmxtrain,1),1) tmxtrain];
                tmliy=tmytrain;
                tm_a=tmlix\tmliy;
                pred_y=tmlix*tm_a;
                tm_a=tm_a';
                tmli_a=[tmli_a;tm_a];
                tmrbrx=tmxtrain(:,15)-tmxtrain(:,14); %rbr-prerbr
                tmrbry=tmytrain-tmxtrain(:,end);    %rad - prerad
                tm_a1=tmrbrx\tmrbry;  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
                tmrbr_a=[tmrbr_a;tm_a1];
            end
            for fi=1:nfolds
                
                
            end
            a=mean(tmli_a);
            a1=mean(tmrbr_a);
            models_a{traini,i}=a;
            models_rbr{traini,i}=a1;
        end
    end
    save(modelfile,'models_li','models_rbf','models_a','models_rbr','traintimerange');
    return;
end
%

timeran=0:60:300;
tl=length(timeran);
nfolds=5;
meanwinsize=3;



if ~bModeling
    t=load(modelfile);
    % RMSE and MAE test
    % li,svr_li,svr_rbf,rbrt,rshift
    meanmae=zeros(tl,5);
    meanrmse=zeros(tl,5);
    MAE_li=zeros(BPS_N,nfolds,tl);
    MAE_rbf=zeros(BPS_N,nfolds,tl);
    RMSE_li=zeros(BPS_N,nfolds,tl);
    RMSE_rbf=zeros(BPS_N,nfolds,tl);
    for ti=1:tl
%         if ti==4
%             ti
%         end
        MAE=zeros(BPS_N,5);
        RMSE=zeros(BPS_N,5);
        timeahead=timeran(ti);
        predmatf=sprintf('%ssftmp_%d_%d.mat',datasetDir,timeahead,meanwinsize);
        %         if timeahead~=0
        %             predmatf=sprintf('sftmp_%d_%d.mat',timeahead,meanwinsize);
        %         else
        %             predmatf=sprintf('sftmp_%d.mat',meanwinsize);
        %         end
        %         rbr=od_b_n(showstarti:showendi,i);
        %         fit_y=od_rad_norm(showstarti:showendi,i);
        %         fit_x=[ones(size(r)) r g b rbr];
        t1=load(predmatf);
        od_r_n=t1.od_r_n;   od_g_n=t1.od_g_n;   od_b_n=t1.od_b_n;
        od_prer_n=t1.od_prer_n; od_preg_n=t1.od_preg_n; od_preb_n=t1.od_preb_n;
        od_rbr_n=t1.od_rbr_n;
        od_prerbr_n=t1.od_prerbr_n;
        od_cf=t1.od_cf;
        od_rad_norm=t1.od_rad_norm;
        od_prerad_norm=t1.od_prerad_norm;
        
        od_rad=t1.od_rad_ori;
        od_radshift=t1.od_radshift;
        showstarti=t1.showstarti;
        showendi=t1.showendi;
        timelabel=t1.timelabel;
        NinHour=t1.NinHour;
        %od_rbr_n=t1.od_r_n;
        for i=1:BPS_N
            %             if i~=6
            %                 continue
            %             end
            r_m=od_r_n(showstarti:showendi,i,3);
            g_m=od_g_n(showstarti:showendi,i,3);
            b_m=od_b_n(showstarti:showendi,i,3);
            r_min=od_r_n(showstarti:showendi,i,1);
            g_min=od_g_n(showstarti:showendi,i,1);
            b_min=od_b_n(showstarti:showendi,i,1);
            r_max=od_r_n(showstarti:showendi,i,2);
            g_max=od_g_n(showstarti:showendi,i,2);
            b_max=od_b_n(showstarti:showendi,i,2);
            
            prer=od_prer_n(showstarti:showendi,i);
            preg=od_preg_n(showstarti:showendi,i);
            preb=od_preb_n(showstarti:showendi,i);
            prerad=od_prerad_norm(showstarti:showendi,i);
            rbr=od_rbr_n(showstarti:showendi,i);
            prerbr=od_prerbr_n(showstarti:showendi,i);
            cldf=od_cf(showstarti:showendi);
            fit_y=od_rad_norm(showstarti:showendi,i);
            rad_non=od_rad(showstarti:showendi,i);
            rad_shift=od_radshift(showstarti:showendi,i);
            
            ind=find(isnan(prerad));
            for tmi=1:length(ind)
                tmind=ind(tmi);
                if tmind>2 && tmind<=length(prerad)-2
                    tmvals=prerad(tmind-2:tmind+2);
                elseif tmind>length(prerad)-2
                    tmvals=prerad(tmind-2:end);
                else
                    tmvals=prerad(tmind:tmind+2);
                end
                prerad(tmind)=mean(tmvals(~isnan(tmvals)));
            end
            
            if ~isempty(find(isnan(fit_y),1))
                ind=find(isnan(fit_y));
                for tmi=1:length(ind)
                    tmind=ind(tmi);
                    if tmind>2 && tmind<=length(fit_y)-2
                        tmvals=fit_y(tmind-2:tmind+2);
                    elseif tmind>length(fit_y)-2
                        tmvals=fit_y(tmind-2:end);
                    else
                        tmvals=fit_y(tmind:tmind+2);
                    end
                    fit_y(tmind)=mean(tmvals(~isnan(tmvals)));
                end
            end
            
            if ~isempty(find(isnan(rad_shift),1))
                ind=find(isnan(rad_shift));
                for tmi=1:length(ind)
                    tmind=ind(tmi);
                    if tmind>2 && tmind<=length(rad_shift)-2
                        tmvals=rad_shift(tmind-2:tmind+2);
                    elseif tmind>length(rad_shift)-2
                        tmvals=rad_shift(tmind-2:end);
                    else
                        tmvals=rad_shift(tmind:tmind+2);
                    end
                    rad_shift(tmind)=mean(tmvals(~isnan(tmvals)));
                end
            end
            
            
            
            invalidind=isnan(r_m)|isnan(g_m)|isnan(b_m)|rbr==0|cldf==0|prerad==0;
            if ~isempty(find(prer(~invalidind)==0|preg(~invalidind)==0|preb(~invalidind)==0|isnan(prerbr(~invalidind)),1))
                i
            end
            r1=r_m(~invalidind);  g1=g_m(~invalidind); b1=b_m(~invalidind);
            prer1=prer(~invalidind);  preg1=preg(~invalidind); preb1=preb(~invalidind);
            rbr1=rbr(~invalidind);   cldf1=cldf(~invalidind);   fit_y1=fit_y(~invalidind);
            prerad1=prerad(~invalidind); prerbr1=prerbr(~invalidind);
            r_min1=r_min(~invalidind);  g_min1=g_min(~invalidind);  b_min1=b_min(~invalidind);
            r_max1=r_max(~invalidind);  g_max1=g_max(~invalidind);  b_max1=b_max(~invalidind);
            rad_shift1=rad_shift(~invalidind);  rad_non1=rad_non(~invalidind);
            
            if length(fit_y1)~=length(fit_y)
                %disp(length(fit_y1));
            end
            %fit_x=[ones(size(r1)) r1 g1 b1 rbr1 cldf1 prerad1];
            %xtrain_norm=[r1 g1 b1 rbr1 cldf1 prerad1];
            %fit_x=[ones(size(r1)) r1 g1 b1 prer1 preg1 preb1 prerbr1 rbr1 cldf1 prerad1];
            %fit_x=[ones(size(r1)) r1 g1 b1 prer1 preg1 preb1 prerbr1 rbr1 cldf1];
            xtrain_norm=[r1 r_min1 r_max1 g1 g_min1 g_max1 b1 b_min1 b_max1 prer1 preg1 preb1 prerbr1 rbr1 cldf1 prerad1];
            invalidma=markInvalid(xtrain_norm,'nan+0');
            if ~isempty(find(invalidma,1))
                disp('[WARNING]: There is invalid value in training dataset, remove it');
                tmrecinvalid=false(size(xtrain_norm,1),1);
                for atti=1:size(xtrain_norm,2)
                    tmrecinvalid=tmrecinvalid|invalidma(:,atti);
                end
            else
                tmrecinvalid=false(size(xtrain_norm,1),1);
            end
            %ytrain_norm=ytrain_norm(~tmrecinvalid);
            xtrain_norm=xtrain_norm(~tmrecinvalid,:);
            fit_y1=fit_y1(~tmrecinvalid);
            prerad1=prerad1(~tmrecinvalid);
            
            %xtrain_norm=[r1 g1 b1 prer1 preg1 preb1 prerbr1 rbr1 cldf1 prerad1];
            %fit_x=[ones(size(r)) r g b rbr cldf prerad];
            a=t.models_a{ti,i};  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
            a=a';
            a_rbr=t.models_rbr{ti,i};
            fit_rbr_x=rbr1-prerbr1;
            fit_rbr_x=fit_rbr_x(~tmrecinvalid);
            %fit_rbr_y=(fit_y-prerad);
            pre_y_rbr=fit_rbr_x*a_rbr+prerad1;
            tmli_x=[ones(size(xtrain_norm,1),1) xtrain_norm];
            pre_y=tmli_x*a;
            pre_y(pre_y>1)=1;   pre_y_rbr(pre_y_rbr>1)=1;
            pre_y(pre_y<0)=0;   pre_y_rbr(pre_y_rbr<0)=0;
            %xtrain_norm=[r g b rbr cldf prerad];
            %             if ~isempty(find(isnan(xtrain_norm),1))
            %                 i
            %             end
            
            for fi=1:nfolds
                model_li=t.models_li{ti,i,fi};
                model_rbf=t.models_rbf{ti,i,fi};
                [predicted_label, accuracy, y1]=svmpredict(fit_y1,xtrain_norm,model_li);
                [predicted_label, accuracy, y2]=svmpredict(fit_y1,xtrain_norm,model_rbf);
                y1(y1>1)=1; y1(y1<0)=0;
                y2(y2>1)=1; y2(y2<0)=0;
                MAE_li(i,fi,ti)=mean(abs(y1-fit_y1));
                RMSE_li(i,fi,ti)=sqrt(mean((y1-fit_y1).^2));
                MAE_rbf(i,fi,ti)=mean(abs(y2-fit_y1));
                RMSE_rbf(i,fi,ti)=sqrt(mean((y2-fit_y1).^2));
            end
            MAE(i,1)=mean(abs(pre_y-fit_y1));
            if ~isempty(find(isnan(pre_y),1)) || ~isempty(find(isnan(fit_y1),1)) 
                pre_y;
            end
            
            RMSE(i,1)=sqrt(mean((pre_y-fit_y1).^2));
            MAE(i,4)=mean(abs(pre_y_rbr-fit_y1));
            RMSE(i,4)=sqrt(mean((pre_y_rbr-fit_y1).^2));
            %RShift
            %MAE(i,5)=mean(abs(prerad1-fit_y1));
            %RMSE(i,5)=sqrt(mean((prerad1-fit_y1).^2));
            MAE(i,5)=mean(abs(rad_shift1-rad_non1)./950);
            RMSE(i,5)=sqrt(mean(((rad_shift1-rad_non1)./950).^2));
            if isnan(RMSE(i,5))
                rad_shift1
            end
%             if timeahead==180
%                 f1=figure();
%                 outputf=sprintf('%spred%d_bps%d.png',outputDir,timeahead,i);
%                 %titStr=sprintf('Estimated GHI of 3 models vs. GHI of Panel %d',i);
%                 %title(titStr);
%                 %legStr={'linear_{all}','SVR_{linear}','SVR_{rbf}','linear_{\delta}','GHI_{norm}'};
%                 legStr={'linear_{all}','SVR_{rbf}','GHI_{norm}'};
%                 xStr='EST(UTC/GMT-5)';
%                 grid on;
%                 hold on;
%                 %xlabel(xStr,'Fontsize',20);
%                 ylabel('Normalized GHI (0~1)','Fontsize',20);
%                 plot(pre_y,'--*b','linewidth',1);
%                 %plot(y1,'--xm','linewidth', 1);
%                 plot(y2,'--.r','linewidth', 1);
%                 %plot(pre_y_rbf,'--.c','linewidth', 1);
%                 plot(fit_y1,'-k','linewidth', 1);
%                 set(gca,'Fontsize', 20);
%                 axis([1 showendi-showstarti+1 0.1 1.1]);
%                 set(gca,'XTick',1:NinHour:showendi-showstarti+1);
%                 set(gca,'XTickLabel',timelabel);
%                 legend(legStr,'location','North','Orientation','horizontal');
%                 legend boxoff;
%                 set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
%                 hold off;
%                 print(f1,outputf,'-dpng');
%                 print(f1,'-depsc','-r300',[outputf(1:end-4) '.eps']);
%             end
        end
        %for li
        bpsmae_li=zeros(BPS_N,1);
        bpsrmse_li=zeros(BPS_N,1);
        bpsmae_rbf=zeros(BPS_N,1);
        bpsrmse_rbf=zeros(BPS_N,1);
        for i=1:BPS_N
            tmmae=MAE_li(i,:,ti);
            maeval_li=mean(tmmae(:));
            tmmae=MAE_rbf(i,:,ti);
            maeval_rbf=mean(tmmae(:));
            tmrmse=RMSE_li(i,:,ti);
            rmseval_li=mean(tmrmse(:));
            tmrmse=RMSE_rbf(i,:,ti);
            rmseval_rbf=mean(tmrmse(:));
            bpsmae_li(i)=maeval_li;
            bpsmae_rbf(i)=maeval_rbf;
            bpsrmse_li(i)=rmseval_li;
            bpsrmse_rbf(i)=rmseval_rbf;
        end
        meanmae(ti,2)=mean(bpsmae_li);
        meanrmse(ti,2)=mean(bpsrmse_li);
        meanmae(ti,3)=mean(bpsmae_rbf);
        meanrmse(ti,3)=mean(bpsrmse_rbf);
        
        %         %li
        %         tmmae=MAE_li(:,:,ti);
        %         meanvals=mean(tmmae);
        %         minmae=min(meanvals);
        %         meanmae(ti,2)=minmae;
        %         tmrmse=RMSE_li(:,:,ti);
        %         meanvals=mean(tmrmse);
        %         minrmse=min(meanvals);
        %         meanrmse(ti,2)=minrmse;
        %         % rbf
        %         tmmae=MAE_rbf(:,:,ti);
        %         meanvals=mean(tmmae);
        %         minmae=min(meanvals);
        %         meanmae(ti,3)=minmae;
        %         tmrmse=RMSE_rbf(:,:,ti);
        %         meanvals=mean(tmrmse);
        %         minrmse=min(meanvals);
        %         meanrmse(ti,3)=minrmse;
        
        
        
        
        meanmae(ti,1)=mean(MAE(:,1));
        %meanmae(ti,2)=mean(MAE(:,2));
        %meanmae(ti,3)=mean(MAE(:,3));
        meanmae(ti,4)=mean(MAE(:,4));
        meanmae(ti,5)=mean(MAE(:,5));
        meanrmse(ti,1)=mean(RMSE(:,1));
        %meanrmse(ti,2)=mean(RMSE(:,2));
        %meanrmse(ti,3)=mean(RMSE(:,3));
        meanrmse(ti,4)=mean(RMSE(:,4));
        meanrmse(ti,5)=mean(RMSE(:,5));
        
        
    end
    meanmae(1,5)=0;    %RShift has no error if no shif
    meanrmse(1,5)=0;
end
%return;
%Generate bar graph for prediction
%indexiplot=[1 3 5 7 9 11 13]; %0,10,20,40,60
%% lineplot out prediction results
indexiplot=[2 3 4 5 6 7]; %0,10,20,40,60
f_b=figure();
hold on;
grid on;
tmindex=1:30;
x=1:3;
plot(x,meanmae(2:4,5),'-+g','linewidth', 2,'MarkerSize',10);
plot(x,meanmae(2:4,1),'-sb','linewidth',2,'MarkerSize',10);
plot(x,meanmae(2:4,2),'-dm','linewidth', 2,'MarkerSize',10);
plot(x,meanmae(2:4,3),'-xr','linewidth', 2,'MarkerSize',10);
plot(x,meanmae(2:4,4),'-oc','linewidth', 2,'MarkerSize',10);

legStr={'RShift','linear_{all}','SVR_{linear}','SVR_{rbf}','linear_{\delta}'};
set(gca,'Fontsize', 20);
%xlabels={'0','1min','2min','3min'};
xlabels={'1min','2min','3min'};
ylabel('Average MAE');
axis([1 3 0 0.2]);
set(gca,'Fontsize', 20);
set(gca,'XTick',1:3);
set(gca,'XTickLabel',xlabels);
set(gca,'YTick',0.02:0.02:0.11);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='predmae_20131203.jpg';
print(f_b,'-djpeg ',outputf);
print(f_b,'-depsc','-r300',[outputf(1:end-4) '.eps']);



indexiplot=[1 2 3 4 5 6]; %0,10,20,40,60
f_b=figure();
hold on;
grid on;
tmindex=1:30;
x=1:3;
plot(x,meanrmse(2:4,5),'-+g','linewidth', 2,'MarkerSize',10);
plot(x,meanrmse(2:4,1),'-sb','linewidth',2,'MarkerSize',10);
plot(x,meanrmse(2:4,2),'-dm','linewidth', 2,'MarkerSize',10);
plot(x,meanrmse(2:4,3),'-xr','linewidth', 2,'MarkerSize',10);
plot(x,meanrmse(2:4,4),'-oc','linewidth', 2,'MarkerSize',10);

legStr={'RShift','linear_{all}','SVR_{linear}','SVR_{rbf}','linear_{\delta}'};
set(gca,'Fontsize', 20);
%xlabels={'0','1min','2min','3min'};
xlabels={'1min','2min','3min'};
ylabel('Average RMSE');
axis([1 3 0 0.4]);
set(gca,'YTick',0.07:0.03:0.2);
set(gca,'Fontsize', 20);
set(gca,'XTick',1:3);
set(gca,'XTickLabel',xlabels);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='predrmse_20131203.jpg';
print(f_b,'-djpeg ',outputf);
print(f_b,'-depsc','-r300',[outputf(1:end-4) '.eps']);

return;


%% plot out prediction results
indexiplot=[1 2 3 4 5 6]; %0,10,20,40,60
f_b=figure();
hold on;
grid on;
bardata1=zeros(30,1);   bardata2=zeros(30,1); bardata3=zeros(30,1);
bardata4=zeros(30,1);   bardata5=zeros(30,1);
tmindex=1:30;
for tmi=1:30
    tmr=rem(tmi,5);
    if tmr==0
        tmindexi=indexiplot(floor(tmi/5));
    else
        tmindexi=indexiplot(floor(tmi/5)+1);
    end
    if tmr==1
        bardata1(tmi)=meanmae(tmindexi,1);
    elseif tmr==2
        bardata2(tmi)=meanmae(tmindexi,2);
    elseif tmr==3
        bardata3(tmi)=meanmae(tmindexi,3);
    elseif tmr==4
        bardata4(tmi)=meanmae(tmindexi,4);
    elseif tmr==0
        bardata5(tmi)=meanmae(tmindexi,5);
    end
end
h1=bar(bardata1,'r');
h2=bar(bardata2,'g');
h3=bar(bardata3,'b');
h4=bar(bardata4,'c');
h5=bar(bardata5,'y');

legStr={'li','SVR_{li}','SVR_{rbf}','RBR','RShift'};
set(gca,'Fontsize', 20);
%xlabels={'0','10s','20s','40s','1min'};
xlabels={'0','1min','2min','3min','4min','5min'};
%xlabel(xStr);
ylabel('Average MAE');
axis([0 31 0.02 0.12]);
set(gca,'Fontsize', 16);
set(gca,'XTick',3:5:28);
set(gca,'XTickLabel',xlabels);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='predmae.jpg';
print(f_b,'-djpeg ',outputf);
print(f_b,'-depsc','-r300',[outputf(1:end-4) '.eps']);




indexiplot=[1 2 3 4 5 6]; %0,10,20,40,60
f_b=figure();
hold on;
grid on;
bardata1=zeros(30,1);   bardata2=zeros(30,1); bardata3=zeros(30,1);
bardata4=zeros(30,1);   bardata5=zeros(30,1);
tmindex=1:30;
for tmi=1:30
    tmr=rem(tmi,5);
    if tmr==0
        tmindexi=indexiplot(floor(tmi/5));
    else
        tmindexi=indexiplot(floor(tmi/5)+1);
    end
    if tmr==1
        bardata1(tmi)=meanrmse(tmindexi,1);
    elseif tmr==2
        bardata2(tmi)=meanrmse(tmindexi,2);
    elseif tmr==3
        bardata3(tmi)=meanrmse(tmindexi,3);
    elseif tmr==4
        bardata4(tmi)=meanrmse(tmindexi,4);
    elseif tmr==0
        bardata5(tmi)=meanrmse(tmindexi,5);
    end
end
h1=bar(bardata1,'r');
h2=bar(bardata2,'g');
h3=bar(bardata3,'b');
h4=bar(bardata4,'c');
h5=bar(bardata5,'y');

legStr={'li','SVR_{li}','SVR_{rbf}','RBR','RShift'};
set(gca,'Fontsize', 16);
%xlabels={'0','10s','20s','40s','1min'};
xlabels={'0','1min','2min','3min','4min','5min'};
%xlabel(xStr);
ylabel('Average RMSE');
axis([0 31 0.07 0.22]);
set(gca,'Fontsize', 16);
set(gca,'XTick',3:5:28);
set(gca,'XTickLabel',xlabels);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='predrmse.jpg';
print(f_b,'-djpeg ',outputf);
print(f_b,'-depsc','-r300',[outputf(1:end-4) '.eps']);

return;

%% previous code
f1=figure();
legStr={'li','SVR_{li}','SVR_{rbf}'};
xStr='Solar Panel #';
hold on;
grid on;
xlabel(xStr,'Fontsize', 16);
ylabel('Mean Absolute Error','Fontsize', 16);
plot(1:BPS_N,MAE(:,1),'--xr','linewidth', 2);
plot(1:BPS_N,MAE(:,2),'--+g','linewidth', 2);
plot(1:BPS_N,MAE(:,3),'--ob','linewidth', 2);

set(gca,'Fontsize', 16);
axis([1 25 0.055 0.1]);
set(gca,'XTick',1:25);
set(gca,'XTickLabel',1:25);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='modelsmae.jpg';
print(f1,'-djpeg ',outputf);
print(f1,'-depsc','-r300',[outputf(1:end-4) '.eps']);


f2=figure();
legStr={'li','SVR_{li}','SVR_{rbf}'};
xStr='Solar Panel #';
hold on;
grid on;
xlabel(xStr,'Fontsize', 16);
ylabel('Root Mean Squre Error','Fontsize', 16);
plot(1:BPS_N,RMSE(:,1),'--xr','linewidth', 2);
plot(1:BPS_N,RMSE(:,2),'--+g','linewidth', 2);
plot(1:BPS_N,RMSE(:,3),'--ob','linewidth', 2);

set(gca,'Fontsize', 16);
axis([1 25 0.09 0.15]);
set(gca,'XTick',1:25);
set(gca,'XTickLabel',1:25);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='modelsrmse.jpg';
print(f2,'-djpeg ',outputf);
print(f2,'-depsc','-r300',[outputf(1:end-4) '.eps']);


sec1=1:10;
sec2=11:20;
sec3=21:25;
secs={sec1,sec2,sec3};
maes=zeros(3,3);
rmses=zeros(3,3);
for seci=1:3
    tmsec=secs{seci};
    maes(seci,1)=mean(MAE(tmsec,1));
    maes(seci,2)=mean(MAE(tmsec,2));
    maes(seci,3)=mean(MAE(tmsec,3));
    rmses(seci,1)=mean(RMSE(tmsec,1));
    rmses(seci,2)=mean(RMSE(tmsec,2));
    rmses(seci,3)=mean(RMSE(tmsec,3));
end
f3=figure();
hold on;
legStr={'li','SVR_{li}','SVR_{rbf}'};
set(gca,'Fontsize', 16);
h1=bar([maes(1,1) 0 0 maes(2,1) 0 0  maes(3,1) 0 0],'r');
h2=bar([0 maes(1,2) 0 0 maes(2,2) 0 0 maes(3,2) 0],'b');
h3=bar([0 0 maes(1,3) 0 0 maes(2,3) 0 0 maes(3,3)],'y');
xlabels={'Panel 1~10','Panel 11~20','Panel 21~25'};
%xlabel(xStr);
ylabel('Average of MAE');
axis([0 10 0.05 0.095]);
set(gca,'Fontsize', 16);
set(gca,'XTick',2:3:8);
set(gca,'XTickLabel',xlabels);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='secmae.jpg';
print(f3,'-djpeg ',outputf);
print(f3,'-depsc','-r300',[outputf(1:end-4) '.eps']);




MAE