function CSLRemovebyAbnormal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STARTUP to get all possible parameter settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
startup;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
location=location1;
azshift=AzumithShift1;
zeshift=ZenithShift1;
avail_f=sprintf('avail_tt1.mat');
sbazmatf=('/home/mikegrup/TSIPipeline/BNLPipeline/preprocessing/sbazimuth.mat');
haazmatf=('/home/mikegrup/TSIPipeline/BNLPipeline/preprocessing/haazimuth.mat');



lon=location.longitude;
lat=location.latitude;
alt=location.altitude;
dvs=[
    [2012 07 21 19 50 46],
    [2012 07 21 19 05 42],
    [2012 07 30 14 05 22],
    [2012 07 30 14 10 22],
    [2012 07 30 14 15 23],
    [2012 07 30 14 20 23],
    [2012 07 30 19 35 56],
    [2012 08 05 19 35 52],
    [2012 08 09 14 50 17],
    [2012 08 09 19 20 40],
    [2012 08 26 19 55 43],
    [2012 08 28 19 30 46],
    [2012 08 28 19 55 48],
    [2012 08 28 18 45 47]
    ];

dns=datenum(dvs);
l=length(dns);
sbazimuth=load(sbazmatf);                % load all sbmask timenumbers and azumith
haazimuth=load(haazmatf);                % load all hamask timenumbers and azumith

for i=1:l
    tmdn=dns(i);
    tmdv=dvs(i,:);
    sun=INNER_GETAandZ(tmdv,lat,lon,alt);
    az=sun.azimuth/180*pi+azshift/180*pi;
    ze=sun.zenith/180*pi+zeshift/180*pi;
    haindexi=GetAZIndex(haazimuth.azs,haazimuth.zes,az,ze);
    sbindexi=GetAZIndex_wThres(sbazimuth.tsdn,sbazimuth.azs,sbazimuth.zes,tmdn,az,ze);
    dispstr=sprintf('mask: %s,az= %f, ori:%s,az = %f, sbindexi=%d',...
        datestr(sbazimuth.tsdn(sbindexi),'[yyyy mm dd HH MM SS]'),...
        sbazimuth.azs(sbindexi),datestr(tmdn,'[yyyy mm dd HH MM SS]'),az,sbindexi);
    disp(dispstr);    
end

end