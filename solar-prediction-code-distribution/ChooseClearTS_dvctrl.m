% ChooseClearTS:
%   It will choose clear sky situations during the given radiation files.
%   The output file will be written as a matfile
%   Version: 1.0    2012/3/30  
%   INPUT:
%       RadDir      --  radiation file location
%       imgDir      --  image files
%       startdv     --  start date vector
%       enddv       --  end date vector
%       UndistortionParameters.mat(in)  --  site information here
%       
%   OUTPUT:
%       CSL_TS.mat  --  Clera Sky Library TimeStamps
%           #_tsdn    --  date number of each record

function ChooseClearTS_dvctrl(RadDir,imgDir,startdv,enddv)
if(RadDir(end)~='/')
    RadDir=sprintf('%s%s',RadDir,'/');
end
if(imgDir(end)~='/')
    imgDir=sprintf('%s%s',imgDir,'/');
end
load('UndistortionParameters.mat');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tmstep=datenum([0,0,0,0,0,30]);       %tmstep -- time step of consecutive images.
%which is 30s by default Here can be changed to 1s for BNL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
startdn=datenum(startdv);
enddn=datenum(enddv);
iter_i=1;
tmdn=startdn;
while(tmdn<enddn)
    tmdn=startdn+iter_i*tmstep;
    iter_i=iter_i+1;
end
NofTS=iter_i-1;     %total number of timestamps


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   1. Enumerate all image files
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmpi(SITE,'TWP')==0
    disp('ATTENTION: SITE is not TWP');
end
cenx=Hi/2;
ceny=Wi/2;
Distctrl=200;
maskctrl=zeros(Hi,Wi);      % Mask control is used for cloud fraction test area mask
ncount=0;
for i=1:Hi
    for j=1:Wi
        xi=i-cenx;
        yi=j-ceny;
        if(sqrt(xi^2+yi^2)<Distctrl)
            ncount=ncount+1;
            maskctrl(i,j)=1;
        end
    end
end
nTotalPixels=ncount;

cloudthres=0.65;        %   cloud R/B ratio threshold
cfthres=0.4;            %   cloud fraction threshold
% allfiles=dir(imgDir);
% [m,~]=size(allfiles);
m=NofTS;    %    use N of Timestamps instead
% tsdn=zeros(m-2,1);
tsdn=zeros(m,1);
tmdn=startdn;
i=1;        %interation index
while(tmdn<enddn)
    tmdv=datevec(tmdn);
    filename=GetImgFromDV_TWPC1(tmdv);
    inputf=sprintf('%s%s',imgDir,filename);
    inputim=imread(inputf);
    f1=double(inputim(:,:,1));
    f3=double(inputim(:,:,3));
    rbma=(f1./f3).*maskctrl;    %R/B ratio
    cf=length(find(rbma>cloudthres))/ncount;
    if(cf<cfthres)
        tsdn(i)=datenum(tmdv);
    end
    tmdn=startdn+i*tmstep;
    i=i+1;
end
tsdn=tsdn(tsdn>0);
outputp='CSL_TS.mat';
save(outputp,'tsdn');

end