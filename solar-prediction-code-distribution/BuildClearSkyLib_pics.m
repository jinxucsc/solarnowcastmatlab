% BuildClearSkyLib:
%   If we have clear sky timestamps to build a library. Then read it and
%   Generate all the mask files and put them together into sbmask and
%   hamask files.
%   INPUT:
%       DataDir         --  Contains the original images
%       HAMaskDir      --  Contains the HA output mask files
%       SBMaskDir      --  Contains the SB output mask files
%       SBMaskOutDir    -- Direcotry, contains output sbmasks
%       TestDir(test)       --  test the mask images
%       RemovalParameters.mat(in)   --  all the readin thresholds
%       UndistortionParameters.mat(in)--Original Image information
%       cls_ts.mat(in)              --  all clear sky timestamp (OBSOLETE)
%       sbmask_pixelrange.bmp       --  bmp file (black -> invalid white -> valid)
%                                       pixels in line regression cannot
%                                       pick points out of white area
%   OUTPUT:
%       HAMaskDir/*    -- All the mask files(.mat) will be put here
%       SBMaskDir/*    -- All the mask files(.mat) will be put here
%       TestDir/*    -- Test result for itself. Fore later manually pick
%
%   Version: 1.0    2012/3/30
%   Version: 1.1    2013/01/24
%       Use SBMask only for testing of July data of 2012
%       Make it as newest version of BNL C1 data for later usage

function BuildClearSkyLib_pics(pics_vec,HAMaskDir,SBMaskOutDir)
npics=length(pics_vec);

for i=1:npics
    
    
end




end