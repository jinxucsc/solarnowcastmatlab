function imgoutma=INNER_Undist(imginma,undist2orima,Wu,Hu)
imgoutma=zeros(Hu,Wu,3);

for Xu=1:Wu
    for Yu=1:Hu
        yo=round(undist2orima(Yu,Xu,1));
        xo=round(undist2orima(Yu,Xu,2));
        if(xo==0&&yo==0)
            continue;
            %                     unimg(Yu,Xu,1)=0;%black
            %                     unimg(Yu,Xu,2)=0;
            %                     unimg(Yu,Xu,3)=0;
        else
            imgoutma(Yu,Xu,1)=imginma(yo,xo,1);
            imgoutma(Yu,Xu,2)=imginma(yo,xo,2);
            imgoutma(Yu,Xu,3)=imginma(yo,xo,3);
        end
    end
end

end