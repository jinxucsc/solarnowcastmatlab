 load('./tmhma_new.mat');
 
 
 H_ran=[1000:100:5000 5500:500:40000];
 
 
hl=length(H_ran);
H_all=H_valid;
H_all(:,:)=0;
for i=1:hl
    H=H_ran(i);
    if H<=5000
        Hdiff=abs(H_valid(:)-H);
        [hmin tmind]=min(Hdiff);
        if hmin<100
            H_all(tmind)=H_valid(tmind);
        end
    else
        Hdiff=abs(H_valid(:)-H);
        [hmin tmind]=min(Hdiff);
        if hmin<500
            H_all(tmind)=H;
        end
    end
end

H_all

save('./tmhma_new.mat');