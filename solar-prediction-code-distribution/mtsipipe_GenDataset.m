% mtsipipe_GenDataset
%   This function is called to generate mtsi dataset for radiation forecast. Previous version is
%       test_multicld_sfplot_20131203, test_multicld_sfplot. The function will take filled images
%       and radiation values into training/test dataset based on different timeran defination
% INPUT:
%       datevecobj              --- [startdv;enddv], See datevectorparser.m for details.
%       timerange               --- Array, prediction time/times.
%       mode                    --- String, for parsing running environment
%       cldmodelmatf            --- MAT file, SVM model for cloud detection
%       configfpath             --- String, mtsi_startup filepath
%       SolarFarmRadRootDir     ---	Directory, which is the root of solar radiation data (See
%                                   /home/mikegrup/D/workspace/SolarFarmDataProcessing/BPS_SP2A_H_Avg_MIN_days_dat/
%       FilledTSI3Dir           --- Directory, containing all the filled TSI3 images
%       datasetDir              --- Directory, containing all output dataset files
%       testplotDir             --- Directory, for UNIT TEST of plots
% OUTPUT:
%       datasetDir/*.mat        --- MAT files, output file
%       testplotDir/*           --- Plots for test
%
%
% PREREQUISITE:
%       mtsipipe2 ---  Get layers information and tracking results
%       mtsipipe_fill --- Generate the filling results
% VERSION: 1.0      2013/12/04
%   Previously test_multicld_sfplot_20131203, test_multicld_sfplot
% VERSION: 1.1      2013/12/30
%   Extend code to generate days dataset instead of ONEDAY dataset.
% VERSION: 1.2      2014/01/20
%   Add new features. 
% USAGE:
%     SolarFarmRadRootDir='/home/mikegrup/D/workspace/SolarFarmDataProcessing/BPS_SP2A_H_Avg_MIN_days_dat/';
%     FilledTSI3Dir='./testfill';
%     datasetDir='./dataset_20131203';
%     testplotDir='./sftestplot_20131203/';
%     mtsipipe_GenDataset([2013 05 14 0 0 0;2013 05 15 0 0 0],0,'laptop','model_rbr.mat','mtsi_startup',SolarFarmRadRootDir,FilledTSI3Dir,datasetDir,testplotDir);
%
function mtsipipe_GenDataset(datevecobj,timerange,mode,cldmodelmatf,configfpath,SolarFarmRadRootDir,FilledTSI3Dir,datasetDir,testplotDir)
%mtsi_startup;
run(configfpath);
%bMulti=modepaser(mode,'multicore');
bServer=modepaser(mode,'server');
bPlot=modepaser(mode,'plot');
%bSkip=modepaser(mode,'skip');
%bRun=modepaser(mode,'run');
%bCldMatMod=modepaser(mode,'cldmatmod');
bPlotVisible=modepaser(mode,'visible');
if bPlotVisible
    set(0,'DefaultFigureVisible', 'on');
else
    set(0,'DefaultFigureVisible', 'off');
end

if bServer
    dataDir='~/workdata/mtsi_stich/';
else
    dataDir='/data/workdata/mtsi_stich/';
end
dataDir=FormatDirName(dataDir);

assert((exist(cldmodelmatf,'file')~=0),'Cloud Model mat file doen''s exist!');

outputDir=datasetDir;
outputDir=FormatDirName(outputDir);
if ~exist(outputDir,'dir')
    mkdir(outputDir);
end
sfradDir=SolarFarmRadRootDir;
sfradDir=FormatDirName(sfradDir);
FilledTSI3Dir=FormatDirName(FilledTSI3Dir);

ONEDAY=60*24;   % in minutes
BPS_N=25;       % BPS number
ONEMIN_DN=datenum([0 0 0 0 1 0]);
MAXRAD=950;     MINRAD=0;


% Get Avail mat files for database
mtsi_radavailf='mtsi_radavail.mat';
if ~exist(mtsi_radavailf,'file')
    [LISFRADavail,LISFRADavail_days,LISFRAD_all,LISFRAD_days]=TO_CheckLISFRadAvail(sfradDir);
    save(mtsi_radavailf,'LISFRADavail','LISFRADavail_days','LISFRAD_all','LISFRAD_days');
else
    t=load(mtsi_radavailf);
    LISFRADavail=t.LISFRADavail;
    %LISFRADavail_days=t.LISFRADavail_days;
    LISFRAD_all=t.LISFRAD_all;
    %LISFRAD_days=t.LISFRAD_days;
end
mtsi_filledavailf='mtsi_filledtsiavail.mat';
if ~exist(mtsi_filledavailf,'file')
    [fill_avail,fill_avail_days]=tmp_CheckFilledTSIAvail(FilledTSI3Dir);
    save(mtsi_filledavailf,'fill_avail','fill_avail_days');
else
    t=load(mtsi_filledavailf);
    fill_avail=t.fill_avail;
    fill_avail_days=t.fill_avail_days;
end

[startdv,enddv,startdn,enddn]=datevectorparser(datevecobj); % Parse into startdv and enddv
alldns=fill_avail(fill_avail>=startdn & fill_avail<enddn);
alldns_days=fill_avail_days(fill_avail_days>=startdn & fill_avail_days<enddn);
NDAYS=length(alldns_days);
NRECS=length(alldns);
assert(NDAYS~=0 && NRECS~=0,'There is no TSI image in that time range');
alldvs=datevec(alldns);
alldvs_est=GMT2EST(alldvs);
alldns_est=datenum(alldvs_est);
indma=zeros(NRECS,1);
for i=1:NRECS
    tmdn=alldns_est(i);
    indexi=find(LISFRADavail==tmdn,1);
    if ~isempty(indexi)
        indma(i)=indexi;
    end
end
ind=indma(indma~=0);
%indma=(LISFRADavail==alldns);
allrad=LISFRAD_all(ind,:);
alldns=alldns(indma~=0);
NRECS=length(alldns);

%% Read SVM Model file for CLD fraction
t=load(cldmodelmatf);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
model=t.model;
x1_min=t.x1_min;
x1_max=t.x1_max;
x2_min=t.x2_min;
x2_max=t.x2_max;
x3_min=t.x3_min;
x3_max=t.x3_max;
x4_min=t.x4_min;
x4_max=t.x4_max;
h=t.h;
w_1=model.SVs'*model.sv_coef;b_1=-model.rho;
% predicted_labels=svmpredict_li(x_valid,w_1,b_1);
%     predicted_labels(predicted_labels==1)=0;        % cld
%     predicted_labels(predicted_labels==-1)=1;       % sky
x_min=[x1_min x2_min x3_min x4_min];
x_max=[x1_max x2_max x3_max x4_max];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Start Main process of dataset generation
timeran=timerange;
tl=length(timeran);
meanwinsize=3;
cldpixelwinsize=9;
meanwinsize_half=floor(meanwinsize/2);
cldpixelwinsize_half=floor(meanwinsize/2);
% obpoints=[122 124 125 144 147 152 162 182 184];
% obpoints=obpoints+600;
str1=datestr(startdv,'yyyymmdd.HHMMSS');
str2=datestr(enddv,'yyyymmdd.HHMMSS');

for ti=1:tl
    od_cf=zeros(NRECS,1);  %cloud fraction, it has nothing to do with BPS number

    timeahead=timeran(ti);
    timeahead_dn=datenum([0 0 0 0 0 timeahead]);
    tmfilename=[str1 '-' str2];
    cffile=sprintf('%s%s_%d_cf.mat',outputDir,tmfilename,timeahead);
    rgbfile=sprintf('%s%s_%d_%d_rgb.mat',outputDir,tmfilename,timeahead,meanwinsize);
    tmpfile=sprintf('%s%s_%d_%d_all.mat',outputDir,tmfilename,timeahead,meanwinsize);    % sftmp.mat is the radiation file for current frame
    %if 1
    if ~exist(cffile,'file')
        for i=1:NRECS
            tmdn=alldns(i);
            tmdn_a=tmdn-timeahead_dn;
            tmdv_a=datevec(tmdn_a);
            filename_a=GetImgFromDV_BNL(tmdv_a,TSI3);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   for current version of mtsipipe_TSI3fill.m
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            filledimgpath=sprintf('%s%s_shifttest.jpg',FilledTSI3Dir,filename_a);
            paraspath=sprintf('%s%s_fillparas.mat',FilledTSI3Dir,filename_a);
            %             if ~exist(filledimgpath,'file') && ~exist(paraspath,'file')
            %                 continue;
            %             end
            if ~exist(filledimgpath,'file')
                continue;
            end
            imma=imread(filledimgpath);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            xtrain3=tmp_img2traindata_rbr(imma,h,x_min,x_max);
            pl3=svmpredict_li(xtrain3,w_1,b_1);
            pl3(pl3==1)=0;        % cld
            pl3(pl3==-1)=1;       % sky
            imma3_cldma=false(UNDISTH,UNDISTW);
            imma3_cldma(:)=~pl3;
            nTotal=UNDISTH*UNDISTW;
            nCld=length(find(imma3_cldma));
            %[imma3_std,imma3_std_invalid]=tmp_GetImgStdMatFromDataDir(tmdv_a,TSI3,StdBlockSize,dataDir);
            %imma3_cldma_n=(imma3_std>StdCldThres|imma3_cldma)&imma3_std_invalid;
            %nTotal=length(find(imma3_std_invalid));
            %nCld=length(find(imma3_cldma_n));
            od_cf(i)=nCld./nTotal;
        end
        save(cffile,'od_cf');
    else
        tmt=load(cffile);
        od_cf=tmt.od_cf;
    end
    %continue;
    
    od_r=zeros(NRECS,BPS_N,3); %min,max,mean
    od_g=zeros(NRECS,BPS_N,3);
    od_b=zeros(NRECS,BPS_N,3);
    od_prer=zeros(NRECS,BPS_N,3);
    od_preg=zeros(NRECS,BPS_N,3);
    od_preb=zeros(NRECS,BPS_N,3);
    od_rad=zeros(NRECS,BPS_N);
    od_prerad=zeros(NRECS,BPS_N);
    od_arad=zeros(NRECS,BPS_N);
    od_Href=zeros(NRECS,1);
    if timeahead~=0        
        if 1
            %if ~exist(rgbfile,'file')
            for i=1:NRECS
                tmdn=alldns(i);
%                 if i~=320
%                     continue;
%                 end
                %             if tmdn<datenum([2013 05 25 17 0 0]);
                %                 continue;
                %             end
                tmdv=datevec(tmdn);         % pred
                tmdn_a=tmdn-timeahead_dn;   % ground-truth
                tmdn_1min=tmdn_a-ONEMIN_DN; % ground-truth - 1min as previous timestamp
                tmdv_a=datevec(tmdn_a);
                tmdv_1min=datevec(tmdn_1min);
                filename_a=GetImgFromDV_BNL(tmdv_a,TSI3);
                filename_1min=GetImgFromDV_BNL(tmdv_1min,TSI3);
                inputf_1min=sprintf('%s%s_shifttest.jpg',FilledTSI3Dir,filename_1min);
                inputf_a=sprintf('%s%s_shifttest.jpg',FilledTSI3Dir,filename_a);
                if ~exist(inputf_1min,'file') || ~exist(inputf_a,'file')
                    %disp(inputf);
                    continue;
                end
                i_a=find(abs(alldns-tmdn_a)<1e-6,1);
                if isempty(i_a)
                    disp(['Previous timestamp is not found in alldns array! tmdv_a = ' datestr(tmdv_a)]);
                    continue;
                end
                i_1min=find(abs(alldns-tmdn_1min)<1e-6,1);
                if isempty(i_1min)
                    disp(['ONEMIN of previous timestamp is not found in alldns array! tmdv_a = ' datestr(tmdv_1min)]);
                    continue;
                end
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %   for current version of mtsipipe_TSI3fill.m
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % Correction: groundtruth t-1,  pred: t,
                paraspath=sprintf('%s%s_fillparas.mat',FilledTSI3Dir,filename_a);
                t=load(paraspath);
                HRef=t.HRef;    MVRef=t.MVRef; %[mvx1 mvy1;mvx2 mvy2]
                if isempty(HRef)
                    H=10000;
                    disp(['Height Reference is null. Use default value H= ' int2str(H) ', timestamp=' datestr(tmdv_a)]);
                else
                    H=HRef(1);
                    movement_x=MVRef(1,1)*round(timeahead/10);
                    movement_y=MVRef(1,2)*round(timeahead/10);
                end
                %H=9000;
                %MV_refx=MVRef(1,1);     MV_refy=MVRef(1,2);
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %imma=imread(inputf);   % impossible to fetch prediction image
                imma_a=imread(inputf_a); 
                imma_1min=imread(inputf_1min);  % ONEMIN of t - 1
                [x,y]=GetBPSPts(tmdv,TSI3,H);           % SP(t) information
                [x_1min,y_1min]=GetBPSPts(tmdv_1min,TSI3,H);           % OneMIN ahead of SP(t-1) information
                %[x_a,y_a]=GetBPSPts(tmdv_a,TSI3,H);
                x_pre = x-movement_x;
                y_pre = y-movement_y;
                x_pre=round(x_pre);  y_pre=round(y_pre);
                x_1min=round(x_1min);   y_1min=round(y_1min);
                
                od_Href(i)=H;
                for j=1:BPS_N
                    if y_pre(j)-meanwinsize_half<1 || y_pre(j)+meanwinsize_half>UNDISTH ||...
                            x_pre(j)-meanwinsize_half<1 || x_pre(j)+meanwinsize_half>UNDISTW
                        continue;
                    end
                    if y_1min(j)-meanwinsize_half<1 || y_1min(j)+meanwinsize_half>UNDISTH ||...
                            x_1min(j)-meanwinsize_half<1 || x_1min(j)+meanwinsize_half>UNDISTW
                        continue;
                    end
                    
                    sj=y_pre(j)-meanwinsize_half;   ej=y_pre(j)+meanwinsize_half;
                    si=x_pre(j)-meanwinsize_half;   ei=x_pre(j)+meanwinsize_half;
                    rvals=imma_a(sj:ej,si:ei,1);
                    gvals=imma_a(sj:ej,si:ei,2);
                    bvals=imma_a(sj:ej,si:ei,3);
                    od_r(i,j,3)=round(mean(double(rvals(:))));
                    od_g(i,j,3)=round(mean(double(gvals(:))));
                    od_b(i,j,3)=round(mean(double(bvals(:))));
                    od_r(i,j,1)=round(min(double(rvals(:))));
                    od_g(i,j,1)=round(min(double(gvals(:))));
                    od_b(i,j,1)=round(min(double(bvals(:))));
                    od_r(i,j,2)=round(max(double(rvals(:))));
                    od_g(i,j,2)=round(max(double(gvals(:))));
                    od_b(i,j,2)=round(max(double(bvals(:))));
                    
                    sj=y_1min(j)-meanwinsize_half;   ej=y_1min(j)+meanwinsize_half;
                    si=x_1min(j)-meanwinsize_half;   ei=x_1min(j)+meanwinsize_half;
                    rvals=imma_1min(sj:ej,si:ei,1);
                    gvals=imma_1min(sj:ej,si:ei,2);
                    bvals=imma_1min(sj:ej,si:ei,3);
                    od_prer(i,j,3)=round(mean(double(rvals(:))));
                    od_preg(i,j,3)=round(mean(double(gvals(:))));
                    od_preb(i,j,3)=round(mean(double(bvals(:))));
                    od_prer(i,j,1)=round(min(double(rvals(:))));
                    od_preg(i,j,1)=round(min(double(gvals(:))));
                    od_preb(i,j,1)=round(min(double(bvals(:))));
                    od_prer(i,j,2)=round(max(double(rvals(:))));
                    od_preg(i,j,2)=round(max(double(gvals(:))));
                    od_preb(i,j,2)=round(max(double(bvals(:))));
                    
                end
                tmrad=allrad(i,:);
                tmrad_a=allrad(i_a,:);
                tmrad_pre=allrad(i_1min,:);
                od_rad(i,:)=tmrad(:);
                od_arad(i,:)=tmrad_a(:);
                od_prerad(i,:)=tmrad_pre(:);
            end
            save(rgbfile,'od_r','od_g','od_b','od_prer','od_preg',...
                'od_preb','od_rad','od_prerad','od_arad','od_Href');
            
        else
            t=load(rgbfile);
            od_r=t.od_r;
            od_g=t.od_g;
            od_b=t.od_b;
            od_prer=t.od_prer;
            od_preg=t.od_preg;
            od_preb=t.od_preb;
            od_rad=t.od_rad;
            od_prerad=t.od_prerad;
            od_arad=t.od_arad;
            od_Href=t.load(od_Href);
        end
        
    
    % if timeahead ==0
    else 
        if 1
        %if ~exist(rgbfile,'file')
        for i=1:NRECS

            tmdn=alldns(i);            
%             if tmdn<datenum([2013 05 25 17 0 0]);
%                 continue;
%             end
            tmdv=datevec(tmdn);
            tmdn_a=tmdn-ONEMIN_DN;
            tmdv_a=datevec(tmdn_a);
            filename=GetImgFromDV_BNL(tmdv,TSI3);
            filename_a=GetImgFromDV_BNL(tmdv_a,TSI3);
            inputf=sprintf('%s%s_shifttest.jpg',FilledTSI3Dir,filename);
            inputf_a=sprintf('%s%s_shifttest.jpg',FilledTSI3Dir,filename_a);
            if ~exist(inputf,'file') || ~exist(inputf_a,'file')
                %disp(inputf);
                continue;
            end
            i_a=find(abs(alldns-tmdn_a)<1e-6,1);
            if isempty(i_a)
                disp(['Previous timestamp is not found in alldns array! tmdv_a = ' datestr(tmdv_a)]);
                continue;
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   for current version of mtsipipe_TSI3fill.m
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            paraspath=sprintf('%s%s_fillparas.mat',FilledTSI3Dir,filename_a);
            t=load(paraspath);
            HRef=t.HRef;    %MVRef=t.MVRef;
            if isempty(HRef)
                H=10000;
                disp(['Height Reference is null. Use default value H= ' int2str(H) ', timestamp=' datestr(tmdv_a)]);
            else
                H=HRef(1);
            end
            %H=9000;
            %MV_refx=MVRef(1,1);     MV_refy=MVRef(1,2);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            imma=imread(inputf);
            imma_a=imread(inputf_a);
            [x,y]=GetBPSPts(tmdv,TSI3,H);
            [x_a,y_a]=GetBPSPts(tmdv_a,TSI3,H);
            x=round(x);  y=round(y);
            x_a=round(x_a);  y_a=round(y_a);
            for j=1:BPS_N
                if y_a(j)-meanwinsize_half<1 || y_a(j)+meanwinsize_half>UNDISTH ||...
                        x_a(j)-meanwinsize_half<1 || x_a(j)+meanwinsize_half>UNDISTW
                    continue;
                end
                if y(j)-meanwinsize_half<1 || y(j)+meanwinsize_half>UNDISTH ||...
                        x(j)-meanwinsize_half<1 || x(j)+meanwinsize_half>UNDISTW
                    continue;
                end
                
                rvals=imma(y(j)-meanwinsize_half:y(j)+meanwinsize_half,x(j)-meanwinsize_half:x(j)+meanwinsize_half,1);
                gvals=imma(y(j)-meanwinsize_half:y(j)+meanwinsize_half,x(j)-meanwinsize_half:x(j)+meanwinsize_half,2);
                bvals=imma(y(j)-meanwinsize_half:y(j)+meanwinsize_half,x(j)-meanwinsize_half:x(j)+meanwinsize_half,3);
                od_r(i,j,3)=round(mean(double(rvals(:))));
                od_g(i,j,3)=round(mean(double(gvals(:))));
                od_b(i,j,3)=round(mean(double(bvals(:))));
                
                od_r(i,j,1)=round(min(double(rvals(:))));
                od_g(i,j,1)=round(min(double(gvals(:))));
                od_b(i,j,1)=round(min(double(bvals(:))));
                od_r(i,j,2)=round(max(double(rvals(:))));
                od_g(i,j,2)=round(max(double(gvals(:))));
                od_b(i,j,2)=round(max(double(bvals(:))));
                rvals=imma_a(y_a(j)-meanwinsize_half:y_a(j)+meanwinsize_half,x_a(j)-meanwinsize_half:x_a(j)+meanwinsize_half,1);
                gvals=imma_a(y_a(j)-meanwinsize_half:y_a(j)+meanwinsize_half,x_a(j)-meanwinsize_half:x_a(j)+meanwinsize_half,2);
                bvals=imma_a(y_a(j)-meanwinsize_half:y_a(j)+meanwinsize_half,x_a(j)-meanwinsize_half:x_a(j)+meanwinsize_half,3);
                od_prer(i,j)=round(mean(double(rvals(:))));
                od_preg(i,j)=round(mean(double(gvals(:))));
                od_preb(i,j)=round(mean(double(bvals(:))));
            end
            tmrad=allrad(i,:);
            tmrad_a=allrad(i_a,:);
            od_rad(i,:)=tmrad(:);
            od_prerad(i,:)=tmrad_a(:);
        end
        save(rgbfile,'od_r','od_g','od_b','od_prer','od_preg','od_preb','od_rad','od_prerad');
        
        else
            t=load(rgbfile);
            od_r=t.od_r;
            od_g=t.od_g;
            od_b=t.od_b;
            od_prer=t.od_prer;
            od_preg=t.od_preg;
            od_preb=t.od_preb;
            od_rad=t.od_rad;
            od_prerad=t.od_prerad;
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %    PLOT Test for 25 BPS
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %             time.year    =  tmdv(1);
    %             time.month   =  tmdv(2);
    %             time.day     =  tmdv(3);
    %             time.hour    =  tmdv(4);
    %             time.min    =   tmdv(5);
    %             time.sec    =   tmdv(6);
    %             time.UTC    =   0;
    %             sun = sun_position(time, location3);
    %             az=sun.azimuth;
    %             ze=sun.zenith;
    %             az=az/180*pi;   ze=ze/180*pi;
    %             [undistx,undisty]=TO_AZ2UNDISTCoor(az,ze);
    %             undistx=round(undistx);  undisty=round(undisty);
    %             %    for j=1:BPS_N
    %             %        imma(y(j),x(j),1)=255;
    %             %        imma(y(j),x(j),2)=0;
    %             %        imma(y(j),x(j),3)=0;
    %             %    end
    %             %    imma(undisty,undistx,1)=0;
    %             %    imma(undisty,undistx,2)=255;
    %             %    imma(undisty,undistx,3)=0;
    %             figure();
    %             imshow(imma);
    %             hold on;
    %             for j=1:BPS_N
    %                 plot(x(j),y(j),'+b');
    %                 plot(x_a(j),y_a(j),'or');
    %                 %text(x(j),y(j),['\color{black} ' 'BPS' int2str(j)],'FontSize',18);
    %             end
    %             hold off;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    od_dns=alldns;
    od_r_mean=od_r(:,:,3);
    od_g_mean=od_g(:,:,3);
    od_b_mean=od_b(:,:,3);
    od_r_n=od_r./255;
    od_g_n=od_g./255;
    od_b_n=od_b./255;
    od_prer_n=od_prer./255;
    od_preg_n=od_preg./255;
    od_preb_n=od_preb./255;
    od_rbr=od_r_mean./od_b_mean;
    od_rbr(isnan(od_rbr))=0;
    od_prerbr=od_prer_n(:,:,3)./od_preb_n(:,:,3);
    od_prerbr(isnan(od_prerbr))=0;
    maxval=1.5; minval=0;
    od_rbr(od_rbr==inf|od_rbr>maxval)=maxval;
    od_rbr_n=(od_rbr-minval)./(maxval-minval);
    od_prerbr(od_prerbr==inf|od_prerbr>maxval)=maxval;
    od_prerbr_n=(od_prerbr-minval)./(maxval-minval);
    
    od_rad(od_rad>MAXRAD)=MAXRAD;
    od_prerad(od_prerad>MAXRAD)=MAXRAD;
    od_arad(od_arad>MAXRAD)=MAXRAD;
    od_rad_n=(od_rad-MINRAD)./(MAXRAD-MINRAD);
    od_prerad_n=(od_prerad-MINRAD)./(MAXRAD-MINRAD);
    od_arad_n=(od_arad-MINRAD)./(MAXRAD-MINRAD);
    
    invalidma=(od_r_n==0&od_g_n==0&od_b_n==0);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %    PLOT Test for 25 BPS
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    plotoutDir='unittest_25plot';
    plotoutDir=FormatDirName(plotoutDir);
    if ~exist(plotoutDir,'dir')
        mkdir(plotoutDir);
    end
    for tmi=1:BPS_N
        f1=figure();
        hold on;
        plot(od_r_n(:,tmi,3),'.--r');
        plot(od_g_n(:,tmi,3),'x--g');
        plot(od_b_n(:,tmi,3),'*--b');
        plot(od_rad_n(:,tmi),'*-k');
        hold off;
        outputf=sprintf('%s%d_test.jpg',plotoutDir,tmi);
        print(f1,outputf,'-djpeg');
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    save(tmpfile);
    continue;

    
    %     showstartdv=oddv+[0 0 0 showstarth 0 0];
%     showenddv=oddv+[0 0 0 showendh 0 0];
%     showstartdn=datenum(showstartdv);   showenddn=datenum(showenddv);
%     showstarti=round((showstartdn-startdn)/ONEMINDN)+1;
%     showendi=round((showenddn-startdn)/ONEMINDN)+1;
%     timelabel=showstarth:showendh;
%     NinHour=60;
    
    %rbr and prerbr
%     od_rbr=od_r_mean./od_b_mean;
%     od_rbr(isnan(od_rbr))=0;
    %tmvals=od_rbr(od_rbr~=inf);
    %maxval=max(tmvals(:)); minval=min(tmvals(:));
%     maxval=1.5; minval=0;
%     od_rbr(od_rbr==inf|od_rbr>maxval)=maxval;
%     od_rbr_n=(od_rbr-minval)./(maxval-minval);
%     
%     % generate all predata for training data and test data
%     od_prerad_norm=od_rad_norm;
%     od_prerad_norm(:)=0;
%     od_prer_n=zeros(size(od_r_n,1),size(od_r_n,2));
%     od_preg_n=od_prer_n;    od_preb_n=od_prer_n;
%     od_prerbr_n=od_rbr_n;   od_prerbr_n(:)=0;

%     if timeahead==0
%         nframes=1;
%         od_prerad_norm(1+nframes:end,:)=od_rad_norm(1:end-nframes,:);
%         tmr=od_r_n(:,:,3);
%         tmg=od_g_n(:,:,3);
%         tmb=od_b_n(:,:,3);
%         od_prer_n=od_prer/255;
%         od_preg_n=od_preg/255;
%         od_preb_n=od_preb/255;
%         od_prerbr=od_prer./od_preb;
%         od_prerbr(isnan(od_prerbr))=0;
%         od_prerbr(od_prerbr==inf|od_prerbr>maxval)=maxval;
%         od_prerbr_n=(od_prerbr-minval)./(maxval-minval);
%         %         od_prer_n(1+nframes:end,:)=tmr(1:end-nframes,:);
%         %         od_preg_n(1+nframes:end,:)=tmg(1:end-nframes,:);
%         %         od_preb_n(1+nframes:end,:)=tmb(1:end-nframes,:);
%         %         od_prerbr_n(1+nframes:end,:)=od_rbr_n(1:end-nframes,:);
%         od_radshift=od_rad_ori;
%         od_radshift(od_radshift==0)=nan;
%     else
%         nframes=floor(timeahead/60); % previous timestamp, but ahead radiation
%         od_prerad_norm(1+nframes:end,:)=od_rad_norm(1:end-nframes,:);
%         od_prer_n=od_prer/255;
%         od_preg_n=od_preg/255;
%         od_preb_n=od_preb/255;
%         od_prerbr=od_prer./od_preb;
%         od_prerbr(isnan(od_prerbr))=0;
%         od_prerbr(od_prerbr==inf|od_prerbr>maxval)=maxval;
%         od_prerbr_n=(od_prerbr-minval)./(maxval-minval);
%         od_radshift=od_rad;
%         od_radshift(:,:)=0;
%         od_radshift(1+nframes:end,:)=od_rad_ori(1:end-nframes,:);
%         od_radshift(od_radshift==0)=nan;
%     end
    
    
    
    
    % plot out
    %'sftestplot_20131203/';
    if bPlot
        testplotDir=FormatDirName(testplotDir);
        if ~exist(testplotDir,'dir')
            mkdir(testplotDir);
        end
        od_rad(od_rad==0)=nan;
        od_rad(od_rad>1000)=1000;
        od_rad_norm=od_rad./1000;
        od_r_n=od_r/255;
        od_g_n=od_g/255;
        od_b_n=od_b/255;
        showstarth=10;
        showendh=15;
        invalidma=(od_r_n==0&od_g_n==0&od_b_n==0);
        od_r_n(invalidma)=nan;
        od_g_n(invalidma)=nan;
        od_b_n(invalidma)=nan;
        oddv=startdv;oddv(4:6)=0;
        
        showstartdv=oddv+[0 0 0 showstarth 0 0];
        showenddv=oddv+[0 0 0 showendh 0 0];
        showstartdn=datenum(showstartdv);   showenddn=datenum(showenddv);
        showstarti=round((showstartdn-startdn)/ONEMINDN)+1;
        showendi=round((showenddn-startdn)/ONEMINDN)+1;
        timelabel=showstarth:showendh;
        NinHour=60;
        for si=1:BPS_N
            %     if i~=25
            %         continue;
            %     end
            outputf=sprintf('%ssfplot_bps%d_%d.png',testplotDir,si,timeahead);
            f1=figure();
            titStr=sprintf('Normalized RGB on TSI3 vs. GHI on BPS = %d ',i);
            legStr={'R','G','B','GHI_norm'};
            xStr='EST(UTC/GMT-5)';
            %yStr='Radiation Val(W/m^2)';
            grid on;
            hold on;
            xlabel(xStr);
            %ylabel(yStr);
            plot(od_r_n(showstarti:showendi,si),'--rx');
            plot(od_g_n(showstarti:showendi,si),'--gx');
            plot(od_b_n(showstarti:showendi,si),'--bx');
            plot(od_rad_norm(showstarti:showendi,si),'k');
            set(gca,'XTick',1:NinHour:showendi-showstarti+1);
            set(gca,'XTickLabel',timelabel);
            title(titStr);
            legend(legStr,'location','NorthOutside');
            hold off;
            print(f1,outputf,'-dpng');
        end
    end
    
end
%return;











end