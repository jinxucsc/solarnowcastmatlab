% This function is trying to find the best high altitude cloud using
%   It starts from startp on TSI3, gets recSize rectangle area as reference area. It will search the
%   whole area to find the best H using nccsumall
%   Previous version test_ncc3d_tsi32.m
%       fomula:     similarity = ncc31_21+ncc31_11+ncc_21_22+ncc_31_32+ncc_11_12
%       
%   INPUT:
%       datevector_obj      --- obj of current and next frame time
%       TSI3StartPt         --- 2x1 double, [starty startx]
%       TSI3RecSize         --- 2x1 double, [recH recW]
%       MVWinSize           --- Int, Search MV in [y-MVWinSize:y+MVWinSize,x-MVWinSize:x+MVWinSize]
%       dataDir             --- String, TSI mat Root directory
%       HSearchRange        --- Nx1 double, H range for searching
%       NCCMiniThres        --- double, Min ncc for single ncc operation.Smaller than it equals 0
%       TSI3BlkMask         --- [UNDISTH,UNDISTW] boolean, TSI3BlkMask

%   OUTPUT:
%       H               --- Most possible height
%       MV              --- [MV_y MV_x]
%       HMV             --- [HMV32_y HMV32_x;HMV31_y HMV31_x]
%       cc_all          --- Dim=[Hcc,Wcc,H_l]
%
%   Version 2.0:        2013/10/07
%   Version 3.0:        2013/12/18
%           Use 3 frames (9 images)
function [H,MV,HMV,cc_all,maxcc]=nccsim_3f(datevector_obj,TSI3StartPt,TSI3RecSize,MVWinSize,dataDir,HSearchRange,NCCMiniThres,TSI3BlkMask,CLDFRACTHRES,configfpath)
%mv_x=0;mv_y=0;H=0;HMV_x=0;HMV_y=0;maxcc=0;cc_all=0;
%mtsi_startup;
run(configfpath);
% CLDFRACTHRES=0.3;

[tmdv tmdv_next tmdn tmdn_next]=datevectorparser(datevector_obj);
recH=TSI3RecSize(1);
recW=TSI3RecSize(2);
undistp31=TSI3StartPt;
uy31=undistp31(1);
ux31=undistp31(2);


t=load(HeightMatrixValidMatf);
hma32_valid=t.H_valid_32;
HMV32_x=t.HMVx_valid_32;
HMV32_y=t.HMVy_valid_32;
hma31_valid=t.H_valid_31;
HMV31_x=t.HMVx_valid_31;
HMV31_y=t.HMVy_valid_31;
H_range_full=t.H_range_full;
H_l=length(HSearchRange);
full_valid=zeros(H_l,1);
H_range_full=int32(H_range_full);
H_range=int32(HSearchRange);
for tmi=1:H_l
    ind=find(H_range_full==H_range(tmi),1);
    try
        full_valid(tmi)=ind;
    catch err
        err.message
    end
end

hma32_valid=hma32_valid(full_valid);
HMV32_x=HMV32_x(full_valid);
HMV32_y=HMV32_y(full_valid);
hma31_valid=hma31_valid(full_valid);
HMV31_x=HMV31_x(full_valid);
HMV31_y=HMV31_y(full_valid);


uy32=uy31-MVWinSize;
ux32=ux31-MVWinSize;
%SearchWinSize=SearchWinSize_ext*2+1;
uy31_e=uy31+recH-1;
ux31_e=ux31+recW-1;
uy32_e=uy31_e+MVWinSize;
ux32_e=ux31_e+MVWinSize;
recH_search=uy32_e-uy32+1;
recW_search=ux32_e-ux32+1;
maxRecSize_search=max([recH_search recW_search]);
imma11=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv,TSI1,dataDir));
imma21=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir));
imma31=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir));
imma12=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv_next,TSI1,dataDir));
imma22=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv_next,TSI2,dataDir));
imma32=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv_next,TSI3,dataDir));

[imma11_cldma,imma21_cldma,imma31_cldma]=clouddetector(tmdv,'model_rbr.mat',dataDir,configfpath);


blk31=single(TO_GetImgVal_Pad([uy31 ux31],[recH recW],imma31));
blk31_mask=TO_GetImgVal_Pad([uy31 ux31],[recH recW],TSI3BlkMask);
blk31(~blk31_mask)=0;  % invalid false=0
blk31_cld=logical(TO_GetImgVal_Pad([uy31 ux31],[recH recW],imma31_cldma));
cf31=length(find(blk31_cld))/(recH*recW);
blk32_search=single(TO_GetImgVal_Pad([uy32 ux32],[recH_search recW_search],imma32));
%blk22_search=imma22(uy22:uy22_e,ux22:ux22_e);
if 0~=std(blk31(:)) && 0~=std(blk32_search(:))
    cc=normxcorr2_blk(blk31,blk32_search);
    cc(cc<NCCMiniThres|isnan(cc))=0;
else
    cc=zeros(recH+recH_search-1,recW+recW_search-1);
end


[Hcc,Wcc]=size(cc);
cc_all=zeros(Hcc,Wcc,H_l);

sim_hrange=zeros(H_l,1);

for i=1:H_l
%     if i==47
%         i
%     end
%     if i==10
%         i
%     end
    ux21=round(ux31+HMV32_x(i));
    uy21=round(uy31+HMV32_y(i));
    uy22=round(uy21-MVWinSize);
    ux22=round(ux21-MVWinSize);
    
    ux11=round(ux31+HMV31_x(i));
    uy11=round(uy31+HMV31_y(i));
    uy12=round(uy11-MVWinSize);
    ux12=round(ux11-MVWinSize);
    
    blk21=single(TO_GetImgVal_Pad([uy21 ux21],[recH recW],imma21));
    blk11=single(TO_GetImgVal_Pad([uy11 ux11],[recH recW],imma11));
    blk21_cld=logical(TO_GetImgVal_Pad([uy21 ux21],[recH recW],imma21_cldma));
    blk11_cld=logical(TO_GetImgVal_Pad([uy11 ux11],[recH recW],imma11_cldma));
    cf21=length(find(blk21_cld))/(recH*recW);
    cf11=length(find(blk11_cld))/(recH*recW);
    if abs(cf21-cf31)>CLDFRACTHRES
        sim32=0;
    else
        if  0~=std(blk31(:))&& 0~=std(blk21(:))
            cc32=normxcorr2_blk(blk31,blk21);
            sim32=cc32(recH,recW);
        else
            sim32=0;
        end
        
        if sim32<NCCMiniThres || isnan(sim32)
            sim32=0;
        end
    end
    
    if abs(cf11-cf31)>CLDFRACTHRES
        sim31=0;
    else
        if  0~=std(blk31(:)) && 0~=std(blk11(:))
            cc31=normxcorr2_blk(blk31,blk11);
            sim31=cc31(recH,recW);
        else
            sim31=0;
        end
        
        if sim31<NCCMiniThres || isnan(sim31)
            sim31=0;
        end
    end
    
    sim_hrange(i)=sim32+sim31;
    if sim_hrange(i)==0
        continue;
    end
    blk22_search=single(TO_GetImgVal_Pad([uy22 ux22],[recH_search recW_search],imma22));
    blk12_search=single(TO_GetImgVal_Pad([uy12 ux12],[recH_search recW_search],imma12));
    if sim32==0||std(blk21(:))==0|| 0==std(blk22_search(:))
        cc_2=zeros(Hcc,Wcc);
    else
        cc_2=normxcorr2_blk(blk21,blk22_search);
        cc_2(cc_2<NCCMiniThres|isnan(cc_2))=0;
    end
    if sim31==0||std(blk11(:))==0 || 0==std(blk12_search(:))
        cc_1=zeros(Hcc,Wcc);
    else
        cc_1=normxcorr2_blk(blk11,blk12_search);
        cc_1(cc_1<NCCMiniThres|isnan(cc_1))=0;
    end
    
    cc_all(:,:,i)=sim_hrange(i)+(cc+cc_2+cc_1);
    
end

[maxcc,ind]=max(cc_all(:));
[tmy,tmx,hind]=ind2sub(size(cc_all),ind);
H=H_range(hind);
HMV_y= [HMV32_y(hind);HMV31_y(hind)];
HMV_x= [HMV32_x(hind);HMV31_x(hind)];
mv_x=tmx-recW-MVWinSize;
mv_y=tmy-recH-MVWinSize;    
MV=[mv_y mv_x];
HMV=[HMV_y HMV_x];

end