function undistma_filled=TO_FillBlack_new(undistma_ref,undistma_fill,BlkHma,bstarty,bstartx,bendy,bendx)

[nBlocky nBlockx]=size(BlkHma);

undistma_filled=undistma_fill;
fill_indma=(undistma_fill(:,:,1)==0&undistma_fill(:,:,2)==0);


Hvals=BlkHma(BlkHma>0);
Hvals=unique(Hvals);
Hvals=sort(Hvals,'descend');
nCldLayers=length(Hvals);

for i=1:nCldLayers
    cldH=Hvals(i);
    cldind=find(BlkHma==cldH);
    tml=length(cldind);
    [mv_Hx,mv_Hy]=TO_H2MV('./tmhma_new.mat',cldH);
    for li=1:tml
       tmind=cldind(li);
       tmsy=bstarty(tmind);
       tmsx=bstartx(tmind);
       tmey=bendy(tmind);
       tmex=bendx(tmind);
       
       tmsy1=tmsy+mv_Hy;
       tmsx1=tmsx+mv_Hx;
       tmey1=tmey+mv_Hy;
       tmex1=tmex+mv_Hx;
       [rec_vals,~]=TO_GetImgVal_Pad([tmsy1,tmsx1],[tmey1-tmsy1+1 tmex1-tmsx1+1],fill_indma,30);
       if isempty(find(rec_vals==1,1))
           continue;
       end
       undistma_filled=TO_SetImgVal_Pad([tmsy1 tmsx1],undistma_ref(tmsy:tmey,tmsx:tmex,:),undistma_filled);
       %undistma_filled(tmsy1:tmey1,tmsx1:tmex1,:)=undistma_ref(tmsy:tmey,tmsx:tmex,:);
    end
end

for ci=1:3
    f=undistma_filled(:,:,ci);
    o=undistma_fill(:,:,ci);
    f(~fill_indma)=o(~fill_indma);
    undistma_filled(:,:,ci)=f;
end
% undistma_filled(~fill_indma,1)=undistma_fill(~fill_indma,1);
% undistma_filled(~fill_indma,2)=undistma_fill(~fill_indma,2);
% undistma_filled(~fill_indma,3)=undistma_fill(~fill_indma,3);


end