function [ConnMarks,ConnValidVec]=test_stdblkmerge(blkma,blkvals,std_thres,length_thres,ncc_thres,meangreyThres)
[nBlocky,nBlockx]=size(blkma);
[~,~,blkH,blkW]=size(blkvals);
tmN=blkH*blkW;
searchq={};
ConnMarks=zeros(nBlocky,nBlockx);
% tmblk=zeros(blkH,blkW);
% for i=1:nBlocky
%     for j=1:nBlockx
%         if ~blkma(i,j)
%             continue;
%         end
%         if ConnMarks(i,j)==0
%             tmblk(:,:)=blkvals(i,j,:,:);
%             putq(searchq,tmblk);
%         end
%     end
% end

bAllMarked=false;
NCONN=200;
ConnBlk_mean=cell(NCONN,1);
ConnBlk_std=cell(NCONN,1);
ConnBlk_refblk=cell(NCONN,1);
ConnBlk_sumofblk=cell(NCONN,1);
ConnBlk_Nofblk=cell(NCONN,1);
for ci=1:NCONN
    if isempty(find(ConnMarks==0&blkma==1,1))
        bAllMarked=true;
        break;
    end
    tmbSearchedNei=false(nBlocky,nBlockx);
    ind=find(ConnMarks==0&blkma==1,1);
    [sy sx]=ind2sub(size(ConnMarks),ind);
    q1={};
    q1{end+1}=[sy sx];
    while ~isempty(q1)
        if ConnEnd_Condt(ci)
            % the Connection Area end condition is reached.
            break;
        end
        item=q1{1};
        q1=q1(2:end);
        sy=item(1);sx=item(2);
        tmbSearchedNei(sy,sx)=true;
        if merge_condt(sy,sx,ci,ncc_thres)
            ConnMarks(sy,sx)=ci;
            % Add neighbors into search queue
            for i=sy-1:sy+1
                for j=sx-1:sx+1
                    if i<1 || j<1 || i>nBlocky || j>nBlockx
                        continue;
                    end
                    if ~blkma(i,j)
                        continue;   % not in search range
                    end
                    if 0~=ConnMarks(i,j)
                        continue;   % already marked
                    end
                    if true==tmbSearchedNei(i,j)
                        continue;
                    end
                    q1{end+1}=[i j];
                end
            end
        end
    end
end

ConnValidVec=false(NCONN,1);
for ci=1:NCONN
    if length(find(ConnMarks==ci))>=4
        ConnValidVec(ci)=true;
    end
end
validconnN=length(find(ConnValidVec));

%% subfunction


% subfunction is to judge whether this blk can be merged
    function bMerge=merge_condt(ii,jj,conni,ccThreshold)
        tmblk=zeros(blkH,blkW);
        tmblk(:,:)=blkvals(ii,jj,:,:);
        invalidma=tmblk==0;
        %tmblk(invalidma)=nan;
        tmblk_mean=mean(tmblk(~invalidma));
        tmblk_std=std(tmblk(~invalidma));
        bMerge=false;
        if isempty(ConnBlk_mean{conni})
            ConnBlk_mean{conni}=tmblk_mean;
            ConnBlk_std{conni}=tmblk_std;
            countn=ones(blkH,blkW);
            countn(invalidma)=0;
            ConnBlk_Nofblk{conni}=countn;
            ConnBlk_refblk{conni}=tmblk;
            ConnBlk_sumofblk{conni}=tmblk;
            bMerge=true;
            return;
        end
        tmc_mean=ConnBlk_mean{conni};
        tmc_std=ConnBlk_std{conni};
        tmc_blk=ConnBlk_refblk{conni};
        if ~isempty(find(tmc_blk==0,1));
            bMerge=true;
        else
%             cc=dot((tmc_blk(:)-tmc_mean),(tmblk(:)-tmblk_mean))/tmc_std/tmblk_std/tmN;
%             if cc>=ccThreshold
%                 bMerge=true;
%             end
            if abs(tmblk_mean-tmc_mean)<meangreyThres
                bMerge=true;
            end
        end
        if bMerge
            countn=ones(blkH,blkW);
            countn(invalidma)=0;
            ConnBlk_sumofblk{conni}=ConnBlk_sumofblk{conni}+tmblk;
            ConnBlk_Nofblk{conni}=ConnBlk_Nofblk{conni}+countn;
            tmc_blk_new=ConnBlk_sumofblk{conni}./ConnBlk_Nofblk{conni};
            ConnBlk_refblk{conni}=tmc_blk_new;
            ConnBlk_mean{conni}=nanmean(tmc_blk_new(:));
            ConnBlk_std{conni}=nanstd(tmc_blk_new(:));
        end
        
    end

% subfunction is to judge whether the connection area reaches the END condition
%   Condt 1. If std value of the average value exceeds the threshold
%   Condt 2. If blk is too large(x or y direction has N blocks, N > length_thres)
    function bConnEnd=ConnEnd_Condt(conni)
        bConnEnd=false;
        if ~isempty(ConnBlk_std{conni}) && ConnBlk_std{conni}>=std_thres
            bConnEnd=true;
        else
            tmind=find(ConnMarks==conni);
            if isempty(tmind)
                bConnEnd=false;
            else
                [tmy tmx]=ind2sub(size(ConnMarks),tmind);
                disty=max(tmy)-min(tmy);
                distx=max(tmx)-min(tmx);
                maxdist=max([disty distx]);
                if maxdist>=length_thres
                    bConnEnd=true;
                end
            end
            
        end
    end
end