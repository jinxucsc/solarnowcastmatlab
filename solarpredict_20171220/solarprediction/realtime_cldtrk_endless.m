% Main function to run cloud tracking
% VERSION 1.0 2014-05-19
function realtime_cldtrk_endless(configfpath)
if nargin==0
    configfpath='mtsi_startup';
end
%configfpath='mtsi_startup';
run(configfpath);
TO_setRealtimeModuleStatus(TO_getmodulename(),TO_str2modstate('normal'),datevec(now),ModuleStatusRecFile);
if bWriteDiary
    diary(CldtrkDiaryFile);
end

bUseParfor=true;  % switch to use parfor. default settings in realtime_cldtrk is FALSE. This configuration will use latest image to do cloud tracking.
if CldtrkMatPoolSize>1
    bUseParfor=true;
    if matlabpool('size')~=0
        matlabpool('close');
    end
    matlabpool(CldtrkMatPoolSize);
else
    bUseParfor=false;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Logger setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
logfile=CldtrkLogFile;
logger=log4m.getLogger(logfile);
loggerID='realtime_cldtrk';
logger.setLogLevel(logger.DEBUG);
logger.setCommandWindowLevel(logger.DEBUG);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%mtsipipe2_realtime_endless
% keeps monitoring the active folder and try to call mtsipipe2 to extract the layers and height
HRefDir=CldLayerRefDir;
HRefDir=FormatDirName(HRefDir);
set_dn=java.util.HashSet;
cldmodelmatf=SVMCloudModelFile;
latestdn=0;
DNDIFF = datenum([0 0 0 1 0 0]); % consider only 30 minutes ahead

try
    while(1)
        files3= dir(utsi3Dir);
        m=size(files3,1);
        utc_time = java.lang.System.currentTimeMillis;
        a=utc_time/1000; % conver to UNIX TIMESTAMP
        nowdn=(a/86400 + datenum(1970,1,1)); % UTC now datenum
        tmdns=[];
        startdn=nowdn-DNDIFF;
        %startdn=datenum([2014 04 09 17 0 0]);   % debug only
        for i = 1:m
            filename=files3(i).name;
            tmdv=GetDVFromImg(filename);
            if (0==tmdv) continue; end % not a regular file
            tmdn=datenum(tmdv);
            if(tmdn<startdn || set_dn.contains(tmdn))
                continue;  % if searched or out of considered range
            end
            tmdns=[tmdns tmdn];
        end
        tmdns=sort(tmdns,'descend'); % sort and get the lastest
        %tmdns=sort(tmdns,'ascend'); % DEBUG in-order
        % %For cloud tracking, current version is sequential not parallel processing here
        %parfor i = 1:length(tmdns)
        %    tmdn=tmdns(i);
        %    tmdv=datevec(tmdn);
        %    disp(['Cloud Tracking at ' datestr(tmdv) ' will be executed.']);
        %    bStatus=realtime_cldtrk(tmdv,HRefDir,cldmodelmatf,configfpath,logger);
        %end
        for i = 1:length(tmdns)
            tmdn=tmdns(i);
            tmdv=datevec(tmdn);
            set_dn.add(tmdn);
            %disp(['Cloud Tracking at ' datestr(tmdv) ' will be executed.']);
            bStatus=realtime_cldtrk(tmdv,HRefDir,cldmodelmatf,configfpath,logger);
            if bStatus>0
                break;  % Tracking completed
            end
        end
        
    end
catch err
    logger.error(loggerID,sprintf('%s--line %d  msg:%s',err.stack(1).name,err.stack(1).line,err.message));
    if bWriteDiary
        diary off;
    end
    TO_setRealtimeModuleStatus(TO_getmodulename(),TO_str2modstate('error'),datevec(now),ModuleStatusRecFile);
    return;
end

end