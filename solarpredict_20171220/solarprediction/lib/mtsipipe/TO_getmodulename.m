% TO_getmodulename is to get 1x6 char modulename from a realtime module. The name table is
% predefined here:
%
%    'realtime_preprocessingPipe_endless','prepro';
%    'realtime_cldtrk_endless','cldtrk';
%    'realtime_mtsipipe_TSI3fill_endless','tsifil';
%    'realtime_mtsipipe_GenDataset_endless','gendat';
%    'realtime_bpssec_qc_endless','radval'
%   
%   Current verion covers 5 realtime modules
% VERSION 1.0 2014-05-20
function modulename=TO_getmodulename()
modulenameTable={
    'realtime_preprocessingPipe_endless','prepro';
    'realtime_cldtrk_endless','cldtrk';
    'realtime_mtsipipe_TSI3fill_endless','tsifil';
    'realtime_mtsipipe_GenDataset_endless','gendat';
    'realtime_bpssec_qc_endless','radval'
};
[tasks,~]=dbstack;
assert(length(tasks)>=2,'This function must be used within realtime module. It is meaningless to call it directly.');

[M,N]=size(modulenameTable);
for i=1:M
    if strcmpi(tasks(2).name,modulenameTable{i,1})
        modulename=modulenameTable{i,2};
        return;
    end
end
warning(['Not called within a realtime module: ' tasks(2).name]);


end