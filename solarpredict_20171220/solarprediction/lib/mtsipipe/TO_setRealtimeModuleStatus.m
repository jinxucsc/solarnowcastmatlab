%   TO_setRealtimeModuleStatus is to set realtime module status within a status file
%   By default, each module is represented by modulename (a 6-characters string). The format is
%   like:
%       module(1x6char) time('yyyy-mm-dd HH:MM:SS) state(int)
%   eg:
%       prepro 0000-00-00 00:00:00 0
%   
%   VERSION 1.0 2014-05-20
%   Refer to TO_getmodulename for more information about 1x6 char modulename
%   Refer to TO_c2status for more information about state code
function TO_setRealtimeModuleStatus(modulename,mode,timedv,statusfile)
TFORMAT='yyyy-mm-dd HH:MM:SS';
if ~exist(statusfile,'file')
    initialStr= ... 
        ['prepro 0000-00-00 00:00:00 0' char(10) ...
        'cldtrk 0000-00-00 00:00:00 0' char(10) ...
        'tsifil 0000-00-00 00:00:00 0' char(10) ...
        'gendat 0000-00-00 00:00:00 0' char(10) ...
        'radval 0000-00-00 00:00:00 0' char(10)]
    fid = fopen(statusfile,'w');
    fprintf(fid,'%s',initialStr);
    fclose(fid);
end

fid = fopen(statusfile,'r');
C=textscan(fid,'%s %s %s %d');
fclose(fid);
names = cell2mat(C{1});
[N,~]=size(names);
dvs = datevec([cell2mat(C{2}) repmat(' ',N,1) cell2mat(C{3})]);
modes = C{4};
for i=1:N
    if strcmpi(names(i,:),modulename)
        break;
    end
end

assert(i~=N,['Unknown modulename: ' modulename]);

modes(i)=mode;
dvs(i,:)=timedv;
fid = fopen(statusfile,'w');
for i=1:N
    fprintf(fid,'%s %s %d\n',names(i,:),datestr(dvs(i,:),TFORMAT),modes(i));
end
fclose(fid);


end

