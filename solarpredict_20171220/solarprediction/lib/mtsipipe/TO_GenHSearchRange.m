function Hran=TO_GenHSearchRange(H)
tmH=round(double(H)/100)*100;
minH=500;
maxH=15000;
coverage_Hl=1600;
coverage_HH=4000;
HThres_low=5000;

if tmH<minH
    assert(false,'[ERROR]: Reference Height is smaller than minimum!');
elseif tmH>maxH
    assert(false,'[ERROR]: Reference Height is larger than maximum!');
elseif tmH<=minH+coverage_Hl/2
    tmHsran=500:100:500+coverage_Hl;
elseif tmH>minH+coverage_Hl/2 && tmH<=HThres_low
    tmHsran=tmH-coverage_Hl/2:100:tmH+coverage_Hl/2;
elseif tmH>maxH-coverage_HH/2
    tmHsran=maxH-coverage_HH:500:maxH;
elseif tmH>HThres_low && tmH<=maxH-coverage_HH/2
    tmHsran=tmH-coverage_HH/2:500:tmH+coverage_HH/2;
else
    assert(false,'[ERROR]: Not in the boundary!');
end




% if tmH<minH
%     disp('[ERROR]: Reference Height is smaller than minimum!');
% elseif tmH>=500 && tmH<minH+500
%     tmHsran=500:100:tmH+500;
% elseif tmH>minH+500 && tmH<3000
%     tmHsran=tmH-500:100:tmH+500;
% elseif tmH>=3000 && tmH<5000
%     tmHsran=tmH-1000:200:tmH+1000;
% elseif tmH>=5000
%     tmHsran=tmH-1000:200:tmH+1000;
% else
%     disp(tmH);
%     assert(0,'Unknown classification!');
% end

% if tmH<3000 && tmH>=1500
%     tmHsran=tmH-500:100:tmH+500;
% elseif tmH>=3000 && tmH<5000
%     tmHsran=tmH-1000:200:tmH+1000;
% elseif tmH>=5000
%     tmHsran=tmH-1000:200:tmH+1000;
% elseif tmH<1500 && tmH>=500
%     tmHsran=500:100:tmH+500;
% elseif tmH<500
%     disp('[ERROR]: Reference Height is smaller than 1000!');
% end
Hran=tmHsran;


end