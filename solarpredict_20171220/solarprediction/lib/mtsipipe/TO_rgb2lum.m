function imma_l=TO_rgb2lum(imma)
imma_l=round(0.2126*imma(:,:,1)+0.7152*imma(:,:,2)+0.0722*imma(:,:,3));
end