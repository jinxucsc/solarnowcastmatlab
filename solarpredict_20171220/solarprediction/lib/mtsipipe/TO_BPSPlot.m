function TO_BPSPlot(plottmdv,H,imagedv,FilledTSI3Dir)
filename3=GetImgFromDV_BNL(imagedv,'tsi3');
f3=[filename3 '_shifttest.jpg'];
FilledTSI3Dir=FormatDirName(FilledTSI3Dir);
inputf3 = sprintf('%s%s',FilledTSI3Dir,f3);
if(~exist(inputf3,'file'))
    disp(['[WARNING]: There is no such file like: ' inputf3]);
    return;
end
[x,y]=GetBPSPts(plottmdv,'tsi3',H);           % SP(t) information
imma3=imread(inputf3);
figure();
imshow(imma3);
hold on;
plot(x,y,'xr');
hold off;
end