%   TO_str2modstate is to parse mode string into a integer of state code or parse state code to mode
%   string
%   This function is predefined. Current version supports:
%       1   --- normal
%       -1  --- error
%       0 --- NAN
%   VERSION 1.0 2014-05-20
function mode = TO_str2modstate(modObj)
if isscalar(modObj)
    switch (modObj)
        case 1
            mode = 'normal';
        case -1
            mode = 'error';
        case 0
            mode = 'nan';
    end
else
    if strcmpi(modObj,'normal')
        mode = 1;
    elseif strcmpi(modObj,'error')
        mode = -1;
    else
        mode = 0;
    end
end
end