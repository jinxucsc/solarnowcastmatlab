%   Multiple TSI startup
%   Version 1.0     2013-07-29
%   Version 1.1     2013-08-26
%       Generate mtsi_startup_mini.m
%   Version 2.0     2014-05-14

%% TSI Runtime parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  start of mtsi_startup_mini.m %%%%%%%%%%%%%%%%%%
%mtsi_startup_mini;
%   original image and undist image related parameters
centrep.x=320;
centrep.y=240;
ORIH=480;
ORIW=640;
UNDISTH=500;
UNDISTW=500;
UNDISTR=240;
ViewAngle=120;

% ViewAngle=140;
ViewAngle_r=ViewAngle/180*pi;
VA=ViewAngle;
VA_r=ViewAngle_r;

%label of site
TSI1='tsi1';
TSI2='tsi2';
TSI3='tsi3';


% First TSI:
% 	40°51’57.72”N
% 	72°53’04.71”W
% 	Elevation is about 65 feet above sea level.
location1.longitude = -72.885;
location1.latitude = 40.866;
location1.altitude = 20;        %metre
r1=9;
h1=15.6811;
AzumithShift1=0;
ZenithShift1=0;
rcentrep1.x=305;
rcentrep1.y=235;
% rcentrep1.x=310;
% rcentrep1.y=240;


% Second TSI:
% 	40deg 51'54.62''N
% 	72deg 51' 04.58''W
% 	Elevation 50ft
location2.longitude = -72.8513;
location2.latitude = 40.8652;
location2.altitude = 15.24;        %metre
r2=9;
h2=15.6811;
AzumithShift2=0;
ZenithShift2=0;
rcentrep2.x=318;
rcentrep2.y=246;


% Third TSI:
%   40°51’28.81”N, 72°51’27.18”W
% 	Elevation is about 65 feet above sea level.
location3.longitude = -72.8575;
location3.latitude = 40.8580;
location3.altitude = 20;        %metre
r3=9;
h3=15.6811;
AzumithShift3=0;
ZenithShift3=0;
% rcentrep1.x=305;
% rcentrep1.y=235;
rcentrep3.x=307;
rcentrep3.y=242;


%   Camera parameters
cw1=3.58;             %ccdwidth=3.6mm
ch1=2.69;           %ccdheight=4.576mm
crx1=640;            %ccd resolution X=288
cry1=480;            %ccd resolution Y=352
fo1=4;             %focal length 5.5mm
cw2=3.58;             %ccdwidth=3.6mm
ch2=2.69;           %ccdheight=4.576mm
crx2=640;            %ccd resolution X=288
cry2=480;            %ccd resolution Y=352
fo2=4;             %focal length 5.5mm
cw3=3.58;             %ccdwidth=3.6mm
ch3=2.69;           %ccdheight=4.576mm
crx3=640;            %ccd resolution X=288
cry3=480;            %ccd resolution Y=352
fo3=4;             %focal length 5.5mm

% 25 Solar Panels related
BPS_N=25;       % Total Number of solar panels

% HA MASK files for TSI (Preprocessed with Photoshop)
bmpmaskorif1='bnltsiskyimageC1.a1.20120727.165525.jpg.20120727165525.bmp';
bmpmaskorif2='bnltsiskyimageC2.a1.20130510.165300.jpg.20130510165300.bmp';
bmpmaskorif3='bnltsiskyimageC3.a1.20130510.165300.jpg.20130510165300.bmp';

% SVM classifier for cloud detection
SVMCloudModelFile='./model_rbr.mat';

% BMP file containing default sky colors
TSIDefaultSkyColorFile='./skyfill.bmp';


%   Generate ori2undist matrix if not exist
tsi1undistmat='tsi1_ori2undist.mat';
tsi2undistmat='tsi2_ori2undist.mat';
tsi3undistmat='tsi3_ori2undist.mat';

%tsiDistance_12=2836.6;   % metre
%tsiDistance=2836.6;   % metre

% reg expression for TSI image
TSIFileExpression='bnltsiskyimageC[123].a[1].\d{8}.\d{6}.jpg.\d{14}.jpg$';
TSIBMPFileExpression='bnltsiskyimageC[123].a[1].\d{8}.\d{6}.jpg.\d{14}.bmp$';
TSIMATExpression='bnltsiskyimageC[123].a[1].\d{8}.\d{6}.jpg.\d{14}.mat$';

% PERMERNANT VARIABLES
TENSECS_DN=datenum([0 0 0 0 0 10]);
ONEDAY_DN=datenum([0 0 1 0 0 0]);
ONEMIN_DN=datenum([0 0 0 0 1 0]);

% Time Format of date string
DATETIMEFORMAT='yyyy-mm-dd HH:MM:SS';

% TSI SB and HA Mask root directory
%MASKDATAROOTDIR='./MASKROOT';
%MASKDATAROOTDIR='/home/mikegrup/RecentMatProjects/TSIPipeline/BNLPipeline/preprocessing_TSIs/';
%------------------------------------------------------------------------%
%   Multicore related
%------------------------------------------------------------------------%
%ServerType = 64;        % 64bit/32bit matlab
%MaxNumofMatlabInst=26;  % Maximum number of matlab instances running at the same time. Like matlabpool
PreproMatPoolSize = 1;         % matlabpool(size) for preprocessing. 1 means no parallel processing
CldtrkMatPoolSize = 12;        % matlabpool(size) for cloud tracking. 1 means no parallel processing
TSIStiMatPoolSize = 1;         % matlabpool(size) for cloud tracking. 1 means no parallel processing
ForecastMatPoolSize = 1;       % matlabpool(size) for cloud tracking. 1 means no parallel processing


%------------------------------------------------------------------------%
%   Data Collection configuration
%------------------------------------------------------------------------%
RealtimePipeDataDir='./data/';                  % Root directory of all the data
otsi1Dir=[RealtimePipeDataDir 'oTSI1/'];
otsi2Dir=[RealtimePipeDataDir 'oTSI2/'];
otsi3Dir=[RealtimePipeDataDir 'oTSI3/'];
utsi1Dir=[RealtimePipeDataDir 'tsi1_undist_mat/'];
utsi2Dir=[RealtimePipeDataDir 'tsi2_undist_mat/'];
utsi3Dir=[RealtimePipeDataDir 'tsi3_undist_mat/'];
cldtsi1Dir=[RealtimePipeDataDir 'tsi1_cld_mat/'];
cldtsi2Dir=[RealtimePipeDataDir 'tsi2_cld_mat/'];
cldtsi3Dir=[RealtimePipeDataDir 'tsi3_cld_mat/'];
CldLayerRefDir=[RealtimePipeDataDir 'href/'];
CldLayerRefSimDir=[RealtimePipeDataDir 'href_simple/'];
SBHAMASKROOTDIR = RealtimePipeDataDir;      % SB/HA root dir. Under this directory, sbmaskDir = './data/sbmask', hamaskDir = './data/hamask'
logDir='./log/';                            % Log file root directory
TSI3FilledDir='./newfilled/';               % Containing all filled images
LocalCorrectionDir = [RealtimePipeDataDir 'localcorrection/'];      % Directory containing local correction mat files 
SVRModelDir='./models/';                    % Support Vector Regression Models files

% Logging Configuration & Diary Configuration
PreproLogFile=[logDir 'realtime_preprocessing.log'];    % preprocessing log file
CldtrkLogFile=[logDir 'realtime_cldtrk.log'];           % cloud tracking log file
TSI3FillLogFile=[logDir 'realtime_stich.log'];
GendataLogFile=[logDir 'realtime_gendata.log'];
QualityControlLogFile=[logDir 'realtime_qc.log'];

bWriteDiary=true;       %   if true, the pipelien will output command window info to diary file. This is important when run mtsipipe in daemon mode
PreproDiaryFile=[logDir 'realtime_preprocessing.diary'];
CldtrkDiaryFile=[logDir 'realtime_cldtrk.diary'];           
TSI3FillDiaryFile=[logDir 'realtime_stich.diary'];
GendataDiaryFile=[logDir 'realtime_gendata.diary'];
QualityControlDiaryFile=[logDir 'realtime_qc.diary'];

ModuleStatusRecFile = [logDir 'modulestatus.dat'];  % keep the status of realtime modules

%------------------------------------------------------------------------%
%   Preprocessing Presettings
%------------------------------------------------------------------------%
SBMASK_VALIDMASK = 'sbmask_pixelrange.bmp'; % SB Mask Valid search range(white is valid). This is preprocessed using Photoshop.
MASKTIMESPAN = 60;  % Seconds between two consecutive images to do SB/HA masking

% BuildClearSkyLib_wBMPMask.m
SDIST_START2END=200;        %   Generated search array length, which is
SunSpotRange=20;            %   Sun range around sunpositon which will not
FillRadius=230;              %   Fill the Sun range(SPX-ran:SPX+ran)
UpperLine_DistMovement=43;       %  Upperline is to control the upper end of
%  the shadow band
%   generate any edge detection
%   distributed along the azumith line.
BoundSearchRange=   35;    % from x-30 ->   x+30Edge
BoundaryThres=      0;    % drop range exceeds 30 then treat as boundary
BrightThres=        150;   % if lines points larger than threshold no boundary searche
SearchPixelLen=   180;     % The search pixel number along the mid line
LineShift=  4;      %   Edge move N pixels out to get tolerant line
AngleDist=5/180*pi;        %   Upper bound and lower bound should have less than this threshold angle dfference to middline
TwoLineDist= 40;    %    minimum dist between two edge lines.





%------------------------------------------------------------------------%
%  Cloud Tracking configuration (realtime_cldtrk.m)
%------------------------------------------------------------------------%
NCLDTRKFRAMS=3;     % N of frames that cloud tracking is used. e.g. value = 3 means cloud tracking is extracted from consecutive 3 frames: 14:00:00,14:00:10,14:00:20
H_sran_low=500:100:5000;                   % low altitude search range
H_sran_high=5500:500:15000;                 %  high altitude search range
H_sran_simple=[2000:500:5000 5500:1000:20000];
H_sran=[H_sran_low H_sran_high];
MAXNUMOFLAYER=2;            % maximum layers for extraction
SingleLayerHDiffThreshold = 1500;   % If two centroid is within this range, treat it as single layer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Normalized Cross Correlation(NCC) Related Configuration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ncc_thres= 0.6;      % the NCC threshold of similarity check. Smaller than this value will be treated as 0 (no similarity)
NEIGHBOR_NTHRES=2;  % Number of neighbor CF that are valid for cloud tracking
INVALIDPIXELCOUNT = 0.3; % 0~1 Threshold of invalid pixels number within a cloud field
MotionVectorMaxDistFromRef = 5; % The maximum euclidian distance from reference motion vectors in searching
MaxNofDetectionMotionVector = 20;   % Maximum number of motion vectors to do ncc check. e.g. value = 20 calculate ncc values next for height for at most 20 motion vectors(top 20 of similarity score)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module 1: Simple Cloud Tracking Module Configuration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SimpleCldTrkNCCThres=0;                   % Normalized cross correlation threshold of using simple cloud tracking method.
                                            % If the ncc score of simple tracking is larger than
                                            % this threshold. The cloud tracking will not go further.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module 2: Cloud Detection Module Configuration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
StdCldThres=5;                          % Std deviation compenstaion threshold
StdBlockSize=11;                        % Std calculation block size 11x11  
StdBlockHalfSize=floor(StdBlockSize/2);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module 3:  Cloud Field Extraction Module Configuration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ConnMinSize=10;     % CF Minimum size, if smaller than this value just treat as noise
ConnMaxSize=80;     % CF max size, larger than that will be divided
MaxConnNum=30;      % Maximum CF considered in this pipeline


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module 4:  History Layers Extraction Configuration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NHISTFRAMES=5;     % pick last n frames as history layers reference
DEFAULTHREF=[1100 3700];
NOFVALIDHIST=floor(NHISTFRAMES/2);     % if more than half hist frames have valid cloud tracking results then use them as history reference


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module 5: Cloud Track Module Configuration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NextFrameMVSearchWinSize=10;                % MV search range on next frame
CLDFRACTHRES=0.3;                           % Cloud Fraction difference threshold for nccsim usage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module 6: Cloud Clustering Module Configuration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%------------------------------------------------------------------------%
%  TSI3fill Configuration
%------------------------------------------------------------------------%
OvercastCloudFractionThreshold=0.8;      % Cloud fraction threshold to define overcast case
ClearSkyCloudFractionThreshold=0.05;      % Cloud fraction threshold to define clear sky case
% GetLayersInfo_realtime Configuration
HistoryLayerTimeThres = datenum([0 0 0 0 20 0]);   % Use last 20 minutes cloud tracking results to extract current layer information
DefaultHeightReference = 3000;      % If cloud tracking failed within time threshold choose default H as reference for filling
DefaultMVReference = [0,0];         % Motion Vectors [MVx MVy] as default value


%------------------------------------------------------------------------%
%  GenDataset Configuration (forecast used)
%------------------------------------------------------------------------%
ForecastTimeRange = 60:60:600;      % Forecast time range form 60s to 600s, the interval is 60s
SVRModelFile = [SVRModelDir '20130530.000000-20130531.000000_models_no19.mat'];
BPSClearSkyMatFile='./bpscs.mat';         % Defined clear sky values of GHI. Generated in previous clear sky fitting.
                                          % Usage: csvals_oneday = TO_getBPSCSVal(daydv,BPSClearSkyMatFile,'sec');
TSIFeatureWindowSize = 7;   % Extract features within a 7x7 window
                                          
%------------------------------------------------------------------------%
%  BPS QualityControl Configuration
%------------------------------------------------------------------------%
BPSRadiationQualityControlFile = './bpsvalid.dat';   % This file indicates quality control status of all BPS stations
QualityControlInterval = 300;                % Run qc main program every 300s 
QCRADFILE='./data/BPSSEC.dat';              % Generated by collection program. recent radiation records from database


%------------------------------------------------------------------------%
%  Remote MySQL Access configuration. This Configuration will affect getting realtime data and
%  publishing realtime forecast
%------------------------------------------------------------------------%
BPSRealtimeTable='BPS_SEC_tmpfs';              % Keep the latest prediction results
BPSPredictionTable='BPS_Prediction_tmpfs';              % Keep the latest prediction results
BPSPredictionTable_full='BPS_Prediction_full_tmpfs';    % DEBUG USE: Full table of all predicted results
BPSPredictionTable_lc='BPS_Prediction_lc_tmpfs';                    % Prediction table but with local correction
replacecmmd='REPLACE INTO %s values (%d,%s,%6.2f);';    % String format of REPLACE
insertcmmd='INSERT INTO %s values(%d,%s,%s,%6.2f,%6.2f);'; % String format of INSERT. This is to save all possible prediction
MySQLServerURL='solar.bnl.gov';     % Server name or URL                            
username='realtime_matlab';         % Username to access MySQL database (Please make sure that this user have proper privileges to do operations)
password='realtime_matlab';         % Password to access MySQL database
DBName='SolarFarm';                 % Database name to get/put data
MySQLBinPath='';                    % MySQL binary path. empty means that MySQL is already in the system path.
MySQLExeTempOutputFile='./data/insertprediction.log';  % Temp output file for debuging


%------------------------------------------------------------------------%
%  SITE DEFINITION
%------------------------------------------------------------------------%
%   Current setting is: 3 sites
SITES={TSI1;TSI2;TSI3};
locations=[location1;location2;location3];
AzumithShifts=[AzumithShift1;AzumithShift2;AzumithShift3];
ZenithShifts=[ZenithShift1;ZenithShift2;ZenithShift3];
rcentreps=[rcentrep1;rcentrep2;rcentrep3];
cws=[cw1;cw2;cw3];  
chs=[ch1;ch2;ch3];
crxs=[crx1;crx2;crx3];
crys=[cry1;cry2;cry3];
fos=[fo1;fo2;fo3];
hs=[h1;h2;h3];
rs=[r1;r2;r3];
BMPMASKORIFS={bmpmaskorif1;bmpmaskorif2;bmpmaskorif3};
UNDISTMATS={tsi1undistmat,tsi2undistmat,tsi3undistmat};
otsiDirs={otsi1Dir;otsi2Dir;otsi3Dir};
utsiDirs={utsi1Dir;utsi2Dir;utsi3Dir};



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  end of mtsi_startup_runtime.m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% UNDIST Matrix Generation
if ~exist(tsi1undistmat,'file')
    ORI_WIDTH=ORIW;
    ORI_HEIGHT=ORIH;
    rcentrep=rcentrep1;
    AzumithShift=AzumithShift1;
    ZenithShift=ZenithShift1;
    cw=cw1;             %ccdwidth=3.6mm
    ch=ch1;           %ccdheight=4.576mm
    crx=crx1;            %ccd resolution X=288
    cry=cry1;            %ccd resolution Y=352
    fo=fo1;             %focal length 5.5mm
    h=h1;
    r=r1;
    Hu=UNDISTH;
    Wu=UNDISTW;
    Ru=UNDISTR;
    u2oma=zeros(Hu,Wu,2);    %[Xor,Yor] = [u2oma(i,j,1) u2oma(i,j,2)]
    o2uma=zeros(ORIH,ORIW,2);    %[Xu , Yu] = [o2uma(i,j,1) o2uma(i,j,2)]
    u2rma=zeros(Hu,Wu,2);    %[ry rx]=H*[u2rma(i,j,1) u2rma(i,j,2)]
%     r2uma=zeros(Hu,Wu,2);    %[uy ux]=[r2uma(i,j,1) r2uma(i,j,2)]./H
    undistcen.x=Wu/2;
    undistcen.y=Hu/2;
    CBH=5000;
    DistTotal=CBH*tan(VA_r/2);
    for Xu=1:Wu
        for Yu=1:Hu
            %   1. From undistorted image to world relative coor
            %   (Xu,Yu) -> (Xwr,Ywr,Zwr or CBH) ->(Zenith, Azumith)
            %         if(Xu==251&&Yu==200)
            %             Xu
            %         end
            imagey=undistcen.y-Yu;
            imagex=Xu-undistcen.x;
            DistCen=sqrt(imagey^2+imagex^2);
            if(DistCen>=Ru)
                continue;
            end
            reald=DistCen/Ru*DistTotal;
            tz=reald/CBH;
            Zenith=atan(tz);
            
            %find azumith
            if(imagex==0&&imagey>0)
                Azumith=0;
            elseif imagex==0&&imagey<0
                Azumith=pi;
            elseif imagex==0&&imagey==0
                Zenith=-3*pi;
                Azumith=-3*pi;
            elseif imagey==0&&imagex>0
                Azumith=pi/2;
            elseif imagey==0&&imagex<0
                Azumith=pi*3/2;
            end
            
            if(imagex>0&&imagey>0)
                Azumith=atan(imagex/imagey);
            end
            if(imagex<0&&imagey>0)
                Azumith=2*pi+atan(imagex/imagey);
            end
            if(imagex<0&&imagey<0)
                Azumith=pi+atan(imagex/imagey);
            end
            if(imagex>0&&imagey<0)
                Azumith=pi+atan(imagex/imagey);
            end
            
            %   2. (Zenith,Azumith) -> (Xor,Yor)
            if(Azumith==-3*pi&&Zenith==-3*pi)
                %symbol of image centre
                Xor=rcentrep.x;
                Yor=rcentrep.y;
            else
                [Xor,Yor]=CalImageCoorFrom_paras(...
                    Azumith,Zenith,...
                    AzumithShift,ZenithShift,...
                    h,r,...
                    cw,ch,crx,cry,fo,...
                    rcentrep...
                );
            end
            Yor=round(Yor);
            Xor=round(Xor);
            if Xor > ORIW || Xor <1 || Yor>ORIH||Yor<1
                continue;
            end
            u2oma(Yu,Xu,1)=Yor;
            u2oma(Yu,Xu,2)=Xor;
            u2rma(Yu,Xu,1)=tan(VA_r/2)/UNDISTR*sqrt(Yu^2+Xu^2)*cos(Azumith);
            u2rma(Yu,Xu,2)=tan(VA_r/2)/UNDISTR*sqrt(Yu^2+Xu^2)*sin(Azumith);
            o2uma(Yor,Xor,1)=Yu;
            o2uma(Yor,Xor,2)=Xu;
            
        end
    end
    % 3. Save the original matrix mapping orim
    save(tsi1undistmat,'u2oma','u2rma','ViewAngle','Ru','h','r','ORI_WIDTH','ORI_HEIGHT','Wu','Hu','CBH');
end

if ~exist(tsi2undistmat,'file')
    ORI_WIDTH=ORIW;
    ORI_HEIGHT=ORIH;
    rcentrep=rcentrep2;
    AzumithShift=AzumithShift2;
    ZenithShift=ZenithShift2;
    cw=cw2;             %ccdwidth=3.6mm
    ch=ch2;           %ccdheight=4.576mm
    crx=crx2;            %ccd resolution X=288
    cry=cry2;            %ccd resolution Y=352
    fo=fo2;             %focal length 5.5mm
    h=h2;
    r=r2;
    Hu=UNDISTH;
    Wu=UNDISTW;
    Ru=UNDISTR;
    u2oma=zeros(Hu,Wu,2);    %[Xor,Yor] = [u2oma(i,j,1) u2oma(i,j,2)]
    o2uma=zeros(ORIH,ORIW,2);    %[Xu , Yu] = [o2uma(i,j,1) o2uma(i,j,2)]
    u2rma=zeros(Hu,Wu,2);    %[ry rx]=H*[u2rma(i,j,1) u2rma(i,j,2)]
    undistcen.x=Wu/2;
    undistcen.y=Hu/2;
    CBH=5000;
    DistTotal=CBH*tan(VA_r/2);
    for Xu=1:Wu
        for Yu=1:Hu
            %   1. From undistorted image to world relative coor
            %   (Xu,Yu) -> (Xwr,Ywr,Zwr or CBH) ->(Zenith, Azumith)
            %         if(Xu==251&&Yu==200)
            %             Xu
            %         end
            imagey=undistcen.y-Yu;
            imagex=Xu-undistcen.x;
            DistCen=sqrt(imagey^2+imagex^2);
            if(DistCen>=Ru)
                continue;
            end
            reald=DistCen/Ru*DistTotal;
            tz=reald/CBH;
            Zenith=atan(tz);
            
            %find azumith
            if(imagex==0&&imagey>0)
                Azumith=0;
            elseif imagex==0&&imagey<0
                Azumith=pi;
            elseif imagex==0&&imagey==0
                Zenith=-3*pi;
                Azumith=-3*pi;
            elseif imagey==0&&imagex>0
                Azumith=pi/2;
            elseif imagey==0&&imagex<0
                Azumith=pi*3/2;
            end
            
            if(imagex>0&&imagey>0)
                Azumith=atan(imagex/imagey);
            end
            if(imagex<0&&imagey>0)
                Azumith=2*pi+atan(imagex/imagey);
            end
            if(imagex<0&&imagey<0)
                Azumith=pi+atan(imagex/imagey);
            end
            if(imagex>0&&imagey<0)
                Azumith=pi+atan(imagex/imagey);
            end
            
            %   2. (Zenith,Azumith) -> (Xor,Yor)
            if(Azumith==-3*pi&&Zenith==-3*pi)
                %symbol of image centre
                Xor=rcentrep.x;
                Yor=rcentrep.y;
            else
                [Xor,Yor]=CalImageCoorFrom_paras(...
                    Azumith,Zenith,...
                    AzumithShift,ZenithShift,...
                    h,r,...
                    cw,ch,crx,cry,fo,...
                    rcentrep...
                );
            end
            Yor=round(Yor);
            Xor=round(Xor);
            if Xor > ORIW || Xor <1 || Yor>ORIH||Yor<1
                continue;
            end
            u2oma(Yu,Xu,1)=Yor;
            u2oma(Yu,Xu,2)=Xor;
            u2rma(Yu,Xu,1)=tan(VA_r/2)/UNDISTR*sqrt(Yu^2+Xu^2)*cos(Azumith);
            u2rma(Yu,Xu,2)=tan(VA_r/2)/UNDISTR*sqrt(Yu^2+Xu^2)*sin(Azumith);
            o2uma(Yor,Xor,1)=Yu;
            o2uma(Yor,Xor,2)=Xu;
        end
    end
    % 3. Save the original matrix mapping orim
    save(tsi2undistmat,'u2oma','u2rma','ViewAngle','Ru','h','r','ORI_WIDTH','ORI_HEIGHT','Wu','Hu','CBH');
end

if ~exist(tsi3undistmat,'file')
    ORI_WIDTH=ORIW;
    ORI_HEIGHT=ORIH;
    rcentrep=rcentrep3;
    AzumithShift=AzumithShift3;
    ZenithShift=ZenithShift3;
    cw=cw3;             %ccdwidth=3.6mm
    ch=ch3;           %ccdheight=4.576mm
    crx=crx3;            %ccd resolution X=288
    cry=cry3;            %ccd resolution Y=352
    fo=fo3;             %focal length 5.5mm
    h=h3;
    r=r3;
    Hu=UNDISTH;
    Wu=UNDISTW;
    Ru=UNDISTR;
    u2oma=zeros(Hu,Wu,2);    %[Xor,Yor] = [u2oma(i,j,1) u2oma(i,j,2)]
    o2uma=zeros(ORIH,ORIW,2);    %[Xu , Yu] = [o2uma(i,j,1) o2uma(i,j,2)]
    u2rma=zeros(Hu,Wu,2);    %[ry rx]=H*[u2rma(i,j,1) u2rma(i,j,2)]
%     r2uma=zeros(Hu,Wu,2);    %[uy ux]=[r2uma(i,j,1) r2uma(i,j,2)]./H
    undistcen.x=Wu/2;
    undistcen.y=Hu/2;
    CBH=5000;
    DistTotal=CBH*tan(VA_r/2);
    for Xu=1:Wu
        for Yu=1:Hu
            %   1. From undistorted image to world relative coor
            %   (Xu,Yu) -> (Xwr,Ywr,Zwr or CBH) ->(Zenith, Azumith)
            %         if(Xu==251&&Yu==200)
            %             Xu
            %         end
            imagey=undistcen.y-Yu;
            imagex=Xu-undistcen.x;
            DistCen=sqrt(imagey^2+imagex^2);
            if(DistCen>=Ru)
                continue;
            end
            reald=DistCen/Ru*DistTotal;
            tz=reald/CBH;
            Zenith=atan(tz);
            
            %find azumith
            if(imagex==0&&imagey>0)
                Azumith=0;
            elseif imagex==0&&imagey<0
                Azumith=pi;
            elseif imagex==0&&imagey==0
                Zenith=-3*pi;
                Azumith=-3*pi;
            elseif imagey==0&&imagex>0
                Azumith=pi/2;
            elseif imagey==0&&imagex<0
                Azumith=pi*3/2;
            end
            
            if(imagex>0&&imagey>0)
                Azumith=atan(imagex/imagey);
            end
            if(imagex<0&&imagey>0)
                Azumith=2*pi+atan(imagex/imagey);
            end
            if(imagex<0&&imagey<0)
                Azumith=pi+atan(imagex/imagey);
            end
            if(imagex>0&&imagey<0)
                Azumith=pi+atan(imagex/imagey);
            end
            
            %   2. (Zenith,Azumith) -> (Xor,Yor)
            if(Azumith==-3*pi&&Zenith==-3*pi)
                %symbol of image centre
                Xor=rcentrep.x;
                Yor=rcentrep.y;
            else
                [Xor,Yor]=CalImageCoorFrom_paras(...
                    Azumith,Zenith,...
                    AzumithShift,ZenithShift,...
                    h,r,...
                    cw,ch,crx,cry,fo,...
                    rcentrep...
                );
            end
            Yor=round(Yor);
            Xor=round(Xor);
            if Xor > ORIW || Xor <1 || Yor>ORIH||Yor<1
                continue;
            end
            u2oma(Yu,Xu,1)=Yor;
            u2oma(Yu,Xu,2)=Xor;
            u2rma(Yu,Xu,1)=tan(VA_r/2)/UNDISTR*sqrt(Yu^2+Xu^2)*cos(Azumith);
            u2rma(Yu,Xu,2)=tan(VA_r/2)/UNDISTR*sqrt(Yu^2+Xu^2)*sin(Azumith);
            o2uma(Yor,Xor,1)=Yu;
            o2uma(Yor,Xor,2)=Xu;
            
        end
    end
    % 3. Save the original matrix mapping orim
    save(tsi3undistmat,'u2oma','u2rma','ViewAngle','Ru','h','r','ORI_WIDTH','ORI_HEIGHT','Wu','Hu','CBH');
end




%% Hight to UNDIST matrix generation
% Height related
HeightMatrixMatf='hma.mat';
HeightMatrixValidMatf='hma_valid.mat';
if ~exist(HeightMatrixValidMatf,'file')
    if ~exist(HeightMatrixMatf,'file')
        disp(['height mat file ' HeightMatrixMatf ' does not exist. function will be called']);
        TO_GenHMVMatrix(HeightMatrixMatf);
    end
    H_range_full=500:100:20000;
    tml=length(H_range_full);
    H_valid_31=zeros(tml,1);
    H_valid_13=zeros(tml,1);
    H_valid_12=zeros(tml,1);
    H_valid_21=zeros(tml,1);
    H_valid_32=zeros(tml,1);
    H_valid_23=zeros(tml,1);
    
    HMVx_valid_31=zeros(tml,1);
    HMVx_valid_13=zeros(tml,1);
    HMVx_valid_12=zeros(tml,1);
    HMVx_valid_21=zeros(tml,1);
    HMVx_valid_32=zeros(tml,1);
    HMVx_valid_23=zeros(tml,1);
    HMVy_valid_31=zeros(tml,1);
    HMVy_valid_13=zeros(tml,1);
    HMVy_valid_12=zeros(tml,1);
    HMVy_valid_21=zeros(tml,1);
    HMVy_valid_32=zeros(tml,1);
    HMVy_valid_23=zeros(tml,1);
    
    t=load(HeightMatrixMatf);
    Hma=t.Hma;
    hma31=Hma.hma31;
    hma13=Hma.hma13;
    hma21=Hma.hma21;
    hma12=Hma.hma12;
    hma32=Hma.hma32;
    hma23=Hma.hma23;

    H_start=min(H_range_full);
    H_end=max(H_range_full);
    H_l=length(H_range_full);
    
    hma12_invalidma=(hma12<H_start|hma12>=H_end|hma12==inf);
    hma21_invalidma=(hma21<H_start|hma21>=H_end|hma21==inf);
    hma13_invalidma=(hma13<H_start|hma13>=H_end|hma13==inf);
    hma31_invalidma=(hma31<H_start|hma31>=H_end|hma31==inf);
    hma32_invalidma=(hma32<H_start|hma32>=H_end|hma32==inf);
    hma23_invalidma=(hma23<H_start|hma23>=H_end|hma23==inf);
    hma12_valid=hma12;
    hma21_valid=hma21;
    hma13_valid=hma13;
    hma31_valid=hma31;
    hma23_valid=hma23;
    hma32_valid=hma32;
    hma12_valid(hma12_invalidma)=0;
    hma21_valid(hma21_invalidma)=0;
    hma13_valid(hma13_invalidma)=0;
    hma31_valid(hma31_invalidma)=0;
    hma23_valid(hma23_invalidma)=0;
    hma32_valid(hma32_invalidma)=0;

    
    for i=1:tml
        H=H_range_full(i);
        [HMVx_valid_12(i),HMVy_valid_12(i),H_valid_12(i)]=TO_MTSI_GetValidH(hma12_valid,H);
        [HMVx_valid_21(i),HMVy_valid_21(i),H_valid_21(i)]=TO_MTSI_GetValidH(hma21_valid,H);
        [HMVx_valid_13(i),HMVy_valid_13(i),H_valid_13(i)]=TO_MTSI_GetValidH(hma13_valid,H);
        [HMVx_valid_31(i),HMVy_valid_31(i),H_valid_31(i)]=TO_MTSI_GetValidH(hma31_valid,H);
        [HMVx_valid_23(i),HMVy_valid_23(i),H_valid_23(i)]=TO_MTSI_GetValidH(hma23_valid,H);
        [HMVx_valid_32(i),HMVy_valid_32(i),H_valid_32(i)]=TO_MTSI_GetValidH(hma32_valid,H);
        
    end
    
    save(HeightMatrixValidMatf,'HMVx_valid_12','HMVx_valid_21','HMVx_valid_13','HMVx_valid_31','HMVx_valid_23','HMVx_valid_32'...
        ,'HMVy_valid_12','HMVy_valid_21','HMVy_valid_13','HMVy_valid_31','HMVy_valid_23','HMVy_valid_32'...
        ,'H_valid_12','H_valid_21','H_valid_13','H_valid_31','H_valid_23','H_valid_32','H_range_full','Hma'...
        );
end


%% Undist to Azimuth Zenith 
% Generate the UNDIST image undist coordinate -> (az,ze) matrix
u2azmatf='u2az.mat';
if ~exist(u2azmatf,'file')
    u2az=zeros(UNDISTH,UNDISTW);
    u2ze=zeros(UNDISTH,UNDISTW);
    for uy=1:UNDISTH
        for ux=1:UNDISTW
            %[u2az(uy,ux),u2ze(uy,ux)]=TO_UNDISTCoor2AZ(ux,uy);
            undistx=ux;undisty=uy;
            undistcen_x=UNDISTW/2;
            undistcen_y=UNDISTH/2;
            rx=undistx-undistcen_x;
            ry=undistcen_y-undisty;
            
            r=sqrt((rx^2+ry^2));
            tan_va=tan(ViewAngle_r/2);
            % r/UNDISTR=tan(ze)/tan(va)
            tan_ze=r/UNDISTR*tan_va;
            zenith=atan(tan_ze);
            
            if(rx==0)
                if(ry>0)
                    azimuth=0;
                elseif(ry==0)
                    azimuth=-9999;
                else
                    azimuth=pi;
                end
            elseif(ry==0)
                if(rx>0)
                    azimuth=pi/2;
                else%rx<0
                    azimuth=3*pi/2;
                end
            else
                if(rx>0&&ry>0)
                    azimuth =atan(rx/ry);
                end
                if(rx>0&&ry<0)
                    azimuth =pi+atan(rx/ry);
                end
                if(rx<0&&ry<0)
                    azimuth =pi+atan(rx/ry);
                end
                if(rx<0&&ry>0)
                    azimuth =2*pi+atan(rx/ry);
                end
            end
            
            
            u2az(uy,ux)=azimuth;
            u2ze(uy,ux)=zenith;
        end
    end
    
    save(u2azmatf,'u2az','u2ze');
end


%% Others




%clear AzumithShift,ZenithShift,rcentrep,cw,ch,crx,cry,fo,h,r,Hu,Wu,Ru;

