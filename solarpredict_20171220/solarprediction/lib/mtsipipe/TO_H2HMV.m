% TO_H2HMV
%   This function is to generate HMV based on given H. HMV means the displacement from TSI to
%   another TSI. In this version, HMV means the displacement from TSI3 (middle) to TSI1 and TSI2.
%   Add Version control on 12/05/2013
%   INPUT:
%       H_arr           --- Array, Height array for extraction of HMV
%       site            --- String, See mtsi_startup for site defination. Currently Only support the
%                           TSI3.
%       configfpath     --- mtsi_startup.m
%
%   OUTPUT:
%       HMV_x           --- =[HMV31_x HMV32_x];
%       HMV_y           --- =[HMV31_y HMV32_y];
%
%   PREREQUISITE:
%       Height extraction
%
%   VERSION: 1.0    2013/12/05
%
%   USAGE:
%       [HMV_x,HMV_y]=TO_H2HMV(1000:500:2000,'tsi3','mtsi_startup');
%
function [HMV_x,HMV_y]=TO_H2HMV(H_arr,site,configfpath)
%mtsi_startup;
run(configfpath);
assert(strcmpi(site,TSI3),'TSI3 only for current version');
t=load(HeightMatrixValidMatf);
hma32_valid=t.H_valid_32;
HMV32_x=t.HMVx_valid_32;
HMV32_y=t.HMVy_valid_32;
HMV31_x=t.HMVx_valid_31;
HMV31_y=t.HMVy_valid_31;
H_range_full=t.H_range_full;
H_l=length(H_arr);
full_valid=zeros(H_l,1);
for tmi=1:H_l
    [~,ind]=min(abs(hma32_valid-double(H_arr(tmi))));
    full_valid(tmi)=ind;
end
hma32_valid=hma32_valid(full_valid);
HMV32_x=HMV32_x(full_valid);
HMV32_y=HMV32_y(full_valid);
HMV31_x=HMV31_x(full_valid);
HMV31_y=HMV31_y(full_valid);

HMV_x=[HMV31_x HMV32_x];
HMV_y=[HMV31_y HMV32_y];

    
end