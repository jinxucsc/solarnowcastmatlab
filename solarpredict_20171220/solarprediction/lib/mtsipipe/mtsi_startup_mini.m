%mtsi_startup_mini;
%label of site
TSI1='tsi1';
TSI2='tsi2';
TSI3='tsi3';


%   original image and undist image related parameters
centrep.x=320;
centrep.y=240;
ORIH=480;
ORIW=640;
UNDISTH=500;
UNDISTW=500;
UNDISTR=240;
ViewAngle=120;

% ViewAngle=140;
ViewAngle_r=ViewAngle/180*pi;
VA=ViewAngle;
VA_r=ViewAngle_r;


% First TSI:
% 	40°51’57.72”N
% 	72°53’04.71”W
% 	Elevation is about 65 feet above sea level.
location1.longitude = -72.885;
location1.latitude = 40.866;
location1.altitude = 20;        %metre
r1=9;
h1=15.6811;
AzumithShift1=0;
ZenithShift1=0;
rcentrep1.x=305;
rcentrep1.y=235;
% rcentrep1.x=310;
% rcentrep1.y=240;


% Second TSI:
% 	40deg 51'54.62''N
% 	72deg 51' 04.58''W
% 	Elevation 50ft
location2.longitude = -72.8513;
location2.latitude = 40.8652;
location2.altitude = 15.24;        %metre
r2=9;
h2=15.6811;
AzumithShift2=0;
ZenithShift2=0;
rcentrep2.x=318;
rcentrep2.y=246;


% Third TSI:
%   40°51’28.81”N, 72°51’27.18”W
% 	Elevation is about 65 feet above sea level.
location3.longitude = -72.8575;
location3.latitude = 40.8580;
location3.altitude = 20;        %metre
r3=9;
h3=15.6811;
AzumithShift3=0;
ZenithShift3=0;
% rcentrep1.x=305;
% rcentrep1.y=235;
rcentrep3.x=307;
rcentrep3.y=242;


%   Camera parameters
cw1=3.58;             %ccdwidth=3.6mm
ch1=2.69;           %ccdheight=4.576mm
crx1=640;            %ccd resolution X=288
cry1=480;            %ccd resolution Y=352
fo1=4;             %focal length 5.5mm
cw2=3.58;             %ccdwidth=3.6mm
ch2=2.69;           %ccdheight=4.576mm
crx2=640;            %ccd resolution X=288
cry2=480;            %ccd resolution Y=352
fo2=4;             %focal length 5.5mm
cw3=3.58;             %ccdwidth=3.6mm
ch3=2.69;           %ccdheight=4.576mm
crx3=640;            %ccd resolution X=288
cry3=480;            %ccd resolution Y=352
fo3=4;             %focal length 5.5mm


tsiDistance_12=2836.6;   % metre
%tsiDistance=2836.6;   % metre


%   Generate ori2undist matrix if not exist
tsi1undistmat='tsi1_ori2undist.mat';
tsi2undistmat='tsi2_ori2undist.mat';
tsi3undistmat='tsi3_ori2undist.mat';


% reg expression for TSI image
TSIFileExpression='bnltsiskyimageC[123].a[1].\d{8}.\d{6}.jpg.\d{14}.jpg$';
TSIMATExpression='bnltsiskyimageC[123].a[1].\d{8}.\d{6}.jpg.\d{14}.mat$';

% PERMERNANT VARIABLES
TENSECS_DN=datenum([0 0 0 0 0 10]);