function [chlx,chly]=VISCoor2ChlCoor(visx,visy)
gsetting =  load('bnl_SI_CV_Modeling.mat');
navfDir=gsetting.bnlnavfDir;
navfloatf4=sprintf('%s%s',navfDir,'east_coast_1006011745_G13I04_L_float.nav');
navfloatf=sprintf('%s%s',navfDir,'east_coast_1006011745_G13I01_L_float.nav');
fid=fopen(navfloatf);
[coormap,countn]=fread(fid,'float');
lats=coormap(1:2:end);
lons=coormap(2:2:end);
fclose(fid);
fid=fopen(navfloatf4);
[coormap,countn]=fread(fid,'float');
lats4=coormap(1:2:end);
lons4=coormap(2:2:end);
fclose(fid);

TOP=gsetting.TOP;      %   294
BOTTOM=gsetting.BOTTOM;  %   1294
LEFT=gsetting.LEFT;    %   2160
RIGHT=gsetting.RIGHT;   %   3160
ORIH=gsetting.ORIH;      %   original height
ORIW=gsetting.ORIW;      %   original width
ORIH4=gsetting.ORIH4;      %   Channel4 height
ORIW4=gsetting.ORIW4;      %   Channel4 width

l= length(visx);
chlx=visx;
chly=visy;
allindexi=sub2ind([ORIW ORIH],visx,visy);
alllon1=lons(allindexi);
alllat1=lats(allindexi);

for i=1:l
    %   From vis coordinate get real lat and lon
    indexi=allindexi(i);
    lat1=alllat1(i);
    lon1=alllon1(i);
    d1=abs(lats4-lat1);
    d2=abs(lons4-lon1);
    if rem(i,1000)==0
        disp(i);
    end
    %   From real lat and lon, get other channels coordinate
    d1=abs(lats4-lat1);
    d2=abs(lons4-lon1);
    [minval,mini]=min(d1.*d1+d2.*d2);
    [newx newy]=ind2sub([ORIW4 ORIH4],mini);  % reverse calculate

    londiff=lons4(mini)-lon1;
    latdiff=lats4(mini)-lat1;
    dist=londiff^2+latdiff^2;
    minidist4=minval;
    chlx(i)=newx;
    chly(i)=newy;
end

end