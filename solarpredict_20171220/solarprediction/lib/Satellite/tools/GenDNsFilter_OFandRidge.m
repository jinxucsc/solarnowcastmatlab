function GenDNsFilter_OFandRidge(OFMatf,RidgeMatF,dnsfilterf)
t1=load(OFMatf);
t2=load(RidgeMatF);
dn1=t1.alltmdn;
dn2=t2.alltmdn;
newdn1=unique(dn1);
newdn2=unique(dn2);
l1=length(newdn1);
l2=length(newdn2);
vind1=false(l1,1);
vind2=false(l2,1);

for i=1:l1
    indexi=find(newdn2==newdn1(i),1);
    if isempty(indexi)
        vind1(i)=false;
    else
        vind1(i)=true;
        vind2(indexi(1))=true;
    end
end

if length(find(vind2==true))~=length(find(vind1==true))
    disp('[ERROR]: Checking the OF mat doesn''t match with sequential motion vector mat!');
    return;
end
newtmdns1=newdn1(vind1);
newtmdns2=newdn2(vind2);

if ~isempty(find(newtmdns1~=newtmdns2,1))
    disp('[ERROR]: Checking the OF mat doesn''t match with sequential motion vector mat!');
    return;
end
newtmdns=newtmdns1;
save(dnsfilterf,'newtmdns');

end