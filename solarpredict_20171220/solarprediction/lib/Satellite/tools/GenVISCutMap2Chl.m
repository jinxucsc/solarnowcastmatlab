%   Function is used for generation of mapping from VIS cutting image
%   cooridnate to chl coordinate
function GenVISCutMap2Chl(navfDir)
gsetting =  load('bnl_SI_CV_Modeling.mat');
navfDir=gsetting.bnlnavfDir;
navfloatf4=sprintf('%s%s',navfDir,'east_coast_1006011745_G13I04_L_float.nav');
navfloatf=sprintf('%s%s',navfDir,'east_coast_1006011745_G13I01_L_float.nav');
fid=fopen(navfloatf);
[coormap,countn]=fread(fid,'float');
lats=coormap(1:2:end);
lons=coormap(2:2:end);
fclose(fid);
fid=fopen(navfloatf4);
[coormap,countn]=fread(fid,'float');
lats4=coormap(1:2:end);
lons4=coormap(2:2:end);
fclose(fid);

TOP=gsetting.TOP;      %   294
BOTTOM=gsetting.BOTTOM;  %   1294
LEFT=gsetting.LEFT;    %   2160
RIGHT=gsetting.RIGHT;   %   3160
ORIH=gsetting.ORIH;      %   original height
ORIW=gsetting.ORIW;      %   original width
ORIH4=gsetting.ORIH4;      %   Channel4 height
ORIW4=gsetting.ORIW4;      %   Channel4 width

CUTW=RIGHT-LEFT+1;
CUTH=BOTTOM-TOP+1;
%%  Use Rect on chls
topleft_y=TOP;
topleft_x=LEFT;
bottomright_y=BOTTOM;
bottomright_x=RIGHT;
[tl_chlx,tl_chly]=VISCoor2ChlCoor(topleft_x,topleft_y);
[br_chlx,br_chly]=VISCoor2ChlCoor(bottomright_x,bottomright_y);
chlcutH=br_chly-tl_chly+1;
chlcutW=br_chlx-tl_chlx+1;

%   1. Generate all the VIS cut coordinate matrix
allviscy=zeros(CUTW*CUTH,1);
allviscx=zeros(CUTW*CUTH,1);
indexi=1;
for xi=1:CUTW
    for yi=1:CUTH
        allviscy(indexi)=yi;
        allviscx(indexi)=xi;
        indexi=indexi+1;
    end
end
% allviscy=501;
% allviscx=501;


%   follows allviscy/CUTH=(allchly-tl_chly+1)/chlcutH;
%           allviscx/CUTW=(allchlx-tl_chlx+1)/chlcutW;
allchly=round(allviscy/CUTH*chlcutH-1+tl_chly);
allchlx=round(allviscx/CUTW*chlcutW-1+tl_chlx);

save('viscut2chlcoor_map_bnl.mat','allchlx','allchly','allviscy','allviscx');
return;


%% ALL points in nav file
%   1. Generate all the VIS cut coordinate matrix
for xi=1:CUTW
    for yi=1:CUTH
        allviscy(indexi)=yi;
        allviscx(indexi)=xi;
        indexi=indexi+1;
    end
end

%   2. Generate all the VIS coordinate vectors (VIS cut -> VIS)
visx=allviscx+LEFT-1;
visy=allviscy+TOP-1;

chlx=visx;
chly=visy;
allindexi=sub2ind([ORIW ORIH],visx,visy);
alllon1=lons(allindexi);
alllat1=lats(allindexi);
premini=0;
parfor i=1:l
    %   From vis coordinate get real lat and lon
    indexi=allindexi(i);
    lat1=alllat1(i);
    lon1=alllon1(i);
    d1=abs(lats4-lat1);
    d2=abs(lons4-lon1);
    if rem(i,1000)==0
        disp(i);
    end
    %   From real lat and lon, get other channels coordinate
    d1=abs(lats4-lat1);
    d2=abs(lons4-lon1);
    [minval,mini]=min(d1.*d1+d2.*d2);
    [newx newy]=ind2sub([ORIW4 ORIH4],mini);  % reverse calculate
    
    chlx(i)=newx;
    chly(i)=newy;
end


save(allchlx,allchly);


end