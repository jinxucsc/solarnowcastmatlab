%   Function is to get gray scale values from larger block with the method
%   of mean for smaller blocks.
%   USAGE:
%   INNER_BLOCK2MeanVals([BLOCKH BLOCKW],[tmBLOCKH tmBLOCKW],newgrayblockvals);
function grayvals=INNER_BLOCK2MeanVals(dimOri,dim,grayblockvals)
% [dimOri(1) dimOri(2) NFILES NCHLS]=size(grayblockvals);
[~,~,NFILES,NCHLS]=size(grayblockvals);
grayvals=zeros(NFILES,NCHLS);
bceny=ceil(dimOri(1)/2);
bcenx=ceil(dimOri(2)/2);
tmBLOCKH=dim(1);
tmBLOCKW=dim(2);
tmBLOCKNP=(tmBLOCKH-1)/2;

for i=1:NFILES
    for j=1:NCHLS
        grayvals(i,j)=mean2(grayblockvals(bceny-tmBLOCKNP:bceny+tmBLOCKNP,...
    bcenx-tmBLOCKNP:bcenx+tmBLOCKNP,i,j));
    end
end

end