function [visx,visy]=VISCutCoor2VISCoor(viscx,viscy)
gsetting =  load('bnl_SI_CV_Modeling.mat');
% TOP=294;      %   294
% BOTTOM=1294;  %   1294
% LEFT=2160;    %   2160
% RIGHT=3160;   %   3160
% ORIH=3000;      %   original height
% ORIW=3600;      %   original width
% ORIH4=750;      %   Channel4 height
% ORIW4=900;      %   Channel4 width
TOP=gsetting.TOP;      %   294
BOTTOM=gsetting.BOTTOM;  %   1294
LEFT=gsetting.LEFT;    %   2160
RIGHT=gsetting.RIGHT;   %   3160
ORIH=gsetting.ORIH;      %   original height
ORIW=gsetting.ORIW;      %   original width
ORIH4=gsetting.ORIH4;      %   Channel4 height
ORIW4=gsetting.ORIW4;      %   Channel4 width

% sx1=rx1-LEFT+1;
% sy1=ry1-TOP+1;
visx=viscx+LEFT-1;
visy=viscy+TOP-1;

end