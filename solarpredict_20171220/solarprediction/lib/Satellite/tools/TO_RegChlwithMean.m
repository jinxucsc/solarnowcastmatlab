%   Some irregular cases are found as they provide chl2 - chl6 zeros due to
%   errors. This function is to regularize the chl with block provided in
%   inputs.
function newchl=TO_RegChlwithMean(chl,chlblock,meanNP)
l=length(chl);
[BH,BW,l1]=size(chlblock);
if l1~=l
    disp('chl and chlblocks have different dimension!');
    return;
end

ind=find(chl==0);
tml=length(ind);
chly=round(BH/2);
chlx=round(BW/2);
tmblock=zeros(2*meanNP+1,2*meanNP+1);
newchl=chl;
for i=1:tml
    chli=ind(i);
    tmblock(:,:)=chlblock(chly-meanNP:chly+meanNP,chlx-meanNP:chlx+meanNP,chli);
    tt=tmblock(tmblock>0);
    if isempty(tt)
        meanval=-1;
    else
        meanval=mean(tt);
    end
    newchl(chli)=meanval;
end

end