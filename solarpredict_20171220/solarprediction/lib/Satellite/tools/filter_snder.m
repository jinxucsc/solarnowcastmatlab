%   Function is designed to find all the sounder images and put them into
%   outputDir. Function will look into all the sub directories and find the
%   sounder images by image names
%   Sample of the inputfile  .../goes13.2012.122.194625.sndr.BAND_07.jpg
%   INPUT:
%       inputDir    --  Directory, root directory for search
%       outputDir   --  Directory, output directory containing all
%                       subdirectories with name of the band
%   OUTPUT:
%       band1~band19 -- Directory, contains 19 band images
function filter_snder(inputDir,outputDir)
if(inputDir(end)~='/')
    inputDir=sprintf('%s%s',inputDir,'/');
end
if(outputDir(end)~='/')
    outputDir=sprintf('%s%s',outputDir,'/');
end
if ~exist(outputDir,'dir')
    mkdir(outputDir);
    banddirs=cell(19);
    for iband=1:19
        banddirs{iband}=sprintf('%sband%d/',outputDir,iband);
        mkdir(banddirs{iband});
    end
else
    banddirs=cell(19);
    for iband=1:19
        banddirs{iband}=sprintf('%sband%d/',outputDir,iband);
    end
end

allfiles=dir(inputDir);
[m,~]=size(allfiles);
for i=3:m   %   remove . and ..
    if allfiles(i).isdir    % sub directories, do iterations
        dirpath=sprintf('%s%s',inputDir,allfiles(i).name);
        filter_snder(dirpath,outputDir);
    else
        %goes13.2012.122.194625.sndr.BAND_07.jpg
        filename=allfiles(i).name;
        if length(filename)~=39
            continue;
        end
        tmstr1=filename(1:6);
        if ~strcmpi(tmstr1,'goes13')
            continue;
        end
        tmstr1=filename(24:27); %sndr
        if ~strcmpi(tmstr1,'sndr')
            continue;
        end
        
        %   if sndr image classify with band #
        tmstr1=filename(end-5:end-4);
        iband=str2num(tmstr1);
        tmdir=banddirs{iband};
        inputf=sprintf('%s%s',inputDir,filename);
        outputf=sprintf('%s%s',tmdir,filename);
        copyfile(inputf,outputf);
    end
end

end