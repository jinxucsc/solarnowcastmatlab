function [chlx,chly]=VISCutCoor2ChlCoor(viscx,viscy)
[visx,visy]=VISCutCoor2VISCoor(viscx,viscy);
[chlx,chly]=VISCoor2ChlCoor(visx,visy);
end