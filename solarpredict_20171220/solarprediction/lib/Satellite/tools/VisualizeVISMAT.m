function VisualizeVISMAT(SIDir,oSIDir)
if(SIDir(end)~='/')
    SIDir=sprintf('%s%s',SIDir,'/');
end
if(oSIDir(end)~='/')
    oSIDir=sprintf('%s%s',oSIDir,'/');
end
mkdir(oSIDir);
allfiles=dir(SIDir);
[NFILES,~]=size(allfiles);
for i=1:NFILES
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        inputf=sprintf('%s%s',SIDir,filename);
        outputf=sprintf('%s%s.jpg',oSIDir,filename(1:end-4));
        if exist(outputf,'file')
            continue;
        end
        t=load(inputf,'-mat');
        imwrite(t.outputm,outputf,'jpg');
    end
end

end