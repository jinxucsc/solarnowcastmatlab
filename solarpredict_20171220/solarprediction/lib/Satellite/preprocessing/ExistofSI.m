function [filename,fullpath,tmdn]=ExistofSI(SIDir,startdn,enddn)
ONEMIN=datenum([0 0 0 0 1 0]);
iter_tmdn=1;
tmdn=startdn;
TAIL='G13I01';
while(tmdn<enddn)
    tmdv=datevec(tmdn);
    filename=GetSIfromDV(tmdv,TAIL);
    fullpath=sprintf('%s%s',SIDir,filename);
    if(exist(fullpath,'file')==0)
        tmdn=startdn+iter_tmdn*ONEMIN;
        iter_tmdn=iter_tmdn+1;
        continue;
    else
        return;
    end
end
fullpath=[];
end