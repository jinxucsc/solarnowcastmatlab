%   outdv=SI_TIMEExT(inputf):
%   Extract date vector from one image name. MUST haveG13 string in the
%   filename!
%   CAUTION:    Image is 3600x3000
%               Basic format of filname is - .../.../1203241415G13I01.tif
%   IN:
%       inputf  --   string, filename or filepath
%
%   OUT:
%       outdv   --   datevector, output time format
%   VERSION 1.0     2012/04/05
function outdv=SI_TIMEExT(inputf)
i_13=findstr(inputf,'G13');
if(i_13==0)
    disp('Error input format');
    return;
end
iyear=i_13-10;
imon=iyear+2;
iday=imon+2;
ih=iday+2;
im=ih+2;
isec=im+2;

nyear=2000+str2num(inputf(iyear:iyear+1));
nmonth=str2num(inputf(imon:imon+1));
nday=str2num(inputf(iday:iday+1));
nhour=str2num(inputf(ih:ih+1));
nmin=str2num(inputf(im:im+1));
% nsec=str2num(inputf(isec:isec+1));
nsec=0;
outdv=[nyear,nmonth,nday,nhour,nmin,nsec];

end