%   filename=SI_TIMEExT(dv):
%   Generate filename based on date vector
%   CAUTION:    Image is 3600x3000
%               Basic format of filname is - .../.../1203241415G13I01.tif
%               NOT tested for years that are NOT 2012
%   IN:
%       dv  --   datevector, input date information
%       TAIL --  string information of satellite and channel
%               'G13I01'
%
%   OUT:
%       filename -- string , output filename
%                   '1203241415G13I01.tif'
%   VERSION 1.0     2012/04/05

function filename=GetSIfromDV(dv,TAIL)
nyear=dv(1)-2000;
nmonth=dv(2);
nday=dv(3);
nhour=dv(4);
nmin=dv(5);
% nsec=dv(6);

syear=int2str(nyear);
if(nmonth<10)
    smon=sprintf('%s%d','0',nmonth);
else
    smon=int2str(nmonth);
end
if(nday<10)
    sday=sprintf('%s%d','0',nday);
    else
    sday=int2str(nday);
end
if(nmin<10)
    smin=sprintf('%s%d','0',nmin);
else
    smin=int2str(nmin);
end
if(nhour<10)
    shour=sprintf('%s%d','0',nhour);
else
    shour=int2str(nhour);
end

% if(nsec<10)
%     ssec=sprintf('%s%d','0',nsec);
% else
%     ssec=int2str(nsec);
% end


filename=sprintf('%s%s%s%s%s%s.tif',syear,smon,sday,shour,smin,TAIL);

end