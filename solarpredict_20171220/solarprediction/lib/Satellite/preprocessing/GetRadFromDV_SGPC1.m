%   Get radiation file by datevector and site name
%   This is only for SGP and c1 stream only
%   datevector  including year,month,day,hour,min,seconds
%   radDir      Radiation directory with '/'
%   eg.     sgpqcrad1longC1.c1.20110401.000000.cdf
%   eg.     sgpqcrad1longE9.c1.20120306.000000.cdf
%   eg.     sgpqcrad1longE11.c1.20120215.000000.cdf
%   VERSION 1.0     2012/04/08
function filename=GetRadFromDV_SGPC1(datevector,site)
site=upper(site);
prefix=['sgpqcrad1long',site,'.c1'];  %sgpqcrad1longC1.c1
year=int2str(datevector(1));
month=int2str(datevector(2));
if(length(month)==1)
    month=strcat('0',month);
end
day=int2str(datevector(3));
if(length(day)==1)
    day=strcat('0',day);
end
%twpqcrad1longC1.c1.20110403.000000.cdf
filename=sprintf('%s.%s%s%s.000000.cdf',prefix,year,month,day);

end
