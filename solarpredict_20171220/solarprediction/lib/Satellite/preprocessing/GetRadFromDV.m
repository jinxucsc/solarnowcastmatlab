function fullname=GetRadFromDV(option,datevector,radDir)
%   option  0   sgp
%           1   twp
%   datevector  including year,month,day,hour,min,seconds
%   radDir      Radiation directory with '/'

%twpqcrad1longC1.c1.20110403.000000.cdf
%sgpqcrad1longC1.c1.20110401.000000.cdf
if(option==1)
    prefix='twpqcrad1longC1.c1.';
else
    prefix='sgpqcrad1longC1.c1.';
end

year=int2str(datevector(1));
month=int2str(datevector(2));
if(length(month)==1)
    month=strcat('0',month);
end
day=int2str(datevector(3));
if(length(day)==1)
    day=strcat('0',day);
end

%twpqcrad1longC1.c1.20110403.000000.cdf
FileName=sprintf('%s%s%s%s.000000.cdf',prefix,year,month,day);
fullname=sprintf('%s%s',radDir,FileName);


end
