function fullname=GetBNLRadMatfFromDV(site,datevector)
%   site        'C1','E11'...
%                       
%   datevector  including year,month,day,hour,min,seconds
%
%
%   bnlqcrad1longC1.c1.20110401.000000.mat

year=int2str(datevector(1));
month=int2str(datevector(2));
if(length(month)==1)
    month=strcat('0',month);
end
day=int2str(datevector(3));
if(length(day)==1)
    day=strcat('0',day);
end

%bnlqcrad1longC1.c1.20110401.000000.mat
fullname=sprintf('bnlqcrad1long%s.c1.%s%s%s.000000.mat',upper(site),year,month,day);


end
