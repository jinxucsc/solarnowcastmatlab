%IsConsecutive(filename1,filename2)
%   to judge whether
%       0<datevec(filename2)-datevec(filename1)<=40min
%   VERSION 1.0     2012/04/08
function bConsecutive=IsConsecutive(filename1,filename2,PERIODMAX)
% PERIODMAX=40;
tmdv1=SI_TIMEExT(filename1);
tmdn1=datenum(tmdv1);
tmdv2=SI_TIMEExT(filename2);
tmdn2=datenum(tmdv2);
dn30=datenum([0 0 0 0 PERIODMAX 0]);

if  (tmdn2-tmdn1<=dn30) && (tmdn2-tmdn1>0)
    bConsecutive=1;
else
    bConsecutive=0;
end

end