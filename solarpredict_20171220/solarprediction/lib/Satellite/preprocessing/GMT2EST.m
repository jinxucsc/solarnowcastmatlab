%   EST to GMT transferring
function outputdvs=GMT2EST(inputdvs)
outputdvs=inputdvs;
outputdvs(:,4)=outputdvs(:,4)-5;
outputdvs=datevec(datenum(outputdvs));
end