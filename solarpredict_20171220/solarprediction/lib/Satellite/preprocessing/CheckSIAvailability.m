%CheckSIAvailability:
%   This function is to generate usable period of satellite images. Put all
%   available days into a mat file
%   IN:
%       SIDir(in)   --  Directory, contains all the input satellite images
%   OUT:
%       save('SIavailability.mat','SIavail');             
%               --   mat file of all available days  
%       save('SIavailability_all.mat','SIavail','SIAll'); 
%               --   mat file of available days and timestamps
%   VERSION 1.0     2012/04/06
function CheckSIAvailability(SIDir)
% SIDir='SatelliteImages/';
% SIDir='SatelliteImage/';
allfiles=dir(SIDir);

[m,~]=size(allfiles);
SIavail=zeros(m,1);
SIAll=zeros(m,1);
iter_i=1;
for i=1:m
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        inputf=sprintf('%s%s',SIDir,filename);
        tmdv=SI_TIMEExT(inputf);
        tmdn=datenum(tmdv);
        SIAll(i)=tmdn;
        tmdv(4)=0;
        tmdv(5)=0;
        tmdv(6)=0;
        tmdn=datenum(tmdv);
        SIavail(i)=tmdn;
    else
        iter_i=iter_i+1;
    end
end
SIavail=SIavail(iter_i:end,1);
SIAll=SIAll(iter_i:end,1);
SIavail=unique(SIavail);
SIAll=sort(SIAll);
SIavail=sort(SIavail);

save('SIavailability.mat','SIavail');
save('SIavailability_all.mat','SIavail','SIAll');

end