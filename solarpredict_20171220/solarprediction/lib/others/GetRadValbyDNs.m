%   Function is designed to get radiation vals from reading radDir when
%   given the datenumber vector(tmdns) and the sitename(site)
%
%   The INVALID tmdns(tmdns = 0) and no record in radDir, will be shown as
%   radvals = -1. All radvals < 0 but not -1 will be 0.
%   INPUT:
%       tmdns   --  Datenumbers, requested DNs (mx1)  
%       radDir  --  String, radiation file directory path
%       site    --  String, SGP sitename 
%       meanl   --  mean value length mean(rad-meanl:rad+meanl)
%                   0 -> no mean
%   OUTPUT:
%       radvals --  vector (mx1)
%
%   VERSION:    1.0     Date:   2012-07-20
%        
function radvals = GetRadValbyDNs(tmdns,radDir,site,meanl)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Suppose the granuality of radiation is 1min, 1440 per day
ONEDAY=1440;
UNIT=datenum([0 0 0 0 1 0]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tmdvs=datevec(tmdns);
if(radDir(end)~='/')
    radDir=sprintf('%s%s',radDir,'/');
end
[m,~]=size(tmdvs);
radvals=zeros(m,1);
for i=1:m
    tmdv=tmdvs(i,:);
    if tmdv(1)==0
        radvals(i)=-1;
        continue;
    end
    daydv=tmdv;
    daydv(4)=0;
    daydv(5)=0;
    daydv(6)=0;
    filename=GetRadFromDV_SGPC1(daydv,site);
    inputf=sprintf('%s%s',radDir,filename);
    if ~exist(inputf,'file')
        %        showstr=sprintf('File does not exist: %s',filename);
        %        disp(showstr);
        radvals(i)=-1;
        continue;
    end
    ncid=netcdf.open(inputf,'NC_NOWRITE');
    dniID=netcdf.inqVarID(ncid,'short_direct_normal');
    dni=netcdf.getVar(ncid,dniID);
    indexi=floor((datenum(tmdv)-datenum(daydv))/UNIT)+1;
    if(indexi>meanl&&indexi<ONEDAY-meanl)
        tmdni=dni(indexi-meanl:indexi+meanl);
        tmdni=tmdni(tmdni>=0);
        radvals(i)=mean(tmdni);
    elseif indexi<meanl
        tmdni=dni(indexi:indexi+meanl);
        tmdni=tmdni(tmdni>=0);
        radvals(i)=mean(tmdni);
    else
        tmdni=dni(indexi-meanl:indexi);
        tmdni=tmdni(tmdni>=0);
        radvals(i)=mean(tmdni);
    end
    netcdf.close(ncid)
end
radvals(radvals<0&radvals~=-1)=0;

end
