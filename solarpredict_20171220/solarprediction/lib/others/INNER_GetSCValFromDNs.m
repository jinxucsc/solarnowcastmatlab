%   Function is designed to get sky cover from SC file(stored in SCDir)
%   when given the datenumber(tmdns) and the days they cover(daydns), and
%   the related real filenames. SC_opaque, SC_thin are the two output based
%   on the input. Invalid filename is represented as 'N', invalid sc values
%   are -1 by default.
%
function [SC_opaque,SC_thin]=INNER_GetSCValFromDNs(tmdns,daydns,rfilenames,SCDir)
if(SCDir(end)~='/')
    SCDir=sprintf('%s%s',SCDir,'/');
end
tmdvs=datevec(tmdns);
daydvs=datevec(daydns);
[m,~]=size(tmdvs);
SC_opaque=zeros(m,1);
SC_thin=zeros(m,1);
for i=1:m
    %   1. Find tmdv day datevec to extract filename from rfilenames
    tmdv=tmdvs(i,:);
    daydv=tmdv;
    daydv(4)=0;
    daydv(5)=0;
    daydv(6)=0;
    tmdaydn=datenum(daydv);
    %   locate the file of sc
    indexi=find(daydns==tmdaydn,1);
    filename=rfilenames{indexi(1)};
    SC_opaque(i)=-1;    % -1 means no record found in the cdf file
    SC_thin(i)=-1;
    if strcmpi(filename,'N')
        continue;
    end
    %   2. Read file in and get cdf data
    fpath=sprintf('%s%s',SCDir,filename);
    ncid=netcdf.open(fpath,'NC_NOWRITE');
    opID=netcdf.inqVarID(ncid,'percent_opaque');
    thinID=netcdf.inqVarID(ncid,'percent_thin');
    btID=netcdf.inqVarID(ncid,'base_time');
    toID=netcdf.inqVarID(ncid,'time_offset');
    opdata=netcdf.getVar(ncid,opID);
    thindata=netcdf.getVar(ncid,thinID);
    basetime=netcdf.getVar(ncid,btID);
    timeoffset=netcdf.getVar(ncid,toID);
    tmdn=tmdns(i);
    %   3. Get offset value and get instanious value
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    basedn=double(basetime)/86400+datenum([1970 1 1 0 0 0]);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tmoffset=round((tmdn-basedn)*86400);
    distarr=abs(timeoffset-tmoffset);
    [minval,mini]=min(distarr);
    if(minval<10)   % less than 20 seconds
        SC_opaque(i)=opdata(mini);
        SC_thin(i)=thindata(mini);
    else
        netcdf.close(ncid);
        continue;
    end
    netcdf.close(ncid);
end




end