function filename=GetFilenameFromAbsPath(abspath)
abspath=regexprep(abspath,'\','/');
splitstr='/';
indexi=strfind(abspath,splitstr);
if isempty(indexi)  % 1.txt -> 1.txt
    filename=abspath;
elseif abspath(end)=='/'    % .../1/ -> 1   Get directory name
    % this is a directory
    substr1=abspath(indexi(end-1)+1:end-1);
    filename=substr1;
else                        % .../1.txt -> 1.txt
    substr1=abspath(indexi(end)+1:end); 
    if isempty(substr1)
        filename='';
    else
        filename=substr1;
    end    
end

end