%   This function is to convert real coordinate of destination([longitude lattitude altitude]) 
%   to relative 3D coordinate to origin
%   INPUT:
%       ori_lon         --- longitude of origin
%       ori_lat         --- lattitude of origin
%       ori_alt         --- altitude of origin
%       dest_lon         --- longitude of destination
%       dest_lat         --- lattitude of destination
%       dest_alt         --- altitude of destination
%   OUTPUT:
%       [rx,ry,rz]      --- Relative coordinate to origin in KM
%   Version 1.0     2013-06-14
function [rx,ry,rz]=TO_RealCoor2RelativeCoor(ori_lon,ori_lat,ori_alt,dest_lon,dest_lat,dest_alt)
disty=pos2dist(ori_lat,dest_lon,dest_lat,dest_lon,1);
distx=pos2dist(dest_lat,ori_lon,dest_lat,dest_lon,1);
disty=disty*1000;   %km -> m
distx=distx*1000;   %km -> m
if ori_lat-dest_lat>0
    cy=-1;
else
    cy=1;
end
if ori_lon-dest_lon>0
    cx=-1;
else
    cx=1;
end

rz=dest_alt-ori_alt;
ry=cy*disty;
rx=cx*distx;

end


