function Rec_Permutation(q_sub,vargarin)
global GLOBAL_QUEUE_ALL;

if ~isempty(vargarin)
    varargin1=vargarin;
    tmvals=varargin1{1};
    tml=length(tmvals);
    if 1==length(varargin1)
        %bLast=true;
        for i=1:tml
            Rec_Permutation([q_sub tmvals(i)],{});
        end
    else
        %bLast=false;
        for i=1:tml
            Rec_Permutation([q_sub tmvals(i)],varargin1(2:end));
        end
        
    end        
else
    GLOBAL_QUEUE_ALL=[GLOBAL_QUEUE_ALL;q_sub];
end
end