%   Function is to generate permutations for vectors(any number) given in the input
%   USAGE:
%       permutation=GenPermutation(1:2,3:4)
%       
%         permutation =
% 
%          1     3
%          1     4
%          2     3
%          2     4
%          
%          
%        permutation=GenPermutation(1:4)
%          
%          permutation =
% 
%          1
%          2
%          3
%          4
%   This function will call Rec_Permutation function
%   VERSION 1.0     2013-08-26    
function permutation=GenPermutation(varargin)
global GLOBAL_QUEUE_ALL;
bNeedBackup=false;
if ~isempty(GLOBAL_QUEUE_ALL)
    bNeedBackup=true;
    tmbackup=GLOBAL_QUEUE_ALL;
end
GLOBAL_QUEUE_ALL=[];
nDim=length(varargin);
assert(nDim>0,'INPUT vectors should be at least one');
for i=1:nDim
    assert(~isempty(varargin{i}),'INPUT vectors should be non-empty vectos');
end

Rec_Permutation([],varargin);

permutation=GLOBAL_QUEUE_ALL;
if bNeedBackup
    GLOBAL_QUEUE_ALL=tmbackup;
else
    clear GLOBAL_QUEUE_ALL;
end




end