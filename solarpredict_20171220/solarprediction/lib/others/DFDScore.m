% function DFDScore is to calculate the DFD between two images.
%   @version 1.0        2013-05-02
function [DFDval avgDFDval]=DFDScore(im1,im2,maskma,p)
if nargin ==2
    maskma=1;
    p=1;    % p=1 MAE p=2 MSE
elseif nargin==3
    p=1;    % p=1 MAE p=2 MSE
else
    
end

[m1,n1]=size(im1);
[m2,n2]=size(im2);


if m1~=m2 || n1~=n2
    DFDval=-1;
    avgDFDval=-1;
    disp('Image size doesn''t  match!');
    return;
else
    if maskma==1
        maskma=true(m1,n1);
    end
    %     for i=1:m1
    %         for j=1:n1
    %             if ~maskma(i,j)
    %                 continue;
    %             end
    %             tmu=uma(i,j);
    %             tmv=vma(i,j);
    %             newi=i+tmu;
    %             newj=j+tmv;
    %             if  newi<1 ||newi>m1 ||newj<1 ||newj>n1
    %                 pdiff=im2(i,j);
    %             else
    %                 pdiff=abs(im1(i,j)-im2(newi,newj));
    %             end
    %             all_diff=all_diff+pdiff;
    %         end
    %     end
    %     DFDval=all_diff;
    %     totall=length(find(maskma>0));
    %     avgDFDval=DFDval/totall;
    if p==1
        diffma=abs(double(im1)-double(im2));
    else
        diffma=abs(double(im1)-double(im2)).^p;
    end
    sumval=sum(sum(diffma(maskma)));
    totall=length(find(maskma));
    %     for i=1:m1
    %         for j=1:n1
    %             if maskma(i,j)
    %                 sumval=sumval+diffma(i,j);
    %             end
    %         end
    %     end
    DFDval=sumval;
    avgDFDval=sumval./totall;
end



end