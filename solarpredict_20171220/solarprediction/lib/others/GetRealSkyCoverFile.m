%   To get real filename as 
%   sgptsiskycoverC1.b1.20120204.000000.cdf may appear as:
%   sgptsiskycoverC1.b1.20120204.133400.cdf
%   CAUTION:
%       prefilenames MUST be cell
%       'N' character is assigned when the filename doesnot exist

function filenames=GetRealSkyCoverFile(SCDir,prefilenames)
if(SCDir(end)~='/')
    SCDir=sprintf('%s%s',SCDir,'/');
end

[m,~]=size(prefilenames);
filenames=cell(m,1);
allfiles=dir(SCDir);
[nfile,~]=size(allfiles);
for i=1:m
    prefilename=prefilenames{i};
    fpath=sprintf('%s%s',SCDir,prefilename);
    if exist(fpath,'file')
        filenames{i}=prefilename;
    end
    %prefix=sgptsiskycoverC1.b1.20120204.         000000.cdf
    %
    prefix=prefilename(1:end-10);
    prefix=lower(prefix);
    bfound=false;
    for j=1:nfile
        rfilename=allfiles(j).name;
        index=strfind(lower(rfilename),prefix);
        if ~isempty(index)
            filenames{i}=rfilename;
            bfound=true;
            break;
        end
    end
    if ~bfound
        filenames{i}='N';
    end
end




end