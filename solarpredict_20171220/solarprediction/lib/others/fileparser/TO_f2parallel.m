function fhandle_parallel=TO_f2parallel(fhandle)

% 1. Get outputparams and inputparams
[inputparams,outputparams]=TO_fIOparser(fhandle);


% 2. Rewrite a function with loading inputparams and outputparams
fhandle_parallel=TO_fgenerator(fhandle,inputparams,outputparams);



end
