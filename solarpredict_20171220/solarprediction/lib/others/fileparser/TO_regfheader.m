% Function is to regularize fheader format by:
%   Rule 1: Remove all special characters and  make function as first keyword
%   Rule 2: Replace all backspaces in output parameters [a b  c] to [a,b,c]
%   Rule 3: Remove all backspace in output parameters [a,b] and input parameters (a,b)
function regline=TO_regfheader(fheaderline)
% Rule 1: Remove all special characters before function making function as first keyword
tline=regexprep(fheaderline,'\s',' ');
tline=regexprep(tline,'^ +','');

% Rule 2: Replace all backspaces in output parameters [a b  c] to [a,b,c]
s1='['; s2=']';
si1=strfind(tline,s1);
si2=strfind(tline,s2);
if ~isempty(si1)&&~isempty(si2)
    assert(length(si1)==1&&length(si2)==1,'Wrong input of fheader. There should be only one pair of []');
    tmstr=tline(si1(1)+1:si2(1)-1);
    tmstr1=tmstr;
    tmstr=regexprep(tmstr,'^ +','');    % remove head backspaces
    tmstr=regexprep(tmstr,' +$','');    % remove tail backspaces
    tmstr=regexprep(tmstr,'\s+',' ');   % contains only one backspace
    tmstr=regexprep(tmstr,'\w+ +','${strrep($0,'' '','','')}');    % replace all backspaces into ,
    tline=strrep(tline,tmstr1,tmstr);
end

% Rule 3: Remove all backspace in output parameters [a,b] and input parameters (a,b)
s1='['; s2=']';
si1=strfind(tline,s1);
si2=strfind(tline,s2);
if ~isempty(si1)&&~isempty(si2)
    assert(length(si1)==1&&length(si2)==1,'Wrong input of fheader. There should be only one pair of []');
    tmstr=tline(si1(1):si2(1));
    regtmstr=regexprep(tmstr,' ','');
    tline=strrep(tline,tmstr,regtmstr);
end

s1='('; s2=')';
si1=strfind(tline,s1);
si2=strfind(tline,s2);
if ~isempty(si1)&&~isempty(si2)
    assert(length(si1)==1&&length(si2)==1,'Wrong input of fheader. There should be only one pair of ()');
    tmstr=tline(si1(1):si2(1));
    regtmstr=regexprep(tmstr,' ','');
    tline=strrep(tline,tmstr,regtmstr);
end

regline=tline;

end

