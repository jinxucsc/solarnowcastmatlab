% Finding input parames and output paras rules
%   Rule 1: first non-comment line start with regular 'function' keyword
%   Rule 2: outputparams:  '=' left to function
%   Rule 3: inputparams:  functionname(...)
function [inputparams,outputparams]=TO_fIOparser(fhandle)
F=functions(fhandle);
Fpath=F.file;
Ffunction=F.function;
assert(exist(Fpath,'file')~=0,'Function does not exist!');
Fcontent=fileread(Fpath);

% Rule 1: Find function line
delimiters={'\n'};
tlines=strsplit(Fcontent,delimiters);
lc=length(tlines);
EXP_comment='^\s*%';
EXP_functionkeyword='^\s*function';
for i=1:lc
    tline=tlines{i};
    starti= regexp(tline,EXP_comment, 'once');
    if ~isempty(starti);
        continue;
    end
    starti= regexp(tline,EXP_functionkeyword, 'once');
    if isempty(starti);
        continue;
    else
        break;
    end
end
functionkeywordline=tline;
functionkeywordline=TO_regfheader(functionkeywordline); % regularize fheader 


% Rule 2: extract outputparams from function to '='
%   function TO_fparser(fhandle)
EXP_nooutput=sprintf('function +%s',Ffunction);
%   function inputparams = TO_fparser(fhandle)
EXP_output1=sprintf('function +\\w+ *= *%s',Ffunction);
EXP_outputstr1='\w+ *=';
%   function [inputparams,outputparams]=TO_fparser(fhandle)
EXP_outputs=sprintf('function +\\[\\w+(,\\w+)*\\] *= *%s',Ffunction);
EXP_outputstrs='\[\w+(,\w+)*\]';
bNooutput=false;
if ~isempty(regexp(functionkeywordline,EXP_nooutput,'once'))
    bNooutput=true;
    outputparams=[];
elseif ~isempty(regexp(functionkeywordline,EXP_output1,'once'))
    bOutput1=true;
    outputparams=cell(1,1);
    [si,ei]=regexp(functionkeywordline,EXP_outputstr1,'once');
    outputparams{1}=functionkeywordline(si:ei-1);   % remove last = symbol
elseif ~isempty(regexp(functionkeywordline,EXP_outputs,'once'))
    bOutputs=true;
    [si,ei]=regexp(functionkeywordline,EXP_outputstrs,'once');
    tmstr=functionkeywordline(si+1:ei-1);   %remove [ and ]
    delimiters={','};
    tmstrs=strsplit(tmstr,delimiters);
    nouts=length(tmstrs);
    outputparams=cell(nouts,1);
    for i=1:nouts
        outputparams{i}=tmstrs{i};
    end
else
    assert(false,'Output parameters cannot be resolved! Please check your input MATLAB file!');
end

% Rule 3: extract outputparams from functionname(...)
%  function ... = TO_fparser
EXP_noinput=sprintf('= *%s *^\\(',Ffunction);
if bNooutput
    % no output means no equal sign
    EXP_inputs=sprintf('function *%s\\(\\w+(,\\w+)*\\)',Ffunction);
else
    EXP_inputs=sprintf('= *%s\\(\\w+(,\\w+)*\\)',Ffunction);
end
EXP_inputstrs='\(\w+(,\w+)*\)';
if ~isempty(regexp(functionkeywordline,EXP_noinput,'once'))
    bNoinput=true;
    inputparams=[];
elseif ~isempty(regexp(functionkeywordline,EXP_inputs,'once'))
    bInputs=true;
    [si,ei]=regexp(functionkeywordline,EXP_inputstrs,'once');
    tmstr=functionkeywordline(si+1:ei-1);   %remove ( and )
    delimiters={','};
    tmstrs=strsplit(tmstr,delimiters);
    nins=length(tmstrs);
    inputparams=cell(nins,1);
    for i=1:nins
        inputparams{i}=tmstrs{i};
    end
else
    assert(false,'Input parameters cannot be resolved! Please check your input MATLAB file!');
end


end