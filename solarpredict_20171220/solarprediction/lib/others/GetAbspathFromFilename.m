function abspath=GetAbspathFromFilename(filename)
filename=regexprep(filename,'\','/');
indexi=strfind(filename,'/');
if isempty(indexi)
    %   1.txt -> WD/1.txt
    str1=pwd;
    str1=FormatDirName(str1);
    abspath=sprintf('%s%s',str1,filename);
else
    if strcmpi(filename(1),'/')
        % /home/mikegrup/1.txt
        abspath=filename;
    elseif strcmpi(filename(1:2),'./')
        % ./1.txt -> WD/1.txt
        str1=pwd;
        str1=FormatDirName(str1);
        abspath=sprintf('%s%s',str1,filename(3:end));
    else
        % 1/2/3.txt -> WD/1/2/3.txt
        str1=pwd;
        str1=FormatDirName(str1);
        abspath=sprintf('%s%s',str1,filename);
    end
end


end