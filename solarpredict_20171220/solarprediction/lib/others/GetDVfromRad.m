function startdv=GetDVfromRad(filename)
%   function is designed to find datenumber of filename
%   IN:
%       filename        sgpqcrad1longC1.c1.20100714.000000.cdf
%                       or .../sgpqcrad1longC1.c1.20100714.000000.cdf
%
%   OUT:
%       startdv         The start time stamp in date vector form        
%
indexs=find(filename=='.');
yi=indexs(2)+1;
mi=yi+4;
di=mi+2;

year=str2num(filename(yi:yi+3));
month=str2num(filename(mi:mi+1));
day=str2num(filename(di:di+1));
hour=0;
minute=0;
second=0;

startdv=[year month day hour minute second];
end