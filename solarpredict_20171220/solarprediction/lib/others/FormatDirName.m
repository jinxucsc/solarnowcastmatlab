%   function is a TOOL function to regulate the input directory name to
%   MATLAB format for later usage
%   VERSION:    1.0     2012-07-26
function outName=FormatDirName(inName)
if isempty(inName)
    outName=inName;
    return;
end
%   Replace the '\' windows version splitter into '/' 
outName=regexprep(inName, '\', '/');

%   add '/' to the end if necessary
if(outName(end)~='/')
    outName=sprintf('%s%s',outName,'/');
end

end