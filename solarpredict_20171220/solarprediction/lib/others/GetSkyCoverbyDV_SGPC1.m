%   function is designed for getting SC filename based on the datevector
%   given in the inputs. 
function filename=GetSkyCoverbyDV_SGPC1(datevector,site)
%sgptsiskycoverC1.b1.20120204.133400.cdf
prefix=['sgptsiskycover',site,'.b1'];  %sgpqcrad1longC1.c1
year=int2str(datevector(1));
month=int2str(datevector(2));
if(length(month)==1)
    month=strcat('0',month);
end
day=int2str(datevector(3));
if(length(day)==1)
    day=strcat('0',day);
end
filename=sprintf('%s.%s%s%s.000000.cdf',prefix,year,month,day);

end