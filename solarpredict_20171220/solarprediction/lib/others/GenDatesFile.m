%   Funtion is a tool for generating the dates.txt which is the input for
%   TSI downloading taken by script downloadbydate.py working on srv365-03
%       2012-01-01 00:00:00_2012-01-02 00:00:00;
function GenDatesFile(tmdns)
outputf='dates.txt';
l=length(tmdns);
fid=fopen(outputf,'w');
textstr='';
% ONEDAY_DN=datenum([0 0 1 0 0 0]);
for i=1:l
    tmdn=tmdns(i);
    tmdv=datevec(tmdn);
    nextdv=tmdv+[0 0 1 0 0 0];
    nextdn=datenum(nextdv);
    nextdv=datevec(nextdn);
    tmline=sprintf('%4d-%02d-%02d %02d:%02d:%02d_%4d-%02d-%02d %02d:%02d:%02d;\n',...
        tmdv(1),tmdv(2),tmdv(3),tmdv(4),tmdv(5),tmdv(6),...
        nextdv(1),nextdv(2),nextdv(3),nextdv(4),nextdv(5),nextdv(6)...
    );
    textstr=sprintf('%s%s',textstr,tmline);
end
fprintf(fid,'%s',textstr);
fclose(fid);

end