%   Function is to call a matlab function as a subprocess
function [status, result]=subprocess_matlab(matlabcmmd,maxinstn,servertype)
%   Check the matlab instance number, if exceeds the maximum number, the
%   matlab will not call subprocess until some of them finished. Check
%   routine runs for every 5 seconds
if nargin==1
    maxinstn=12;
    servertype=32;
elseif nargin==2
    servertype=32;
else
    
end

bwait=false;
n=CheckMatlabInst();
while n>=maxinstn
    bwait=true;
    pause(5);
    n=CheckMatlabInst();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   MATLAB environment settings
%   Before use the matlab subprocess, you need to make sure the parameters
%   settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nodisplay=1;
nodesktop=1;
if servertype==64
    glnx86=0;
else
    glnx86=1;
end
matlabbin='matlab';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nodisplay==1
    nodisplaystr='-nodisplay';
else
    nodisplaystr='';
end
if nodesktop==1
    nodesktopstr='-nodesktop';
else
    nodesktopstr='';
end
if glnx86==1
    glnx86str='-glnx86';
else
    glnx86str='';
end

if matlabcmmd(end)~=';'
    matlabcmmd=sprintf('%s;',matlabcmmd);
end
% 'matlab -glnx86 -r "test1(''4.mat'');exit;" -nodisplay -nodesktop &'
cmmd=sprintf('%s %s %s %s -r "%sexit;" &',...
    matlabbin,nodisplaystr,nodesktopstr,glnx86str,matlabcmmd);
[status, result]=system(cmmd);
%   Previous wait then wait for certain period instead of soon getting
%   start
if bwait==true
    pause(3);
end

end