%   Function is to get zenith and azimuth angles by giving multiple
%   datenumbers vector(tmdns, m x 1) and the location
%   zes azs in degree(m x 1)
%   INVALID value is set to be -1
function [azs zes]=GetSunAnglesbyDNs(tmdns,location)
m=length(tmdns);
tmdvs=datevec(tmdns);
zes=zeros(m,1);
azs=zeros(m,1);
for i=1:m
   if(tmdns(i)==0) 
       zes(i)=-1;
       azs(i)=-1;
       continue;
   end
   time.year    =  tmdvs(i,1);
   time.month   =  tmdvs(i,2);
   time.day     =  tmdvs(i,3);
   time.hour    =  tmdvs(i,4);
   time.min    =   tmdvs(i,5);
   time.sec    =   tmdvs(i,6);    %should be the same as 0
   time.UTC    =   0;
   sun = sun_position(time, location);
   zes(i)=sun.zenith/180*pi;
   azs(i)=sun.azimuth/180*pi;
end

end