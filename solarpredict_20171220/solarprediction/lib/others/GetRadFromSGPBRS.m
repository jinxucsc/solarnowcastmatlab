%   Function is designed to get irradiance from brs files. Due to the lack
%   of sqpqcrad1long data stream. Use the SGPBRS file instead for
%   extraction of DNI and DN
%   INPUT:
%       brsDir          --      Directory of all brs files
%   OUTPUT:
%       sgpbrsrad.mat   --      mat file, contains datenums of file name
%                               and brsradvals 1440 x 1
function GetRadFromSGPBRS(brsDir)
if ~exist(brsDir,'dir')
    disp('No such BRS Directory');
    return;
end
if(brsDir(end)~='/')
    brsDir=sprintf('%s%s',brsDir,'/');
end

ONEDAY=1440;            % radiation freq = 1min
allfiles=dir(brsDir);
[m,~]=size(allfiles);
brsdns=zeros(m,1);
brsradvals=zeros(ONEDAY,m);
for i=1:m
    if ~allfiles(i).isdir
        filename=allfiles(i).name;
        tmdv=GetDVFromRadFile(filename);
        ncpath=sprintf('%s%s',brsDir,filename);
        ncid=netcdf.open(ncpath,'NC_NOWRITE');
        varID1=netcdf.inqVarID(ncid,'down_short_diffuse_hemisp');
        varID2=netcdf.inqVarID(ncid,'short_direct_normal');
        Data1=netcdf.getVar(ncid,varID1);
        Data2=netcdf.getVar(ncid,varID2);
        if length(Data2)~=ONEDAY
            continue;       % data is not complete! skip this day
        end
        Data2(Data2<0)=0;
        Data1(Data1<0)=0;
        brsradvals(:,i)=Data2(:);
        brsdns(i)=datenum(tmdv);
    end
end
indexi=find(brsdns~=0);
brsdns=brsdns(indexi);
brsradvals=brsradvals(:,indexi);
save('sgpbrsrad.mat','brsdns','brsradvals');
end