function DirPath=GetDirPathFromPath(filename)
%   filename is relative path -> abspath
%   abspath -> abspath
splitchar='/';
abspath=GetAbspathFromFilename(filename);
if exist(abspath,'dir')
   %    1. directory then get parent directory path
   dirpath=FormatDirName(abspath);
   indexi=strfind(dirpath,splitchar);
   DirPath=dirpath(1:indexi(end-1));
elseif exist(abspath,'file')
    %    2. file then get directory path
   indexi=strfind(abspath,splitchar);
   DirPath=abspath(1:indexi(end));
else
    disp('[ERROR]: No such file or directory!');
end

end