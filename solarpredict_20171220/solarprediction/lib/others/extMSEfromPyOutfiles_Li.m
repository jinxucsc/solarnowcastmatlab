%   This function is designed to get all the MSE values from py script
%   output files(SVMSlipt.py). And scatter them into a plot
%   INPUT:
%       PyOutDir        --  Python output txt files
%       Dataoutf        --  Path for data output matfile
%   SVR Linear Formal output format:
%       %6f                 (C value only!)
%       10.000000
%
function extMSEfromPyOutfiles_Li(PyOutDir,Dataoutf)
switch nargin
    case 1
        PyOutDir=FormatDirName(PyOutDir);
        PyOutDirName=PyOutDir(1:end-1);     %remove the '/' from the end
        Dataoutf=sprintf('%s.mat',PyOutDirName);
    case 2
        %svroutf,UpperThres,LowerThres
    otherwise
        disp('[ERROR]: Invalid inputs!');
        return;
end
PyOutDir=FormatDirName(PyOutDir);

allfiles=dir(PyOutDir);
[m,~]=size(allfiles);
tmchar='.';
cvals=zeros(m,1);
gvals=zeros(m,1);
bvalidma=false(m,1);
MSEvals=zeros(m,1);
MSEstr='Meansquarederror=';
lenMSEstr=length(MSEstr);
spacestr=' ';
UpperC='C';
for i=1:m
    if allfiles(i).isdir==1
        MSEvals(i)=-1;
        continue;
    end
    filename=allfiles(i).name;
    try
        cvals(i)=str2double(filename);
    catch e
        MSEvals(i)=-1;
        continue;
    end
    bvalidma(i)=true;
    inputf=sprintf('%s%s',PyOutDir,filename);
    tmid=fopen(inputf,'r');
    tmstr=fscanf(tmid,'%s');
    fclose(tmid);
    %    tmstr=textscan(tmid,'%s');
    indexi=strfind(tmstr,MSEstr);
    if isempty(indexi)
        MSEvals(i)=-1;
        continue;
    else
        starti=indexi(1)+lenMSEstr;
        tmsubstr=tmstr(starti:end);
        indexi=strfind(tmsubstr,UpperC);
        endi=indexi(1)-1;
        MSEval=str2double(tmsubstr(1:endi));
        MSEvals(i)=MSEval;
    end
    
end
save(Dataoutf,'cvals','gvals','MSEvals');
%   Test for scatter plot
scatter_svr_li_cmse(Dataoutf,0.1,0);
end