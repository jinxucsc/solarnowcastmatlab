function X_norm=TO_NormOfMatrix(X,maxx,minx)
if nargin==1
    maxx=max(max(X));
    minx=min(min(X));
end
maxdist=(maxx-minx);

X_norm=(X-minx)./maxdist;


end