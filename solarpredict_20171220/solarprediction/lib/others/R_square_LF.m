%   Function is designed to calculate the R-square value between the
%   real values and predicted values.
function [rsq,ssr,sst]=R_square_LF(realvals,prevals)
l1=length(realvals);
l2=length(prevals);
if l1~=l2
    disp('Inputs doesn''t match in dim!');
end
diffval=realvals-prevals;
ssr=sum(diffval.^2);
sst=(l1-1)*var(realvals);

% y_bar=mean(realvals);
% ssr=0;
% sst=0;
% for i=1:l1
%     ssr=ssr+(prevals(i)-y_bar)^2;
%     sst=sst+(realvals(i)-y_bar)^2;
% end
% if(sst~=var(realvals)*l1)
%     disp('Test of Errors');
% end
if(sst~=0)
    rsq=1-ssr/sst;
else
    rsq=0;
end

end