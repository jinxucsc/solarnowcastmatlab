%   VISFilter_daytime is to filter out cutted BNL VIS images with these
%   criterium:
%       1.  daylight time (EST 7:00AM ~ 17:00PM)
%       2.  no dark image which (black < 80%)
%       3.  G13 in name (remove G15 pattern)
%       4.  No bad format of jpg
%       5.  No bad filename (remove lastest.mat or )
%       6.  Filter with other channels (must be jpg format and exist)
%       INPUT:
%           oriSIDir        --- original SI directory
%               vis/ ir2/ ir3/ ir4/ ir6/
%           newSIDir        --- new SI directory
%               vis/ ir2/ ir3/ ir4/ ir6/
%       OUTPUT:
%           newSIDir        --- new SI directory
%               vis/* ir2/* ir3/* ir4/* ir6/*
%
%   VERSION 1.0         2013-02-12
function VISFilter_daytime(oriSIDir,newSIDir)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%PARAMETERS SETTINGS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

starth=7;
endh=17;
odstartdn=datenum([0 0 0 starth 0 0]);
odenddn=datenum([0 0 0 endh 0 0]);
VISBlackThres=0.8;


NCHANNELS=5;        %   5 Channels at present
nchannels=NCHANNELS;
TAIL='G13I01';       %   Channel 1 TAIL for generation of VIS filename
TAIL1=TAIL;
TAIL2='G13I02';       %   Channel 2 TAIL for generation of CHL#2 filename
TAIL3='G13I03';       %   Channel 3 TAIL for generation of CHL#3 filename
TAIL4='G13I04';       %   Channel 4 TAIL for generation of CHL#4 filename
TAIL5='G13I05';       %   Channel 5 TAIL for generation of CHL#5 filename
TAIL6='G13I06';       %   Channel 6 TAIL for generation of CHL#6 filename

oriSIDir=FormatDirName(oriSIDir);
newSIDir=FormatDirName(newSIDir);
mkdir(newSIDir);

bnlcutDir=sprintf('%svis/',oriSIDir);
chl2Dir=sprintf('%sir2/',oriSIDir);
chl3Dir=sprintf('%sir3/',oriSIDir);
chl4Dir=sprintf('%sir4/',oriSIDir);
chl6Dir=sprintf('%sir6/',oriSIDir);
newbnlcutDir=sprintf('%svis/',newSIDir);
newchl2Dir=sprintf('%sir2/',newSIDir);
newchl3Dir=sprintf('%sir3/',newSIDir);
newchl4Dir=sprintf('%sir4/',newSIDir);
newchl6Dir=sprintf('%sir6/',newSIDir);


mkdir(newbnlcutDir);
mkdir(newchl2Dir);
mkdir(newchl3Dir);
mkdir(newchl4Dir);
mkdir(newchl6Dir);

bnlcutDir=FormatDirName(bnlcutDir);

allfiles=dir(bnlcutDir);
[m,~]=size(allfiles);       %   m = n of SI
for i=1:m
    if(allfiles(i).isdir==1)
        continue;   %   if dir then skip this file
    end
    
    % Filter in filename
    filename=allfiles(i).name;
    try
        gmtdv=SI_TIMEExT(filename);
    catch e
        dispstr=sprintf('%s is not in formal VIS file format',filename);
        disp(dispstr);
        continue;
    end
    chl2f=GetSIfromDV(gmtdv,TAIL2);
    chl3f=GetSIfromDV(gmtdv,TAIL3);
    chl4f=GetSIfromDV(gmtdv,TAIL4);
    chl6f=GetSIfromDV(gmtdv,TAIL6);
    
    
    % Filter of G15 or other irregular filename
    if length(gmtdv)==1 || gmtdv(1)==0
        dispstr=sprintf('%s is not in formal VIS file format',filename);
        disp(dispstr);
        continue;
    end
    
    
    % Filter in daylight time
    tmdv=GMT2EST(gmtdv);    % to local time EST
    daydv=tmdv;
    daydv(1)=0;
    daydv(2)=0;
    daydv(3)=0;
    daydn=datenum(daydv);
    if daydn<odstartdn || daydn>odenddn
        dispstr=sprintf('%s is not in daylight time range',filename);
        disp(dispstr);
        continue;
    end
    visf=sprintf('%s%s',bnlcutDir,filename);
    outputf=sprintf('%s%s',newbnlcutDir,filename);
    
    
    % Filter of other channels
    orichl2f=sprintf('%s%s',chl2Dir,chl2f);
    orichl3f=sprintf('%s%s',chl3Dir,chl3f);
    orichl4f=sprintf('%s%s',chl4Dir,chl4f);
    orichl6f=sprintf('%s%s',chl6Dir,chl6f);
    newchl2f=sprintf('%s%s',newchl2Dir,chl2f);
    newchl3f=sprintf('%s%s',newchl3Dir,chl3f);
    newchl4f=sprintf('%s%s',newchl4Dir,chl4f);
    newchl6f=sprintf('%s%s',newchl6Dir,chl6f);
    if ~exist(orichl2f,'file')
        dispstr=sprintf('%s doesnot exist!',orichl2f);
        disp(dispstr);
        continue;
    end
    if ~exist(orichl3f,'file')
        dispstr=sprintf('%s doesnot exist!',orichl3f);
        disp(dispstr);
        continue;
    end
    if ~exist(orichl4f,'file')
        dispstr=sprintf('%s doesnot exist!',orichl4f);
        disp(dispstr);
        continue;
    end
    if ~exist(orichl6f,'file')
        dispstr=sprintf('%s doesnot exist!',orichl6f);
        disp(dispstr);
        continue;
    end
    try
        t=imread(orichl2f);
    catch e
        % chl2 ~ 6 are not regular jpg format
        dispstr=sprintf('%s is not in regular tif format!',orichl2f);
        disp(dispstr);
        continue;
    end
    try
        t=imread(orichl3f);
    catch e
        % chl2 ~ 6 are not regular jpg format
        dispstr=sprintf('%s is not in regular tif format!',orichl3f);
        disp(dispstr);
        continue;
    end
    try
        t=imread(orichl4f);
    catch e
        % chl2 ~ 6 are not regular jpg format
        dispstr=sprintf('%s is not in regular tif format!',orichl4f);
        disp(dispstr);
        continue;
    end
    try
        t=imread(orichl6f);
    catch e
        % chl2 ~ 6 are not regular jpg format
        dispstr=sprintf('%s is not in regular tif format!',orichl6f);
        disp(dispstr);
        continue;
    end
    
    
    % Filter of image format
    try
        t=load(visf,'-mat');
        visma=t.outputm;
    catch e
        dispstr=sprintf('%s is bad image and cannot be resolved!',visf);
        disp(dispstr);
        continue;
    end
    
    
    % Filter of Black Threshold
    [SVISH,SVISW]=size(visma);
    if double(length(find(visma<10)))/(SVISH*SVISW) >= 0.8
        % black area larger than 80%, then skip this image
        dispstr=sprintf('%s is too dark or damaged so it cannot be resolved!',visf);
        disp(dispstr);
        continue;
    end
    
    
    % copyfile
    copyfile(visf,outputf);
    copyfile(orichl2f,newchl2f);
    copyfile(orichl3f,newchl3f);
    copyfile(orichl4f,newchl4f);
    copyfile(orichl6f,newchl6f);
end

end