function bInrange=IsDNInDay(tmdn,daystartdn,dayenddn)
if tmdn<daystartdn||tmdn>dayenddn
    bInrange=0;
else
    bInrange=1;
end
end