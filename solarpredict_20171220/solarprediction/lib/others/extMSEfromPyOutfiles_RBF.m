%   This function is designed to get all the MSE values from py script
%   output files(SVMSlipt.py). And scatter them into a plot
%   INPUT:
%       PyOutDir        --  Python output txt files
%       Dataoutf        --  Path for data output matfile
%   SVR RBF Formal output format:
%       %6f_%6f
%       10.000000_8.000000
%
function extMSEfromPyOutfiles_RBF(PyOutDir,Dataoutf)
switch nargin
    case 1
        PyOutDir=FormatDirName(PyOutDir);
        PyOutDirName=PyOutDir(1:end-1);     %remove the '/' from the end
        Dataoutf=sprintf('%s.mat',PyOutDirName);
    case 2
        %svroutf,UpperThres,LowerThres
    otherwise
        disp('[ERROR]: Invalid inputs!');
        return;
end
PyOutDir=FormatDirName(PyOutDir);

allfiles=dir(PyOutDir);
[m,~]=size(allfiles);
tmchar='_';
cvals=zeros(m,1);
gvals=zeros(m,1);
bvalidma=false(m,1);
MSEvals=zeros(m,1);
MSEstr='Meansquarederror=';
lenMSEstr=length(MSEstr);
spacestr=' ';
UpperC='C';
for i=1:m
    filename=allfiles(i).name;
    indexi=strfind(filename,tmchar);
    if isempty(indexi)
        continue;    % no '_' means no need
    end
    try
        cvals(i)=str2double(filename(1:indexi-1));
        gvals(i)=str2double(filename(indexi+1:end));
    catch e
        continue;
    end
    bvalidma(i)=true;
    inputf=sprintf('%s%s',PyOutDir,filename);
    tmid=fopen(inputf,'r');
    tmstr=fscanf(tmid,'%s');
    fclose(tmid);
    %    tmstr=textscan(tmid,'%s');
    indexi=strfind(tmstr,MSEstr);
    if isempty(indexi)
        MSEvals(i)=NaN;
        continue;
    else
        starti=indexi(1)+lenMSEstr;
        tmsubstr=tmstr(starti:end);
        indexi=strfind(tmsubstr,UpperC);
        endi=indexi(1)-1;
        MSEval=str2double(tmsubstr(1:endi));
        MSEvals(i)=MSEval;
    end
    
end
save(Dataoutf,'cvals','gvals','MSEvals');
%   Test for scatter plot
scatter_svr_cgmse(Dataoutf);
end