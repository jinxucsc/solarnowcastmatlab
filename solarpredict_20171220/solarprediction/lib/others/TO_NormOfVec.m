function [newX maxX,minX,lenX]=TO_NormOfVec(X,minX,maxX)
assert(nargin==1|nargin==3,'error of input');
if nargin==1
lenX=length(X);
minX=min(X);
maxX=max(X);
newX=(X-minX)./(maxX-minX);
elseif nargin==3
    assert(minX<maxX,'Did you put wrong order of min and max?');
    lenX=length(X);
    newX=(X-minX)./(maxX-minX);
end