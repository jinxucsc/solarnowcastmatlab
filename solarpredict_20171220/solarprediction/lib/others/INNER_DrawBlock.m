function outma=INNER_DrawBlock(tmma,BLOCKNP,py,px)
smallma=tmma(py-BLOCKNP:py+BLOCKNP,px-BLOCKNP:px+BLOCKNP);
outma=tmma;
%   Edge should be 
smallma(:,1)=0;
smallma(:,2*BLOCKNP+1)=0;
smallma(1,:)=0;
smallma(2*BLOCKNP+1,:)=0;
outma(py-BLOCKNP:py+BLOCKNP,px-BLOCKNP:px+BLOCKNP)=smallma(:,:);
end