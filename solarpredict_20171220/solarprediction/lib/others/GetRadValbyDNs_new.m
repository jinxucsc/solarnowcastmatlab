%   Function is designed to get radiation vals from reading radDir when
%   given the datenumber vector(tmdns) and the sitename(site). Modification
%   is that use rad matfile as the default radiation dataset, instead of
%   instead of previous radDir for qcrad1long file.
%
%   The INVALID tmdns(tmdns = 0) and no record in radDir, will be shown as
%   radvals = -1. All radvals < 0 but not -1 will be 0.
%   INPUT:
%       tmdns   --  Datenumbers, requested DNs (mx1)  
%       radmatf --  String, Path of allrad mat file
%       site    --  String, SGP sitename 
%       meanl   --  mean value length mean(rad-meanl:rad+meanl)
%                   0 -> no mean
%   OUTPUT:
%       radvals --  vector (mx1)
%       csvals  --  vector (mx1)
%   VERSION:    1.1     Date:   2012-07-26
%   VERSION:    1.2     2012-08-01
%       use radcs.mat for input instead; output csvals too.
function [radvals csvals]= GetRadValbyDNs_new(tmdns,radmatf,site,meanl)
if ~strcmpi(site,'c1')
    disp('[ERROR]: This version is used for SGP c1 test ONLY!');
end
t=load(radmatf);
allraddns=t.raddns;
allradvals=t.radvals;
allcsvals=t.csvals;
tmdaydvs=datevec(allraddns);
tmdaydvs(:,4)=0;
tmdaydvs(:,5)=0;
tmdaydvs(:,6)=0;
daydns=datenum(tmdaydvs);
daydns=unique(daydns);
daydns=daydns(daydns>0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Suppose the granuality of radiation is 1min, 1440 per day
gsetting =  load('bnl_SI_CV_Modeling.mat');
ONEDAY=gsetting.ONEDAY_MIN;
UNIT=gsetting.ONEMIN_DN;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tmdvs=datevec(tmdns);
[m,~]=size(tmdvs);
radvals=zeros(m,1);
csvals=zeros(m,1);
for i=1:m
    tmdv=tmdvs(i,:);
    if tmdv(1)==0||tmdns(i)==0
        radvals(i)=-1;
        csvals(i)=-1;
        continue;
    end
    daydv=tmdv;
    daydv(4)=0;
    daydv(5)=0;
    daydv(6)=0;
    daydn=datenum(daydv);
    iday=find(daydns==daydn,1);
    if isempty(iday)
        continue;   % no record of the day found in rad mat file
    end
    indexi=floor((datenum(tmdv)-datenum(daydv))/UNIT)+1;
    if(indexi>meanl&&indexi<ONEDAY-meanl)
        tmdni=allradvals(indexi-meanl:indexi+meanl,iday);
        tmdni=tmdni(tmdni>=0);
        radvals(i)=mean(tmdni);
        tmdni=allcsvals(indexi-meanl:indexi+meanl,iday);
        tmdni=tmdni(tmdni>=0);
        csvals(i)=mean(tmdni);
    elseif indexi<meanl
        tmdni=allradvals(indexi:indexi+meanl,iday);
        tmdni=tmdni(tmdni>=0);
        radvals(i)=mean(tmdni);
        tmdni=allcsvals(indexi:indexi+meanl,iday);
        tmdni=tmdni(tmdni>=0);
        csvals(i)=mean(tmdni);
    else
        tmdni=allradvals(indexi-meanl:indexi,iday);
        tmdni=tmdni(tmdni>=0);
        radvals(i)=mean(tmdni);
        tmdni=allcsvals(indexi-meanl:indexi,iday);
        tmdni=tmdni(tmdni>=0);
        csvals(i)=mean(tmdni);
    end
    if isnan(radvals(i))
        i
        radvals(i)=0;
    end
    if isnan(csvals(i))
        csvals(i)=0;
    end
end
indexi=find(radvals<0&radvals~=-1);
radvals(indexi)=0;
csvals(indexi)=0;
end
