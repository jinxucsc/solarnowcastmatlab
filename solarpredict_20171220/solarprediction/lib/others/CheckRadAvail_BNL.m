%   After downloading and ploting out the radiation values from DB:
%   MG_BNLDBRadPlot([2012 02 01 0 0 0],[2012 08 01 0 0 0],'/home/mikegrup/Downloads/D/mat/radiation/bnl/bnlradtest_DB/','/home/mikegrup/Downloads/D/mat/radiation/bnl/bnlradtest_DBimg/',30,'linux');)
%   MANUALLY pick the available dates from the plots and delete the cases
%   which have:
%       1. irregular jumps in the night time
%       2. containing any +7999 values due to device failure
%   This function will generate the available dates based on filtered plots
%   directory. Use the available dates to generate bnlrad.mat for later
%   usage instead of reading files iteratively.
%   INPUT:
%       BNLRadDir       --- Directory, containing the filtered plots
%   OUTPUT:
%       BNLRadAvail.mat --- Mat file, includes available dates
%       bnlrad.mat      --- Mat file, includes all the radiation vals and
%                           datenumber
%           
%   VERSION: 1.0                2012-08-15       
function CheckRadAvail_BNL(BNLRadDir,BNLRadImgDir)
BNLRadDir=FormatDirName(BNLRadDir);
BNLRadImgDir=FormatDirName(BNLRadImgDir);
allfiles=dir(BNLRadImgDir);
[m,~]=size(allfiles);
alltmdns=[];
testdns=[];
alltmrads=[];
for i=1:m
    if allfiles(i).isdir==1
        continue;
    end
    filename=allfiles(i).name;
    tmdaydv=GetDVfromRad(filename);
    testdns=[testdns;datenum(tmdaydv)];
    prefixname=filename(1:end-4);   % remove .jpg
    inputf=sprintf('%s%s',BNLRadDir,prefixname);
    t=load(inputf);
    tmdns=t.onedayrad(:,1);
    tmrads=t.onedayrad(:,2);
    alltmdns=[alltmdns tmdns];
    alltmrads=[alltmrads tmrads];
end
alltmdvs=datevec(alltmdns);
alltmdvs(:,4)=0;
alltmdvs(:,5)=0;
alltmdvs(:,6)=0;
daydns=datenum(alltmdvs);
daydns=unique(daydns);

save('BNLRadAvail.mat','alltmdns','daydns');
raddns=alltmdns;
radvals=alltmrads;
save('bnlrad.mat','raddns','radvals','daydns');
end