function scatter_svr_li_cmse(svroutf,UpperThres,LowerThres)
switch nargin
    case 1
        %svroutf
        UpperThres=0.04;
        LowerThres=0;
    case 3
        %svroutf,UpperThres,LowerThres
    otherwise
        disp('[ERROR]: Invalid inputs!');
        return;
end
load(svroutf);
set(0,'DefaultFigureVisible', 'off');
indexi=find(MSEvals<UpperThres&MSEvals>LowerThres);
newmse=MSEvals(indexi);
newc=cvals(indexi);

pic1=figure;
hold on;
grid on;
titStr=sprintf('Convergence test of C vs. MSE');
xStr='log2c';%
yStr='MSE';
h=scatter(newc,newmse);
[minval,mini]=min(newmse);
x = get(h,'XData'); 		% Get the plotted data
y = get(h,'YData');
xval = x(mini);		% Find the index of the min and max
yval = y(mini);
plot(xval,yval,'+b');
minvalstr=sprintf('min mse val is %6f, log2c = %f',...
    minval,newc(mini));
text(xval+0.25,yval+0.005,minvalstr,...
	'VerticalAlignment','middle',...
	'HorizontalAlignment','left',...
	'FontSize',14);
title(titStr);
xlabel(xStr);
ylabel(yStr);
hold off;
plotoutf=sprintf('%s.jpg',svroutf);
print(pic1,plotoutf,'-djpeg');
end