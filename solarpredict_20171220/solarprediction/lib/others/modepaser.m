function bStringIn=modepaser(mode,string_str)
bStringIn=nan;
if ~ischar(mode)
    disp('mode you give here should be string/char');
    return;
end
if ~ischar(string_str)
    disp('You should give the parser string as input');
    return;
end

delimiters={' ','\f','\n','\r','\t','\v','-','+',','};
try
    C=strsplit(mode,delimiters);
catch
    mode1=mode;
    regexprep(mode1,'[\f\n\r\t\v-+,]',' ');
    C=strsplit(mode1);
end

lc=length(C);
bStringIn=false;
for i=1:lc
    if strcmpi(C{i},string_str);
        bStringIn=true;
        break;
    end
end
    
end