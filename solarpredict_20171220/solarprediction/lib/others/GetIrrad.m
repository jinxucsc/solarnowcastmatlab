%Given the nc file path, it then get down_short_diffuse_hemisp and
%short_direct_normal value out, and put them into two arrays.
%   IN:     ncpath  absolute path of netCDF file
%   OUT:    Radia   n*1440 array, n stands for days in netCDF file
%           Norm    Nomalized n*1440 array, n stands for days in netCDF file
%      
function [Radia,Norm,DataN1,DataN2,Timest]=GetIrrad(ncpath)
%% netCDF Read 
ncid=netcdf.open(ncpath,'NC_NOWRITE');
varID1=netcdf.inqVarID(ncid,'down_short_diffuse_hemisp');
varID2=netcdf.inqVarID(ncid,'short_direct_normal');
Data1=netcdf.getVar(ncid,varID1);
Data2=netcdf.getVar(ncid,varID2);
Timest=0;
if 0
%test for timestamp
varID_time=netcdf.inqVarID(ncid,'base_time');
base_time=netcdf.getVar(ncid,varID_time);
varID_time=netcdf.inqVarID(ncid,'time_offset');
time_offset=netcdf.getVar(ncid,varID_time);
varID_time=netcdf.inqVarID(ncid,'time');
time=netcdf.getVar(ncid,varID_time);
end

if 0
% test for resolution
varID=netcdf.inqVarID(ncid,'resolution_description');
resolution_description=netcdf.getVar(ncid,varID);
varID=netcdf.inqVarID(ncid,'image_display_height');
image_display_height=netcdf.getVar(ncid,varID);
varID=netcdf.inqVarID(ncid,'image_display_width');
image_display_width=netcdf.getVar(ncid,varID);
end

[m,n]=size(Data1);
Radia=zeros(m,1);
min=Data1(1,1)+Data2(1,1);
max=Data1(1,1)+Data2(1,1);
DataN1=Data1;
DataN1(DataN1<0)=0;
DataN2=Data2;
DataN2(DataN2<0)=0;

Radia=DataN1+DataN2;
Norm=Radia;

if 0
    for i= 1:m
        Radia(i,1)=DataN1(i,1)+DataN2(i,1);
        if(Radia(i,1)>max)
            max=Radia(i,1);
            pos1=i;
        end
        if(Radia(i,1)<min)
            min=Radia(i,1);
            pos2=i;
        end

    end
    %normalize
    dist=max-min;
    Norm=zeros(m,1);
    for i=1:m
        Norm(i,1)=(Radia(i,1)-min)/dist;
    end
end
%get the jump points and its timestamp




%% Draw pic paras
ONEDAY=1440;
StartM=[0	31	60	91	121	152	182	213	244	274	305	335];
EndM=[31	60	91	121	152	182	213	244	274	305	335	366];
set(0,'DefaultFigureVisible', 'on');
%set(0,'DefaultFigureVisible', 'off');

%% One day
if 0
    pikmon=0;
    pikday=0;
    drawset=Norm(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    drawset=NormalizeArray(drawset);
    pic1=figure;  
    plot(drawset);
    title('One day Solar Radiation Plot');grid on;
    hold on;
    set(gca,'XTick',0:60:1440);
    timelabel=[18 19 20 21 22 23 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18];
    set(gca,'XTickLabel',timelabel);
    legend('One day Radiation','One day Radia',4)
    hold off;

    pic2=figure;  
    title('One day Solar Radiation Plot');grid on;
    hold on;
    drawset=DataN1(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    plot(drawset);
    drawset=DataN2(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    plot(drawset,'r');
    set(gca,'XTick',0:60:1440);
    timelabel=[18 19 20 21 22 23 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18];
    set(gca,'XTickLabel',timelabel);
    legend('hemisp','Normal',4);
    hold off;    
end
%% all value
if 0
%   one day data
pikmon=0;
pikday=0;
drawset=Norm(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
drawset=NormalizeArray(drawset);
pic1=figure;  
plot(drawset);
title('One day Solar Radiation Plot');grid on;
hold on;
set(gca,'XTick',0:60:1440);
timelabel=[18 19 20 21 22 23 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18];
set(gca,'XTickLabel',timelabel);
legend('One day Radiation','One day Radia',4)
hold off;

pic2=figure;  
title('One day Solar Radiation Plot');grid on;
hold on;
drawset=DataN1(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
plot(drawset);
drawset=DataN2(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
plot(drawset,'r');
set(gca,'XTick',0:60:1440);
timelabel=[18 19 20 21 22 23 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18];
set(gca,'XTickLabel',timelabel);
legend('hemisp','Normal',4);
hold off;


%   one month data(30 days)
pikday=0;       %picked day starts from 1
pikmon=1;       %1-12
%   StartDay=StartM(pikmon);
%   EndDay=EndM(pikmon);
for i =1: 12    %12 months
    pikmon=i;   
    StartDay=StartM(pikmon)+pikday;
    EndDay=EndM(pikmon)+pikday;
    drawset=Norm(1+StartDay*ONEDAY:EndDay*ONEDAY);
    drawset=NormalizeArray(drawset);
    
    pic1=figure;    
    plot(drawset);
    hold on;
    set(gca,'XTick',1:1440:31*1440+1);
    set(gca,'XTickLabel',1:1:32);   
    title(strcat('Monthly Solar Radiation Plot of Month ',int2str(i)));
    grid on;
    hold off;
    outputstr=strcat('d:\\img\\Monthly',int2str(i),'.jpg');
    print(pic1,'-djpeg ',outputstr);
    
    
    pic2=figure;
    hold on;
    drawset=DataN1(1+StartDay*ONEDAY:EndDay*ONEDAY);
    plot(drawset);
    drawset=DataN2(1+StartDay*ONEDAY:EndDay*ONEDAY);
    plot(drawset,'r');
    set(gca,'XTick',1:1440:31*1440+1);
    set(gca,'XTickLabel',1:1:32);
    title(strcat('Monthly Solar Radiation Plot of Month ',int2str(i)));
    grid on;
    %legend('H','N');
    hold off;
    outputstr=strcat('d:\\img1\\Monthly',int2str(i),'.jpg');
    print(pic2,'-djpeg ',outputstr);
    
end


%   5 days data
pikmon=1;
pikday=0;
DAYS=5;
for i=1:12
    pikmon=i;   
    StartDay=StartM(pikmon)+pikday;
    EndDay=StartDay+DAYS;
    
    drawset=Norm(1+StartDay*ONEDAY:EndDay*ONEDAY);
    drawset=NormalizeArray(drawset);
    pic1=figure;
    plot(drawset);
    set(gca,'XTick',1:1440:DAYS*1440+1);
    set(gca,'XTickLabel',1:1:DAYS+1);
    title(strcat(int2str(DAYS),' days Solar Radiation Plot of Month',int2str(i)));
    grid on;
    %legend('5 days Radiation',4);
    outputstr=strcat('d:\\img\\days',int2str(i),'.jpg');
    print(pic1,'-djpeg ',outputstr);

    pic2=figure;
    hold on;
    drawset=DataN1(1+StartDay*ONEDAY:EndDay*ONEDAY);
    plot(drawset);
    drawset=DataN2(1+StartDay*ONEDAY:EndDay*ONEDAY);
    plot(drawset,'r');
    set(gca,'XTick',1:1440:DAYS*1440+1);
    set(gca,'XTickLabel',1:1:DAYS+1);
    title(strcat(int2str(DAYS),' days Solar Radiation Plot of Month',int2str(i)));
    grid on;
    %legend('hemsp','normal',4);
    hold off;
    outputstr=strcat('d:\\img1\\days',int2str(i),'.jpg');
    print(pic2,'-djpeg ',outputstr);
    
end


%   one year data
    pikmon=0;
    pikday=1;
    pikyear=0;
    DAYS=365;
    StartDay=pikday;
    EndDay=StartDay+DAYS;
    
    drawset=Norm(1+StartDay*ONEDAY:EndDay*ONEDAY);
    drawset=NormalizeArray(drawset);
    pic1=figure;
    plot(drawset);
    hold on;
    set(gca,'XTick',1:1440*30:360*1440+1);
    set(gca,'XTickLabel',1:12);
   % set(gca,'XTickLabel',['Jan' 'Feb' 'Mar' 'Apr' 'May' 'Jun' 'Jul' 'Aug' 'Sep' 'Oct' 'Nov' 'Dec']);
    title('One year Solar Radiation Plot');grid on;
    %legend('One year Radiation value',4);
    hold off;
    outputstr=strcat('d:\\img\\annual','.jpg');
    print(pic1,'-djpeg ',outputstr);

    pic2=figure;
    hold on;
    drawset=DataN1(1+StartDay*ONEDAY:EndDay*ONEDAY);
    plot(drawset);
    drawset=DataN2(1+StartDay*ONEDAY:EndDay*ONEDAY);
    plot(drawset,'r');
    set(gca,'XTick',1:1440*30:360*1440+1);
    set(gca,'XTickLabel',1:12);
    %set(gca,'XTickLabel',['Jan' 'Feb' 'Mar' 'Apr' 'May' 'Jun' 'Jul' 'Aug' 'Sep' 'Oct' 'Nov' 'Dec']);
    title('One year Solar Radiation Plot');grid on;
    %legend('hemsp','normal',4);
    hold off;
    outputstr=strcat('d:\\img1\\annual','.jpg');
    print(pic2,'-djpeg ',outputstr);
    
end

%% Max Radia value
%   5 day avg
if 0
DAYS=5;

for j=1:12
    avgday=zeros(1440,1);
    avghemi=zeros(1440,1);
    avgnorm=zeros(1440,1);
       
    for i=1:DAYS
        %for maximum
        for k=1:ONEDAY
            if(avgday(k,1)<Radia(1+(StartM(j)+i-1)*ONEDAY+(k-1),1))
                avgday(k,1)=Radia(1+(StartM(j)+i-1)*ONEDAY+(k-1),1);
            end
            if(avghemi(k,1)<DataN1(1+(StartM(j)+i-1)*ONEDAY+(k-1),1))
                avghemi(k,1)=DataN1(1+(StartM(j)+i-1)*ONEDAY+(k-1),1);
            end
            if(avgnorm(k,1)<DataN2(1+(StartM(j)+i-1)*ONEDAY+(k-1),1))
                avgnorm(k,1)=DataN2(1+(StartM(j)+i-1)*ONEDAY+(k-1),1);            
            end
        end
        
       %for avg
       %avgday=avgday+Radia(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);
       %avghemi=avghemi+DataN1(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);
       %avgnorm=avgnorm+DataN2(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);

    end
    avgday=avgday/DAYS;
    avghemi=avghemi/DAYS;
    avgnorm=avgnorm/DAYS;
    
    %one day data
    pikday=0;
    drawset=avgday(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    drawset=NormalizeArray(drawset);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(SGP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if 1
        drawset1=drawset(10*60+1:1440,1);
        drawset1=[drawset1(:,1);drawset(1:4*60,1)];
        ONEDAY_NEW=1080;    %18 hours to draw
        timelabel=[4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22];
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(TWP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if 0
        drawset1=drawset(18*60+1:1440,1);
        drawset1=[drawset1(:,1);drawset(1:12*60,1)];
        ONEDAY_NEW=1080;    %18 hours to draw
        timelabel=[4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22];
    end
    
    pic1=figure;  
    plot(drawset1);
    title(strcat('5 days Max Solar Radiation Plot Month ',int2str(j),' Date from 1 to ',int2str(DAYS)));
    grid on;
    hold on;
    set(gca,'XTick',1:60:ONEDAY_NEW+1);     %set hours+1 labels
        set(gca,'XTickLabel',timelabel);
    %legend('One day Radiation',4);
    hold off;
    outputstr=strcat('d:\\img\\daysmax',int2str(j),'.jpg');
    print(pic1,'-djpeg ',outputstr);
    
    pic2=figure;  
    title(strcat('5 days Max Solar Radiation Plot Month ',int2str(j),' Date from 1 to ',int2str(DAYS)));
    grid on;
    hold on;
    drawset=avghemi(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    %SGP
    drawset1=drawset(10*60+1:1440,1);
    drawset1=[drawset1(:,1);drawset(1:4*60,1)];         
    %TWP
    %drawset1=drawset(18*60+1:1440,1);
    %drawset1=[drawset1(:,1);drawset(1:12*60,1)];        
    
    plot(drawset1);
    drawset=avgnorm(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    %SGP
    drawset1=drawset(10*60+1:1440,1);
    drawset1=[drawset1(:,1);drawset(1:4*60,1)];         
    %TWP
    %drawset1=drawset(18*60+1:1440,1);
    %drawset1=[drawset1(:,1);drawset(1:12*60,1)];        
    
    plot(drawset1,'r');
    set(gca,'XTick',1:60:ONEDAY_NEW+1);
    set(gca,'XTickLabel',timelabel);
    %legend('One day Radiation',4);
    hold off;
    outputstr=strcat('d:\\img1\\daysmax',int2str(j),'.jpg');
    print(pic2,'-djpeg ',outputstr);
end


%   one year avg
avgday=zeros(1440,1);
avghemi=zeros(1440,1);
avgnorm=zeros(1440,1);
DAYS=366;
for i=1:DAYS
        %for maximum
        for k=1:ONEDAY
            if(avgday(k,1)<Radia(1+(StartM(1)+i-1)*ONEDAY+(k-1),1))
                avgday(k,1)=Radia(1+(StartM(1)+i-1)*ONEDAY+(k-1),1);
            end
            if(avghemi(k,1)<DataN1(1+(StartM(1)+i-1)*ONEDAY+(k-1),1))
                avghemi(k,1)=DataN1(1+(StartM(1)+i-1)*ONEDAY+(k-1),1);
            end
            if(avgnorm(k,1)<DataN2(1+(StartM(1)+i-1)*ONEDAY+(k-1),1))
                avgnorm(k,1)=DataN2(1+(StartM(1)+i-1)*ONEDAY+(k-1),1);            
            end
        end
        
       %for avg
       %avgday=avgday+Radia(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);
       %avghemi=avghemi+DataN1(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);
       %avgnorm=avgnorm+DataN2(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);
end
    avgday=avgday/DAYS;
    avghemi=avghemi/DAYS;
    avgnorm=avgnorm/DAYS;
    %one day data
    pikday=0;
    drawset=avgday(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    drawset=NormalizeArray(drawset);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(SGP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if 1
        drawset1=drawset(10*60+1:1440,1);
        drawset1=[drawset1(:,1);drawset(1:4*60,1)];
        ONEDAY_NEW=1080;    %18 hours to draw
        timelabel=[4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22];
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(TWP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if 0
        drawset1=drawset(18*60+1:1440,1);
        drawset1=[drawset1(:,1);drawset(1:12*60,1)];
        ONEDAY_NEW=1080;    %18 hours to draw
        timelabel=[4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22];
    end
    
    pic1=figure;  
    plot(drawset1);
    title('Max day Solar Radiation Plot of a year');
    grid on;
    hold on;
    set(gca,'XTick',1:60:ONEDAY_NEW+1);
    set(gca,'XTickLabel',timelabel);
    %legend('One day Radiation',4);
    hold off;
    outputstr=strcat('d:\\img\\yearmax','.jpg');
    print(pic1,'-djpeg ',outputstr);
    
    pic2=figure;  
    title('Max day Solar Radiation Plot of a year');
    grid on;
    hold on;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(TWP)or drawset(SGP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    drawset=avghemi(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    %SGP
    drawset1=drawset(10*60+1:1440,1);
    drawset1=[drawset1(:,1);drawset(1:4*60,1)];         
    %TWP
    %drawset1=drawset(18*60+1:1440,1);
    %drawset1=[drawset1(:,1);drawset(1:12*60,1)]; 
    
    plot(drawset1);
    drawset=avgnorm(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    %SGP
    drawset1=drawset(10*60+1:1440,1);
    drawset1=[drawset1(:,1);drawset(1:4*60,1)];         
    %TWP
    %drawset1=drawset(18*60+1:1440,1);
    %drawset1=[drawset1(:,1);drawset(1:12*60,1)];
    
    plot(drawset1,'r');
    set(gca,'XTick',1:60:ONEDAY_NEW+1);
    set(gca,'XTickLabel',timelabel);
    %legend('hemisp','Normal',4);
    hold off;
    outputstr=strcat('d:\\img1\\yearmax','.jpg');
    print(pic2,'-djpeg ',outputstr);
    

%   One month avg
for j=1:1:12
    DAYS=EndM(j)-StartM(j);
    avgday=zeros(1440,1);
    avghemi=zeros(1440,1);
    avgnorm=zeros(1440,1);
    for i=1:DAYS
        %for maximum
        for k=1:ONEDAY
            if(avgday(k,1)<Radia(1+(StartM(1)+i-1)*ONEDAY+(k-1),1))
                avgday(k,1)=Radia(1+(StartM(1)+i-1)*ONEDAY+(k-1),1);
            end
            if(avghemi(k,1)<DataN1(1+(StartM(1)+i-1)*ONEDAY+(k-1),1))
                avghemi(k,1)=DataN1(1+(StartM(1)+i-1)*ONEDAY+(k-1),1);
            end
            if(avgnorm(k,1)<DataN2(1+(StartM(1)+i-1)*ONEDAY+(k-1),1))
                avgnorm(k,1)=DataN2(1+(StartM(1)+i-1)*ONEDAY+(k-1),1);            
            end
        end
     
       %for avg
       %avgday=avgday+Radia(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);
       %avghemi=avghemi+DataN1(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);
       %avgnorm=avgnorm+DataN2(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);    
    end
    avgday=avgday/DAYS;
    avghemi=avghemi/DAYS;
    avgnorm=avgnorm/DAYS;
    
    %one day data
    pikday=0;
    drawset=avgday(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    drawset=NormalizeArray(drawset);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(SGP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if 1
        drawset1=drawset(10*60+1:1440,1);
        drawset1=[drawset1(:,1);drawset(1:4*60,1)];
        ONEDAY_NEW=1080;    %18 hours to draw
        timelabel=[4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22];
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(TWP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if 0
        drawset1=drawset(18*60+1:1440,1);
        drawset1=[drawset1(:,1);drawset(1:12*60,1)];
        ONEDAY_NEW=1080;    %18 hours to draw
        timelabel=[4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22];
    end
    
    
    pic1=figure;  
    plot(drawset1);
    title(strcat('Max Day Solar Radiation Plot of Month ',int2str(j)));
    grid on;
    hold on;
    set(gca,'XTick',1:60:ONEDAY_NEW+1);
    set(gca,'XTickLabel',timelabel);
    %legend('One day Radiation',4);
    hold off;
    outputstr=strcat('d:\\img\\MonthlyMax',int2str(j),'.jpg');
    print(pic1,'-djpeg ',outputstr);
    
    
    pic2=figure;  
    title(strcat('Max Day Solar Radiation Plot of Month ',int2str(j)));
    grid on;
    hold on;
    drawset=avghemi(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    %SGP
    drawset1=drawset(10*60+1:1440,1);
    drawset1=[drawset1(:,1);drawset(1:4*60,1)];         
    %TWP
    %drawset1=drawset(18*60+1:1440,1);
    %drawset1=[drawset1(:,1);drawset(1:12*60,1)];        
    
    plot(drawset1);
    drawset=avgnorm(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    %SGP
    drawset1=drawset(10*60+1:1440,1);
    drawset1=[drawset1(:,1);drawset(1:4*60,1)];         
    %TWP
    %drawset1=drawset(18*60+1:1440,1);
    %drawset1=[drawset1(:,1);drawset(1:12*60,1)];        
    
    plot(drawset1,'r');
    set(gca,'XTick',1:60:ONEDAY_NEW+1);
    set(gca,'XTickLabel',timelabel);
    %legend('hemisp','Normal',4);
    hold off;
    outputstr=strcat('d:\\img1\\MonthlyMax',int2str(j),'.jpg');
    print(pic2,'-djpeg ',outputstr);
    
end

end

%% Avg
%   5 day avg
if 0
DAYS=5;

for j=1:12
    avgday=zeros(1440,1);
    avghemi=zeros(1440,1);
    avgnorm=zeros(1440,1);
       
    for i=1:DAYS
       %for avg
       avgday=avgday+Radia(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);
       avghemi=avghemi+DataN1(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);
       avgnorm=avgnorm+DataN2(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);

    end
    avgday=avgday/DAYS;
    avghemi=avghemi/DAYS;
    avgnorm=avgnorm/DAYS;
    
    %one day data
    pikday=0;
    drawset=avgday(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    drawset=NormalizeArray(drawset);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(SGP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if 1
        drawset1=drawset(10*60+1:1440,1);
        drawset1=[drawset1(:,1);drawset(1:4*60,1)];
        ONEDAY_NEW=1080;    %18 hours to draw
        timelabel=[4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22];
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(TWP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if 0
        drawset1=drawset(18*60+1:1440,1);
        drawset1=[drawset1(:,1);drawset(1:12*60,1)];
        ONEDAY_NEW=1080;    %18 hours to draw
        timelabel=[4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22];
    end
    
    pic1=figure;  
    plot(drawset1);
    title(strcat('5 days Avg Solar Radiation Plot Month ',int2str(j),' Date from 1 to ',int2str(DAYS)));
    grid on;
    hold on;
    set(gca,'XTick',1:60:ONEDAY_NEW+1);     %set hours+1 labels
        set(gca,'XTickLabel',timelabel);
    %legend('One day Radiation',4);
    hold off;
    outputstr=strcat('d:\\img\\daysavg',int2str(j),'.jpg');
    print(pic1,'-djpeg ',outputstr);
    
    pic2=figure;  
    title(strcat('5 days Avg Solar Radiation Plot Month ',int2str(j),' Date from 1 to ',int2str(DAYS)));
    grid on;
    hold on;
    drawset=avghemi(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    %SGP
    drawset1=drawset(10*60+1:1440,1);
    drawset1=[drawset1(:,1);drawset(1:4*60,1)];         
    %TWP
    %drawset1=drawset(18*60+1:1440,1);
    %drawset1=[drawset1(:,1);drawset(1:12*60,1)];        
    
    plot(drawset1);
    drawset=avgnorm(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    %SGP
    drawset1=drawset(10*60+1:1440,1);
    drawset1=[drawset1(:,1);drawset(1:4*60,1)];         
    %TWP
    %drawset1=drawset(18*60+1:1440,1);
    %drawset1=[drawset1(:,1);drawset(1:12*60,1)];        
    
    plot(drawset1,'r');
    set(gca,'XTick',1:60:ONEDAY_NEW+1);
    set(gca,'XTickLabel',timelabel);
    %legend('One day Radiation',4);
    hold off;
    outputstr=strcat('d:\\img1\\daysavg',int2str(j),'.jpg');
    print(pic2,'-djpeg ',outputstr);


end



%   one year avg
avgday=zeros(1440,1);
avghemi=zeros(1440,1);
avgnorm=zeros(1440,1);
DAYS=366;
for i=1:DAYS
       %for avg
       avgday=avgday+Radia(1+(StartM(1)+i-1)*ONEDAY:(StartM(1)+i)*ONEDAY,1);
       avghemi=avghemi+DataN1(1+(StartM(1)+i-1)*ONEDAY:(StartM(1)+i)*ONEDAY,1);
       avgnorm=avgnorm+DataN2(1+(StartM(1)+i-1)*ONEDAY:(StartM(1)+i)*ONEDAY,1);
end
    avgday=avgday/DAYS;
    avghemi=avghemi/DAYS;
    avgnorm=avgnorm/DAYS;
    %one day data
    pikday=0;
    drawset=avgday(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    drawset=NormalizeArray(drawset);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(SGP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if 1
        drawset1=drawset(10*60+1:1440,1);
        drawset1=[drawset1(:,1);drawset(1:4*60,1)];
        ONEDAY_NEW=1080;    %18 hours to draw
        timelabel=[4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22];
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(TWP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if 0
        drawset1=drawset(18*60+1:1440,1);
        drawset1=[drawset1(:,1);drawset(1:12*60,1)];
        ONEDAY_NEW=1080;    %18 hours to draw
        timelabel=[4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22];
    end
    
    pic1=figure;  
    plot(drawset1);
    title('Avg day Solar Radiation Plot of a year');
    grid on;
    hold on;
    set(gca,'XTick',1:60:ONEDAY_NEW+1);
    set(gca,'XTickLabel',timelabel);
    %legend('One day Radiation',4);
    hold off;
    outputstr=strcat('d:\\img\\yearavg','.jpg');
    print(pic1,'-djpeg ',outputstr);
    
    pic2=figure;  
    title('Avg day Solar Radiation Plot of a year');
    grid on;
    hold on;
    drawset=avghemi(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    %SGP
    drawset1=drawset(10*60+1:1440,1);
    drawset1=[drawset1(:,1);drawset(1:4*60,1)];         
    %TWP
    %drawset1=drawset(18*60+1:1440,1);
    %drawset1=[drawset1(:,1);drawset(1:12*60,1)];        
    
    plot(drawset1);
    drawset=avgnorm(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    %SGP
    drawset1=drawset(10*60+1:1440,1);
    drawset1=[drawset1(:,1);drawset(1:4*60,1)];         
    %TWP
    %drawset1=drawset(18*60+1:1440,1);
    %drawset1=[drawset1(:,1);drawset(1:12*60,1)];   
    
    plot(drawset1,'r');
    set(gca,'XTick',1:60:ONEDAY_NEW+1);
    set(gca,'XTickLabel',timelabel);
    %legend('hemisp','Normal',4);
    hold off;
    outputstr=strcat('d:\\img1\\yearavg','.jpg');
    print(pic2,'-djpeg ',outputstr);
    


%   One month avg
for j=1:1:12
    DAYS=EndM(j)-StartM(j);
    avgday=zeros(1440,1);
    avghemi=zeros(1440,1);
    avgnorm=zeros(1440,1);
    for i=1:DAYS
       %for avg
       avgday=avgday+Radia(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);
       avghemi=avghemi+DataN1(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);
       avgnorm=avgnorm+DataN2(1+(StartM(j)+i-1)*ONEDAY:(StartM(j)+i)*ONEDAY,1);    
    end
    avgday=avgday/DAYS;
    avghemi=avghemi/DAYS;
    avgnorm=avgnorm/DAYS;
    
    %one day data
    pikday=0;
    drawset=avgday(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    drawset=NormalizeArray(drawset);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(SGP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if 1
        drawset1=drawset(10*60+1:1440,1);
        drawset1=[drawset1(:,1);drawset(1:4*60,1)];
        ONEDAY_NEW=1080;    %18 hours to draw
        timelabel=[4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22];
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Generate a customized drawset(TWP)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if 0
        drawset1=drawset(18*60+1:1440,1);
        drawset1=[drawset1(:,1);drawset(1:12*60,1)];
        ONEDAY_NEW=1080;    %18 hours to draw
        timelabel=[4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22];
    end
    
    
    pic1=figure;  
    plot(drawset1);
    title(strcat('Avg Day Solar Radiation Plot of Month ',int2str(j)));
    grid on;
    hold on;
    set(gca,'XTick',1:60:ONEDAY_NEW+1);
    set(gca,'XTickLabel',timelabel);
    %legend('One day Radiation',4);
    hold off;
    outputstr=strcat('d:\\img\\MonthlyAvg',int2str(j),'.jpg');
    print(pic1,'-djpeg ',outputstr);
    
    
    pic2=figure;  
    title(strcat('Avg Day Solar Radiation Plot of Month ',int2str(j)));
    grid on;
    hold on;
    drawset=avghemi(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    %SGP
    drawset1=drawset(10*60+1:1440,1);
    drawset1=[drawset1(:,1);drawset(1:4*60,1)];         
    %TWP
    %drawset1=drawset(18*60+1:1440,1);
    %drawset1=[drawset1(:,1);drawset(1:12*60,1)];        
    
    plot(drawset1);
    drawset=avgnorm(1+pikday*ONEDAY:1440+pikday*ONEDAY,1);
    %SGP
    drawset1=drawset(10*60+1:1440,1);
    drawset1=[drawset1(:,1);drawset(1:4*60,1)];         
    %TWP
    %drawset1=drawset(18*60+1:1440,1);
    %drawset1=[drawset1(:,1);drawset(1:12*60,1)];        
    
    plot(drawset1,'r');
    set(gca,'XTick',1:60:ONEDAY_NEW+1);
    set(gca,'XTickLabel',timelabel);
    %legend('hemisp','Normal',4);
    hold off;
    outputstr=strcat('d:\\img1\\MonthlyAvg',int2str(j),'.jpg');
    print(pic2,'-djpeg ',outputstr);
end
end

end
