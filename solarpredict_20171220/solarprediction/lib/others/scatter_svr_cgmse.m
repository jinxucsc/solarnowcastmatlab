function scatter_svr_cgmse(svroutf,UpperThres,LowerThres)
switch nargin
    case 1
        %svroutf
        UpperThres=0.04;
        LowerThres=0;
    case 3
        %svroutf,UpperThres,LowerThres
    otherwise
        disp('[ERROR]: Invalid inputs!');
        return;
end
load(svroutf);
set(0,'DefaultFigureVisible', 'off');
indexi=MSEvals<UpperThres&MSEvals>LowerThres;
newmse=MSEvals(indexi);
newc=cvals(indexi);
newg=gvals(indexi);
testDir='./';
pic1=figure;
hold on;
grid on;
testf=sprintf('%s_scatter_c2g.jpg',testDir);
titStr=sprintf('Convergence test of C vs. G');
xStr='log2g';%
yStr='log2c';
h=scatter(newg,newc,20,newmse);
[minval,mini]=min(newmse);
x = get(h,'XData'); 		% Get the plotted data
y = get(h,'YData');
xval = x(mini);		% Find the index of the min and max
yval = y(mini);
plot(xval,yval,'+b');
minvalstr=sprintf('min val is %6f, log2c = %f, log2g = %f',...
    minval,newc(mini),newg(mini));
text(xval+0.25,yval+0.25,minvalstr,...
	'VerticalAlignment','middle',...
	'HorizontalAlignment','left',...
	'FontSize',14);
title(titStr);
xlabel(xStr);
ylabel(yStr);
print(pic1,'-djpeg ',testf);
maxxval=max(newc);
maxyval=max(newg);
hold off;
parentdir=GetDirPathFromPath(svroutf);
filename=GetFilenameFromAbsPath(svroutf);
plotoutf=sprintf('%sscat_%s.jpg',parentdir,filename);
print(pic1,plotoutf,'-djpeg');
end