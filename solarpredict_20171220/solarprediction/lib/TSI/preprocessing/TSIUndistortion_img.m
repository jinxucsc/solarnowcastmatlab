%   Function TSIUndistortion_img(inputf) is designed to do undistortion based on undist matrix
%   This function is currently used for BNL c1,c2,c3 site.(TSI1,TSI2,TSI3)
%   INPUT:
%       inputmatf       --- Input mat file
%           inputfs     --- Input file list
%           outputfs    --- Output file list
%   OUTPUT:
%   Version 1.0         2013-05-28
function TSIUndistortion_img(inputmatf,site)
mtsi_startup;   % Load TSIs undistortion parameters

t=load(inputmatf);
inputfs=t.inputfs;
outputfs=t.outputfs;

if strcmpi(site,TSI1)
    t=load(tsi1undistmat);
elseif strcmpi(site,TSI2)
    t=load(tsi2undistmat);
elseif strcmpi(site,TSI3)
    t=load(tsi3undistmat);
end
orim=round(t.u2oma);

l=length(inputfs);
for i=1:l
    inputf=inputfs{i};
    outputf=outputfs{i};
    try
        imma=imread(inputf);
    catch
        continue;   % not a image or irregular format
    end
    %   Get Undistortion image
    undistma=uint8(zeros(UNDISTH,UNDISTW));
    for ui=1:UNDISTH
        for uj=1:UNDISTW
            tmoriy=orim(ui,uj,1);
            tmorix=orim(ui,uj,2);
            if tmorix~=0&&tmoriy~=0 %invalid values
                undistma(ui,uj,1)=imma(tmoriy,tmorix,1);
                undistma(ui,uj,2)=imma(tmoriy,tmorix,2);
                undistma(ui,uj,3)=imma(tmoriy,tmorix,3);
            end
        end
    end
    imwrite(undistma,outputf,'jpg');
end


end