% function is to get sun AZ from DV given the location following the
% special form of
%               location.longitude = -72.8513;
%               location.latitude = 40.8652;
%               location.altitude = 15.24;        %metre
%   OUTPUT Az and Ze are all in radius not in degree!
function [Az,Ze]=GetSunAZfromDV(tmdv,location)
        time.year    =  tmdv(1);
        time.month   =  tmdv(2);
        time.day     =  tmdv(3);
        time.hour    =  tmdv(4);
        time.min    =   tmdv(5);
        time.sec    =   tmdv(6);
        time.UTC    =   0;
        sun = sun_position(time, location);  % compute the sun position
        Z=sun.zenith;
        Ze=Z/180*pi;
        A=sun.azimuth;
        Az=A/180*pi;
end