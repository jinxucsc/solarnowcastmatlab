%   GetTimeVec is a BNL INNER function which gets time vector from original
%   name.
%   From:   2011-12-01_13_03_09.0.jpeg
%   To:     [2011  12 1 13 3 9]
function tmdv=GetTimeVec(filename)
%       From: 2011-12-01_13_03_09.0.jpeg
mindice=strfind(filename,'-');
nindice=strfind(filename,'_');
pindice=strfind(filename,'.');
filename(1:mindice(1)-1);
years=filename(1:mindice(1)-1);
months=filename(mindice(1)+1:mindice(2)-1);
days=filename(mindice(2)+1:nindice(1)-1);
hours=filename(nindice(1)+1:nindice(2)-1);
minutes=filename(nindice(2)+1:nindice(3)-1);
seconds=filename(nindice(3)+1:pindice(1)-1);

year=str2num(years);
month=str2num(months);
day=str2num(days);
hour=str2num(hours);
minute=str2num(minutes);
second=str2num(seconds);
tmdv=[year month day hour minute second];
end