function FilterImgbyFreq(TSIDir,TSIoutDir,Freq,Thres2Rep,Filterdnf)
if nargin==2
    Freq=10;
    Thres2Rep=1;
    Filterdnf='./tsiavail.mat';
elseif nargin==3
    Thres2Rep=1;
    Filterdnf='./tsiavail.mat';
elseif nargin==4    
    Filterdnf='./tsiavail.mat';
else
    
end

TSIDir=FormatDirName(TSIDir);
TSIoutDir=FormatDirName(TSIoutDir);
if ~exist(TSIoutDir,'dir')
    mkdir(TSIoutDir);
end

allfiles=dir(TSIDir);
[m,~]=size(allfiles);
if m==2
    disp('[WARNNING]: Empty directory, no need to apply filter here!');
    return;
end
%   First image
filename=allfiles(3).name;
inputf=sprintf('%s%s',TSIDir,filename);
startdn=datevec(GetDVFromImg(filename));
ONESEC=datenum([0 0 0 0 0 1]);
nextdn=startdn+Freq*ONESEC;
expectdn=startdn;
Thres2Rep_dn=Thres2Rep*ONESEC;
minexpdn=expectdn-Thres2Rep_dn;
maxexpdn=expectdn+Thres2Rep_dn;
tmdns=zeros(m,1);
cindice=zeros(m,1);
repdn=zeros(m,1);
filestarti=1;
for i=1:m
    if(allfiles(i).isdir~=1)
        tmdns(i)=datenum(GetDVFromImg(allfiles(i).name));
    else
        filestarti=filestarti+1;
    end
end
startdn=tmdns(filestarti);
tmdn=startdn;
TIMESPAN=ONESEC*Freq;
expdn=tmdn+TIMESPAN;
startdv=datevec(startdn);
startdv(4)=0;
startdv(5)=0;
startdv(6)=0;
stdstartdn=datenum(startdv);    % start of the day 00:00:00
cmpdn=stdstartdn;
iter_i=1;
i=1;
%   Choose the right timestamps
while i<=m
    if(tmdns(i)==0)
        i=i+1;
        continue;
    end
    [tmval,tmi]=min(abs(tmdns(i:end)-cmpdn));
    if(tmval<=Thres2Rep_dn)
        cindice(i+tmi-1)=1;
        if(tmval~=0)
            repdn(i+tmi-1)=cmpdn;
        end
        i=i+tmi-1+1;
    else
        if(tmdns(i+tmi-1)<cmpdn)   % search from the bigger one
            i=i+tmi-1+1;
        else
            i=i+tmi-1;             % search from the bigger one    
        end
    end
    cmpdn=stdstartdn+iter_i*TIMESPAN;
%     disp(datestr(cmpdn));
    iter_i=iter_i+1;
end

save(Filterdnf,'tmdns','cindice','repdn','Freq','Thres2Rep');

%   Delete/Replace files
for i=1:m
    if(allfiles(i).isdir~=1)
        if(cindice(i)==0)
            %   Do not use this file
%             filename=allfiles(i).name;
%             inputf=sprintf('%s%s',TSIDir,filename);
%             delete(inputf);
        elseif(repdn(i)~=0)
            filename=allfiles(i).name;
            nTSI=TO_isTSIImg(filename);
            inputf=sprintf('%s%s',TSIDir,filename);
            tmdn=repdn(i);
            outputf=sprintf('%s%s',TSIoutDir,GetImgFromDV_BNL(datevec(tmdn),nTSI));
            copyfile(inputf,outputf);
        else
            % no replacement, choose reserve original one
            filename=allfiles(i).name;
            inputf=sprintf('%s%s',TSIDir,filename);
            outputf=sprintf('%s%s',TSIoutDir,filename);
            copyfile(inputf,outputf);
        end
    end
end

end