%This function is to reformat the files into formal form
%       From: 2011-12-01_13_03_09.0.jpeg
%       to:   bnltsiskyimageC1.a1.20111201.130309.jpg.20111201130309.jpg
function BNLNameFormatting_C1(inputDir)
allfiles=dir(inputDir);
[m,~]=size(allfiles);
for i=1:m
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        inputf=sprintf('%s%s',inputDir,filename);
        tmdv=GetTimeVec(filename);
        newfilename=GetImgFromDV_BNLC1(tmdv);
        outputf=sprintf('%s%s',inputDir,newfilename);
        copyfile(inputf,outputf);
        delete(inputf);
    end
end

end