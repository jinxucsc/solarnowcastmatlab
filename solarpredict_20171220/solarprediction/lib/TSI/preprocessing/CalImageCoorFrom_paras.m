%   function will takes all the parameters as input. For more information
%   please refer to CalImageCoorFrom_ALL which uses mat file as input
%   instead
function [x,y]=CalImageCoorFrom_paras(...
    Azumith,Zenith,...
    AzumithShift,ZenithShift,...
    h,r,...
    cw,ch,crx,cry,fo,...
    rcentrep...
)
alpha=Azumith+AzumithShift/180*pi;
beta=Zenith+ZenithShift/180*pi;
% centrex=centrep.x+CenShiftX;
% centrey=centrep.y+CenShiftY;
centrex=rcentrep.x;
centrey=rcentrep.y;

% if(abs(Zenith-2.094395102393195)<0.0001)
%     Zenith
% end

%% Parameters Setting
%   All parameters come from *.mat file
% cw=3.58;             %ccdwidth=3.6mm
% ch=2.69;           %ccdheight=4.576mm
% crx=640;            %ccd resolution X=288
% cry=480;            %ccd resolution Y=352
% fo=4;             %focal length 5.5mm

up_va=asin(r/(h+r));    %range of va
down_va=0; %#ok<NASGU>
up_theta=pi/2-up_va;    %#ok<NASGU> %range of theta angle
down_theta=0; %#ok<NASGU>
Zup=pi-up_va;          %maximum zenith that can be shown
% h=15;                %15 inches observer-to-mirror distance
% r=9;                 %9 inches mirror big radius

%% Calculation
if(Zenith>Zup||Zenith<0)
    % out of cal range
    x=-999;
    y=-999;
    return;
end

if(Zenith==0)
    x=centrep.x;
    y=centrep.y;
    return;
end

if(Zenith==pi/2)
    p=[4*(r+h)^2,0,-4*(r+h)^2+r^2,0,(r+h)^2-r^2];
else
    a=(h+r)/r;
    b=tan(beta);
    p=[4*a^2*b^2+4*a^2,0,-4*a^2*b^2+1-4*a^2+b^2,-2*a*b,a^2*b^2-b^2];
end
results=roots(p);

if(Zenith<=pi/2)
    result=results(results>0);
    result=result(result<=1);
    result=result(imag(result)==0);
    theta=asin(result);
    theta=theta(theta<=beta/2);    %theta smaller than beta(Zenith)
    theta=theta(theta>=(beta-up_va)/2);    %theta smaller than beta(Zenith)
    va=beta-2*theta;            %va=view angle
    theta=theta(theta>va);
else
    result=results(results>0);
    result=result(result<=1);
    result=result(imag(result)==0);
    theta=asin(result);
    theta=theta(theta<=beta/2);    %theta smaller than beta(Zenith)
    theta=theta(theta>=(beta-up_va)/2);
    va=beta-2*theta;                %va=view angle
    [tmv,tmi]=max(va); %#ok<ASGLU>
    theta=theta(tmi,1);
    va=beta-2*theta;                %va=view angle
end

testr=sin(pi-va-theta)/sin(va)-(h+r)/r;
if(abs(testr)>=0.00001)
    disp(testr);
end
if(length(theta)~=1)
    disp(theta);
end
if(alpha==0||alpha==pi/2||alpha==3*pi/2||alpha==pi||alpha==2*pi)
    al=pi/3;
    ccdy=tan(va)*fo/(sqrt(1+tan(al)^2));
    ccdx=tan(al)*ccdy;
    imagex=ccdx*crx/cw;
    imagey=ccdy*cry/ch;
    dist=sqrt(imagex^2+imagey^2);
    if(alpha==0)
        imagex=0;
        imagey=dist;
    elseif(alpha==pi/2)
        imagex=dist;
        imagey=0;
    elseif(alpha==pi)
        imagex=0;
        imagey=-dist;
    elseif(alpha==pi*3/2)
        imagex=-dist;
        imagey=0;
    elseif(alpha==pi*2)
        imagex=dist;
        imagey=0;
    end
    %not special cases
elseif (alpha<pi/2||alpha>3*pi/2)
    ccdy=tan(va)*fo/(sqrt(1+tan(alpha)^2)); % 0-90 270-360 y>0
    ccdx=tan(alpha)*ccdy;
    imagex=ccdx*crx/cw;
    imagey=ccdy*cry/ch;
else
    ccdy=-tan(va)*fo/(sqrt(1+tan(alpha)^2));% 90-270 y<0
    ccdx=tan(alpha)*ccdy;
    imagex=ccdx*crx/cw;
    imagey=ccdy*cry/ch;
end

% if(alpha<pi/2||alpha>3*pi/2)
%     ccdy=tan(va)*fo/(sqrt(1+tan(alpha)^2)); % 0-90 270-360 y>0
% else
%     ccdy=-tan(va)*fo/(sqrt(1+tan(alpha)^2));% 90-270 y<0
% end
%
% ccdx=tan(alpha)*ccdy;
% imagex=ccdx*crx/cw;
% imagey=ccdy*cry/ch;

%% coordinate transfer
x=imagex+centrex;   %already upside down
y=centrey-imagey;




end