%GetRadFromDB_freq(MySQLBinPath,startdv,enddv,Outputf,freq):
%   This function is to used for reading radiation data from MySQL server
%   which is solar.bnl.gov at present. The user name is 'redownload' and
%   password is 'radandtsi'. Use freq to control the timespan between two
%   consecutive images
%   The radiation values are CMP which is direct normal values at present.
%
%   IN:
%       MySQLBinPath    --  Where your MySQL server or client located.
%                           Something like (must with last slash!)
%                           C:\Program Files\MySQL\MySQL Server 5.5\bin\
%       startdv,enddv   --  The time span you requested. in datevector form
%                           eg. [2012 01 01 0 0 0],[2012 01 02 23 59 0] 
%       Outputf         --  The output file which you wanna put radiation
%                           values in
%       Freq            --  Extract every N rows record in DB
%                           eg. freq=N, two consecutive images will have N
%                           seconds difference
%   OUT:
%       Outputf         --  the output format should be like
%       (line 1) TmStamp                 CHP
%       (line 2) 2011-11-12 13:52:02     752.6
%       (line 3) 2011-11-12 13:52:03     752.5
%                ...                     ...
%   VERSION 1.0     2012-04-18

function GetRadFromDB_freq(MySQLBinPath,startdv,enddv,Outputf,freq)
MySQLServerURL='solar-db.bnl.gov';
username='redownload';
password='radandtsi';
DBName='bnlwx_new';
TBName='SolarBaseStation_Sec1';
startstr=sprintf('%d-%d-%d %d:%d:%d',...
    startdv(1),startdv(2),startdv(3),startdv(4),startdv(5),startdv(6));
endstr=sprintf('%d-%d-%d %d:%d:%d',...
    enddv(1),enddv(2),enddv(3),enddv(4),enddv(5),enddv(6));



% select TmStamp,CHP from(select @row := @row +1 As t1.rownum,t1.TmStamp,CHP from 
%   (select @row :=0)r,(select * from bnlwx_new.SolarBaseStation_Sec1 where 
%   TmStamp>'2012-04-01 00:00:00' and TmStamp<='2012-04-02 00:00:00') t1)ranked 
%   where rownum %4=1;
MySQLcmmd=sprintf(...
    'select TmStamp,CHP from(select @row := @row +1 As rownum,t1.TmStamp,t1.CHP from (select @row :=0)r,(select * from %s.%s where TmStamp>=''%s'' and TmStamp<=''%s'') t1)ranked where rownum %%%d=0;',...
    DBName,TBName, startstr,endstr, freq...  
);
cmmd=sprintf(...
    '"%s"mysql -h %s -u %s -p"%s" %s -e"%s" > %s',...
    MySQLBinPath,MySQLServerURL,username,password,DBName,MySQLcmmd,Outputf...
    );
% cmmd=sprintf(...
%     '"%s"mysql -h %s -u %s -p"%s" %s -e"SELECT TmStamp,CHP from %s.%s where TmStamp >= ''%s'' and TmStamp <= ''%s'';" > %s',...
%     MySQLBinPath,MySQLServerURL,username,password,DBName,DBName,TBName,startstr,endstr,Outputf...                   
%     );
disp(cmmd);
dos(cmmd);

end

