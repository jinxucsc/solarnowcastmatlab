%GetRadFromDB_freq(MySQLBinPath,startdv,enddv,Outputf,freq):
%   This function is to used for reading radiation data from MySQL server
%   which is solar.bnl.gov at present. The user name is 'redownload' and
%   password is 'radandtsi'. Use freq to control the timespan between two
%   consecutive images
%   The radiation values are CMP which is direct normal values at present.
%
%   IN:
%       MySQLBinPath    --  Where your MySQL server or client located.
%                           Something like (must with last slash!)
%                           C:\Program Files\MySQL\MySQL Server 5.5\bin\
%       startdv,enddv   --  The time span you requested. in datevector form
%                           eg. [2012 01 01 0 0 0],[2012 01 02 23 59 0] 
%       Outputf         --  The output file which you wanna put radiation
%                           values in
%       Freq            --  Extract every N rows record in DB
%                           eg. freq=N, two consecutive images will have N
%                           seconds difference
%   OUT:
%       Outputf         --  the output format should be like
%       (line 1) TmStamp                 CHP
%       (line 2) 2011-11-12 13:52:02     752.6
%       (line 3) 2011-11-12 13:52:03     752.5
%                ...                     ...
%   VERSION 1.0     2012-04-18


Demo to run:

GetRadFromDB('C:\Program Files\MySQL\MySQL Server 5.5\bin\',[2012 01 01 0 0 0],[2012 01 02 0 0 0],'allrad2.log',120);