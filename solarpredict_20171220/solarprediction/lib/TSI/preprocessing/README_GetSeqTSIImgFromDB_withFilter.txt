%GetSeqTSIImgFromDB_withFilter(startdv,enddv,Freq,RepFreq,minfreq,Stationn,imgoutDir)
%   This function is to used for reading TSI image data from MySQL server
%   which is solar.bnl.gov at present. The user name is 'redownload' and
%   password is 'radandtsi'. The matlab code will call java code internally
%   to complete the downloading of TSI. This function will get sequential
%   images based on freq input.
%   The radiation values are CMP which is direct normal values at present.
%
%   IN:
%       startdv         --  start date vector,  [2012 03 01 0 0 0]
%       enddv           --  end date vector,  [2012 03 01 12 0 0]
%       freq            --  INT, final time span of consecutive images
%       RepFreq         --  INT, if TS is missing, in TS-ReqFreq~TS+ReqRreq
%                           range, it will choose one to replace
%       minFreq         --  INT, this is the Timespan to downloaded from DB,
%                           always smaller than freq.
%       Stationn        --  Station #, C1 -> 1
%       imgoutDir       --  Directory, put downloaded images into this
%
%   OUT:
%       imgoutDir\*     --  output images
%                ...                     ...
%   VERSION 1.0     2012-04-20


REquirements and attention:
1.	You need to install java and configure the path.
2.	Your computer should be in the BNL local network(VPN doesn't work when you are not in BNL)
3.	Freq is the number to control downloading every several consecutive images. So if the images are not continous in the database, the timespan is not exactly the same.
4.	GetTSIImgfromDB.jar needs config.xml to provide parameters such as MySQL login information and Download task control information.


1. Demo: Get the image from DB
GetSeqTSIImgFromDB_withFilter([2012 04 03 12 0 0],[2012 04 03 14 0 0],120,10,10,1,'D:\outputimg\');

