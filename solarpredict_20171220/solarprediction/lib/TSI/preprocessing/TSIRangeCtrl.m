% Range control is to restrict all TSI images under certain range which is
% designated by your inputs. 
%   CAUTION: This function will delete input directory based on the range 
%               you give.
%   CAUTION: This function works only for LOCAL TIME as startdv and enddv 
%               are all local threshold.
%   INPUT:
%   inputDir    --- Directory, input tsi images
%   startdv --- daytime startdv, valid only [0 0 0 * * *]
%   enddv   --- daytime enddv, valid only [0 0 0 * * *]
%   MODE    --- 'delete'    delete without warning
%               'verbose'   (by default) with deletion warning only
%   OUTPUT:     
%   inputDir    --- Directory, input tsi images after delete!    
%
%   VERSION 1.0     2012-12-12
%   
function TSIRangeCtrl(inputDir,startdv,enddv,MODE)
if nargin==3
    MODE='verbose';
end
if strcmpi(MODE,'verbose')
    mode=1;
else    strcmpi(MODE,'delete')
    mode=2;
end
inputDir=FormatDirName(inputDir);
allfiles=dir(inputDir);
[m,~]=size(allfiles);
startdv(1)=0;
startdv(2)=0;
startdv(3)=0;
enddv(1)=0;
enddv(2)=0;
enddv(3)=0;
startdn=datenum(startdv);
enddn=datenum(enddv);
for i=1:m
    if allfiles(i).isdir==1 
        continue;
    end
    filename=allfiles(i).name;
    tmdv=GetDVFromImg(filename);
    estdv=GMT2EST(tmdv);
    oddv=estdv;
    oddv(1)=0;oddv(2)=0;oddv(3)=0;
    oddn=datenum(oddv);
    if oddn>=startdn&&oddn<=enddn
        continue;
    end
    inputf=sprintf('%s%s',inputDir,filename);
    if mode==1
        dispstr=sprintf('delete file: %s',inputf);
        disp(dispstr);
    elseif mode==2
        delete(inputf);
    end
end



end