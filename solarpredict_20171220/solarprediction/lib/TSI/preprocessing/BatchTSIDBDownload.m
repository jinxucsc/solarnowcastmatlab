function BatchTSIDBDownload(startdv,enddv,Freq,Stationn,imgoutDir)
daystartdv=startdv;
daystartdv(4)=0;
daystartdv(5)=0;
daystartdv(6)=0;
ONEDAYDN=datenum([0 0 1 0 0 0]);
dayenddv=enddv;
dayenddv(4)=0;
dayenddv(5)=0;
dayenddv(6)=0;
daystartdn=datenum(daystartdv);
dayenddn=datenum(dayenddv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   This is for window path name
imgoutDir=regexprep(imgoutDir,'/','\');       
if(imgoutDir(end)~='\')
    imgoutDir=sprintf('%s%s',imgoutDir,'\');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

idays=1;
daydn=daystartdn;
starth=6+5;
endh=23;
dns=[];
while(daydn<=dayenddn)
    dns=[dns daydn];
    daydn=daystartdn+idays*ONEDAYDN;
    idays=idays+1;
end
[~,m]=size(dns);

parfor i=1:m
    daydn=dns(i);
    daydv=datevec(daydn);
    odstartdv=daydv;
    odenddv=daydv;
    odstartdv(4)=starth;
    odenddv(4)=endh;
    foldername=GetImgFromDV_BNLC1(daydv);
    fulloutdir=sprintf('%s%s',imgoutDir,foldername);
    mkdir(fulloutdir);
    GetTSIImgFromDB(odstartdv,odenddv,Freq,Stationn,fulloutdir);
end

% while(daydn<=dayenddn)
%     daydv=datevec(daydn);
%     odstartdv=daydv;
%     odenddv=daydv;
%     odstartdv(4)=starth;
%     odenddv(4)=endh;
%     foldername=GetImgFromDV_BNLC1(daydv);
%     fulloutdir=sprintf('%s%s',imgoutDir,foldername);
%     mkdir(fulloutdir);
%     GetTSIImgFromDB(odstartdv,odenddv,Freq,Stationn,fulloutdir);
%     daydn=daystartdn+idays*ONEDAYDN;
%     idays=idays+1;
% end
end