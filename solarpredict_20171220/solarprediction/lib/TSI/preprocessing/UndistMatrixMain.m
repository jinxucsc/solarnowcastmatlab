function output=UndistMatrixMain(Ru,Wu,Hu,CBH,ViewAngle,SITE)
% ViewAngle=atan(7.772307256187960e+03/5000);

PARAS=load('UndistortionParameters.mat');
%   h=15.8611;r=9
% r=R;
% ORI_WIDTH=640;
% ORI_HEIGHT=480;
% rcentrep.x=ORI_WIDTH/2;
% rcentrep.y=ORI_HEIGHT/2;
ORI_WIDTH=PARAS.Wi;
ORI_HEIGHT=PARAS.Hi;
rcentrep.x=ORI_WIDTH/2+PARAS.CenShiftX;     %real original center(rotating center) 
rcentrep.y=ORI_HEIGHT/2+PARAS.CenShiftY;

cw=PARAS.cw;             %ccdwidth=3.6mm
ch=PARAS.ch;           %ccdheight=4.576mm
crx=PARAS.crx;            %ccd resolution X=288
cry=PARAS.cry;            %ccd resolution Y=352
fo=PARAS.fo;             %focal length 5.5mm
h=PARAS.h;
r=PARAS.r;
centrep=PARAS.centrep;

% undistm=zeros(Hu,Wu,2); %(Xu,Yu)
orim=zeros(Hu,Wu,2);%(Xor,Yor)

undistcen.x=Wu/2;
undistcen.y=Hu/2;
DistTotal=CBH*tan(ViewAngle);

for Xu=1:Wu
    for Yu=1:Hu
        %   1. From undistorted image to world relative coor
        %   (Xu,Yu) -> (Xwr,Ywr,Zwr or CBH) ->(Zenith, Azumith)
%         if(Xu==251&&Yu==200)
%             Xu
%         end
        imagey=undistcen.y-Yu;
        imagex=Xu-undistcen.x;
        DistCen=sqrt(imagey^2+imagex^2);
        if(DistCen>=Ru)
            continue;
        end
        reald=DistCen/Ru*DistTotal;
        tz=reald/CBH;
        Zenith=atan(tz);
        
        %find azumith
        if(imagex==0&&imagey>0)
            Azumith=0;
        elseif imagex==0&&imagey<0
            Azumith=pi;
        elseif imagex==0&&imagey==0
            Zenith=-3*pi;
            Azumith=-3*pi;
        elseif imagey==0&&imagex>0
             Azumith=pi/2;
        elseif imagey==0&&imagex<0
            Azumith=pi*3/2;
        end

        if(imagex>0&&imagey>0)
            Azumith=atan(imagex/imagey);
        end
        if(imagex<0&&imagey>0)
            Azumith=2*pi+atan(imagex/imagey);
        end
        if(imagex<0&&imagey<0)
            Azumith=pi+atan(imagex/imagey);
        end
        if(imagex>0&&imagey<0)
            Azumith=pi+atan(imagex/imagey);
        end
        
        %   2. (Zenith,Azumith) -> (Xor,Yor)
        if(Azumith==-3*pi&&Zenith==-3*pi)
            %symbol of image centre
            Xor=rcentrep.x;
            Yor=rcentrep.y;
        else
            [Xor,Yor]=CalImageCoorFrom_ALL_INNER(Azumith,Zenith);
        end
        orim(Yu,Xu,1)=Yor;
        orim(Yu,Xu,2)=Xor;
    end
end

% 3. Save the original matrix mapping orim
save('undist2ori.mat','orim','ViewAngle','Ru','h','r','ORI_WIDTH','ORI_HEIGHT','Wu','Hu','CBH');


function [x,y]=CalImageCoorFrom_ALL_INNER(Azumith,Zenith)
%   Based on shift parameters described in UndistortionParameters.mat do
%   adjustment to center of image and azumith,zenith
% load('UndistortionParameters.mat');
alpha=Azumith+PARAS.AzumithShift;
beta=Zenith+PARAS.ZenithShift;
% centrex=rcentrep.x+PARAS.CenShiftX;
% centrey=rcentrep.y+PARAS.CenShiftY;
centrex=rcentrep.x;
centrey=rcentrep.y;


if(abs(Zenith-2.094395102393195)<0.0001)
    Zenith
end

%% Parameters Setting
%   All parameters come from *.mat file
% cw=3.58;             %ccdwidth=3.6mm
% ch=2.69;           %ccdheight=4.576mm
% crx=640;            %ccd resolution X=288
% cry=480;            %ccd resolution Y=352
% fo=4;             %focal length 5.5mm

up_va=asin(r/(h+r));    %range of va
down_va=0;
up_theta=pi/2-up_va;    %range of theta angle
down_theta=0;
Zup=pi-up_va;          %maximum zenith that can be shown
% h=15;                %15 inches observer-to-mirror distance
% r=9;                 %9 inches mirror big radius

%% Calculation
if(Zenith>Zup||Zenith<0)
    % out of cal range
    x=-999;
    y=-999;
end

if(Zenith==0)
    x=centrep.x;
    y=centrep.y;
    return;
end

if(Zenith==pi/2)
    p=[4*(r+h)^2,0,-4*(r+h)^2+r^2,0,(r+h)^2-r^2];
else
    a=(h+r)/r;
    b=tan(beta);
    p=[4*a^2*b^2+4*a^2,0,-4*a^2*b^2+1-4*a^2+b^2,-2*a*b,a^2*b^2-b^2];
end
results=roots(p);

if(Zenith<=pi/2)
    result=results(results>0);
    result=result(result<=1);
    theta=asin(result);
    theta=theta(theta<=beta/2);    %theta smaller than beta(Zenith)
    theta=theta(theta>=(beta-up_va)/2);    %theta smaller than beta(Zenith)
    va=beta-2*theta;            %va=view angle
    theta=theta(theta>va);
else
    result=results(results>0);
    result=result(result<=1);
    theta=asin(result);
    theta=theta(theta<=beta/2);    %theta smaller than beta(Zenith)
    theta=theta(theta>=(beta-up_va)/2);
    va=beta-2*theta;                %va=view angle
    [tmv,tmi]=max(va);
    theta=theta(tmi,1);
    va=beta-2*theta;                %va=view angle
end

testr=sin(pi-va-theta)/sin(va)-(h+r)/r;
if(abs(testr)>=0.00001)
    disp(testr);
end
if(length(theta)~=1)
    disp(theta);
end
if(alpha==0||alpha==pi/2||alpha==3*pi/2||alpha==pi||alpha==2*pi)
    al=pi/3;
    ccdy=tan(va)*fo/(sqrt(1+tan(al)^2));
    ccdx=tan(al)*ccdy;
    imagex=ccdx*crx/cw;
    imagey=ccdy*cry/ch;
    dist=sqrt(imagex^2+imagey^2);
    if(alpha==0)
        imagex=0;
        imagey=dist;
    elseif(alpha==pi/2)
        imagex=dist;
        imagey=0;
    elseif(alpha==pi)
        imagex=0;
        imagey=-dist;
    elseif(alpha==pi*3/2)
        imagex=-dist;
        imagey=0;
    elseif(alpha==pi*2)
        imagex=dist;
        imagey=0;
    end
    %not special cases
elseif (alpha<pi/2||alpha>3*pi/2)
    ccdy=tan(va)*fo/(sqrt(1+tan(alpha)^2)); % 0-90 270-360 y>0
    ccdx=tan(alpha)*ccdy;
    imagex=ccdx*crx/cw;
    imagey=ccdy*cry/ch;
else
    ccdy=-tan(va)*fo/(sqrt(1+tan(alpha)^2));% 90-270 y<0
    ccdx=tan(alpha)*ccdy;
    imagex=ccdx*crx/cw;
    imagey=ccdy*cry/ch;
end

% if(alpha<pi/2||alpha>3*pi/2)
%     ccdy=tan(va)*fo/(sqrt(1+tan(alpha)^2)); % 0-90 270-360 y>0
% else
%     ccdy=-tan(va)*fo/(sqrt(1+tan(alpha)^2));% 90-270 y<0
% end
%
% ccdx=tan(alpha)*ccdy;
% imagex=ccdx*crx/cw;
% imagey=ccdy*cry/ch;

%% coordinate transfer
x=imagex+centrex;   %already upside down
y=centrey-imagey;
end


end