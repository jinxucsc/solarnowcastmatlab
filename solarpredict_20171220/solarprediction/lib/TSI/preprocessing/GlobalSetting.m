clear all;
close all;
clc;

% img_name='twptsiskyimageC1.a1.20100911.000500.jpg.20100911000500'; %% the first TSI image
% 
% num_frame=720; %% numbum of images which need to be undistorted
% undist_type=4; %% choose the undistortion algorithm
% img_type=0; %% output image type: 0 is RGB; 1 is grayscale; 2 is red-blue ratio(RBR)
% name_type=1; %% image name format: 1 is old, 2 is new;

% BarUndist_pad=20;
% BarUndist_k1=-0.0000005;
% BarUndist_k2=-0.0000005;
% BarUndist_shrinkX=10;
% BarUndist_shiftCX=0;
% BarUndist_shrinkY=10;
% BarUndist_shiftCY=-3;

%Batch Parameters
inputDir='input/';      %   Store all input files (original images)
outputDir='output/';
undistDir='undist/';
ArmDir='armdir/';       % store temprary files that need to remove arm and shadow band

%   Site and Device Control
%   SITE=BNL --> site=0
%   SITE=TWP --> site=1
%   SITE=SGP --> site=2
SITE='TWP';
DEVICEN='C1';
site=0;         

if(strcmpi(SITE,'BNL')==1&&strcmpi(DEVICEN,'C1')==1)
    %   Original Image Parameters
    Wi=640;         %   Width of original image
    Hi=480;         %   Height of original image
    centrep.x=Wi/2; %   center of the original image
    centrep.y=Hi/2;
    CenShiftX=10;
    CenShiftY=10;
    %   Site Location
    location.longitude = -72.885;
    location.latitude = 40.866;
    location.altitude = 20;        %metre
    
    %   Undistortion parameters for Sun position Calculation
    perimeter=45; % perimeter of flat circle (len) (small->in, large->out)
    R=9.0;%9.03; % Radius of the len   (small->out, large->in)
    r=R;
    h=15.6811; % Height of camera (down to the surface of lens) (small->out, large->in)
    final_w_t=1000; % output image width in pixel scale
    f=0.5;          % output image shrink scale (final output is f*final_w_t)
    cw=3.58;             %ccdwidth=3.6mm
    ch=2.69;           %ccdheight=4.576mm
    crx=Wi;            %ccd resolution X=288
    cry=Hi;            %ccd resolution Y=352
    fo=4;             %focal length 5.5mm
    
    %   ArmRemoval Batch Parameters
%     HoldArmMask=double(rgb2gray(imread('add/HoldingArm3.jpg')));
%     ShadBanMask=double(rgb2gray(imread('add/ShadowBand3.jpg')));
    pHoldArmMask='add/HoldingArm3.jpg';
    pShadBanMask='add/ShadowBand3.jpg';
    AzumithShift=3;
    ZenithShift=0;
    
end

if(strcmpi(SITE,'TWP')==1&&strcmpi(DEVICEN,'C1')==1)
    %   Original Image Parameters
    Wi=480;         %   Width of original image
    Hi=640;         %   Height of original image
    centrep.x=Wi/2; %   center of the original image
    centrep.y=Hi/2;
%   centrep.x=240;centrep.y=320;
%   rcentrep.x=235;rcentrep.y=329;
    rcentrep.x=236;rcentrep.y=327;
    %CenShiftX=-5;
    %CenShiftY=9;
    CenShiftX=-4;
    CenShiftY=7;
    %CenShiftY=9;
    %   Site Location
    location.longitude = 147.425; 
    location.latitude = -2.06; 
    location.altitude = 4;
    
    %   Undistortion parameters for Sun position Calculation
    perimeter=45; % perimeter of flat circle (len) (small->in, large->out)
    R=9.0;%9.03; % Radius of the len   (small->out, large->in)
    r=R;
    h=14; % Height of camera (down to the surface of lens) (small->out, large->in)
    final_w_t=1000; % output image width in pixel scale
    f=0.5;          % output image shrink scale (final output is f*final_w_t)
    cw=3.6;             %ccdwidth=3.6mm
    ch=4.576;           %ccdheight=4.576mm
    crx=Wi;            %ccd resolution X=288
    cry=Hi;            %ccd resolution Y=352
    fo=5.5;             %focal length 5.5mm
    
    %   ArmRemoval Batch Parameters
%     HoldArmMask=double(rgb2gray(imread('add/HoldingArm3.jpg')));
%     ShadBanMask=double(rgb2gray(imread('add/ShadowBand3.jpg')));
    pHoldArmMask='add/HoldingArm3.jpg';
    pShadBanMask='add/ShadowBand3.jpg';
    AzumithShift=0;
    ZenithShift=0;
end

if(strcmpi(SITE,'TWP')==1&&strcmpi(DEVICEN,'C3')==1)
    %   Original Image Parameters
    Wi=480;         %   Width of original image
    Hi=640;         %   Height of original image
    centrep.x=Wi/2; %   center of the original image
    centrep.y=Hi/2;
    CenShiftX=10;
    CenShiftY=10;
    %   Site Location
    location.longitude = 147.425; 
    location.latitude = -2.06; 
    location.altitude = 4;
    
    %   Undistortion parameters for Sun position Calculation
    perimeter=45; % perimeter of flat circle (len) (small->in, large->out)
    R=9.0;%9.03; % Radius of the len   (small->out, large->in)
    r=R;
    h=14; % Height of camera (down to the surface of lens) (small->out, large->in)
    final_w_t=1000; % output image width in pixel scale
    f=0.5;          % output image shrink scale (final output is f*final_w_t)
    cw=3.6;             %ccdwidth=3.6mm
    ch=4.576;           %ccdheight=4.576mm
    crx=Wi;            %ccd resolution X=288
    cry=Hi;            %ccd resolution Y=352
    fo=5.5;             %focal length 5.5mm
    
    %   ArmRemoval Batch Parameters
%     HoldArmMask=double(rgb2gray(imread('add/HoldingArm3.jpg')));
%     ShadBanMask=double(rgb2gray(imread('add/ShadowBand3.jpg')));
    pHoldArmMask='add/HoldingArm3.jpg';
    pShadBanMask='add/ShadowBand3.jpg';
    AzumithShift=0;
    ZenithShift=0;
end

if(strcmpi(SITE,'SGP')==1&&strcmpi(DEVICEN,'C1')==1)
    %   Original Image Parameters
    Wi=288;         %   Width of original image
    Hi=352;         %   Height of original image
    centrep.x=Wi/2; %   center of the original image
    centrep.y=Hi/2;
    CenShiftX=6;
    CenShiftY=4;
    %   Site Location
    location.longitude = -97.4850;
    location.latitude = 36.6060;
    location.altitude = 320;
    
    %   Undistortion parameters for Sun position Calculation
    perimeter=45; % perimeter of flat circle (len) (small->in, large->out)
    R=9.0;%9.03; % Radius of the len   (small->out, large->in)
    r=R;
    h=14; % Height of camera (down to the surface of lens) (small->out, large->in)
    final_w_t=1000; % output image width in pixel scale
    f=0.5;          % output image shrink scale (final output is f*final_w_t)
    cw=3.6;             %ccdwidth=3.6mm
    ch=4.576;           %ccdheight=4.576mm
    crx=Wi;            %ccd resolution X=288
    cry=Hi;            %ccd resolution Y=352
    fo=5.5;             %focal length 5.5mm
    
    %   ArmRemoval Batch Parameters
%     HoldArmMask=double(rgb2gray(imread('add/HoldingArm3.jpg')));
%     ShadBanMask=double(rgb2gray(imread('add/ShadowBand3.jpg')));
    pHoldArmMask='add/HoldingArm3.jpg';
    pShadBanMask='add/ShadowBand3.jpg';
    AzumithShift=0;
    ZenithShift=0;
end



perimeter=45; % perimeter of flat circle (len) (small->in, large->out)
R=9.0;%9.03; % Radius of the len   (small->out, large->in)
r=R;    
h=14.0; % Height of camera (down to the surface of lens) (small->out, large->in)
final_w_t=1000; % output image width in pixel scale 
f=0.5;          % output image shrink scale (final output is f*final_w_t)


%   Undistortion Mapping control
Ru=240;     %   Undistortion Radius of view range
Wu=500;     %   Width of undistortion image
Hu=500;     %   Height of undistortion image
CBH=5000;   %   (Optional) Cloud-Base Height, only for precision calculation(in meter)
ViewAngle=60/180*pi;  %   View Angle of TSI or Maximum view range showing on undistortion image

save('UndistortionParameters.mat');

UndistMatrixMain(Ru,Wu,Hu,CBH,ViewAngle,SITE);


