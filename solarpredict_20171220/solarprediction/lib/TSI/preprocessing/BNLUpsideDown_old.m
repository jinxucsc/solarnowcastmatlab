%   This function is to rotate 180 degree of BNL image. At the same time
%   read the FilteredDatenumber.mat in for reference of filtering. If the
%   file date number satisfies the requirement of freq, do tranforming and
%   save it into outputDir.
%
%   IN:
%       inputDir        --- the directory which contains input image
%       outputDir       --- Directory, output images
%       Filterdnf       --- MAT file, conditions for filterring
%   OUT:
%       outputDir/*     --- 
%   VERSION 1.1 2012-04-20
%   VERSION 1.2 2012-04-30

function BNLUpsideDown_old(inputDir,outputDir,Filterdnf)
%   BNLUptoDown is to put all images in inputDir upside down and change it
%   to formal format of the name.
if(inputDir(end)~='/')
    inputDir=sprintf('%s%s',inputDir,'/');
end
if(outputDir(end)~='/')
    outputDir=sprintf('%s%s',outputDir,'/');
end
mkdir(outputDir);
if exist(Filterdnf,'file')   ~=0
    s=load(Filterdnf);
else
    disp('There is no filter datenumber file!function stops');
    return;
end


%   1. Read each file in and output upsidedown image to output/ directory
allfiles=dir(inputDir);
[m,~]=size(allfiles);
filename=allfiles(end).name;
tmdns=s.tmdns;
cindice=s.cindice;
repdn=s.repdn;
ndis=[];
if isempty(strfind(filename,'-')) ==1
    MODE = 1;   % formal format
    parfor i=1:m;
        if(allfiles(i).isdir~=1)
            filename=allfiles(i).name;
            tmdv=GetDVFromImg(filename);
            tmdn=datenum(tmdv);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %                       DEBUG ONLY
            if(tmdn~=tmdns(i))
%                 disp('No match to datenum file!');
                ndis=[ndis i];
            else
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if(cindice(i)==0)
                continue;
            elseif(repdn(i)~=0)
                filename=allfiles(i).name;
                inputf=sprintf('%s%s',inputDir,filename);
                tmdn=repdn(i);
                outputf=sprintf('%s%s',outputDir,GetImgFromDV_BNLC1(datevec(tmdn)));
            else
                % Normal case
                filename=allfiles(i).name;
                inputf=sprintf('%s%s',inputDir,filename);
                outputf=sprintf('%s%s',outputDir,GetImgFromDV_BNLC1(datevec(tmdn)));
            end
            
            %   output exist then continue
            if(exist(outputf,'file')~=0)
                continue;
            end
            
            img1=imread(inputf);
            img1(:,:,1)=flipud(img1(:,:,1));
            img1(:,:,2)=flipud(img1(:,:,2));
            img1(:,:,3)=flipud(img1(:,:,3));
            %column upside down
            img1(:,:,1)=fliplr(img1(:,:,1));
            img1(:,:,2)=fliplr(img1(:,:,2));
            img1(:,:,3)=fliplr(img1(:,:,3));
            imwrite(img1,outputf,'jpg');
        end
    end
else
    MODE = 2;   % original format
    disp('Error, you must do formatting before use this function!');
%     parfor i=1:m;
%         if(allfiles(i).isdir~=1)
%             filename=allfiles(i).name;
%             tmdv=GetTimeVec(filename);
%             newfilename=GetImgFromDV_BNLC1(tmdv);
%             fulloutp=sprintf('%s%s',outputDir,newfilename);
%             fullinputp=sprintf('%s%s',inputDir,filename);
%             if(exist(fulloutp,'file')~=0)
%                 continue;
%             end
%             img1=imread(fullinputp);
%             img1(:,:,1)=flipud(img1(:,:,1));
%             img1(:,:,2)=flipud(img1(:,:,2));
%             img1(:,:,3)=flipud(img1(:,:,3));
%             %column upside down
%             img1(:,:,1)=fliplr(img1(:,:,1));
%             img1(:,:,2)=fliplr(img1(:,:,2));
%             img1(:,:,3)=fliplr(img1(:,:,3));
%             imwrite(img1,fulloutp,'jpg');
%         end
%     end
end
ndis
end