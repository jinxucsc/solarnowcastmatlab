%GetRadFromDB(MySQLBinPath,startdv,enddv,Outputf):
%   This function is to used for reading radiation data from MySQL server
%   which is solar.bnl.gov at present. The user name is 'redownload' and
%   password is 'radandtsi'. 
%   The radiation values are CMP which is direct normal values at present.
%
%   IN:
%       MySQLBinPath    --  Where your MySQL server or client located.
%                           Something like (must with last slash!)
%                           C:\Program Files\MySQL\MySQL Server 5.5\bin\
%       startdv,enddv   --  The time span you requested. in datevector form
%                           eg. [2012 01 01 0 0 0],[2012 01 02 23 59 0] 
%       Outputf         --  The output file which you wanna put radiation
%                           values in
%   OUT:
%       Outputf         --  the output format should be like
%       (line 1) TmStamp                 CHP
%       (line 2) 2011-11-12 13:52:02     752.6
%       (line 3) 2011-11-12 13:52:03     752.5
%                ...                     ...
%   VERSION 1.0     2012-04-18

Requirements and Attention:
1.	You need MySQL installation
2.	Your computer should be in BNL local network(VPN or BNL desktop)
3.	MySQLBinPath must contains last dash '\'
4.	Timespan from startdv to enddv(they are all included)
5.	For output file, first line is the column name not the data
6.	radiation is CMP(Direct Normal). For other values you need to change the code


Demo to run:

GetRadFromDB('C:\Program Files\MySQL\MySQL Server 5.5\bin\',[2012 01 01 0 0 0],[2012 01 02 0 0 0],'allrad2.log');