%GetSeqTSIImgFromDB(startdv,enddv,Freq,Stationn,imgoutDir):
%   This function is to used for reading TSI image data from MySQL server
%   which is solar.bnl.gov at present. The user name is 'redownload' and
%   password is 'radandtsi'. The matlab code will call java code internally
%   to complete the downloading of TSI. This function will get sequential
%   images based on freq input.
%   The radiation values are CMP which is direct normal values at present.
%
%   IN:
%       startdv         --  start date vector,  [2012 03 01 0 0 0]
%       enddv           --  end date vector,  [2012 03 01 12 0 0]
%       freq            --  INT,time span of consecutive images
%       Stationn        --  Station #, C1 -> 1
%       imgoutDir       --  Directory, put downloaded images into this
%
%   OUT:
%       imgoutDir\*     --  output images
%                ...                     ...
%   VERSION 1.0     2012-04-20

function GetSeqTSIImgFromDB(startdv,enddv,Freq,Stationn,imgoutDir)
% MySQLServerURL='solar.bnl.gov';
% username='redownload';
% password='radandtsi';
% DBName='bnlwx_new';
% TBName='SolarBaseStation_Sec1';
Datefrom=sprintf('%d-%d-%d',startdv(1),startdv(2),startdv(3));
Dateto=sprintf('%d-%d-%-d',enddv(1),enddv(2),enddv(3));
Timefrom=sprintf('%d:%d:%d',startdv(4),startdv(5),startdv(6));
Timeto=sprintf('%d:%d:%d',enddv(4),enddv(5),enddv(6));
Frequency=sprintf('%ds',Freq);  %minimum freq = 1s
Stationstr=sprintf('%d',Stationn);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   This is for window path name
regexprep(imgoutDir,'/','\');       
if(imgoutDir(end)~='\')
    imgoutDir=sprintf('%s%s',imgoutDir,'\');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
startdn=datenum(startdv);
enddn=datenum(enddv);
ONESEC=datenum([0 0 0 0 0 1]);
for i=1:inf
    if (startdn+i*ONESEC)==enddn
        break;
    end
end
NofSec=i;
NofImg=fix(NofSec/Freq);
for i=1:NofImg
    tmdn=startdn+(i-1)*Freq*ONESEC;
    tmdv=datevec(tmdn);
    Datefrom=sprintf('%d-%d-%d',tmdv(1),tmdv(2),tmdv(3));
    Dateto=sprintf('%d-%d-%-d',tmdv(1),tmdv(2),tmdv(3));
    Timefrom=sprintf('%d:%d:%d',tmdv(4),tmdv(5),tmdv(6));
    Timeto=sprintf('%d:%d:%d',tmdv(4),tmdv(5),tmdv(6));
    Stationstr=sprintf('%d',Stationn);
    cmmd=sprintf(...
    'java -jar GetTSIImgfromDB.jar %s %s %s %s %s %s %s',...
    Datefrom,Dateto,Timefrom,Timeto,Frequency,Stationstr,imgoutDir);
    dos(cmmd);
end

end

