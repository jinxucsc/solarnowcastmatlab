%   Multi-core undistortion for TSIs(TSI1,TSI2,TSI3). This code is ONLY for undistortion covering no
%   masking. For undistortion and masking, Pls refer to PIPE_UndistortionwithCSL_BNL
%   INPUT:
%       inputDir    --- Directory, input images
%       outputDir   --- Directory, output images
%       site        --- site= 'tsi1','tsi2','tsi3'
%       matlabinst  --- Int, number of maximum matlab instance,Optional
%   OUTPUT:
%       outputDir/* --- Undistorted Images 
%   VERSION 1.0     2013-05-28
function TSIUndistortion_dir(inputDir,outputDir,site,matlabinst)
if nargin==3
    matlabinst=16;
end
allfiles=dir(inputDir);
[nfiles,~]=size(allfiles);
nInst=matlabinst;
nUnits=floor(nfiles/nInst);
inputDir=FormatDirName(inputDir);
outputDir=FormatDirName(outputDir);
if ~exist(outputDir,'dir')
    mkdir(outputDir);
end

for i=1:nInst
    inputmatf=sprintf('undist_%d.mat',i);
    if i==nInst
        instrange=(i-1)*nUnits+1:nfiles;
    else
        instrange=(i-1)*nUnits+1:i*nUnits;
    end
    tml=length(instrange);
    inputfs=cell(tml,1);
    outputfs=cell(tml,1);
    for j=1:tml
        ifile=instrange(j);
        filename=allfiles(ifile).name;
        inputf=sprintf('%s%s',inputDir,filename);
        outputf=sprintf('%s%s',outputDir,filename);
        inputfs{j}=inputf;
        outputfs{j}=outputf;
    end
    save(inputmatf,'inputfs','outputfs');
    cmmd=sprintf('TSIUndistortion_img(''%s'',''%s'');',inputmatf,site);
    disp(cmmd);
    subprocess_matlab(cmmd,nInst);
    %TSIUndistortion_img(inputmatf,site);
end


end