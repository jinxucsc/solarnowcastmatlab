function filename=GetImgFromDV_BNLC1(datevector)
%   option  0   sgp
%           1   twp
%   datevector  including year,month,day,hour,min,seconds
%   imgpath     image directory with '/'

%twptsiskyimageC1.a1.20101031.233500.jpg.20101031233500.jpg

prefix='bnltsiskyimageC1.a1.';

year=int2str(datevector(1));
month=int2str(datevector(2));
if(length(month)==1)
    month=strcat('0',month);
end
day=int2str(datevector(3));
if(length(day)==1)
    day=strcat('0',day);
end
hour=int2str(datevector(4));
if(length(hour)==1)
    hour=strcat('0',hour);
end
minute=int2str(datevector(5));
if(length(minute)==1)
    minute=strcat('0',minute);
end

% SEC='00';
second=int2str(datevector(6));
if(length(second)==1)
    second=strcat('0',second);
end
filename=sprintf('%s%s%s%s.%s%s%s.jpg.%s%s%s%s%s%s.jpg',prefix,year,month,day,hour,minute,second,year,month,day,hour,minute,second);
%img=imread(Fullname);
end