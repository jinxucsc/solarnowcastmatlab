%GetSeqTSIImgFromDB_withFilter(startdv,enddv,Freq,RepFreq,minfreq,Stationn,imgoutDir)
%   This function is to used for reading TSI image data from MySQL server
%   which is solar.bnl.gov at present. The user name is 'redownload' and
%   password is 'radandtsi'. The matlab code will call java code internally
%   to complete the downloading of TSI. This function will get sequential
%   images based on freq input.
%   The radiation values are CMP which is direct normal values at present.
%
%   IN:
%       startdv         --  start date vector,  [2012 03 01 0 0 0]
%       enddv           --  end date vector,  [2012 03 01 12 0 0]
%       freq            --  INT, final time span of consecutive images
%       RepFreq         --  INT, if TS is missing, in TS-ReqFreq~TS+ReqRreq
%                           range, it will choose one to replace
%       minFreq         --  INT, this is the Timespan to downloaded from DB,
%                           always smaller than freq.
%       Stationn        --  Station #, C1 -> 1
%       imgoutDir       --  Directory, put downloaded images into this
%
%   OUT:
%       imgoutDir\*     --  output images
%                ...                     ...
%   VERSION 1.0     2012-04-20

function GetSeqTSIImgFromDB_withFilter(startdv,enddv,Freq,RepFreq,minfreq,Stationn,imgoutDir)
% MySQLServerURL='solar.bnl.gov';
% username='redownload';
% password='radandtsi';
% DBName='bnlwx_new';
% TBName='SolarBaseStation_Sec1';
Datefrom=sprintf('%d-%d-%d',startdv(1),startdv(2),startdv(3));
Dateto=sprintf('%d-%d-%-d',enddv(1),enddv(2),enddv(3));
Timefrom=sprintf('%d:%d:%d',startdv(4),startdv(5),startdv(6));
Timeto=sprintf('%d:%d:%d',enddv(4),enddv(5),enddv(6));
Frequency=sprintf('%ds',minfreq);  %minimum freq = 1s
Stationstr=sprintf('%d',Stationn);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   This is for window path name
regexprep(imgoutDir,'/','\');       
if(imgoutDir(end)~='\')
    imgoutDir=sprintf('%s%s',imgoutDir,'\');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cmmd=sprintf(...
    'java -jar GetTSIImgfromDB.jar %s %s %s %s %s %s %s',...
    Datefrom,Dateto,Timefrom,Timeto,Frequency,Stationstr,imgoutDir);
disp(cmmd);
dos(cmmd);

FilterImgbyFreq(imgoutDir,Freq,RepFreq);
end

