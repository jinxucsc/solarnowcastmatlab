%   This function includes name format and upside down code
%   Date Number format.
%       From: 2011-12-01_13_03_09.0.jpeg
%       to:   bnltsiskyimageC1.a1.20111201.130309.jpg.20111201130309.jpg
%   
%   IN:
%       inputDir        --- the directory which contains input image
%       outputDir       --- Directory, output images    
%   OUT:
%   VERSION 1.1 2012-04-20

function BNLUpsideDown(inputDir,outputDir)
%   BNLUptoDown is to put all images in inputDir upside down and change it
%   to formal format of the name.
if(inputDir(end)~='/')
    inputDir=sprintf('%s%s',inputDir,'/');
end
if(outputDir(end)~='/')
    outputDir=sprintf('%s%s',outputDir,'/');
end
mkdir(outputDir);

%   1. Read each file in and output upsidedown image to output/ directory
allfiles=dir(inputDir);
[m,~]=size(allfiles);

for i=3:m;
    filename=allfiles(i).name;
    fulloutp=sprintf('%s%s',outputDir,filename);
    fullinputp=sprintf('%s%s',inputDir,filename);
    img1=imread(fullinputp);
    img1(:,:,1)=flipud(img1(:,:,1));
    img1(:,:,2)=flipud(img1(:,:,2));
    img1(:,:,3)=flipud(img1(:,:,3));
    %column upside down
    img1(:,:,1)=fliplr(img1(:,:,1));
    img1(:,:,2)=fliplr(img1(:,:,2));
    img1(:,:,3)=fliplr(img1(:,:,3));
    imwrite(img1,fulloutp,'jpg');
end

end