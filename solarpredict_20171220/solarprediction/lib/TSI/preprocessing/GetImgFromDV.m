%   Vesion:     1.0 2013-05-28  Add site into it
function [fullname,filename]=GetImgFromDV(option,datevector,imgpath)
%   option  0   sgp
%           1   twp
%   datevector  including year,month,day,hour,min,seconds
%   imgpath     image directory with '/'

%twptsiskyimageC1.a1.20110501.000000.jpg.20110501000000.jpg

if(option==1)
    prefix='twptsiskyimageC1.a1.';
else
    prefix='sgptsiskyimageC1.a1.';
end
year=int2str(datevector(1));
month=int2str(datevector(2));
if(length(month)==1)
    month=strcat('0',month);
end
day=int2str(datevector(3));
if(length(day)==1)
    day=strcat('0',day);
end
hour=int2str(datevector(4));
if(length(hour)==1)
    hour=strcat('0',hour);
end
minute=int2str(datevector(5));
if(length(minute)==1)
    minute=strcat('0',minute);
end

SEC='00';
second=int2str(datevector(6));
if(length(second)==1)
    second=strcat('0',second);
end
filename=sprintf('%s%s%s%s.%s%s%s.jpg.%s%s%s%s%s%s.jpg',prefix,year,month,day,hour,minute,SEC,year,month,day,hour,minute,second);
fullname=sprintf('%s%s',imgpath,filename);
%img=imread(Fullname);
end