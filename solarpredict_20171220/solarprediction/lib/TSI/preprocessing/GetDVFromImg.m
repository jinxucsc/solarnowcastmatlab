%   FUNCTION datevector=GetDVFromBNLImg(filename)
%
%   Different 1.0 Version is used for different sites based on name
%   Verision 2.0 is ssed for ALL SITES with any prefix or sufix
%   VALID from 2000/01/01 to PRESENT. Before 2000/01/01 will raise error
%   From:
%       ...\bnltsiskyimagec1.a1.20120101.160057.jpg.20120101160057.jpg
%       ...\SGPtsiskyimagec1.a1.20120101.160057.jpg.20120101160057.jpg
%       ...\TWPtsiskyimagec1.a1.20120101.160057.jpg.20120101160057.jpg
%       ...\bnltsiskyimagec1.a2.20120101.160057.jpg.20120101160057.jpg
%   To:
%       datevector = (2012,01,01,16,00,57)
%   Version:    2.0
%   Date:       2012-02-01
%   Version:    2.1 2013-05-28 add filter before getting dv
function datevector=GetDVFromImg(filename)
datevector=0;

filename=GetFilenameFromAbsPath(filename);
indices=strfind(filename,'.');
if length(indices)<5
    return;
end
i=indices(5);
if(strcmp(filename(i+1:i+2),'20')==0)
    disp('Please Check your Image input');
    datevector=[0 0 0 0 0 0];
else
    year=str2num(filename(i+1:i+4));
    month=str2num(filename(i+5:i+6));
    day=str2num(filename(i+7:i+8));
%     hour=str2num(filename(i+10:i+11));
%     minute=str2num(filename(i+12:i+13));
%     second=str2num(filename(i+14:i+15));
    hour=str2num(filename(i+9:i+10));
    minute=str2num(filename(i+11:i+12));
    second=str2num(filename(i+13:i+14));
    datevector=[year month day hour minute second];
end



end