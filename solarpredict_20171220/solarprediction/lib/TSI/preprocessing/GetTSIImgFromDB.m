%GetTSIImgFromDB(startdv,enddv,Freq,Stationn,imgoutDir):
%   This function is to used for reading TSI image data from MySQL server
%   which is solar.bnl.gov at present. The user name is 'redownload' and
%   password is 'radandtsi'. The matlab code will call java code internally
%   to complete the downloading of TSI
%   The radiation values are CMP which is direct normal values at present.
%
%   IN:
%       startdv         --  start date vector,  [2012 03 01 0 0 0]
%       enddv           --  end date vector,  [2012 03 01 12 0 0]
%       freq            --  INT,time span of consecutive images
%       Stationn        --  Station #, C1 -> 1
%       imgoutDir       --  Directory, put downloaded images into this
%
%   OUT:
%       imgoutDir\*     --  output images
%                ...                     ...
%   VERSION 1.0     2012-04-20

function GetTSIImgFromDB(startdv,enddv,Freq,Stationn,imgoutDir)
% MySQLServerURL='solar.bnl.gov';
% username='redownload';
% password='radandtsi';
% DBName='bnlwx_new';
% TBName='SolarBaseStation_Sec1';
Datefrom=sprintf('%d-%d-%d',startdv(1),startdv(2),startdv(3));
Dateto=sprintf('%d-%d-%-d',enddv(1),enddv(2),enddv(3));
Timefrom=sprintf('%d:%d:%d',startdv(4),startdv(5),startdv(6));
Timeto=sprintf('%d:%d:%d',enddv(4),enddv(5),enddv(6));
Frequency=sprintf('%ds',Freq);  %minimum freq = 1s
Stationstr=sprintf('%d',Stationn);

imgoutDir=FormatDirName(imgoutDir);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   This is for window path name
% imgoutDir=regexprep(imgoutDir,'/','\');       
% if(imgoutDir(end)~='\')
%     imgoutDir=sprintf('%s%s',imgoutDir,'\');
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


cmmd=sprintf(...
    'java -jar GetTSIImgfromDB.jar %s %s %s %s %s %s %s',...
    Datefrom,Dateto,Timefrom,Timeto,Frequency,Stationstr,imgoutDir);
disp(cmmd);
dos(cmmd);

end

