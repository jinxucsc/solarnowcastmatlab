function RotateMatrix(inputf)
inputma=rgb2gray(imread(inputf));
inputma(inputma>0)=255;
tmdv=GetDVFromImg(inputf);

sbmaskma=inputma;
sbmaskdn=datenum(tmdv);

save('sbmaskinfo.mat','sbmaskma','sbmaskdn');
end