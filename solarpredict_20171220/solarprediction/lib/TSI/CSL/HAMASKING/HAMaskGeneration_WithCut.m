%HAMaskGeneration
%   This function is to test Green/Blue ratio, and plot them out in outDir.
%   IN:
%       tsiDir  --  tsi image directory
%       outDir  --  output directory that contains all the test images
%
%   OUT:
%       outDir/*    --  all the output GB ratio images

function HAMaskGeneration_WithCut(tsiDir,outDir,haareaf,cameraf)
if(tsiDir(end)~='/')
    tsiDir=sprintf('%s%s',tsiDir,'/');
end
if(outDir(end)~='/')
    outDir=sprintf('%s%s',outDir,'/');
end
mkdir(outDir);
load('UndistortionParameters.mat');

%   Set images visibility
% set(0,'DefaultFigureVisible', 'on');
set(0,'DefaultFigureVisible', 'off');

% hawinma=rgb2gray(imread(smallwinf));
% hawinma(hawinma>0)=1;
% hawinma=logical(hawinma);
SmallWinSize=15;

haarea=rgb2gray(imread(haareaf));
haarea(haarea>0)=1;
haarea=logical(haarea);
caarea=rgb2gray(imread(cameraf));
caarea(caarea>0)=1;
caarea=logical(caarea);
indexarr=find(caarea==0);
[starty,startx]= ind2sub(size(caarea),indexarr(1));
[endy,endx]= ind2sub(size(caarea),indexarr(end));
%   Starty is fixed for camera mask only change startx
Hca=endy-starty+1;
Wca=endx-startx+1;
ca_sy=starty;ca_ey=endy;
rca_sy=starty;rca_ey=endy;


indexarr=find(haarea==0);
[starty,startx]= ind2sub(size(haarea),indexarr(1));
[endy,endx]= ind2sub(size(haarea),indexarr(end));
Ha=endy-starty+1;
Wa=endx-startx+1;
NofWin=Wa-SmallWinSize+1;
SmallWins=logical(zeros(Ha,SmallWinSize,NofWin));
startiy=1;startix=1;
tmsy=startiy;tmey=startiy+Ha-1;
realsy=starty;realey=endy;



allfiles=dir(tsiDir);
[m,~]=size(allfiles);

filename=allfiles(3).name;
inputf=sprintf('%s%s',tsiDir,filename);
inputim=imread(inputf);
[Hi,Wi,~]=size(inputim);
% maxma=int16(maxma);
startpos=zeros(m-2,1);
outim=zeros(endy-starty+1,endx-startx+1,3);
maxma=zeros(endy-starty+1,endx-startx+1);
hamaimg=uint8(zeros(Hi,Wi));
hama=logical(zeros(Hi,Wi));
% outim=int16(outim);
testfilldir='testfill/';
mkdir(testfilldir);

for i=3:m
    %   1. Do the cutting first for the full image
    filename=allfiles(i).name;
    inputf=sprintf('%s%s',tsiDir,filename);
    outputf=sprintf('%s%s.mat',outDir,filename);
    inputim=imread(inputf);
    datevector=GetDVFromImg(filename);
    time.year    =  datevector(1);
    time.month   =  datevector(2);
    time.day     =  datevector(3);
    time.hour    =  datevector(4);
    time.min    =   datevector(5);
    time.sec    =   datevector(6);
    time.UTC    =   0;
    sun = sun_position(time, location);
    az=sun.azimuth;
    if(az>180)
%         tmca_sy=rca_sy-5;
%         tmca_ey=rca_ey-5;
        tmca_sy=rca_sy;
        tmca_ey=rca_ey;
    else
        tmca_sy=rca_sy;
        tmca_ey=rca_ey;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fillf=sprintf('%s%s',testfilldir,filename);
%     if(exist(fillf,'file')~=0)
%         continue;
%     end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    outim(:,:,1)=inputim(starty:endy,startx:endx,1);
    outim(:,:,2)=inputim(starty:endy,startx:endx,2);
    outim(:,:,3)=inputim(starty:endy,startx:endx,3);
    
    %   2.  Do edge detection for the cutted image matrix
    %   Method 1 -- test sobel for blue only
    maxma(:,:)=max(outim(:,:,2),outim(:,:,1));
    maxma(:,:)=max(outim(:,:,3),maxma);
    tmhama=edge(maxma,'sobel');
    
    %   3.  Do correlation based on white count
    lmax=-1;jmax=-1;
    for j=1:NofWin
        tmsx=startix+(j-1);
        tmex=tmsx+SmallWinSize-1;
        l1=length(find(tmhama(tmsy:tmey,tmsx:tmex)>0));
        if(l1>lmax)
            lmax=l1;
            jmax=j;
        end
    end
    startpos(i-2,1)=jmax;
    
    %   4.  Generate mask file
%     tmsx=startix+(jmax-1);
%     tmex=tmsx+SmallWinSize-1;
    realsx=startx+(jmax-1);
    realex=realsx+SmallWinSize-1;
    hama(:)=1;
    hama(realsy:realey,realsx:realex)=0;
    hamaimg=inputim;
    hamaimg(realsy:realey,realsx:realex,1)=0;
    hamaimg(realsy:realey,realsx:realex,2)=0;
    hamaimg(realsy:realey,realsx:realex,3)=0;
    
    camid_x=round((realsx+realex)/2);
    ca_sx=round(camid_x-Wca/2);
    ca_ex=round(camid_x+Wca/2);
    ca_sy=tmca_sy;
    ca_ey=tmca_ey;
    hama(ca_sy:ca_ey,ca_sx:ca_ex)=0;
    hamaimg(ca_sy:ca_ey,ca_sx:ca_ex,1)=0;
    hamaimg(ca_sy:ca_ey,ca_sx:ca_ex,2)=0;
    hamaimg(ca_sy:ca_ey,ca_sx:ca_ex,3)=0;
    
    
    imwrite(hamaimg,fillf,'jpg');
    save(outputf,'hama');
    
end



end