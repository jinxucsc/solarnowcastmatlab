%GenAzimuthMatFile: This function is used for generate azimuthfile from
%   testfill directory. After picking clear sky library cases.All the images
%   left in the testfill directory will be used to extract datenum and
%   azimuth,zenith. Copy the related sbmask and hamask to local sbmask
%   directory and hamask directory.
%   IN:
%       out_hamaskDir       --  Directory, containing all hamasks
%       out_sbmaskoutDir    --  Directory, containing all the sbmask files
%       out_testfilldir     --  Directory, containning all the clear sky
%                               images(after selecting)
%       sbmaskdir(in)       --  Directory, containing its own sbmasks
%       hamaskdir(in)       --  Directory, containing its own hamasks
%       UndistortionParameters.mat(in) --   original image information
%       
%   OUT:
%       HA_Azimuth.mat         --  Azimuth,Zenith Based on hamask 
%       SB_Azimuth.mat         --  Azimuth,Zenith Based on sbmask  
%   VERSION 1.2     2012-04-16    
%       Manually assign hamask and sbmaskout directory and testfill
%       directory. Copy masks files to local directory
%       Seperate Hamask and SBmask. Generate two internal output azimuth
%       file for masking
function GenAzimuthMatFile(in_hamaskDir,in_sbmaskDir,in_hatfDir,in_sbtfDir,out_hamaskDir,out_sbmaskoutDir)
load('UndistortionParameters.mat');
if(out_hamaskDir(end)~='/')
    out_hamaskDir=sprintf('%s%s',out_hamaskDir,'/');
end
if(out_sbmaskoutDir(end)~='/')
    out_sbmaskoutDir=sprintf('%s%s',out_sbmaskoutDir,'/');
end
% if(out_testfillDir(end)~='/')
%     out_testfillDir=sprintf('%s%s',out_testfillDir,'/');
% end
if(exist(out_testfillDir,'dir')==0||exist(out_hamaskDir,'dir')==0)
    disp('Input directory not exist. Please check inputs!');
end
outf1='HA_Azimuth.mat';
outf2='SB_Azimuth.mat';

% testfillDir='testfill/';    %internal input
% sbmaskDir='sbmask/';        %internal input
% hamaskDir='hamask/';        %internal input
% mkdir(sbmaskDir);
% mkdir(hamaskDir);

haallfiles=dir(in_hatfDir);
sballfiles=dir(in_sbtfDir);
[m1,~]=size(haallfiles);
[m2,~]=size(sballfiles);
tsdn1=zeros(m1,1);
azs1=zeros(m1,1);
zes1=zeros(m1,1);
filestart=0;
for i=1 :m1
    if(haallfiles(i).isdir~=1)
        filename=haallfiles(i).name;
        if(strcmpi(filename(end-3:end),'.jpg')==0)
            disp('ERROR: file is not a .jpg file,please check your input!');
            break;
        end
        hamaskfp=sprintf('%s%s.mat',in_hamaskDir,filename);
%         hamaskfp=sprintf('%s%s.mat',hamaskDir,filename);
        out_hamaskfp=sprintf('%s%s.mat',out_hamaskoutDir,filename);
        if(exist(out_hamaskfp,'file')==0)
            disp('ERROR: the sbmask or hamask does not exist!');
            break;
        end
        copyfile(hamaskfp,out_hamaskfp);    % copy the mask files to local directory
        datevector=GetDVFromImg(filename);
        time.year    =  datevector(1);
        time.month   =  datevector(2);
        time.day     =  datevector(3);
        time.hour    =  datevector(4);
        time.min    =   datevector(5);
        time.sec    =   datevector(6);
        time.UTC    =   0;
        sun = sun_position(time, location);
        az=sun.azimuth/180*pi;
        ze=sun.zenith/180*pi;
        azs2(i)=az;
        zes2(i)=ze;
        tsdn2(i)=datenum(datevector);
    else
        filestart=filestart+1;
    end
end
tsdn=tsdn1(filestart+1:end);
azs=azs1(filestart+1:end);
zes=zes1(filestart+1:end);
save(outf1,'tsdn','azs','zes');

tsdn2=zeros(m2,1);
azs2=zeros(m2,1);
zes2=zeros(m2,1);
filestart=0;
for i=1 :m2
    if(haallfiles(i).isdir~=1)
        filename=sballfiles(i).name;
        if(strcmpi(filename(end-3:end),'.jpg')==0)
            disp('ERROR: file is not a .jpg file,please check your input!');
            break;
        end
        sbmaskfp=sprintf('%s%s.mat',in_sbmaskDir,filename);
%         hamaskfp=sprintf('%s%s.mat',hamaskDir,filename);
        out_sbmaskfp=sprintf('%s%s.mat',out_sbmaskoutDir,filename);
        if(exist(out_sbmaskfp,'file')==0)
            disp('ERROR: the sbmask or hamask does not exist!');
            break;
        end
        copyfile(sbmaskfp,out_sbmaskfp);    % copy the mask files to local directory
        datevector=GetDVFromImg(filename);
        time.year    =  datevector(1);
        time.month   =  datevector(2);
        time.day     =  datevector(3);
        time.hour    =  datevector(4);
        time.min    =   datevector(5);
        time.sec    =   datevector(6);
        time.UTC    =   0;
        sun = sun_position(time, location);
        az=sun.azimuth/180*pi;
        ze=sun.zenith/180*pi;
        azs2(i)=az;
        zes2(i)=ze;
        tsdn2(i)=datenum(datevector);
    else
        filestart=filestart+1;
    end
end
tsdn=tsdn2(filestart+1:end);
azs=azs2(filestart+1:end);
zes=zes2(filestart+1:end);
save(outf1,'tsdn','azs','zes');




end