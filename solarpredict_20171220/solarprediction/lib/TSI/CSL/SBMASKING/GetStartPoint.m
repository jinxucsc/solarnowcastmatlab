    function [x,y]= GetStartPoint(x0,y0,a,b,c)
        %ax+by+c=0
        xp=((b^2-a^2)*x0-2*a*b*y0-2*a*c)/(a^2+b^2);
        yp=((a^2-b^2)*y0-2*a*b*x0-2*b*c)/(a^2+b^2);
        x=(x0+xp)/2;
        y=(y0+yp)/2;
    end