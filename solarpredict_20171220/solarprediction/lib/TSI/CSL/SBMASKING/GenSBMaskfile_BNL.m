% GenSBMaskfile_BNL:
%   If we have clear sky timestamps to build a library. Then read it and
%   Generate all the mask files and put them together into sbmask and
%   hamask files.
%
%   INPUT:
%       DataDir         --  Contains the original images
%       SBMaskOutDir    -- Direcotry, contains output sbmasks
%       TestDir(test)       --  test the mask images
%       UndistortionParameters.mat(in)--Original Image information
%
%   OUTPUT:
%       SBMaskOutDir/*    -- All the mask files(.mat) will be put here
%       TestDir/*    -- Test result for itself. Fore later manually pick
%
%	VERSION: 1.0    2012/3/30   BuildClearSkyLib
%   VERSION: 2.0   2012-04-30

function GenSBMaskfile_BNL(DataDir,SBMaskOutDir)
%   if not exist define as default
if(exist('DataDir','var')==0)
    DataDir='input/';
end
if(exist('SBMaskOutDir','var')==0)
    SBMaskOutDir='sbmaskout/';
end
if(DataDir(end)~='/')
    DataDir=sprintf('%s%s',DataDir,'/');
end
if(SBMaskOutDir(end)~='/')
    SBMaskOutDir=sprintf('%s%s',SBMaskOutDir,'/');
end

if(exist('TestDir','var')==0)
    TestDir='test/';
end
if(TestDir(end)~='/')
    TestDir=sprintf('%s%s',TestDir,'/'); %#ok<NASGU>
end
testfilldir='testfill_specialtest_20120409/';        %   test fill is to see SB filling result
mkdir(testfilldir);
mkdir(SBMaskOutDir);
load('UndistortionParameters.mat');
load('reflines.mat');
ref_tmdv=tmdv;
datevector=ref_tmdv;
time.year    =  datevector(1);
time.month   =  datevector(2);
time.day     =  datevector(3);
time.hour    =  datevector(4);
time.min    =   datevector(5);
time.sec    =   datevector(6);
time.UTC    =   0;
sun = sun_position(time, location);
ref_az=sun.azimuth/180*pi+AzumithShift/180*pi;
ref_ze=sun.zenith/180*pi+ZenithShift/180*pi;
ref_as=as;
ref_bs=bs;

% [m,~]=size(tsdn);
% azs=zeros(m,1);
% zes=zeros(m,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pre-set parameters for edge detection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ORI_WIDTH=Wi;
ORI_HEIGHT=Hi;
rcentrep.x=ORI_WIDTH/2+CenShiftX;     %real original center(rotating center)
rcentrep.y=ORI_HEIGHT/2+CenShiftY;

% midpos=zeros(1,2);
% midd=zeros(1,2);

SDIST_START2END=200;        %   Generated search array length, which is
SunSpotRange=20;            %   Sun range around sunpositon which will not
FillRadius=230;              %   Fill the Sun range(SPX-ran:SPX+ran)
UpperLine_DistMovement=43;       %  Upperline is to control the upper end of
%  the shadow band

%   generate any edge detection
%   distributed along the azumith line.
BoundSearchRange=   35;    % from x-30 ->   x+30Edge
BoundaryThres=      0;    % drop range exceeds 30 then treat as boundary
BrightThres=        150;   % if lines points larger than threshold no boundary searche
SearchPixelLen=   200;     % The search pixel number along the mid line
LineShift=  3;      %   Edge move N pixels out to get tolerant line
AngleDist=5/180*pi;        %   Upper bound and lower bound should have less than this threshold angle dfference to middline
TwoLineDist= 40;    %    minimum dist between two edge lines.

inputmatrix=zeros(Hi,Wi);
inputmatrix=uint8(inputmatrix);
greyimg=zeros(Hi,Wi);
greyimg=uint8(greyimg);
greyma=zeros(Hi,Wi);
greyma=uint8(greyma);
gma=zeros(Hi,Wi);
gma=uint8(gma);

maskindexma=false(Hi,Wi);

for xr=1:Wi
    for yr=1:Hi
        if(sqrt((xr-rcentrep.x)^2+(yr-rcentrep.y)^2)<=FillRadius)
            maskindexma(yr,xr)=1;
        end
    end
end

saz=pi;
eaz=3*pi/2;
lon=location.longitude;
lat=location.latitude;
alt=location.altitude;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
allfiles=dir(DataDir);
[m,~]=size(allfiles);
rcentrep_x=rcentrep.x;
rcentrep_y=rcentrep.y;
azshift=AzumithShift;
zeshift=ZenithShift;
ori_w=Wi;
ori_h=Hi;
parfor i =3:m
    filename=allfiles(i).name;
    if(allfiles(i).bytes<=1000)
        continue;
    end
    inputf=sprintf('%s%s',DataDir,filename);
    tmdv=GetDVFromImg(filename);
    %     datevector=datevec(tsdn(i));
    %     datevector=tmdv;
    %     time.year    =  datevector(1);
    %     time.month   =  datevector(2);
    %     time.day     =  datevector(3);
    %     time.hour    =  datevector(4);
    %     time.min    =   datevector(5);
    %     time.sec    =   datevector(6);
    %     time.UTC    =   0;
    %     sun = sun_position(time, location);
    sun=INNER_GETAandZ(tmdv,lat,lon,alt);
    az=sun.azimuth/180*pi;
    ze=sun.zenith/180*pi;
    [x,y]=CalImageCoorFrom_ALL(az,ze);  %(x,y) is the SP on ori
    az=az+azshift/180*pi;
    ze=ze+zeshift/180*pi;       % calibration
    
    %     if(az<saz||az>eaz)
    %         continue;
    %     end
    %     azs(i)=az;
    %     zes(i)=ze;
    %     filename=GetImgFromDV_TWPC1(datevector);
    %     inputf=sprintf('%s%s',DataDir,filename);
    if(exist(inputf,'file')==0)
        continue;
    end
    outfma=sprintf('%s%s.mat',SBMaskOutDir,filename); %#ok<NASGU>
    %     if(exist(outfma,'file')~=0)
    %         continue;
    %     end
    outputf=sprintf('%s%s',testfilldir,filename); %#ok<NASGU>
    if(exist(outputf,'file')~=0)
        continue;
    end
    img1=imread(inputf);    %original image readin
    f=img1(:,:,3);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   SB Mask
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    sbma=edge(f,'sobel');
    sbedgema=sbma;
    %     sbmaskf=sprintf('%s%s.mat',SBMaskDir,filename);
    %     save(sbmaskf,'sbma');
    %     sbma=uint8(zeros(Hi,Wi));
    %     index=((f>=BLUETHRES_SB)&~index1);   %SB Extraction
    %     sbma(index)=1;
    %     %     outm=zeros(Hi,Wi);
    %     %     outp=sprintf('%s%s',TestDir,filename);
    %     %     outm(index)=255;
    %     %     imwrite(outm,outp,'jpg');
    %     sbmaskf=sprintf('%s%s.mat',SBMaskDir,filename);
    %     save(sbmaskf,'sbma');
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   SB Edge test -- single
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    azdiff=az-ref_az;
    if(azdiff==pi/2)
        newref_as=1./ref_as;
    else
        if isempty(find((ref_as.*tan(azdiff)-1)==0, 1)) ==0
            disp('pi/2 appears, do not consider this situation');
            continue;
        end
        newref_as=(ref_as+tan(azdiff))./(1-ref_as.*tan(azdiff));    % angle between reference and present az
    end
    
    aangle=az-pi/2;             % az angle in x,y coordinate system
    if(abs(double(aangle)-double(pi/2))<0.001)
        % x=x1
        a_midline=-9999;        % stands for infinite
        b_midline=-9999;
    else
        a_midline=tan(aangle);
        b_midline=y-a_midline*x;
    end
    midpos=[rcentrep_x,rcentrep_y];
    [tmpx,tmpy]=GetEndPoint(midpos(1),midpos(2),...
        a_midline,b_midline,SDIST_START2END);
    %   Start point has the same vector direction with sun position then
    %   we can get right end point
    midd=zeros(2,1);
    if(sign(x-midpos(1))==sign(tmpx(1)-midpos(1)))
        midd(1)=tmpx(1);
        midd(2)=tmpy(1);
    else
        midd(1)=tmpx(2);
        midd(2)=tmpy(2);
    end
    
    if(x-midpos(1)>0)
        [midd(1),tmpp]=max(tmpx);
        midd(2)=tmpy(1,tmpp);
    else
        [midd(1),tmpp]=min(tmpx);
        midd(2)=tmpy(1,tmpp);
    end
    inputmatrix=sbma;
    greyimg=inputmatrix(:,:);
    [SXArray,SYArray]=GetSearchArray(midpos,midd,SearchPixelLen);
    
    %   Rebuild SXArray and SYArray to fit the edge
    %     [SXArray,SYArray]=ReGenArrayOnBoundary(SXArray,SYArray,x,y,SunSpotRange);
    [B_UpArray,B_DownArray]=FindBoundary(SXArray,SYArray,BoundSearchRange,...
        BoundaryThres,BrightThres,a_midline,b_midline,greyimg);
    %   if there are less than 20 points of boundaries found, skip this
    %   image as the fitting result can not be good
    if (length(B_UpArray)<=20||length(B_DownArray)<=20)
        continue;
    end
    
    LPUpArray=     [B_UpArray(B_UpArray(:,1)>0) B_UpArray(B_UpArray(:,2)>0,2)];
    LPDownArray=   [B_DownArray(B_DownArray(:,1)>0) B_DownArray(B_DownArray(:,2)>0,2)];
    lpdist=LPUpArray(:,1).*a_midline-LPUpArray(:,2)+b_midline;
    id=abs(lpdist-mean(lpdist))<=2*std(lpdist);
    tmpx = LPUpArray(id,1);
    tmpy = LPUpArray(id,2);
    if(isempty(tmpx)~=0||isempty(tmpy)~=0)
        %   disp('No boundary found, skip this TS');
        continue;
    end
    if length(tmpx)<=20||length(tmpx)<=20
        %   Less than twenty points, donot do curve fitting
        continue;
    end
    LPUpArray=[tmpx,tmpy];
    lpdist=LPDownArray(:,1).*a_midline-LPDownArray(:,2)+b_midline;
    id=abs(lpdist-mean(lpdist))<=2*std(lpdist);
    tmpx = LPDownArray(id,1);
    tmpy = LPDownArray(id,2);
    LPDirect=LPDirection(az);
    LPDownArray=[tmpx,tmpy];
    if(isempty(tmpx)~=0||isempty(tmpy)~=0)
        %   disp('No boundary found, skip this TS');
        continue;
    end
    if length(tmpx)<=20||length(tmpx)<=20
        continue;
    end
    
    [as bs]=FindLinearApprox(LPUpArray,LPDownArray,LPDirect);
    LinRegreDropThres=  8;
    [tmpx,tmpy]=GetNewLineArrays(as(1),bs(1),LPUpArray(:,1),LPUpArray(:,2),...
        LinRegreDropThres,LPDirect);
    [tmpx1,tmpy1]=GetNewLineArrays(as(2),bs(2),LPDownArray(:,1),...
        LPDownArray(:,2),LinRegreDropThres,LPDirect);
    if(isempty(tmpx1)~=0||isempty(tmpy1)~=0)
        %   disp('No boundary found, skip this TS');
        continue;
    end
    [asnew bsnew]=FindLinearApprox([tmpx,tmpy],[tmpx1,tmpy1],LPDirect);
    as=asnew;
    bs=bsnew;
    %     save('reflines.mat','LPDirect','as','bs','tmdv');
    
    
    %   filter points that are not regular with AngleDist
    %     midgrad=a_midline;
    midgrad=newref_as;
    if(LPDirect=='x')
        %Ax-y+B=0
        linegrad=as;    %-A/B
        tangenval=abs(linegrad-midgrad)./abs(1+linegrad.*midgrad);
        angledi=atan(tangenval)-AngleDist;
        normindex=find(angledi<=0);
        nofnorm=length(normindex);
        if(nofnorm==2)
            %             continue;
        elseif nofnorm ==1
            continue;
        elseif nofnorm ==0
            continue;
        else
            disp('Wrong dimension of a and b to form two lines!');
            continue;
        end
    else
        %Ay-x+B=0
        linegrad=1./as;    %1/A
        tangenval=abs(linegrad-midgrad)./abs(1+linegrad.*midgrad);
        angledi=atan(tangenval)-AngleDist;
        normindex=find(angledi<=0);
        nofnorm=length(normindex);
        if(nofnorm==2)
            %             continue;
        elseif nofnorm ==1
            %   Choose the normal one,the other one use its gradient
            continue;
        elseif nofnorm ==0
            %   Choose middle line, two lines both use its gradient
            continue;
        else
            disp('Wrong dimension of a and b to form two lines!');
            continue;
        end
    end
    
    
    if(LPDirect=='x')
        %Ax-y+B=0
        if(sign(as(1)*x+bs(1)-y)<0)
            bs(1)=bs(1)-LineShift;
        else
            bs(1)=bs(1)+LineShift;
        end
    else
        %Ay-x+B=0
        if(sign(as(1)*y+bs(1)-x)<0)
            bs(1)=bs(1)-LineShift;
        else
            bs(1)=bs(1)+LineShift;
        end
    end
    if(LPDirect=='x')
        %Ax-y+B=0
        if(sign(as(2)*x+bs(2)-y)<0)
            bs(2)=bs(2)-LineShift;
        else
            bs(2)=bs(2)+LineShift;
        end
    else
        %Ay-x+B=0
        if(sign(as(2)*y+bs(2)-x)<0)
            bs(2)=bs(2)-LineShift;
        else
            bs(2)=bs(2)+LineShift;
        end
    end
    %function [Xarr,Yarr]=GenFillPoints(SPx,SPy,FillRadius)
    
    
    %         Xarr=zeros((2*FillRadius)^2,1);
    %         Yarr=zeros((2*FillRadius)^2,1);
    %         ci=1;
%     SPx=round(x);
%     SPy=round(y);
    cenl_a=-1/a_midline;
    cenl_b=-rcentrep_x*cenl_a+rcentrep_y;
    cenl_sign=sign(x*cenl_a-y+cenl_b);
    uppl_a=cenl_a;
    uppl_b=cenl_b+cenl_sign*sqrt(1+cenl_a^2)*UpperLine_DistMovement;
%     gma=sbma;
    fillouterma=maskindexma;
    gma=true(ori_h,ori_w);
    for xr=1:ori_w
        for yr=1:ori_h
            if(fillouterma(yr,xr)==0)
%                 gma(yr,xr)=1;
                continue;
            end
            %   a. If beyond upper line then fill 1
            if(sign(uppl_a*xr-yr+uppl_b)~=cenl_sign)
                %                 greyimg(yr,xr)=255;
%                 gma(yr,xr)=1;
                continue;
            end
            %   b. Should be inside of two edge lines otherwise fill 1
            if(LPDirect=='x')
                if(sign(as(1)*xr-yr+bs(1))==sign(as(2)*xr-yr+bs(2)))
                    %                     greyimg(yr,xr)=255;
%                     gma(yr,xr)=1;
                    continue;
                end
            else
                if(sign(as(1)*yr-xr+bs(1))==sign(as(2)*yr-xr+bs(2)))
                    %                     greyimg(yr,xr)=255;
%                     gma(yr,xr)=1;
                    continue;
                end
            end
            % c. should be the same side with SP to central Line
            %    otherwise continue without filling
            %             if(sign(cenl_a*xr-yr+cenl_b)~=cenl_sign)
            %                 gma(yr,xr)=1;
            %                 continue;
            %             end
            % d. Should be in Fill Circle
            %    otherwise continue without filling
            %             if(sqrt((xr-x)^2+(yr-y)^2)>FillRadius)
            %                 gma(yr,xr)=1;
            %                 continue;
            %             end
            %             greyimg(yr,xr)=0;   %all condition satisfied
            gma(yr,xr)=0;
        end
    end
    %     greyimg=rgb2gray(img1);
    %     greyimg(gma>0)=255;
    %     greyimg(gma==0)=0;
    %     outfma=sprintf('%s%s.mat',SBMaskOutDir,filename);
    %     outputf=sprintf('%s%s',testfilldir,filename);
    %     imwrite(greyimg,outputf,'jpg');
    %             outputma(ii,:,:)=gma;
    
    %     save(maoutf,'gma');
    %     clear SXArray SYArray B_UpArray B_DownArray LPUpArray LPDownArray aa id tmpx tmpy tmpx1 tmpy1 greyma greyimg inputimg;
    sbma=gma;
    outfma=sprintf('%s%s.mat',SBMaskOutDir,filename);
    INNER_SBMASAVE('sbma',sbma,outfma);
    %     save(outfma,'sbma');
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Test for result put the result to TestDir
    %   Uncomment to use
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %     outputimf=sprintf('%s%s',TestDir,filename);
    %     totalmaskma=~(gma&hama);
    totalmaskma=~(gma);
    %     img2=img1;
    tmcolor=uint8(zeros(ori_h,ori_w));
    for j=1:3
        %         img2(totalmaskma,j)=img1(totalmaskma,j);
        tmcolor(:,:)=img1(:,:,j);
        tmcolor(totalmaskma)=0;
        tmcolor(sbedgema)=255;
        img1(:,:,j)=tmcolor;
    end
    outputf=sprintf('%s%s',testfilldir,filename);
    imwrite(img1,outputf,'jpg');
    %     outputf=sprintf('%s%s.mat',outputDir,filename);
end

end