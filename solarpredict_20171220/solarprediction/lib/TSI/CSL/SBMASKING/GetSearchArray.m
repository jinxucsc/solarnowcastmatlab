% function [sxarray,syarray]=GetSearchArray(a,b,mid,middirect,srange,searchdistance)
% tmpx=mid(1,1);
% tmpy=mid(1,2);
% sxarray=zeros(srange,1);  % dist=4 have 2 3 4 5
% syarray=zeros(srange,1);
% if(a==-9999)
%     % vertical such as x=1
%     sxarray(:)=tmpx;
%     syarray(:)=tmpy+1:tmpy+srange;
%     return;
% end
% diststep=searchdistance/srange;
% for tmpj=1:srange
%     [looptmx,looptmy]=GetEndPoint(tmpx,tmpy,a,b,tmpj*diststep);
%     if(middirect(1,1)-tmpx>0)
%         [sxarray(tmpj),looptmi]=max(looptmx);
%         syarray(tmpj)=looptmy(1,looptmi);
%     else
%         [sxarray(tmpj),looptmi]=min(looptmx);
%         syarray(tmpj)=looptmy(1,looptmi);
%     end
% end


function [sxarray,syarray]=GetSearchArray(midstart,midend,srange)
startx=midstart(1);
starty=midstart(2);
endx=midend(1);
endy=midend(2);
step_x=double((endx-startx))/(srange-1);
step_y=double((endy-starty))/(srange-1);
sxarray=round(midstart(1):step_x:midend(1));   %1xN
syarray=round(midstart(2):step_y:midend(2));   %1xN
tmma=unique([sxarray' syarray'],'rows');       %Nx2
sxarray=tmma(:,1)';
syarray=tmma(:,2)';

end
    

