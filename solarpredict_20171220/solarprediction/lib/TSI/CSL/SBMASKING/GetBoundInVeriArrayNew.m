%   Suppose Right now image is grey scale image which has 0-255 values, But
%   In previous step of preprocessing, only two level of grey scale left.
function [upp,lowp]=...
    GetBoundInVeriArrayNew(lowersarr,uppersarr,bthres,brightthres,imgin) %#ok<INUSD,INUSL>
[~,lsran]=size(lowersarr);   % 2 x N
[~,usran]=size(uppersarr);   % 2 x N


if(lsran==0||usran==0)
    %   means no vertical pos in imgin
    upp=[-9999,-9999];
    lowp=[-9999,-9999];
    return;
end

%   Lower bound search
xarr=lowersarr(1,:);
yarr=lowersarr(2,:);
grayarr=imgin(yarr,xarr);
edgei=find(grayarr>0);      %   White is the edge
if isempty(edgei) == 1
    lowp=[-9999,-9999];      % no such boundary
else
    tmi=edgei(end);         % the first index, closet to (maxsy,maxsy)
    lowp=[xarr(tmi),yarr(tmi)]; % lowp = [x , y]
end

%   Upper bound search
xarr=uppersarr(1,:);
yarr=uppersarr(2,:);
grayarr=imgin(yarr,xarr);
edgei=find(grayarr>0);      %   White is the edge
if isempty(edgei) == 1
    upp=[-9999,-9999];      % no such boundary
else
    tmi=edgei(end);         % the first index, closet to (maxsy,maxsy)
    upp=[xarr(tmi),yarr(tmi)]; % lowp = [x , y]
end

% ceni=ceil(tmpm/2);
% isfound=0;
% for loopj=ceni:-1:3
%     if(vertirany(loopj)==-1)
%         continue;
%     end
%     grayv=imgin(vertirany(loopj),vertiranx(loopj));
%     grayv1=imgin(vertirany(loopj-1),vertiranx(loopj-1));
%     dist1=abs(double(grayv)-double(grayv1));
%     if(dist1>bthres)
%         isfound=1;
%         lowp=[vertiranx(loopj),vertirany(loopj)];
%         break;      %first one then treat it as boundary
%     end
% end
% if(isfound~=1)
%     lowp=[-9999,-9999];      % no such boundary
% end
% 
% isfound=0;
% for loopj=ceni:1:tmpm-2
%     if(vertirany(loopj)==-1)
%         continue;
%     end
%     grayv=imgin(vertirany(loopj),vertiranx(loopj));
%     grayv1=imgin(vertirany(loopj+1),vertiranx(loopj+1));
%     %                 grayv2=imgin(round(vertirany(loopj+2)),round(vertiranx(loopj+2)));
%     dist1=abs(double(grayv)-double(grayv1));
%     if(dist1>bthres)
%         %find possible edge points get the far point
%         isfound=1;
%         upp=[vertiranx(loopj),vertirany(loopj)];
%         break;  %first one then treat it as boundary
%     end
% end
% if(isfound~=1)
%     upp=[-9999,-9999];      % no such boundary
% end


end