function [b_uparray,b_lowarray]=...
    FindBoundary(sxarray,syarray,bSearchRange,bthres,brightthres,a,b,imgin) %#ok<INUSL>
[~,loopmax]=size(sxarray);
b_uparray=zeros(loopmax,2);
b_lowarray=zeros(loopmax,2);

if(a==-9999)
    a_verti=0;
else
    if(a==0)
        a_verti=-9999;      %stands for infinite
    else
        a_verti=-1/a;
    end
end

[m,n]=size(imgin);
mindim=min([m,n]);
vertistartrange=bSearchRange-20;
for loopi=1:loopmax
    tmpx=sxarray(loopi);
    tmpy=syarray(loopi);
    b_verti=tmpy-a_verti*tmpx;  %b=y-a*x
    %     [vertiranx,vertirany]=...
    %         FindVertiSArray(a_verti,b_verti,tmpx,tmpy,bSearchRange,vertistartrange);
    [lowersarr,uppersarr]=...
        FindVertiSArray(a_verti,b_verti,tmpx,tmpy,bSearchRange,vertistartrange);

    
    if  (lowersarr(1,end)<0||lowersarr(1,end)>mindim||...
            lowersarr(2,end)<0||lowersarr(2,end)>mindim||...
            lowersarr(1,1)<0||lowersarr(1,1)>mindim||...
            lowersarr(2,1)<0||lowersarr(2,1)>mindim)
        continue;   % lower search array are not in min dimension
    end
    if  (uppersarr(1,end)<0||uppersarr(1,end)>mindim||...
            uppersarr(2,end)<0||uppersarr(2,end)>mindim||...
            uppersarr(1,1)<0||uppersarr(1,1)>mindim||...
            uppersarr(2,1)<0||uppersarr(2,1)>mindim)
        continue;   % upper search array are not in min dimension
    end
    
    %     if (vertiranx(1)<mindim)        &&(vertiranx(1)>0)  &&...
    %             (vertiranx(end)<mindim) &&(vertiranx(end)>0)&&...
    %             (vertirany(1)<mindim)   &&(vertirany(1)>0)  &&...
    %             (vertirany(end)<mindim) &&(vertirany(end)>0)
    %         %   if start and end points are in minimum dimension range no need
    %         %   to regenerate
    %     else
    %         [vertiranx,vertirany]=ReGenVertiArrayWithDim(vertiranx,vertirany,mindim);
    %     end
    %   Lower bound search
    [~,lsran]=size(lowersarr);   % 2 x N
    [~,usran]=size(uppersarr);   % 2 x N
    if(lsran==0||usran==0)
        %   means no vertical pos in imgin
        upp=[-9999,-9999];
        lowp=[-9999,-9999];
    else
        xarr=lowersarr(1,:);
        yarr=lowersarr(2,:);
        linearind=sub2ind([m n],yarr,xarr);
        grayarr=imgin(linearind);
        edgei=find(grayarr>0);      %   White is the edge
        if isempty(edgei) == 1
            lowp=[-9999,-9999];      % no such boundary
        else
            tmi=edgei(1);         % the first index, closet to (maxsy,maxsy)
            lowp=[xarr(tmi),yarr(tmi)]; % lowp = [x , y]
        end
        
        %   Upper bound search
        xarr=uppersarr(1,:);
        yarr=uppersarr(2,:);
        linearind=sub2ind([m n],yarr,xarr);
        grayarr=imgin(linearind);
        edgei=find(grayarr>0);      %   White is the edge
        if isempty(edgei) == 1
            upp=[-9999,-9999];      % no such boundary
        else
            tmi=edgei(end);         % the end index, closet to (maxey,maxey)
            upp=[xarr(tmi),yarr(tmi)]; % lowp = [x , y]
        end
    end
    %     [tmpup,tmpdown]=...
    %         GetBoundInVeriArray(vertiranx,vertirany,bthres,brightthres,imgin);
    b_uparray(loopi,:)=upp;
    b_lowarray(loopi,:)=lowp;
end

end
