function [x,y,pX,pY]=Search1stRoundofSunPos(img,GrayThreshold,SearchWinSize,SunPos,centrep)
    alpha=SunPos.Azumith;
    beta=SunPos.Zenith;
    [m,n]=size(img);
    %centrep=[300;300];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   1.Find line starting from centrep to edge
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    StartX=0;   %Initial value treat centre as origin
    StartY=0;   
    if(abs(tan(alpha))>=1)
        % should use x
        pX=zeros(n/2,1); %start from origin
        pY=zeros(n/2,1);
        for tmpi=1:n/2
            if(alpha<pi)
                pX(tmpi)=pX(tmpi)+tmpi;
            else
                pX(tmpi)=pX(tmpi)-tmpi;
            end
            pY(tmpi)=pX(tmpi)/tan(alpha);
        end
    else
        % should use y
        pX=zeros(m/2,1); %start from origin
        pY=zeros(m/2,1);
        for tmpi=1:m/2
            if(alpha>pi/2&&alpha<pi*3/2)
                pY(tmpi)=pY(tmpi)-tmpi;
            else
                pY(tmpi)=pY(tmpi)+tmpi;
            end
            pX(tmpi)=pY(tmpi)*tan(alpha);
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   2. Get most possible pixel position
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [loopmax,tmpn]=size(pX);
    cx=centrep(1)+pX;   % coordinate transform
    cy=centrep(2)-pY;
    maxn=0;
    maxPos=zeros(2,1);
    for tmpi=1:loopmax
        tmp=FindNumAround(cx(tmpi),cy(tmpi),img,GrayThreshold,SearchWinSize,m,n);
        if(tmp>maxn)
            maxn=tmp;
            maxPos(1)=cx(tmpi);
            maxPos(2)=cy(tmpi);
        end
    end


    x=maxPos(1);
    y=maxPos(2);
    
    function outputnum=FindNumAround(px,py,img,graythres,winsize,m,n)
        rangex=zeros(2,1);
        rangey=zeros(2,1);
        rangey(2)=round(py+winsize);
        rangey(1)=round(py-winsize);
        rangex(1)=round(px-winsize);
        rangex(2)=round(px+winsize);
        if(rangex(1)<1)
            rangex(1)=1;
        end
        if(rangey(1)<1)
            rangey(1)=1;
        end
        if(rangey(2)>m)
            rangey(2)=m;
        end
        if(rangex(2)>n)
            rangex(2)=n;
        end
        subimg=img(rangey(1):rangey(2),rangex(1):rangex(2));
        outputnum=length(find(subimg>graythres));
    end
end