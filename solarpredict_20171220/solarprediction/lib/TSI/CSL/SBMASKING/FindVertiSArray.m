%2012-04-12 Version 1.1
%   add controlling area code of vertisran
function [lowersarr,uppersarr]=FindVertiSArray(a,b,x0,y0,vertirange,vertisran)
% sranx=zeros(2*vertirange+1,1);
% srany=zeros(2*vertirange+1,1);
if(a==-9999)
    sranx(:)=x0;
    srany(:)=y0-vertirange:y0+vertirange;
    return;
end
%stepdist=1;
verdist=2*vertirange+1;
realrange=vertirange-vertisran;

%   Get (minsx,minsy) -> (minex,miney) range, avoid points in this
%   range later
[tmpsx,tmpsy]=GetEndPoint(x0,y0,a,b,vertisran);
%control area that should avoid searching
[minsx,minsxi]=min(tmpsx);
minsy=tmpsy(minsxi);
[minex,minexi]=max(tmpsx);
miney=tmpsy(minexi);

%   Generate range of upper search range and lower search range
%   (maxsx,maxsy),(maxex,maxey)
[tmpx,tmpy]=GetEndPoint(x0,y0,a,b,vertirange);
[maxsx,maxsxi]=min(tmpx);
maxsy=tmpy(maxsxi);
[maxex,maxexi]=max(tmpx);
maxey=tmpy(maxexi);

% lower upbound searching array (maxsx,maxsy)->(minsx,minsy)
maxsx=round(maxsx);
maxsy=round(maxsy);
minsx=round(minsx);
minsy=round(minsy);
step_x=double(minsx-maxsx)/(realrange-1);
step_y=double((minsy-maxsy))/(realrange-1);
if(step_x==0)
    yarr=round(maxsy:step_y:minsy);
    xarr=zeros(1,length(yarr));
    xarr(:)=minsx;
elseif(step_y==0)
    xarr=round(maxsx:step_x:minsx);
    yarr=zeros(1,length(xarr));
    yarr(:)=minsy;
else
    xarr=round(maxsx:step_x:minsx);     % 1 x N
    yarr=round(maxsy:step_y:minsy);
end

lowersarr=unique([xarr' yarr'],'rows')';   % 1 x N -> N x 2 -> 2 x N
%   Debug only
% [tmm,tmn]=size(lowersarr);
% if(tmm<2)
%     tmm
% end


% upper upbound searching array (minex,miney)->(maxex,maxey)
maxex=round(maxex);
maxey=round(maxey);
minex=round(minex);
miney=round(miney);
step_x=double(maxex-minex)/(realrange-1);
step_y=double((maxey-miney))/(realrange-1);
if(step_x==0)
    yarr=round(miney:step_y:maxey);
    xarr=zeros(1,length(yarr));
    xarr(:)=minex;
elseif(step_y==0)
    xarr=round(minex:step_x:maxex);
    yarr=zeros(1,length(xarr));
    yarr(:)=miney;
else
    xarr=round(minex:step_x:maxex);     % 1 x N
    yarr=round(miney:step_y:maxey);
end

uppersarr=unique([xarr' yarr'],'rows')';   % 1 x N -> N x 2 -> 2 x N


%         miney=tmpsy(1,tmpindex);

%         [tmpx,tmpy]=GetEndPoint(x0,y0,a,b,vertirange);
%         [sranx(1),tmpindex]=min(tmpx);
%         srany(1)=tmpy(1,tmpindex);
%
%         for tmpj=2:verdist
%             [looptmx,looptmy]=GetEndPoint(sranx(1),srany(1),a,b,tmpj-1);
%             [sranx(tmpj),tmpindex]=max(looptmx);
%             srany(tmpj)=looptmy(1,tmpindex);
%             if(sranx(tmpj)>minsx&&sranx(tmpj)<minex)
%                 sranx(tmpj)=-1; %   in the control area
%                 srany(tmpj)=-1; %   -1 means no searching
%             end
%         end
%         sranx=round(sranx);
%         srany=round(srany);
end


