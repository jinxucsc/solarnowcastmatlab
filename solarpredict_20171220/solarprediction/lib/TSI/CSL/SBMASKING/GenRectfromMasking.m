function [left,right,top,bottom]=GenRectfromMasking(maskma)
%   detect 0

% get mini
for i=1:1000
    if isempty(find(maskma(i,:)==0,1))
        continue;
    else
        break;
    end
end
mini=i;

%get maxi
for i=mini:1000
    if ~isempty(find(maskma(i,:)==0,1))
        continue;
    else
        break;
    end
end
maxi=i;

%get minj
for j=1:1000
    if isempty(find(maskma(:,j)==0,1))
        continue;
    else
        break;
    end
end
minj=j;

%get maxj
for j=minj:1000
    if ~isempty(find(maskma(:,j)==0,1))
        continue;
    else
        break;
    end
end
maxj=j;

left=minj;
right=maxj;
top=mini;
bottom=maxi;

end