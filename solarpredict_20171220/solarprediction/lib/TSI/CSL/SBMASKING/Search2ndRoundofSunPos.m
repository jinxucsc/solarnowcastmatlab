function [x,y,xavg,yavg]=Search2ndRoundofSunPos(img,GrayThreshold,SearchWinSize,SearchRange2nd,SunPos,Pos_1st)
    alpha=SunPos.Azumith;
    beta=SunPos.Zenith;
    [m,n]=size(img);
    centrep=[m/2;n/2];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   1.Find the maximum value during second search block
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    maxn=0;
    for loopi=-SearchRange2nd:1:SearchRange2nd
        for loopj=-SearchRange2nd:1:SearchRange2nd
            cx=Pos_1st(1)+loopi;
            cy=Pos_1st(2)+loopj;
            if(cx<1||cx>m||cy<1||cy>n)
                continue;       % exceed boundary
            end
            tmpn=FindNumAround(cx,cy,img,GrayThreshold,SearchWinSize,m,n);
            if(tmpn>maxn)
                maxn=tmpn;
                maxPos(1)=cx;
                maxPos(2)=cy;
            end
        end
    end

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   2.Find the maximum value pos and calculate their average value
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        posxa=-99;    % x array
        posya=-99;    % y array
        for loopi=-SearchRange2nd:1:SearchRange2nd
            for loopj=-SearchRange2nd:1:SearchRange2nd
                cx=Pos_1st(1)+loopi;
                cy=Pos_1st(2)+loopj;
                if(cx<1||cx>m||cy<1||cy>n)
                    continue;       % exceed boundary
                end
                tmpn=FindNumAround(cx,cy,img,GrayThreshold,SearchWinSize,m,n);
                if(tmpn==maxn)
                    if(posxa==-99)
                        % first time
                        posxa=cx;
                        posya=cy;
                    else
                        % not first time
                        posxa=[posxa;cx];
                        posya=[posya;cy];
                    end
                end
            end
        end
    xavg=mean(posxa);
    yavg=mean(posya);
    x=maxPos(1);
    y=maxPos(2);
    [x,y]=FindAvgPosWithThres(xavg,yavg,img,GrayThreshold+20,SearchWinSize+5,m,n);
    [x,y]=FindAvgPosWithThres(x,y,img,GrayThreshold+30,SearchWinSize,m,n);
    [x,y]=FindAvgPosWithThres(x,y,img,GrayThreshold+40,SearchWinSize,m,n);
    
    
    function [x1,y1]=FindAvgPosWithThres(px,py,img,graythres,winsize,m,n)
        rangex=zeros(2,1);
        rangey=zeros(2,1);
        rangey(2)=round(py+winsize);
        rangey(1)=round(py-winsize);
        rangex(1)=round(px-winsize);
        rangex(2)=round(px+winsize);
        if(rangex(1)<1)
            rangex(1)=1;
        end
        if(rangey(1)<1)
            rangey(1)=1;
        end
        if(rangey(2)>m)
            rangey(2)=m;
        end
        if(rangex(2)>n)
            rangex(2)=n;
        end
        subimg=img(rangey(1):rangey(2),rangex(1):rangex(2));
        [tmpm,tmpn]=size(subimg);
        tmpxarray=zeros(tmpn,1);
        tmpyarray=zeros(tmpm,1);
        tmpusedi=1;
        for loopii=1:tmpm
            for loopjj=1:tmpn
                if(subimg(loopii,loopjj)>=graythres)
                    tmpxarray(tmpusedi)=loopii;
                    tmpyarray(tmpusedi)=loopjj;
                    tmpusedi=tmpusedi+1;
                end
            end
        end
        tmpxarray=tmpxarray(tmpxarray>0);
        tmpyarray=tmpyarray(tmpyarray>0);
        x1=rangex(1)+mean(tmpxarray)-1;
        if(isnan(x1)==1)
            x1;
        end
        y1=rangey(1)+mean(tmpyarray)-1;
    end
    
    function outputnum=FindNumAround(px,py,img,graythres,winsize,m,n)
        rangex=zeros(2,1);
        rangey=zeros(2,1);
        rangey(2)=round(py+winsize);
        rangey(1)=round(py-winsize);
        rangex(1)=round(px-winsize);
        rangex(2)=round(px+winsize);
        if(rangex(1)<1)
            rangex(1)=1;
        end
        if(rangey(1)<1)
            rangey(1)=1;
        end
        if(rangey(2)>m)
            rangey(2)=m;
        end
        if(rangex(2)>n)
            rangex(2)=n;
        end
        subimg=img(rangey(1):rangey(2),rangex(1):rangex(2));
        outputnum=length(find(subimg>graythres));
    end
end