%GenAzimuthMatFile_HA: This function is used for generate azimuthfile from
%   hamask directory. 
%   IN:
%       in_hamaskdir       --  Directory, containing all hamasks
%       UndistortionParameters.mat(in) --   original image information
%   OUT:
%       haazimuth.mat         --  clear sky library
%   VERSION 1.2     2012-04-018    
%       
function GenAzimuthMatFile_HA(in_hamaskdir)
load('UndistortionParameters.mat');
outputf='haazimuth.mat';    % Internal input
if(in_hamaskdir(end)~='/')
    in_hamaskdir=sprintf('%s%s',in_hamaskdir,'/');
end
if(exist(in_hamaskdir,'dir')==0)
    disp('Input directory not exist. Please check inputs!');
end

allfiles=dir(in_hamaskdir);
[m,~]=size(allfiles);
tsdn=zeros(m,1);
azs=zeros(m,1);
zes=zeros(m,1);
filestart=0;
for i=1 :m
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        
        if(strcmpi(filename(end-3:end),'.mat')==0)
            disp('ERROR: file is not a .mat file,please check your input!');
            break;
        end
        datevector=GetDVFromImg(filename);
        time.year    =  datevector(1);
        time.month   =  datevector(2);
        time.day     =  datevector(3);
        time.hour    =  datevector(4);
        time.min    =   datevector(5);
        time.sec    =   datevector(6);
        time.UTC    =   0;
        sun = sun_position(time, location);
        az=sun.azimuth/180*pi+AzumithShift/180*pi;
        ze=sun.zenith/180*pi+ZenithShift/180*pi;
        azs(i)=az;
        zes(i)=ze;
        tsdn(i)=datenum(datevector);
    else
        filestart=filestart+1;
    end
end
tsdn=tsdn(filestart+1:end);
azs=azs(filestart+1:end);
zes=zes(filestart+1:end);

save(outputf,'tsdn','azs','zes');
end