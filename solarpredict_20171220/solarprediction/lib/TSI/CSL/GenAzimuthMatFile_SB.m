%GenAzimuthMatFile_SB: This function is used for generate azimuthfile from
%   sbmask directory. 
%   IN:
%       in_sbmaskdir       --  Directory, containing all sbmasks
%       outputf            --  MAT file, which will be used for saving result of
%                              output dn,ze,az.
%       UndistortionParameters.mat(in) --   original image information
%   OUT:
%       sbazimuth.mat         --  clear sky library
%   VERSION 1.2     2012-04-018    
%       
function GenAzimuthMatFile_SB(in_sbmaskdir,outputf)
load('UndistortionParameters.mat');
% outputf='sbazimuth.mat';    % Internal input
if(in_sbmaskdir(end)~='/')
    in_sbmaskdir=sprintf('%s%s',in_sbmaskdir,'/');
end
if(exist(in_sbmaskdir,'dir')==0)
    disp('Input directory not exist. Please check inputs!');
end

allfiles=dir(in_sbmaskdir);
[m,~]=size(allfiles);
tsdn=zeros(m,1);
azs=zeros(m,1);
zes=zeros(m,1);
filestart=0;
for i=1 :m
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        if(strcmpi(filename(end-3:end),'.mat')==0)
            disp('ERROR: file is not a .jpg file,please check your input!');
            break;
        end
        datevector=GetDVFromImg(filename);
        time.year    =  datevector(1);
        time.month   =  datevector(2);
        time.day     =  datevector(3);
        time.hour    =  datevector(4);
        time.min    =   datevector(5);
        time.sec    =   datevector(6);
        time.UTC    =   0;
        sun = sun_position(time, location);
        az=sun.azimuth/180*pi+AzumithShift/180*pi;
        ze=sun.zenith/180*pi+ZenithShift/180*pi;
        azs(i)=az;
        zes(i)=ze;
        tsdn(i)=datenum(datevector);
    else
        filestart=filestart+1;
    end
end
tsdn=tsdn(filestart+1:end);
azs=azs(filestart+1:end);
zes=zes(filestart+1:end);

save(outputf,'tsdn','azs','zes');
end