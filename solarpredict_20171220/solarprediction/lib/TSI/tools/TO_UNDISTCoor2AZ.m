function [azimuth,zenith]=TO_UNDISTCoor2AZ(undistx,undisty,UNDISTW,UNDISTH)
mtsi_startup_mini;
if nargin==2
    UNDISTW=500;
    UNDISTH=500;
end

undistcen_x=UNDISTW/2;
undistcen_y=UNDISTH/2;
rx=undistx-undistcen_x;
ry=undistcen_y-undisty;

r=sqrt((rx^2+ry^2));
tan_va=tan(ViewAngle_r/2);
% r/UNDISTR=tan(ze)/tan(va)
tan_ze=r/UNDISTR*tan_va;
zenith=atan(tan_ze);

if(rx==0)
    if(ry>0)
        azimuth=0;
    elseif(ry==0)
        azimuth=-9999;
    else
        azimuth=pi;
    end
elseif(ry==0)
    if(rx>0)
        azimuth=pi/2;
    else%rx<0
        azimuth=3*pi/2;
    end
else
    if(rx>0&&ry>0)
        azimuth =atan(rx/ry);
    end
    if(rx>0&&ry<0)
        azimuth =pi+atan(rx/ry);
    end
    if(rx<0&&ry<0)
        azimuth =pi+atan(rx/ry);
    end
    if(rx<0&&ry>0)
        azimuth =2*pi+atan(rx/ry);
    end
end






end