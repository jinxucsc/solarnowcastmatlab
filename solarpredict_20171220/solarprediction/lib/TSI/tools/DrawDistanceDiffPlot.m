%DrawDistanceDiffPlot:
%   This function is to draw the distance on the undistorted images. It
%   loads undist2ori.mat first. Then use coor mapping draw out the distance
%   range.
%   INPUT:
%       inputDir(in)        --  Directory, Contains the original images
%       outputDir(in)       --  Directory, Contains output images
%       undist2ori.mat(in)  --  mat file, contains ori->undist mapping
%       dist(in)            --  vector, distance range to draw
%   OUTPUT:
%       outputDir/*    -- All output images
%   VERSION 1.0         2012/04/22

function DrawDistanceDiffPlot
inputDir='input/';
outputDir='output/';
allfiles=dir(inputDir);
[m,~]=size(allfiles);
load('undist2ori.mat');
Resol=Ru/(CBH*tan(ViewAngle));
maxdist=CBH*tan(ViewAngle);
for i=1:m;
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        outp=sprintf('%s%s',outputDir,filename);
        fullp=sprintf('%s%s',inputDir,filename);
        orimg=imread(fullp);
        for dist=0:1000:12000
            if(dist>maxdist)
                continue;
            end
            reald=dist*Resol;
            for az=0:0.5:360
                azr=az/180*pi;
                xr=reald*cos(azr);
                yr=reald*sin(azr);
                xi=round(xr+Wu/2);
                yi=round(Hu/2-yr);
                orimg(yi,xi,1)=255;
                orimg(yi,xi,2)=0;
                orimg(yi,xi,3)=0;
            end
        end
        imwrite(orimg,outp,'jpg');
    end
end


end