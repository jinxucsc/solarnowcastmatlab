% This function is to parse the input date vector/vectors. It will generate startdv and enddv from
% the datevector_obj. datevector_obj(1) will generate startdv, datevector_obj(2) will generate
% enddv.
%   USAGE:
%       datevectorparser([2013 01 01 00 00 00;2013 05 01 00 00 00]);
%           or
%       datevectorparser([2013 01 01 00 00 00 2013 05 01 00 00 00]);
%         startdv =
%                 2013           1           1           0           0           0
% 
%         enddv =
%                 2013           5           1           0           0           0
% 
%         startdn =
%               735235
% 
%         enddn =
%               735355
%       
%       datevectorparser([735235;735355]) or datevectorparser([735235 735355]) 
%         startdv =
%                 2013           1           1           0           0           0
% 
%         enddv =
%                 2013           5           1           0           0           0
% 
%         startdn =
%               735235
% 
%         enddn =
%               735355
%   VERSION 1.0     2013-08-26
function [startdv,enddv,startdn,enddn]=datevectorparser(datevector_obj)

startdv=[];
enddv=[];
startdn=nan;
enddn=nan;
[H,W]=size(datevector_obj);
if 2==H&&6==W
    startdv=datevector_obj(1,:);
    enddv=datevector_obj(2,:);
    startdn=datenum(startdv);
    enddn=datenum(enddv);
elseif 1==H&&12==W
    startdv=datevector_obj(1:6);
    enddv=datevector_obj(7:end);
    startdn=datenum(startdv);
    enddn=datenum(enddv);
elseif (2==H&&1==W) || (2==W&&1==H)
    startdn=datevector_obj(1);
    enddn=datevector_obj(2);
    startdv=datevec(startdn);
    enddv=datevec(enddn);
else
    disp('datevector_obj should be ether datevec or datenum!');
end
assert(startdn<=enddn,'datevector_obj(1) should be larger than datevector_obj(2)');

end