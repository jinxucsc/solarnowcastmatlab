% Convert TSI images to different format
%   Support mode right now:
%       mat2jpg, bmp2jpg, bmp2mat
function TO_ImageFormatConvert(inputDir,outputDir,mode)
allfiles=dir(inputDir);
[nfiles,~]=size(allfiles);
inputDir=FormatDirName(inputDir);
outputDir=FormatDirName(outputDir);
mkdir(outputDir);

if strcmpi(mode,'mat2jpg')
    for i=1:nfiles
        if allfiles(i).isdir==1
            continue;
        end
        f=allfiles(i).name;
        f_noext=f(1:end-4);
        fext=f(end-3:end);
        if ~strcmpi('.mat',fext)
            continue;
        end
        inputf=sprintf('%s%s',inputDir,f);
        outputf=sprintf('%s%s.jpg',outputDir,f_noext);
        t=load(inputf,'imma');
        imma=t.imma;
        imwrite(imma,outputf,'jpg');
    end
end

if strcmpi(mode,'bmp2jpg')
    for i=1:nfiles
        if allfiles(i).isdir==1
            continue;
        end
        f=allfiles(i).name;
        f_noext=f(1:end-4);
        fext=f(end-3:end);
        if ~strcmpi('.bmp',fext)
            continue;
        end
        inputf=sprintf('%s%s',inputDir,f);
        outputf=sprintf('%s%s.jpg',outputDir,f_noext);
        imma=imread(inputf);
        imwrite(imma,outputf,'jpg');
    end
end

if strcmpi(mode,'bmp2mat')
    for i=1:nfiles
        if allfiles(i).isdir==1
            continue;
        end
        f=allfiles(i).name;
        f_noext=f(1:end-4);
        fext=f(end-3:end);
        if ~strcmpi('.bmp',fext)
            continue;
        end
        inputf=sprintf('%s%s',inputDir,f);
        outputf=sprintf('%s%s.mat',outputDir,f_noext);
        imma=imread(inputf);
        save(outputf,'imma');
    end
end





end