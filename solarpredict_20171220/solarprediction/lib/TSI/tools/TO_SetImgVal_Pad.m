function imma_setted=TO_SetImgVal_Pad(startp,rec_vals,imma)
imma_setted=imma;
starty=startp(1);
startx=startp(2);

[recH,recW,c]=size(rec_vals);
[H,W,c]=size(imma);

if starty>=1&&startx>=1 && starty+recH-1<=H && startx+recW-1<=W
    if c==1
        imma_setted(starty:starty+recH-1,startx:startx+recW-1)=rec_vals(:,:);
    elseif c==3
        
        imma_setted(starty:starty+recH-1,startx:startx+recW-1,1)=rec_vals(:,:,1);
        imma_setted(starty:starty+recH-1,startx:startx+recW-1,2)=rec_vals(:,:,2);
        imma_setted(starty:starty+recH-1,startx:startx+recW-1,3)=rec_vals(:,:,3);
    else
        disp('Input Image matrix should be greyscale(HxW) or RGB format(HxWx3)');
    end
    return;
end


% Out of Bound
if starty<1
    ly=starty+recH-1;
    corr_orisy=1;
    corr_oriey=corr_orisy+ly-1;
    corr_sy=2-starty;
    corr_ey=corr_sy+ly-1;
else
    corr_orisy=starty;
    corr_sy=1;
end
if startx<1
    lx=startx+recW-1;
    corr_orisx=1;
    corr_oriex=corr_orisx+lx-1;
    corr_sx=2-startx;
    corr_ex=corr_sx+lx-1;
else
    corr_orisx=startx;
    corr_sx=1;
end
if starty+recH-1>H
    ly=H-starty+1;
    corr_orisy=starty;
    corr_oriey=starty+ly-1;
    corr_sy=1;
    corr_ey=ly;
else
    corr_oriey=starty+recH-1;
    corr_ey=recH;
end
if startx+recW-1>W
    lx=W-startx+1;
    corr_orisx=startx;
    corr_oriex=startx+lx-1;
    corr_sx=1;
    corr_ex=lx;
else
    corr_oriex=startx+recW-1;
    corr_ex=recW;
end
if c==1
    imma_setted(corr_orisy:corr_oriey,corr_orisx:corr_oriex)=rec_vals(corr_sy:corr_ey,corr_sx:corr_ex);
elseif c==3
    imma_setted(corr_orisy:corr_oriey,corr_orisx:corr_oriex,1)=rec_vals(corr_sy:corr_ey,corr_sx:corr_ex,1);
    imma_setted(corr_orisy:corr_oriey,corr_orisx:corr_oriex,2)=rec_vals(corr_sy:corr_ey,corr_sx:corr_ex,2);
    imma_setted(corr_orisy:corr_oriey,corr_orisx:corr_oriex,3)=rec_vals(corr_sy:corr_ey,corr_sx:corr_ex,3);
else
    disp('Input Image matrix should be greyscale(HxW) or RGB format(HxWx3)');
end
assert(size(imma,1)==size(imma_setted,1)&&size(imma,2)==size(imma_setted,2)&&size(imma,3)==size(imma_setted,3),'Dimension Error!');

end