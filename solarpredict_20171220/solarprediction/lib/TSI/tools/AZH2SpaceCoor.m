%   function is to transfer from (A,Z) which is in dome space coordinate
%   system into space/relative space coordinate system(rx,ry,rz). You must
%   specify H as another dim info to do the transfering. (orx,ory,orz) is
%   the relative point means the light directly points at this point in
%   relative coordinate system. No orx information means (0,0,0) the origin
%   of space/relative coordinate system.
function [rx,ry,rz]=AZH2SpaceCoor(az,ze,H,orx,ory,orz)
if nargin==3
    orx=0;
    ory=0;
    orz=0;
end
if ze>pi/2 % not valid since it is under horizontal
    disp('Zenith is below the horizontal!');
end

rx=H*tan(ze)*sin(az)+orx;
ry=H*tan(ze)*cos(az)+ory;
rz=H+orz;


end