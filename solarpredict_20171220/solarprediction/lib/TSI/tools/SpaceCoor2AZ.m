%   TOOL function TO_real2az(rx,ry,rz) is a tool for transferring from
%   relative/space coordinate to azimuth and zenith which is dome space
%   coordinate
%   INPUT:
%       rx  --- Space coordinate X relative the the cetre/origin
%       ry  --- Space coordinate Y relative the the cetre/origin
%       rz  --- Space coordinate Z relative the the cetre/origin
%   OUTPUT:
%       azimuth     --- Azimuth angle in radius
%       Zenith      --- Zenith angle in radius
function [azimuth, zenith]=SpaceCoor2AZ(rx,ry,rz)
ra=sqrt(rx^2+ry^2);
tan_val=ra/rz;
zenith=atan(tan_val);
if(rx==0)
    if(ry>0)
        azimuth=0;
    elseif(ry==0)
        azimuth=-9999;
    else
        azimuth=pi;
    end
elseif(ry==0)
    if(rx>0)
        azimuth=pi/2;
    else%rx<0
        azimuth=3*pi/2;
    end
else
    if(rx>0&&ry>0)
        azimuth =atan(rx/ry);
    end
    if(rx>0&&ry<0)
        azimuth =pi+atan(rx/ry);
    end
    if(rx<0&&ry<0)
        azimuth =pi+atan(rx/ry);
    end
    if(rx<0&&ry>0)
        azimuth =2*pi+atan(rx/ry);
    end
end

end