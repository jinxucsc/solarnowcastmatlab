function [ux uy H]=TO_RelativeCoor2Undist(rx,ry,rz)
mtsi_startup;
H=rz;
ratioval=H*tan(ViewAngle_r/2)/UNDISTR;

UCenX=UNDISTW/2;
UCenY=UNDISTH/2;
% rx=(ux-UCenX)*ratioval;
% ry=(UCenY-uy)*ratioval;
ux=rx/ratioval+UCenX;
uy=UCenY-ry/ratioval;


end