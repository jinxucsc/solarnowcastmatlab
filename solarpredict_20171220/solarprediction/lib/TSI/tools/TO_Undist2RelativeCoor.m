function [rx ry rz]=TO_Undist2RelativeCoor(ux,uy,H)
mtsi_startup;

ratioval=H*tan(ViewAngle_r/2)/UNDISTR;

UCenX=UNDISTW/2;
UCenY=UNDISTH/2;
rx=(ux-UCenX)*ratioval;
ry=(UCenY-uy)*ratioval;
rz=H;

end