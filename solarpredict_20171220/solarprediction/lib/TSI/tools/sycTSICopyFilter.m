%   Function is to synchronize all 3 tsis with picked time range. This can be used for all types of
%   images including original,flipped and undistorted images.
%   INPUT:
%       tsiDirs     --- Cell 3x1. Directories for TSI1,2,3. It can be ori,flipped,undist
%           e.g. {'/home/mikegrup/flip1','/home/mikegrup/flip2','/home/mikegrup/flip3'}
%       outDirs     --- Cell 3x1 Output Directories
%           e.g. {'/home/mikegrup/flip1_sync','/home/mikegrup/flip2_sync','/home/mikegrup/flip3_sync'}
%       ttolarence  --- Int, seconds of tolarence to accept TSI as picked time datenumber
%       freq        --- Int, seconds of time span between two picked time datenumbers
%       startdv,enddv       --- Datevector, start and end of days
%       odstartdv,odenddv   --- Datevector, start and end of daytime
%   OUTPUT:
%       
%   Version 1.0     2013-06-12
%       v1.0 integrate 3 TSIs and daytime dv(odstartdv,odenddv) and days dv(startdv,enddv). It also
%       includes freq and ttolarence for picking images
function sycTSICopyFilter(tsiDirs,outDirs,ttolarence,freq,startdv,enddv,odstartdv,odenddv)
if nargin==2
    ttolarence=3
    freq=10
    startdv=[2010 01 01 0 0 0]
    enddv=[2020 01 01 0 0 0]
    odstartdv=[ 0 0 0 9 0 0]
    odenddv=[0 0 0 16 0 0]
end
ONEDAY_DN=datenum([0 0 1 0 0 0]);
FREQ_DN=datenum([0 0 0 0 0 freq]);

tsi1Dir=tsiDirs{1};
tsi2Dir=tsiDirs{2};
tsi3Dir=tsiDirs{3};
otsi1Dir=outDirs{1};
otsi2Dir=outDirs{2};
otsi3Dir=outDirs{3};


tsi1Dir=FormatDirName(tsi1Dir);
tsi2Dir=FormatDirName(tsi2Dir);
tsi3Dir=FormatDirName(tsi3Dir);
otsi1Dir=FormatDirName(otsi1Dir);
otsi2Dir=FormatDirName(otsi2Dir);
otsi3Dir=FormatDirName(otsi3Dir);

if ~exist(tsi1Dir,'dir')||~exist(tsi2Dir,'dir')||~exist(tsi3Dir,'dir')
    disp('[ERROR]: NO TEST TSI CASES ARE GIVEN!'); 
end
if ~exist(otsi1Dir,'dir')
    mkdir(otsi1Dir);
end
if ~exist(otsi2Dir,'dir')
    mkdir(otsi2Dir);
end
if ~exist(otsi3Dir,'dir')
    mkdir(otsi3Dir);
end

[tsi1avail,tsi1avail_days]=TO_CheckTSIAvail(tsi1Dir);
[tsi2avail,tsi2avail_days]=TO_CheckTSIAvail(tsi2Dir);
[tsi3avail,tsi3avail_days]=TO_CheckTSIAvail(tsi3Dir);

TSI1='tsi1';
TSI2='tsi2';
TSI3='tsi3';

startdn=datenum(startdv);
enddn=datenum(enddv);
odstartdv(1:3)=0;
odenddv(1:3)=0;
odstartdn=datenum(odstartdv);
odenddn=datenum(odenddv);

ndays1=length(tsi1avail_days);
ndays2=length(tsi2avail_days);
ndays3=length(tsi3avail_days);

pickdns=[];
disp(ndays1);
for iday=1:ndays1
    onedaystartdn=tsi1avail_days(iday);
    onedayenddn=tsi1avail_days(iday)+ONEDAY_DN;
    if onedaystartdn<startdn||onedaystartdn>=enddn
        continue;
    end
    gmtodsdn=datenum(EST2GMT(datevec(odstartdn)));
    gmtodedn=datenum(EST2GMT(datevec(odenddn)));
    sdn=onedaystartdn+gmtodsdn;
    edn=onedaystartdn+gmtodedn;
    tmdn=sdn;
    tmindex=1;
    while tmdn < edn
        pickdns=[pickdns;tmdn];
        tmdn=sdn+tmindex*FREQ_DN;
        tmindex=tmindex+1;
    end

end


nTotalDNS=length(pickdns);
disp(nTotalDNS);

for i=1:nTotalDNS
    tmdn=pickdns(i);
    [ttmdn1,tindexi1]=TO_GetTSITolarence(tsi1avail,tmdn,ttolarence);
    [ttmdn2,tindexi2]=TO_GetTSITolarence(tsi2avail,tmdn,ttolarence);
    [ttmdn3,tindexi3]=TO_GetTSITolarence(tsi3avail,tmdn,ttolarence);
    if 0==min([tindexi1,tindexi2,tindexi3])
        continue;
    end
    tmdv=datevec(tmdn);
    tmdv1=datevec(ttmdn1);
    tmdv2=datevec(ttmdn2);
    tmdv3=datevec(ttmdn3);
    f1_t=GetImgFromDV_BNL(tmdv1,TSI1);
    f2_t=GetImgFromDV_BNL(tmdv2,TSI2);
    f3_t=GetImgFromDV_BNL(tmdv3,TSI3);
    inputf1=sprintf('%s%s',tsi1Dir,f1_t);
    inputf2=sprintf('%s%s',tsi2Dir,f2_t);
    inputf3=sprintf('%s%s',tsi3Dir,f3_t);
    f1=GetImgFromDV_BNL(tmdv,TSI1);
    f2=GetImgFromDV_BNL(tmdv,TSI2);
    f3=GetImgFromDV_BNL(tmdv,TSI3);
    outputf1=sprintf('%s%s',otsi1Dir,f1);
    outputf2=sprintf('%s%s',otsi2Dir,f2);
    outputf3=sprintf('%s%s',otsi3Dir,f3);
    copyfile(inputf1,outputf1);
    copyfile(inputf2,outputf2);
    copyfile(inputf3,outputf3);
end


% for i=1:tml
%     tmdn=tsi1avail(i);
%     if tmdn==0
%         continue;
%     end
%     tmdv=datevec(tmdn);
%     odtmdv=GMT2EST(tmdv);
%     odtmdv(1:3)=0;
%     odtmdn=datenum(odtmdv);
%     if tmdn<startdn||tmdn>=enddn||odtmdn<odstartdn||odtmdn>=odenddn
%         continue;
%     end
%     
%     
%     
%     f1=GetImgFromDV_BNL(tmdv,TSI1);
%     f2=GetImgFromDV_BNL(tmdv,TSI2);
%     f3=GetImgFromDV_BNL(tmdv,TSI3);
%     inputf1=sprintf('%s%s',tsi1Dir,f1);
%     inputf2=sprintf('%s%s',tsi2Dir,f2);
%     inputf3=sprintf('%s%s',tsi3Dir,f3);
%     outputf1=sprintf('%s%s',otsi1Dir,f1);
%     outputf2=sprintf('%s%s',otsi2Dir,f2);
%     outputf3=sprintf('%s%s',otsi3Dir,f3);
%     if ~exist(inputf2,'file')|| ~exist(inputf3,'file')
%         tmdv=GetDVFromImg(f1);
%         tmdn=datenum(tmdv);
%         diffdn2=abs(tsi2avail-tmdn);
%         diffdn3=abs(tsi3avail-tmdn);
%         [minval2 mini2]=min(diffdn2);
%         [minval3 mini3]=min(diffdn3);
%         if minval2<=ttolarence_dn && minval3<=ttolarence_dn
%             f2=GetImgFromDV_BNL(datevec(tsi2avail(mini2)),TSI2);
%             f3=GetImgFromDV_BNL(datevec(tsi3avail(mini3)),TSI3);
%             inputf2=sprintf('%s%s',tsi2Dir,f2);
%             inputf3=sprintf('%s%s',tsi3Dir,f3);
%         else
%             continue;
%         end
%     end
%     copyfile(inputf1,outputf1);
%     copyfile(inputf2,outputf2);
%     copyfile(inputf3,outputf3);
% end

end