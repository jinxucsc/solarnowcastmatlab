% This function is to calculate all height matrix of TSI1, TSI2 and TSI3
% Version 1.0       2013-07-22

function TO_GenHMVMatrix(outmatf)
mtsi_startup_mini;

[H_x,H_y]=TO_MV2HMatrix(TSI1,TSI2,outmatf);
xma=(H_x-H_y);
tmindma=(xma>0);
hma=H_y;
hma(tmindma)=H_x(tmindma);
hma_min=H_x;
hma_min(tmindma)=H_y(tmindma);
fr=(hma-hma_min)./hma;
invalidma=(fr>0.1|H_x<=0|H_y<=0);
hma=round((H_x+H_y)./2);
hma(invalidma)=0;
hma12=hma;

[H_x,H_y]=TO_MV2HMatrix(TSI2,TSI1,outmatf);
xma=(H_x-H_y);
tmindma=(xma>0);
hma=H_y;
hma(tmindma)=H_x(tmindma);
hma_min=H_x;
hma_min(tmindma)=H_y(tmindma);
fr=(hma-hma_min)./hma;
invalidma=(fr>0.1|H_x<=0|H_y<=0);
hma=round((H_x+H_y)./2);
hma(invalidma)=0;
hma21=hma;



[H_x,H_y]=TO_MV2HMatrix(TSI2,TSI3,outmatf);
xma=(H_x-H_y);
tmindma=(xma>0);
hma=H_y;
hma(tmindma)=H_x(tmindma);
hma_min=H_x;
hma_min(tmindma)=H_y(tmindma);
fr=(hma-hma_min)./hma;
invalidma=(fr>0.1|H_x<=0|H_y<=0);
hma=round((H_x+H_y)./2);
hma(invalidma)=0;
hma23=hma;



[H_x,H_y]=TO_MV2HMatrix(TSI3,TSI2,outmatf);
xma=(H_x-H_y);
tmindma=(xma>0);
hma=H_y;
hma(tmindma)=H_x(tmindma);
hma_min=H_x;
hma_min(tmindma)=H_y(tmindma);
fr=(hma-hma_min)./hma;
invalidma=(fr>0.1|H_x<=0|H_y<=0);
hma=round((H_x+H_y)./2);
hma(invalidma)=0;
hma32=hma;



[H_x,H_y]=TO_MV2HMatrix(TSI1,TSI3,outmatf);
xma=(H_x-H_y);
tmindma=(xma>0);
hma=H_y;
hma(tmindma)=H_x(tmindma);
hma_min=H_x;
hma_min(tmindma)=H_y(tmindma);
fr=(hma-hma_min)./hma;
invalidma=(fr>0.1|H_x<=0|H_y<=0);
hma=round((H_x+H_y)./2);
hma(invalidma)=0;
hma13=hma;


[H_x,H_y]=TO_MV2HMatrix(TSI3,TSI1,outmatf);
xma=(H_x-H_y);
tmindma=(xma>0);
hma=H_y;
hma(tmindma)=H_x(tmindma);
hma_min=H_x;
hma_min(tmindma)=H_y(tmindma);
fr=(hma-hma_min)./hma;
invalidma=(fr>0.1|H_x<=0|H_y<=0);
hma=round((H_x+H_y)./2);
hma(invalidma)=0;
hma31=hma;


t=load(outmatf);

Hma.hma12=hma12;
Hma.hma21=hma21;
Hma.hma23=hma23;
Hma.hma32=hma32;
Hma.hma13=hma13;
Hma.hma31=hma31;
mvx_ran=t.mvx_ran;
mvy_ran=t.mvy_ran;


save(outmatf);


end