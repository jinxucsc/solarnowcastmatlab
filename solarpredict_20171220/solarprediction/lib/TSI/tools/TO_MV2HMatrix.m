function [H_x,H_y]=TO_MV2HMatrix(site1_ref,site2,outmatf)
mtsi_startup_mini;

if strcmpi(site1_ref,TSI1)
    loc1=location1;
elseif strcmpi(site1_ref,TSI2)
    loc1=location2;
elseif strcmpi(site1_ref,TSI3)
    loc1=location3;
end
if strcmpi(site2,TSI1)
    loc2=location1;
elseif strcmpi(site2,TSI2)
    loc2=location2;
elseif strcmpi(site2,TSI3)
    loc2=location3;
end
mvx_ran=-UNDISTR:UNDISTR;
mvy_ran=-UNDISTR:UNDISTR;
ux1=300;uy1=300;
tml=length(mvx_ran);
H_x=zeros(tml,tml);
H_y=zeros(tml,tml);

[rx ry rz]=TO_RealCoor2RelativeCoor(...
    loc2.longitude,loc2.latitude,loc2.altitude,loc1.longitude,loc1.latitude,loc1.altitude);
mv_ref=[rx -ry];
mv_ref_norm=norm(mv_ref);
angle_thres=3/180*pi;
tan_val=-ry/rx;
for i=1:tml
    mv_y=mvy_ran(i);
    disp(i);
    for j=1:tml
        mv_x=mvx_ran(j);
        mv_tem=[mv_x mv_y];
        if norm(mv_tem)==0
            continue;
        end
%         cosval=acos(dot(mv_tem,mv_ref)/mv_ref_norm/norm(mv_tem));
%         if cosval>angle_thres
%             continue;
%         end
        %[tmH_x,tmH_y]=TO_Undist2H(ux1,uy1,ux1+mv_x,uy1+mv_y,site1_ref,site2);
        [tmH_x,tmH_y]=TO_Undist2H(ux1,uy1,ux1+mv_x,uy1+mv_y,loc1,loc2);
%         if abs(tmH_x-tmH_y)>500
%             continue;
%         end
        H_x(i,j)=tmH_x;
        H_y(i,j)=tmH_y;
    end
end


meanH=zeros(tml,1);
dists=zeros(tml,1);
H_p=[];
D_p=[];
for j=1:tml
    tmma=H_x(:,j);
    mv_x=mvx_ran(j);
    mv_y=-mv_x*tan_val;
    odist=sqrt((mv_x^2)+(mv_y^2));
    dists(j)=odist;
    meanH(j)=mean(tmma(tmma~=0));
    if meanH(j)~=0
       D_p=[D_p odist];
       H_p=[H_p meanH(j)];
    end
end
invalidind=isnan(meanH);
meanH(invalidind)=0;
meanH_new=abs(meanH(1:end-1)-meanH(2:end));
% figure();
% plot(D_p,H_p,'xr');
save(outmatf);

end