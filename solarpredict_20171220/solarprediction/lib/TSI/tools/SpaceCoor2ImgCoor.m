% Space/relative Coor to image coor
%   BNLTSI_paraf = 'BNLTSI1_para.mat'
%                   'BNLTSI2_para.mat'
%                   'BNLTSI3_para.mat'
function [imy,imx,uy,ux]=SpaceCoor2ImgCoor(rx,ry,rz,BNLTSI_paraf)
% if MODE=='TSI1'
%     parafile=sprintf('%sBNLTSI1_para.mat',currentdir);
% %     t=load('./BNLTSI1_para.mat');
% elseif MODE=='TSI2'
%     parafile=sprintf('%sBNLTSI2_para.mat',currentdir);
% %     t=load('./BNLTSI2_para.mat');
% elseif MODE=='TSI3'
%     parafile=sprintf('%sBNLTSI3_para.mat',currentdir);
% %     t=load('./BNLTSI1_para.mat');
% else
%     disp('Unknown input of MODE. MODE should be ''TSI1'', ''TSI2'', ...');
%     return;
% end
t=load(BNLTSI_paraf);


% 1. Relative coordinate to dome shape coor
[az,ze]=SpaceCoor2AZ(rx,ry,rz);

% 2. undistortion coordinate (uy,ux) is calculated directly from (az,ze)
udisty=tan(ze)./tan(t.VA_r/2).*t.UNDISTR.*cos(az);
udistx=tan(ze)./tan(t.VA_r/2).*t.UNDISTR.*sin(az);
ux=round(t.undistcen.x+udistx);
uy=round(t.undistcen.y-udisty);

% 3. Undist 2 original image coor
l=length(uy);
imx=zeros(1,l);
imy=zeros(1,l);
for tml=1:l
    if uy(tml)>t.UNDISTH||uy(tml)<1||ux(tml)>t.UNDISTW||ux(tml)<1
        imy(tml)=-1;
        imx(tml)=-1;
        continue;
    end
    imy(tml)=t.undist2orima(uy(tml),ux(tml),1);
    imx(tml)=t.undist2orima(uy(tml),ux(tml),2);
end

end