%Batch_DrawMatOut:
%   This is a function to draw all the mat files(for storage only) out for
%   test only
%   IN:
%       inDir   --  Directory, all mat files.
%       outDir  --  Directory, all image out files
function Batch_DrawMatOut(inDir,outDir)
if(inDir(end)~='/')
    inDir=sprintf('%s%s',inDir,'/');
end
if(outDir(end)~='/')
    outDir=sprintf('%s%s',outDir,'/');
end
mkdir(outDir);
allfiles=dir(inDir);
[m,~]=size(allfiles);
for i=1:m
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        inputf=sprintf('%s%s',inDir,filename);
        outputf=sprintf('%s%s',outDir,filename);
        s=load(inputf,'-mat');
        inputim=s.outputm;
        imwrite(inputim,outputf,'jpg');
    end
end


end