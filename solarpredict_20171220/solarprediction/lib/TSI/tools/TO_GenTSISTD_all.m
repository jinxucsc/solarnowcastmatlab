%   TO_GenTSISTD_all
%   This is a function generate the STD matrix of TSI images.
%   INPUT:
%       tsidataDir      --- String, directory path for TSI image container
%       outputDir       --- String, directory path for output STD matrix
%       datevector_obj  --- datevector_obj, can extract startdv and enddv
%       configfpath     --- String, Configuration file path to load presettings
%   OUTPUT:
%       outputf=sprintf('%s%s.std.mat',outputDir,filename(1:end-4));
%   USAGE:
%       See tmp_GetImgStdMatFromDataDir.m
%           TO_GenTSISTD_all(tsi1Dir,tsi1D,StdBlockSize,[tmdv;tmdv+[0 0 0 0 0 1]]);
%
%   VERSION 1.0     2013/11/01    
%       
function TO_GenTSISTD_all(tsidataDir,outputDir,StdBlockSize,datevector_obj,configfpath)
if ~exist('configfpath','var')
    configfpath='mtsi_startup_mini';
end
run(configfpath);
if nargin==3
    datevector_obj=[2010 01 01 0 0 0;2020 01 01 0 0 0];    
end

[startdv,enddv,startdn,enddn]=datevectorparser(datevector_obj);
StdBlockHalfSize=floor(StdBlockSize/2);

tsidataDir=FormatDirName(tsidataDir);
outputDir=FormatDirName(outputDir);
if ~exist(outputDir,'dir')
    mkdir(outputDir);
end
allfiles=dir(tsidataDir);
[nfiles,~]=size(allfiles);
assert(nfiles>=3,'input data directory should not be empty');
tmi=3;
tmdv=GetDVFromImg(allfiles(tmi).name);
while(tmdv==0)
    tmi=tmi+1;
    tmdv=GetDVFromImg(allfiles(tmi).name);
end
filename=allfiles(tmi).name;
%disp(filename);
indexi=regexp(filename,TSIFileExpression);
bTSI=false;bMat=false; bBMP=false;
if indexi==1
    bTSI=true;
else
    indexi=regexp(filename,TSIMATExpression);
    if indexi==1
        bMat=true;
    end
end
indexi=regexp(filename,TSIBMPFileExpression);
if (indexi==1)
    bBMP=true;
end
assert(bMat||bTSI||bBMP,'tsidataDir should contain .jpg file or the .mat file!');

uh=UNDISTH; uw=UNDISTW;
for i =1:nfiles
    if allfiles(i).isdir==1
        continue;
    end
    filename=allfiles(i).name;
    tmdv=GetDVFromImg(filename);
    tmdn=datenum(tmdv);
    if tmdn<startdn|| tmdn>=enddn
        continue;
    end
    inputf=sprintf('%s%s',tsidataDir,filename);
    outputf=sprintf('%s%s.std.mat',outputDir,filename(1:end-4));
    %if exist(outputf,'file')
    if 0
        dispstr=sprintf('[WARNING]: Skipped since exist %s',outputf);
        disp(dispstr);
        continue;
    else
        %disp(i);
    end
    if bMat
        t=load(inputf);
        imma3=t.imma;
    else 
        imma3=imread(inputf);
    end
%     imma_std=zeros(uh,uw);
%     imma_lum=single(TO_rgb2lum(imma3));
%     imma_std_invalid=false(uh,uw);
%     for yi=1+StdBlockSize:uh-StdBlockSize
%         for xi=1+StdBlockSize:uw-StdBlockSize
%             tmvals=imma_lum(yi-StdBlockHalfSize:yi+StdBlockHalfSize,...
%                 xi-StdBlockHalfSize:xi+StdBlockHalfSize); 
%             if isempty(find(tmvals==0,1))
%                 imma_std_invalid(yi,xi)=true;
%             end
%             imma_std(yi,xi)=std(tmvals(:));
%         end
%     end
%    imma_std=t1.imma_std;
    imma_lum=single(TO_rgb2lum(imma3));
    [imma_std,imma_std_invalid]=imgstd(double(imma_lum),StdBlockHalfSize);
    imma_std_invalid=logical(imma_std_invalid);
    originalf=inputf;
    imma_std=uint8(round(imma_std));
    save(outputf,'StdBlockSize','StdBlockHalfSize','imma_std','tmdv','originalf','imma_std_invalid');
    %isave(outputf,StdBlockSize,StdBlockHalfSize,imma_std,tmdv,originalf,imma_std_invalid);
    
end




end