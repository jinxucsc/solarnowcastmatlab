%   TOOL function TO_CheckTSIAvail
%       is designed to get availabilty of certain directory using
%       GetDVFromimg function in the library
%   VERSION 1.0     2012/09/28
%   VERSION 2.0     2013/06/12  Add days datenumbers
function [TSIavail,TSIavail_days]=TO_CheckTSIAvail(dirpath)
if ~exist(dirpath,'dir')
    disp(dirpath);
    disp('No such input directory found!'); 
    TSIavail=-1;
    return;
end
dirpath=FormatDirName(dirpath);
allfiles=dir(dirpath);
[nfiles,~]=size(allfiles);
alltmdn=[];
for i=1:nfiles
    if allfiles(i).isdir==1
        continue;
    end
    f1=allfiles(i).name;
    tmdv=GetDVFromImg(f1);
    if tmdv(1)==0
        disp('Directory contains irregular image filename..Function returns unexpected!');
        TSIavail=-1;
        return;
    end
    tmdn=datenum(tmdv);
    alltmdn=[alltmdn tmdn];
end

TSIavail=alltmdn;
alltmdv=datevec(alltmdn);
alltmdv(:,4:6)=0;
TSIavail_days=datenum(alltmdv);
TSIavail_days=unique(TSIavail_days);
TSIavail_days=TSIavail_days(TSIavail_days~=0);

end