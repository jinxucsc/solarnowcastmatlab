% BNLTSI1_para.m
%   This is a bash script to generate a TSI1 parameter file including
%   camera information and undistorion matrix. The output will be a mat
%   file naming as BNLTSI1_para.mat

% Undistortion parameters settings.
UNDISTH=500;
UNDISTW=500;
UNDISTR=240;
ViewAngle=120;
ViewAngle_r=ViewAngle/180*pi;
VA=ViewAngle;
VA_r=ViewAngle_r;
centrep.x=320;
centrep.y=240;
ORIH=480;
ORIW=640;



%   Camera parameters.
cw=3.58;             %ccdwidth=3.6mm
ch=2.69;           %ccdheight=4.576mm
crx=640;            %ccd resolution X=288
cry=480;            %ccd resolution Y=352
fo=4;             %focal length 5.5mm
cw=3.58;             %ccdwidth=3.6mm
ch=2.69;           %ccdheight=4.576mm
crx=640;            %ccd resolution X=288
cry=480;            %ccd resolution Y=352
fo=4;             %focal length 5.5mm
CBH=5000;
DistTotal=CBH*tan(VA_r/2);

% First TSI:
% 	40°51’57.72”N
% 	72°53’04.71”W
% 	Elevation is about 65 feet above sea level.
location.longitude = -72.885;
location.latitude = 40.866;
location.altitude = 20;        %metre



% Sensitive settings are below:
disp('[WARNNING]: the sensitive parameters at present are: ');
r=9
h=15.6811
AzumithShift=0
ZenithShift=0
rcentrep.x=305
rcentrep.y=235
ViewAngle=120
Hu=UNDISTH;
Wu=UNDISTW;
Ru=UNDISTR;
undist2orima=zeros(Hu,Wu,2);    %[Xor,Yor] = [u2oma(i,j,1) u2oma(i,j,2)]
ori2undistma=zeros(ORIH,ORIW,2);    %[Xu , Yu] = [o2uma(i,j,1) o2uma(i,j,2)]
% u2rma=zeros(Hu,Wu,2);    %[ry rx]=H*[u2rma(i,j,1) u2rma(i,j,2)]
undistcen.x=Wu/2;
undistcen.y=Hu/2;

for Xu=1:Wu
    for Yu=1:Hu
        %   1. From undistorted image to world relative coor
        %   (Xu,Yu) -> (Xwr,Ywr,Zwr or CBH) ->(Zenith, Azumith)
        %         if(Xu==251&&Yu==200)
        %             Xu
        %         end
        imagey=undistcen.y-Yu;
        imagex=Xu-undistcen.x;
        DistCen=sqrt(imagey^2+imagex^2);
        if(DistCen>=Ru)
            continue;
        end
        reald=DistCen/Ru*DistTotal;
        tz=reald/CBH;
        Zenith=atan(tz);
        
        %find azumith
        if(imagex==0&&imagey>0)
            Azumith=0;
        elseif imagex==0&&imagey<0
            Azumith=pi;
        elseif imagex==0&&imagey==0
            Zenith=-3*pi;
            Azumith=-3*pi;
        elseif imagey==0&&imagex>0
            Azumith=pi/2;
        elseif imagey==0&&imagex<0
            Azumith=pi*3/2;
        end
        
        if(imagex>0&&imagey>0)
            Azumith=atan(imagex/imagey);
        end
        if(imagex<0&&imagey>0)
            Azumith=2*pi+atan(imagex/imagey);
        end
        if(imagex<0&&imagey<0)
            Azumith=pi+atan(imagex/imagey);
        end
        if(imagex>0&&imagey<0)
            Azumith=pi+atan(imagex/imagey);
        end
        
        %   2. (Zenith,Azumith) -> (Xor,Yor)
        if(Azumith==-3*pi&&Zenith==-3*pi)
            %symbol of image centre
            Xor=rcentrep.x;
            Yor=rcentrep.y;
        else
            [Xor,Yor]=CalImageCoorFrom_paras(...
                Azumith,Zenith,...
                AzumithShift,ZenithShift,...
                h,r,...
                cw,ch,crx,cry,fo,...
                rcentrep...
                );
        end
        Yor=round(Yor);
        Xor=round(Xor);
        if Xor > ORIW || Xor <1 || Yor>ORIH||Yor<1
            continue;
        end
        undist2orima(Yu,Xu,1)=Yor;
        undist2orima(Yu,Xu,2)=Xor;
%         u2rma(Yu,Xu,1)=tan(VA_r/2)/UNDISTR*sqrt(Yu^2+Xu^2)*cos(Azumith);
%         u2rma(Yu,Xu,2)=tan(VA_r/2)/UNDISTR*sqrt(Yu^2+Xu^2)*sin(Azumith);
        ori2undistma(Yor,Xor,1)=Yu;
        ori2undistma(Yor,Xor,2)=Xu;
    end
end


% Save the parameters into present file
save('BNLTSI1_para.mat');
