% Traverse search TSI ROOT directory and put them into a single folder
%   1. Traverse all directories and subdirectories
%   2. Create output directory using the minimum timestamp and max timestamp as folder name
%   3. Change the filename C1 -> C1,C2,C3 if possible (TSIDownloader fixed that problem)
%   VERSION: 1.0        2013-06-24
function TO_TSITraverseSearch(TSIRootDir,outputDir,site)
if nargin==2
    site='tsi1';
end
mtsi_startup;
TSIRootDir=FormatDirName(TSIRootDir);
allfiles=dir(TSIRootDir);
[nfiles,~]=size(allfiles);
outputDir=FormatDirName(outputDir);
if ~exist(outputDir,'dir')
    mkdir(outputDir);
end
% bnltsiskyimageC1.a1.20130601.130000.jpg.20130601130000.jpg
% bnltsiskyimageC2.a1.20130601.130000.jpg.20130601130000.jpg
% bnltsiskyimageC3.a1.20130601.130000.jpg.20130601130000.jpg
tsi1str='C1';
tsi2str='C2';
tsi3str='C3';

expression=TSIFileExpression;
% if strcmpi(site,TSI1)
%     TSIn=1;
% elseif strcmpi(site,TSI2)
%     TSIn=2;
% elseif strcmpi(site,TSI3)
%     TSIn=3;
% end

for i=1:nfiles
    filename=allfiles(i).name;
    if allfiles(i).isdir()==1
        if strcmpi(filename,'.') || strcmpi(filename,'..')
            continue;
        end
        dirname=[TSIRootDir filename];
        TO_TSITraverseSearch(dirname,outputDir,site);
    end
    if isempty(regexp(filename,expression,'ONCE'))
        continue;
    end
    inputf=[TSIRootDir filename];
    outputf=[outputDir filename];
    %     if TSIn==1
    %         outputf=[outputDir filename];
    %     elseif TSIn==2
    %         tmf=filename;
    %         k=strfind(filename,tsi1str);
    %         tmf(k:k+1)=tsi2str;
    %         outputf=[outputDir tmf];
    %     elseif TSIn == 3
    %         tmf=filename;
    %         k=strfind(filename,tsi1str);
    %         tmf(k:k+1)=tsi3str;
    %         outputf=[outputDir tmf];
    %     end
    copyfile(inputf,outputf);
end








end