function [ttmdn,tindex]=TO_GetTSITolarence(tsiavaildns,tmdn,ttolarence)
ttmdn=0;
tindex=0;
ttolarence_dn=datenum([0 0 0 0 0 ttolarence]);
[minval,mini]=min(abs(tsiavaildns-tmdn));
if minval<=ttolarence_dn
    ttmdn=tsiavaildns(mini);
    tindex=mini;
end


end