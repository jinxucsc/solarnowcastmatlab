inputDir='input/';
outputDir='output';

allfiles=dir(inputDir);
[m,~]=size(allfiles);
for i=1:m;
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        inputp=sprintf('%s%s',inputDir,filename);
        %sgpvceil25kC1.b1.20080306.000007.cdf
        %sgpvceil25kC1.b1.20080306.000000.cdf
        dotm=findstr(filename,'.');
        filename=sprintf('%s%s',filename(1:dotm(3)-1),'.000000.cdf');
        outputp=sprintf('%s%s',inputDir,filename);
        movefile(inputp,outputp);
    end
end