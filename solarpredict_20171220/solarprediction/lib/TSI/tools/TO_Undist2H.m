%   Two Undist coordinate on different TSI have been proved to have the same spatial position. Then
%   the height is deriative from undistorted coordinate.
function [H_x,H_y]=TO_Undist2H(ux1,uy1,ux2,uy2,loc1,loc2)

%label of site
% TSI1='tsi1';
% TSI2='tsi2';
% TSI3='tsi3';
% 
% 
% if strcmpi(site1,TSI1)
%     loc1=location1;
% elseif strcmpi(site1,TSI2)
%     loc1=location2;
% elseif strcmpi(site1,TSI3)
%     loc1=location3;
% end
% if strcmpi(site2,TSI1)
%     loc2=location1;
% elseif strcmpi(site2,TSI2)
%     loc2=location2;
% elseif strcmpi(site2,TSI3)
%     loc2=location3;
% end


% (ux uy) to (az,ze)
[az1,ze1]=TO_UNDISTCoor2AZ(ux1,uy1);
[az2,ze2]=TO_UNDISTCoor2AZ(ux2,uy2);


[rx2 ry2 rz2]=TO_RealCoor2RelativeCoor(loc1.longitude,loc1.latitude,loc1.altitude,loc2.longitude,loc2.latitude,loc2.altitude);

%   r_ux1=H_x*tan(ze1)*sin(az1)
%   r_ux2=H_x*tan(ze2)*sin(az2)
%   r_ux2=r_ux1-rx2
H_x=rx2/(tan(ze1)*sin(az1)-tan(ze2)*sin(az2));
H_y=ry2/(tan(ze1)*cos(az1)-tan(ze2)*cos(az2));

%   Spatial line intersection
% rx=H*tan(ze)*sin(az)+orx;
% H_x=(rx2-rx1)./(tan(ze1)*sin(az1)-tan(ze2)*sin(az2));
% H_y=(ry2-ry1)./(tan(ze1)*cos(az1)-tan(ze2)*cos(az2));

end