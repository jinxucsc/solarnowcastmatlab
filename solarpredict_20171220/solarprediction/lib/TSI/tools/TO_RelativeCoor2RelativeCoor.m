function [rx_new,ry_new,rz_new]=TO_RelativeCoor2RelativeCoor(rx,ry,rz,location_old,location_new)
lon1=location_old.longitude;
lat1=location_old.latitude;
alt1=location_old.altitude;
lon2=location_new.longitude;
lat2=location_new.latitude;
alt2=location_new.altitude;

[rx_m,ry_m,rz_m]=TO_RealCoor2RelativeCoor(lon1,lat1,alt1,lon2,lat2,alt2);
rx_new=rx-rx_m;
ry_new=ry-ry_m;
rz_new=rz-rz_m;


end