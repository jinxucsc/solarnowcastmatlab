function [rec_vals,startp_pad]=TO_GetImgVal_Pad(startp,recsize,imma,padd_extsize)
if nargin==3
    padd_extsize=max(recsize);
end
starty=startp(1);
startx=startp(2);
recH=recsize(1);
recW=recsize(2);
[H,W,c]=size(imma);
startp_pad=[nan nan];
if starty>=1&&startx>=1 && starty+recH-1<=H && startx+recW-1<=W
    if c==1
        rec_vals=imma(starty:starty+recH-1,startx:startx+recW-1);
    elseif c==3
        rec_vals=zeros(recH,recW,c);
        rec_vals(:,:,1)=imma(starty:starty+recH-1,startx:startx+recW-1,1);
        rec_vals(:,:,2)=imma(starty:starty+recH-1,startx:startx+recW-1,2);
        rec_vals(:,:,3)=imma(starty:starty+recH-1,startx:startx+recW-1,3);
    else
        disp('Input Image matrix should be greyscale(HxW) or RGB format(HxWx3)');
    end
    return;
end

% Out of Boundary
if 1==length(padd_extsize)
    Hpad=padd_extsize;
    Wpad=padd_extsize;
else
    Hpad=padd_extsize(1);
    Wpad=padd_extsize(2);
end

starty_pad=starty+Hpad;
startx_pad=startx+Wpad;
if starty_pad<1 || startx_pad<1 || starty_pad>H+recH || startx_pad>W+recW
    dispstr=sprintf('[WARNING]: Padding is not enough!startp=[%d %d],recSize=[%d %d], padd_extsize=%d',...
        startp(1),startp(2),recsize(1),recsize(2),padd_extsize);
    %disp(dispstr);
    if c==1
        rec_vals=zeros(recH,recW);
    elseif c==3
        rec_vals=zeros(recH,recW,3);
    end
    return;
end


orisy_pad=1+Hpad;
orisx_pad=1+Wpad;
corr_orisy=1;
corr_oriey=recH;
corr_sx=1;
corr_oriey=recH;
corr_ex=recW;
if starty<1
    ly=starty_pad+recH-orisy_pad;
    corr_orisy=1;
    corr_oriey=corr_orisy+ly-1;
    corr_sy=orisy_pad-starty_pad+1;
    corr_ey=recH;
else
    corr_orisy=starty;
    corr_sy=1;
end
if startx<1
    lx=startx_pad+recW-orisx_pad;
    corr_orisx=1;
    corr_oriex=corr_orisx+lx-1;
    corr_sx=orisx_pad-startx_pad+1;
    corr_ex=recW;
else
    corr_orisx=startx;
    corr_sx=1;
end
if starty+recH-1>H
    ly=H-starty+1;
    corr_orisy=starty;
    corr_oriey=starty+ly-1;
    corr_sy=1;
    corr_ey=ly;
else
    corr_oriey=starty+recH-1;
    corr_ey=recH;
end
if startx+recW-1>W
    lx=W-startx+1;
    corr_orisx=startx;
    corr_oriex=startx+lx-1;
    corr_sx=1;
    corr_ex=lx;
else
    corr_oriex=startx+recW-1;
    corr_ex=recW;
end


if c==1
    rec_vals=zeros(recH,recW);
    try
        rec_vals(corr_sy:corr_ey,corr_sx:corr_ex)=imma(corr_orisy:corr_oriey,corr_orisx:corr_oriex);
    catch e
        disp(e);
        disp([corr_orisy corr_oriey corr_orisx corr_oriex]);
    end
elseif c==3
    rec_vals=zeros(recH,recW,3);
    rec_vals(corr_sy:corr_ey,corr_sx:corr_ex,1)=imma(corr_orisy:corr_oriey,corr_orisx:corr_oriex,1);
    rec_vals(corr_sy:corr_ey,corr_sx:corr_ex,2)=imma(corr_orisy:corr_oriey,corr_orisx:corr_oriex,2);
    rec_vals(corr_sy:corr_ey,corr_sx:corr_ex,3)=imma(corr_orisy:corr_oriey,corr_orisx:corr_oriex,3);
else
    disp('Input Image matrix should be greyscale(HxW) or RGB format(HxWx3)');
end

startp_pad=[starty_pad startx_pad];

end