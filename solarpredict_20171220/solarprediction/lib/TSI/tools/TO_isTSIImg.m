function nTSI=TO_isTSIImg(filename)
filename=GetFilenameFromAbsPath(filename);
TSI1='C1';
TSI2='C2';
TSI3='C3';
indexi=strfind(filename,TSI1);
if ~isempty(indexi)
    nTSI='tsi1';
    return;
end
indexi=strfind(filename,TSI2);
if ~isempty(indexi)
    nTSI='tsi2';
    return;
end
indexi=strfind(filename,TSI3);
if ~isempty(indexi)
    nTSI='tsi3';
    return;
end
nTSI='';

end