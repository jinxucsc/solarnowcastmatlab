function [ux_new,uy_new]=TO_Undist2Undist(ux,uy,H,site)
mtsi_startup;

undistx1=ux;
undisty1=uy;
if strcmpi(site,TSI1)
    % 1. (undistx1,undisty1) to (az1,ze1)
    [az1,ze1]=TO_UNDISTCoor2AZ(undistx1,undisty1);
    
    % 2. (az1,ze1) + altitude to relative coor to TSI1 (rx1,ry1,rz1)
    [rx1 ry1 rz1]=AZH2SpaceCoor(az1,ze1,H,0,0,0);
    
    % 3. (rx1,ry1,rz1) -> (rx2,ry2,rz2),(rx3,ry3,rz3)
    [rx2,ry2,rz2]=TO_RelativeCoor2RelativeCoor(rx1,ry1,rz1,location1,location2);
    [rx3,ry3,rz3]=TO_RelativeCoor2RelativeCoor(rx1,ry1,rz1,location1,location3);
    
    % 4. (rx2,ry2,rz2),(rx3,ry3,rz3) -> (az2,ze2) (az3,ze3)
    [az2,ze2]=TO_RelativeCoor2AZ(rx2,ry2,rz2);
    [az3,ze3]=TO_RelativeCoor2AZ(rx3,ry3,rz3);
    
    % 5. (az2,ze2) (az3,ze3) -> (undistx2,undisty2) (undistx3,undisty3)
    [undistx2,undisty2]=TO_AZ2UNDISTCoor(az2,ze2);
    [undistx3,undisty3]=TO_AZ2UNDISTCoor(az3,ze3);
    
    ux_new=[undistx2 undistx3];
    uy_new=[undisty2 undisty3];
    
elseif strcmpi(site,TSI2)
    % 1. (undistx1,undisty1) to (az1,ze1)
    [az1,ze1]=TO_UNDISTCoor2AZ(undistx1,undisty1);
    
    % 2. (az1,ze1) + altitude to relative coor to TSI1 (rx1,ry1,rz1)
    [rx1 ry1 rz1]=AZH2SpaceCoor(az1,ze1,H,0,0,0);
    
    % 3. (rx1,ry1,rz1) -> (rx2,ry2,rz2),(rx3,ry3,rz3)
    [rx2,ry2,rz2]=TO_RelativeCoor2RelativeCoor(rx1,ry1,rz1,location2,location1);
    [rx3,ry3,rz3]=TO_RelativeCoor2RelativeCoor(rx1,ry1,rz1,location2,location3);
    
    % 4. (rx2,ry2,rz2),(rx3,ry3,rz3) -> (az2,ze2) (az3,ze3)
    [az2,ze2]=TO_RelativeCoor2AZ(rx2,ry2,rz2);
    [az3,ze3]=TO_RelativeCoor2AZ(rx3,ry3,rz3);

    % 5. (az2,ze2) (az3,ze3) -> (undistx2,undisty2) (undistx3,undisty3)
    [undistx2,undisty2]=TO_AZ2UNDISTCoor(az2,ze2);
    [undistx3,undisty3]=TO_AZ2UNDISTCoor(az3,ze3);
    
    ux_new=[undistx2 undistx3];
    uy_new=[undisty2 undisty3];
elseif strcmpi(site,TSI3)
    % 1. (undistx1,undisty1) to (az1,ze1)
    [az1,ze1]=TO_UNDISTCoor2AZ(undistx1,undisty1);
    
    % 2. (az1,ze1) + altitude to relative coor to TSI1 (rx1,ry1,rz1)
    [rx1 ry1 rz1]=AZH2SpaceCoor(az1,ze1,H,0,0,0);
    
    % 3. (rx1,ry1,rz1) -> (rx2,ry2,rz2),(rx3,ry3,rz3)
    [rx2,ry2,rz2]=TO_RelativeCoor2RelativeCoor(rx1,ry1,rz1,location3,location1);
    [rx3,ry3,rz3]=TO_RelativeCoor2RelativeCoor(rx1,ry1,rz1,location3,location2);
    
    % 4. (rx2,ry2,rz2),(rx3,ry3,rz3) -> (az2,ze2) (az3,ze3)
    [az2,ze2]=TO_RelativeCoor2AZ(rx2,ry2,rz2);
    [az3,ze3]=TO_RelativeCoor2AZ(rx3,ry3,rz3);

    % 5. (az2,ze2) (az3,ze3) -> (undistx2,undisty2) (undistx3,undisty3)
    [undistx2,undisty2]=TO_AZ2UNDISTCoor(az2,ze2);
    [undistx3,undisty3]=TO_AZ2UNDISTCoor(az3,ze3);
    
    ux_new=[undistx2 undistx3];
    uy_new=[undisty2 undisty3];
end


end