% Test file to show mask images with 1->255 and 0 ->0
function showmaskimg_sb(DataDir,OutputDir)
if(DataDir(end)~='/')
    DataDir=sprintf('%s%s',DataDir,'/');
end
if(OutputDir(end)~='/')
    OutputDir=sprintf('%s%s',OutputDir,'/');
end
mkdir(OutputDir);
allfiles=dir(DataDir);
[m,~]=size(allfiles);

for i=1:m
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        inmap=sprintf('%s%s',DataDir,filename);
        s=load(inmap,'-mat','sbma');
        inputmatrix=s.sbma;
        inputmatrix(inputmatrix>0)=255;
        inputmatrix=uint8(inputmatrix);
        imgname=filename(1:end-4);  %remove .mat
        outfp=sprintf('%s%s',OutputDir,imgname);
        imwrite(inputmatrix,outfp,'jpg');
    end
end

end