% CopyRadbySite
%   Copy rad file by site
%   Standard format -- sgpqcrad1longE21.c1.20120320.000000.cdf     
%   IN:
%       site 'C1'
%   OUT:
%       current directory/rad/file
%   VERSION 1.0         2012/04/05
function CopyRadbySite(inDir,sitename)
if(inDir(end)~='/')
    inDir=sprintf('%s%s',inDir,'/');
end
sitename=upper(sitename);
allfiles=dir(inDir);
[m,~]=size(allfiles);
outDir='rad/';
mkdir(outDir);
fstr=sprintf('%s%s.c1','sgpqcrad1long',sitename);
for i=1:m
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        indexi=findstr(filename,fstr);
        if(isempty(indexi)==1)
            continue;
        end
        inputf=sprintf('%s%s',inDir,filename);
        outputf=sprintf('%s%s',outDir,filename);
        copyfile(inputf,outputf);
    end
end