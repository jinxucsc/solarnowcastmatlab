New Radiatio plot is based on files in input directory ignoring the sites and name. 
1.	Read all the netCDF files in
2.	Get all the radiance out and plot them in UTC/GMT time format.
3.	Put different radiance plot into output directories


Test:
1.	NewRadPlot for some of days short direct normal completed,demo

		NewRadPlot('D:\Project_images\ForCoorExt');
Output is in test1/

2012-04-05

2.	Test for short direct normal completed, test all the SGP_RAD_SI files, demo

	NewRadPlot('D:\Project_images\SGP_RAD_SI\141645');

Output is renamed as ALL


3.	Test for sgpqcradC1.C1 plots from 3/1 to 3/31 
		NewRadPlot('rad1/');

output is renamed as c1c1_3_1_3_31







CopyRadbySite.me: to copy all the certain rad files to rad/ directory

1.	get 3.1- 3.31 sgprad to rad1/ directory
		
		CopyRadbySite('D:\Project_images\SGP_RAD_SI\141645','C1');

output is renamed as rad1/






2.	Rad_all all the radiation files including all sites and all C1 streams(s1 streams are removed)

	CopyRadbySite('D:/Project_images/SGP_RAD_SI/ALL','C1');
	CopyRadbySite('D:/Project_images/SGP_RAD_SI/ALL','E11');
	CopyRadbySite('D:/Project_images/SGP_RAD_SI/ALL','E12');
	CopyRadbySite('D:/Project_images/SGP_RAD_SI/ALL','E13');
	CopyRadbySite('D:/Project_images/SGP_RAD_SI/ALL','E15');
	CopyRadbySite('D:/Project_images/SGP_RAD_SI/ALL','E21');
	CopyRadbySite('D:/Project_images/SGP_RAD_SI/ALL','E9');

output is renamed as rad_all




