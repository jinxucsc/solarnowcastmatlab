% This function is to calculate the the 3d mask based on 2d mask input.
%   Usage:
%       cloud mask of mask [H,W]
%       Get the cloud mask in RGB image by generating [H,W,3] mask
function mask_3d=TO_3DMaskfrom2D(mask,dim_change)
[H,W]=size(mask);
mask_3d=zeros(H,W,3);

if nargin==1    
    mask_3d(:,:,1)=mask;
    mask_3d(:,:,2)=mask;
    mask_3d(:,:,3)=mask;
else
    tml=length(dim_change);
    for i=1:tml
        tmdim=dim_change(i);
        if tmdim>3 || tmdim<=0
            continue;
        end
        mask_3d(:,:,tmdim)=mask;
    end
end
mask_3d=logical(mask_3d);

end