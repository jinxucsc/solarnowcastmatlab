%NewRadPlot(inDir):
%   New Radiatio plot is based on files in input directory ignoring the sites and name.
%   1.	Read all the netCDF files in
%   2.	Get all the radiance out and plot them in UTC/GMT time format.
%   3.	Put different radiance plot into output directories
%   IN:
%       inDir       --  input directory
%   OUT:
%       different directorys under the same parent folder based on name
%   VERSION 1.0     2012/04/05
function NewRadPlot(inDir)
if(inDir(end)~='/')
    inDir=sprintf('%s%s',inDir,'/');
end
ONEDAY_NEW=1440;
allfiles=dir(inDir);
[m,~]=size(allfiles);
pl1='short_direct_normal';
pl1_dir=sprintf('%s%s',pl1,'/');
mkdir(pl1_dir);
for i=1:m
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        if(strcmpi(filename(end-3:end),'.cdf')==0)
            disp('Wrong input file format!Extension is not cdf');
            continue;
        end
        inputf=sprintf('%s%s',inDir,filename);
        outputf=sprintf('%s%s.jpg',pl1_dir,filename);
        ncid=netcdf.open(inputf,'NC_NOWRITE');
        varID_pl1=netcdf.inqVarID(ncid,pl1);
        sdn=netcdf.getVar(ncid,varID_pl1);
        sdn(sdn<0)=0;
        varID_time=netcdf.inqVarID(ncid,'time');
%         time=netcdf.getVar(ncid,varID_time);
        drawset=sdn;
        timelabel=0:23;
        set(0,'DefaultFigureVisible', 'off');
        pic2=figure;
        titStr=sprintf('One Day Radiation of %s',filename);
        legStr=sprintf(pl1);
        xStr='UTC/GMT';
        yStr='Radiation Val(W/m^2)';
        grid on;
        hold on;
        xlabel(xStr);
        ylabel(yStr);
        plot(drawset,'c');
        set(gca,'XTick',1:60:ONEDAY_NEW+1);
        set(gca,'XTickLabel',timelabel);
        title(titStr);
        legend(legStr,'location','NorthEast');
        hold off;
        print(pic2,'-djpeg ',outputf);
        
    end
end

end