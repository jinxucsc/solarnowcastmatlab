% This function is to use ncc to get most likely motion vector based on this frame and next frame
% given mvtrustThreshold which indicates the ncc threshold of two blocks.
function cc_vals=ncc_mvpick(imma_lum,imma_1faf_lum,startp,recSize,SearchWinSize_ext,mvpickrange)
assert(size(imma_lum,3)==1,'Input must not be RGB image matrix');
assert(size(imma_1faf_lum,3)==1,'Input must not be RGB image matrix');

recH=recSize(1);
recW=recSize(2);
uy1=startp(1);
ux1=startp(2);
mv_x=mvpickrange(:,2);
mv_y=mvpickrange(:,1);
uy1_e=uy1+recH-1;
ux1_e=ux1+recW-1;
uy2=uy1-SearchWinSize_ext; ux2=ux1-SearchWinSize_ext;
uy2_e=uy1_e+SearchWinSize_ext;
ux2_e=ux1_e+SearchWinSize_ext;


blk1=single(TO_GetImgVal_Pad([uy1 ux1],[recH recW],imma_lum,SearchWinSize_ext));
blk2_search=single(TO_GetImgVal_Pad([uy2 ux2],[uy2_e-uy2+1 ux2_e-ux2+1],imma_1faf_lum,SearchWinSize_ext));
cc=normxcorr2_blk(blk1,blk2_search);
if isempty(find(cc,1))
    cc_vals=mvpickrange; 
    cc_vals(:)=0;
    return;
end

mv_n=length(mv_x);
cc_vals=zeros(mv_n,1);

for tmi=1:mv_n   
    tmx=round(mv_x(tmi)+recW+SearchWinSize_ext);   %mv_x=tmx-recW-NextFrameMVSearchWinSize;
    tmy=round(mv_y(tmi)+recH+SearchWinSize_ext);   %mv_y=tmy-recH-NextFrameMVSearchWinSize;
    cc_vals(tmi)=cc(tmy,tmx);
end



end