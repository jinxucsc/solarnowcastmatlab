% ChooseClearTS:
%   It will choose clear sky situations during the given radiation files.
%   The output file will be written as a matfile
%   Version: 1.0    2012/3/30  
%   INPUT:
%       RadDir      --  radiation file location
%       imgDir      --  image files
%       UndistortionParameters.mat(in)  --  site information here
%   OUTPUT:
%       CSL_TS.mat  --  Clera Sky Library TimeStamps
%           #_tsdn    --  date number of each record

function ChooseClearTS(RadDir,imgDir)
if(RadDir(end)~='/')
    RadDir=sprintf('%s%s',RadDir,'/');
end
if(imgDir(end)~='/')
    imgDir=sprintf('%s%s',imgDir,'/');
end
load('UndistortionParameters.mat');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   1. Enumerate all image files
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmpi(SITE,'TWP')==0
    disp('ATTENTION: SITE is not TWP');
end
cenx=Hi/2;
ceny=Wi/2;
Distctrl=200;
maskctrl=zeros(Hi,Wi);      % Mask control is used for cloud fraction test area mask
ncount=0;
for i=1:Hi
    for j=1:Wi
        xi=i-cenx;
        yi=j-ceny;
        if(sqrt(xi^2+yi^2)<Distctrl)
            ncount=ncount+1;
            maskctrl(i,j)=1;
        end
    end
end
nTotalPixels=ncount;

cloudthres=0.65;
cfthres=0.4;
allfiles=dir(imgDir);
[m,~]=size(allfiles);
tsdn=zeros(m-2,1);
for i=1:m
    if(allfiles(i).isdir~=1)
        ii=i-2;
        filename=allfiles(i).name;
        datevector=GetDVFromImg(filename);
        inputf=sprintf('%s%s',imgDir,filename);
        inputim=imread(inputf);
        f1=double(inputim(:,:,1));
        f3=double(inputim(:,:,3));
        rbma=(f1./f3).*maskctrl;    %R/B ratio
        cf=length(find(rbma>cloudthres))/ncount;
        if(cf<cfthres)
            tsdn(ii)=datenum(datevector);
        end
    end
end

tsdn=tsdn(tsdn>0);
outputp='CSL_TS.mat';
save(outputp,'tsdn');
% allfiles=dir(RadDir);
% [m,~]=size(allfiles);
% for i=1:m
%     if(allfiles(i).isdir~=1)
%         ii=i-2;
%         filename=allfiles(i).name;
%     end
% end


end