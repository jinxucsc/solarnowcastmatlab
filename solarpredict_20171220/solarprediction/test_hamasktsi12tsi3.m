function test_hamasktsi12tsi3(inputDir,outputDir)
inputDir=FormatDirName(inputDir);
outputDir=FormatDirName(outputDir);
mkdir(outputDir);

allfiles=dir(inputDir);
[nfiles,~]=size(allfiles);
TSI3='tsi3';
for i =1:nfiles
    if allfiles(i).isdir==1
        continue;
    end
    filename=allfiles(i).name;
    tmdv=GetDVFromImg(filename);
    f3=GetImgFromDV_BNL(tmdv,TSI3);
    inputf=sprintf('%s%s',inputDir,filename);
    outputf=sprintf('%s%s.mat',outputDir,f3);
    copyfile(inputf,outputf);
end
end