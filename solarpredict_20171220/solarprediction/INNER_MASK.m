function maskoutma=INNER_MASK(maskinma,maskma)
for i=1:3
    tmimgma=maskinma(:,:,i);
    tmimgma(maskma)=0;
    maskinma(:,:,i)=tmimgma(:,:);
end
maskoutma=maskinma;
end