function imma_avg=test_genavgImg(countma,imma)

r1=double(imma(:,:,1));
g1=double(imma(:,:,2));
b1=double(imma(:,:,3));
r1=r1./countma;
b1=b1./countma;
g1=g1./countma;
ind=(r1==inf)&(g1==inf)&(b1==inf);
r1(ind)=0;
g1(ind)=0;
b1(ind)=0;
imma_avg=imma;
imma_avg(:,:,1)=r1;
imma_avg(:,:,2)=g1;
imma_avg(:,:,3)=b1;

end