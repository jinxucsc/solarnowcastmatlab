% BuildClearSkyLib_wBMPMask:
%   If we have clear sky timestamps to build a library. Then read it and
%   Generate all the mask files and put them together into sbmask and
%   hamask files.
%   INPUT:
%       realtimedv     --  Datevector, realtime timestamp 
%       imma           --  Image Matrix(UNDISTH x UNDISTW x 3) at realtimedv
%       HAMaskDir      --  Directory, contains the HA output mask files
%       SBMaskOutDir    -- Direcotry, contains the SB output mask files
%       timespan        --  INT, SECONDS thats defines timespan between two
%                           csl mat files
%       site            -- Currently supported TSI1,TSI2,TSI3 in mtsi_startup
%       TestDir(test)       --  test the mask images
%       RemovalParameters.mat(in)   --  all the readin thresholds
%       UndistortionParameters.mat(in)--Original Image information
%       cls_ts.mat(in)              --  all clear sky timestamp (OBSOLETE)
%       sbmask_pixelrange.bmp       --  bmp file (black -> invalid white -> valid)
%                                       pixels in line regression cannot
%                                       pick points out of white area
%   OUTPUT:
%       HAMaskDir/*    -- All the mask files(.mat) will be put here
%       SBMaskDir/*    -- All the mask files(.mat) will be put here
%       TestDir/*    -- Test result for itself. Fore later manually pick
%
%   Version: 1.0    2014/04/23  Previous version: BuildClearSkyLib_wBMPMask
%   Handle only realtime timestamp

function realtime_BuildClearSkyLib_wBMPMask(realtimedv,imma,HAMaskDir,SBMaskOutDir,site,timespan,configfpath,logger,testfillImgDir)
% Set LoggerID
[tasks,~]=dbstack;
loggerID = tasks(1).name;

tmdv=realtimedv;
filename=GetImgFromDV_BNL(tmdv,site);
tmdn=datenum(tmdv);
outfma=sprintf('%s%s.mat',SBMaskOutDir,filename);
if(exist(outfma,'file')~=0)
    logger.debug(loggerID,'SB Mask exist! BuildClearSkyLib will return');
    return;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STARTUP to get all possible parameter settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run(configfpath);   %mtsi_startup;

nsite =length(SITES);
for i=1:nsite
    if site==SITES{i};
        location=locations(i);
        AzumithShift=AzumithShifts(i);
        ZenithShift=ZenithShifts(i);
        rcentrep=rcentreps(i);
        cw=cws(i);
        ch=chs(i);
        crx=crxs(i);
        cry=crys(i);
        fo=fos(i);
        h=hs(i);
        r=rs(i);
        bmpmaskorif=BMPMASKORIFS{i};    % cell of image name
    end
end
bmpmaskf=GetImgFromDV_BNL(GetDVFromImg(bmpmaskorif),site);
bmpmaskf=[bmpmaskf '.mat'];
if ~exist(bmpmaskf,'file')
    BMP2SBMask_BNL(bmpmaskf,site,configfpath);  % generate the mask mat file
end
t=load(bmpmaskf);
bmpmask=t.bmpmask;
bmp_az=t.az;
bmp_az_degree=bmp_az/pi*180;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TSI1_NUM=1;
% TSI2_NUM=2;
% TSI3_NUM=3;
timespan_DN=datenum([0 0 0 0 0 timespan]);
%   if not exist define as default
sbmaskranma=imread(SBMASK_VALIDMASK);
sbmaskranmask=rgb2gray(sbmaskranma);
tmindice=(sbmaskranmask>150);
%     sbmaskranmask(tmindice)=255;
%     sbmaskranmask(~tmindice)=0;
sbmaskranmask=tmindice;
HAMaskDir=FormatDirName(HAMaskDir);
SBMaskOutDir=FormatDirName(SBMaskOutDir);
btestfill=false;
if exist('testfillImgDir','var')
    %testfillImgDir='testfill_specialtest_20130422/';        %   test fill is to see SB filling result
    btestfill=true;
    testfillImgDir=FormatDirName(testfillImgDir);
    if ~exist(testfillImgDir,'dir')
        mkdir(testfillImgDir);
    end
end

if ~exist(HAMaskDir,'dir')
    mkdir(HAMaskDir);
end
if ~exist(SBMaskOutDir,'dir')
    mkdir(SBMaskOutDir);
end

%load('RemovalParameters.mat');  % Preprocessing done for HA and SB mask (obsolete)


midpos=zeros(1,2);
midd=zeros(1,2);


% SDIST_START2END=200;        %   Generated search array length, which is
% SunSpotRange=20;            %   Sun range around sunpositon which will not
% FillRadius=230;              %   Fill the Sun range(SPX-ran:SPX+ran)
% UpperLine_DistMovement=43;       %  Upperline is to control the upper end of
% %  the shadow band
% 
% %   generate any edge detection
% %   distributed along the azumith line.
% BoundSearchRange=   35;    % from x-30 ->   x+30Edge
% BoundaryThres=      0;    % drop range exceeds 30 then treat as boundary
% BrightThres=        150;   % if lines points larger than threshold no boundary searche
% SearchPixelLen=   180;     % The search pixel number along the mid line
% LineShift=  4;      %   Edge move N pixels out to get tolerant line
% AngleDist=5/180*pi;        %   Upper bound and lower bound should have less than this threshold angle dfference to middline
% TwoLineDist= 40;    %    minimum dist between two edge lines.

% inputmatrix=zeros(ORIH,ORIW);
% inputmatrix=uint8(inputmatrix);
greyimg=zeros(ORIH,ORIW);
greyimg=uint8(greyimg);
% greyma=zeros(ORIH,ORIW);
% greyma=uint8(greyma);
gma=zeros(ORIH,ORIW);
gma=uint8(gma);

maskindexma=zeros(ORIH,ORIW);

for xr=1:ORIW
    for yr=1:ORIH
        if(sqrt((xr-rcentrep.x)^2+(yr-rcentrep.y)^2)<=FillRadius)
            maskindexma(yr,xr)=1;
        end
    end
end

saz=pi;
eaz=3*pi/2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Add code for avail mat
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% oriimgdirname=GetFilenameFromAbsPath(DataDir);
% avail_f=sprintf('avail_%s.mat',oriimgdirname);
% if ~exist(avail_f,'file')
%     tmdns=zeros(nfiles,1);
%     for i =1:nfiles
%         if allfiles(i).isdir==1
%             continue;
%         end
%         filename=allfiles(i).name;
%         tmdv=GetDVFromImg(filename);
%         tmdn=datenum(tmdv);
%         tmdns(i)=tmdn;
%     end
%     save(avail_f,'tmdns');
% end
% t=load(avail_f);
% tmdns=t.tmdns;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   DEBUG TEST of CERTAIN tmdn
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         %if tmdn==datenum([2012 07 30 14 15 43])
%         if tmdn==datenum([2012 09 24 15 05 52])
%             disp(tmdn);
%         else
%             continue;
%         end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%inputf=sprintf('%s%s',DataDir,filename);
%tmdv=GetDVFromImg(filename);
%     datevector=datevec(tsdn(i));
datevector=tmdv;
time.year    =  datevector(1);
time.month   =  datevector(2);
time.day     =  datevector(3);
time.hour    =  datevector(4);
time.min    =   datevector(5);
time.sec    =   datevector(6);
time.UTC    =   0;
sun = sun_position(time, location);
az=sun.azimuth/180*pi;
az_degree=sun.azimuth;
ze=sun.zenith/180*pi;
diff_az_degree=bmp_az_degree-az_degree;
%tmbmpmask=imrotate(~bmpmask,diff_az_degree,'crop');
tmbmpmask=sbrotate(~bmpmask,diff_az_degree,rcentrep);


%outputf=sprintf('%s%s',testfillImgDir,filename);
%     if(exist(outputf,'file')~=0)
%         continue;
%     end
f=imma(:,:,3);
newf=rgb2gray(imma);
newf(~tmbmpmask)=0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   HA Mask
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     f1=img1(:,:,1);
%     f2=img1(:,:,2);
%     index1=((f2-f)>=MaxDiffRan)&((f2-f1)>0)&f2<CloudThres;  %SB and HA index1 calculation is the same
%     hama=uint8(zeros(ORIH,ORIW));
%     index=((f>=BLUETHRES_HA)&~index1)|~areamask;   %Holding arm
%     hama(index)=1;
%     %     outm=zeros(ORIH,ORIW);
%     %     outp=sprintf('%s%s',TestDir,filename);
%     %     outm(index)=255;
%     %     imwrite(outm,outp,'jpg');
%     hamaskf=sprintf('%s%s.mat',HAMaskDir,filename);
%     save(hamaskf,'hama');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   SB Mask
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sbma=edge(f,'sobel');
sbedgema=sbma&sbmaskranmask& tmbmpmask;     % add bmp mask here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   DEBUG for bmpmask(uncomment to use)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     tmoutdir='tmpout/';
%     tmf=img1;
%     tmf1=img1(:,:,1);
%     tmf2=img1(:,:,2);
%     tmf3=img1(:,:,3);
%     tmf1(tmbmpmask)=sbma(tmbmpmask)*255;
%     tmf2(tmbmpmask)=sbma(tmbmpmask)*255;
%     tmf3(tmbmpmask)=sbma(tmbmpmask)*255;
%     tmf(:,:,1)=tmf1(:,:);
%     tmf(:,:,2)=tmf2(:,:);
%     tmf(:,:,3)=tmf3(:,:);
%     if ~exist(tmoutdir,'dir')
%         mkdir(tmoutdir);
%     end
%     outputf=sprintf('%s%s',tmoutdir,filename);
%     imwrite(tmf,outputf,'jpg');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   SB Edge test -- single
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%[x,y]=CalImageCoorFrom_ALL(az,ze,TSI1_NUM);
[x,y]=CalImageCoorFrom_paras(...
    az,ze,...
    AzumithShift,ZenithShift,...
    h,r,...
    cw,ch,crx,cry,fo,...
    rcentrep...
    );
az=az+AzumithShift/180*pi;
ze=ze+ZenithShift/180*pi;

aangle=az-pi/2;
if(abs(double(aangle)-double(pi/2))<0.001)
    % x=x1t
    a_midline=-9999;      % stands for infinite
    b_midline=-9999;
    
else
    a_midline=tan(aangle);
    b_midline=y-a_midline*x;
end
midpos=[rcentrep.x,rcentrep.y];
[tmpx,tmpy]=GetEndPoint(midpos(1),midpos(2),...
    a_midline,b_midline,SDIST_START2END);
%   Start point has the same vector direction with sun position then
%   we can get right end point
if(sign(x-midpos(1))==sign(tmpx(1)-midpos(1)))
    midd(1)=tmpx(1);
    midd(2)=tmpy(1);
else
    midd(1)=tmpx(2);
    midd(2)=tmpy(2);
end

if(x-midpos(1)>0)
    [midd(1),tmpp]=max(tmpx);
    midd(2)=tmpy(1,tmpp);
else
    [midd(1),tmpp]=min(tmpx);
    midd(2)=tmpy(1,tmpp);
end
clear tmpx tmpy;
inputmatrix=sbma;
greyimg(:,:)=inputmatrix(:,:);
[SXArray,SYArray]=GetSearchArray(a_midline,b_midline,...
    midpos(1,:),midd(1,:),SearchPixelLen,SDIST_START2END);
%   Rebuild SXArray and SYArray to fit the edge
%     [SXArray,SYArray]=ReGenArrayOnBoundary(SXArray,SYArray,x,y,SunSpotRange);
[B_UpArray,B_DownArray]=FindBoundary(SXArray,SYArray,BoundSearchRange,...
    BoundaryThres,BrightThres,a_midline,b_midline,greyimg);
LPUpArray=     [B_UpArray(B_UpArray(:,1)>0) B_UpArray(B_UpArray(:,2)>0,2)];
LPDownArray=   [B_DownArray(B_DownArray(:,1)>0) B_DownArray(B_DownArray(:,2)>0,2)];
lpdist=LPUpArray(:,1).*a_midline-LPUpArray(:,2)+b_midline;
id=abs(lpdist-mean(lpdist))<=2*std(lpdist);
tmpx = LPUpArray(id,1);
tmpy = LPUpArray(id,2);
if(isempty(tmpx)~=0||isempty(tmpy)~=0)
    %   disp('No boundary found, skip this TS');
    % Abnormal case just use rotation mask
    sbma=~tmbmpmask;
    save(outfma,'sbma');
    logger.debug(loggerID,'No boundary found, skip this TS');
    return;
end
if length(tmpx)<=20||length(tmpx)<=20
    sbma=~tmbmpmask;
    save(outfma,'sbma');
    logger.debug(loggerID,'Length of tmpx is smaller than 20');
    return;
end
LPUpArray=[tmpx,tmpy];
lpdist=LPDownArray(:,1).*a_midline-LPDownArray(:,2)+b_midline;
id=abs(lpdist-mean(lpdist))<=2*std(lpdist);
tmpx = LPDownArray(id,1);
tmpy = LPDownArray(id,2);
LPDirect=LPDirection(az);
LPDownArray=[tmpx,tmpy];
if(isempty(tmpx)~=0||isempty(tmpy)~=0)
    %   disp('No boundary found, skip this TS');
    % Abnormal case just use rotation mask
    sbma=~tmbmpmask;
    save(outfma,'sbma');
    logger.debug(loggerID,'No boundary found, skip this TS');
    return;
end
if length(tmpx)<=20||length(tmpx)<=20
    % Abnormal case just use rotation mask
    sbma=~tmbmpmask;
    save(outfma,'sbma');
    logger.debug(loggerID,'Length of tmpx is smaller than 20');
    return;
end

[as bs]=FindLinearApprox(LPUpArray,LPDownArray,LPDirect);
LinRegreDropThres=  8;
clear tmpx tmpy;
[tmpx,tmpy]=GetNewLineArrays(as(1),bs(1),LPUpArray(:,1),LPUpArray(:,2),...
    LinRegreDropThres,LPDirect);
[tmpx1,tmpy1]=GetNewLineArrays(as(2),bs(2),LPDownArray(:,1),...
    LPDownArray(:,2),LinRegreDropThres,LPDirect);
if(isempty(tmpx1)~=0||isempty(tmpy1)~=0)
    %   disp('No boundary found, skip this TS');
    % Abnormal case just use rotation mask
    sbma=~tmbmpmask;
    save(outfma,'sbma');
    logger.debug(loggerID,'No boundary found, skip this TS');
    return;
end
[asnew bsnew]=FindLinearApprox([tmpx,tmpy],[tmpx1,tmpy1],LPDirect);
as=asnew;
bs=bsnew;
%     save('reflines.mat','LPDirect','as','bs','tmdv');

if(LPDirect=='x')
    %Ax-y+B=0
    if(sign(as(1)*x+bs(1)-y)<0)
        bs(1)=bs(1)-LineShift;
    else
        bs(1)=bs(1)+LineShift;
    end
else
    %Ay-x+B=0
    if(sign(as(1)*y+bs(1)-x)<0)
        bs(1)=bs(1)-LineShift;
    else
        bs(1)=bs(1)+LineShift;
    end
end
if(LPDirect=='x')
    %Ax-y+B=0
    if(sign(as(2)*x+bs(2)-y)<0)
        bs(2)=bs(2)-LineShift;
    else
        bs(2)=bs(2)+LineShift;
    end
else
    %Ay-x+B=0
    if(sign(as(2)*y+bs(2)-x)<0)
        bs(2)=bs(2)-LineShift;
    else
        bs(2)=bs(2)+LineShift;
    end
end
%function [Xarr,Yarr]=GenFillPoints(SPx,SPy,FillRadius)
greyimg=rgb2gray(imma);
gma(:,:)=sbma(:,:);
%         Xarr=zeros((2*FillRadius)^2,1);
%         Yarr=zeros((2*FillRadius)^2,1);
%         ci=1;
SPx=round(x);
SPy=round(y);
cenl_a=-1/a_midline;
cenl_b=-rcentrep.x*cenl_a+rcentrep.y;
cenl_sign=sign(x*cenl_a-y+cenl_b);
uppl_a=cenl_a;
uppl_b=cenl_b+cenl_sign*sqrt(1+cenl_a^2)*UpperLine_DistMovement;

for xr=1:ORIW
    for yr=1:ORIH
        if(maskindexma(yr,xr)==0)
            gma(yr,xr)=1;
            continue;
        end
        %   a. If beyond upper line then fill 1
        if(sign(uppl_a*xr-yr+uppl_b)~=cenl_sign)
            %                 greyimg(yr,xr)=255;
            gma(yr,xr)=1;
            continue;
        end
        %   b. Should be inside of two edge lines otherwise fill 1
        if(LPDirect=='x')
            if(sign(as(1)*xr-yr+bs(1))==sign(as(2)*xr-yr+bs(2)))
                %                     greyimg(yr,xr)=255;
                gma(yr,xr)=1;
                continue;
            end
        else
            if(sign(as(1)*yr-xr+bs(1))==sign(as(2)*yr-xr+bs(2)))
                %                     greyimg(yr,xr)=255;
                gma(yr,xr)=1;
                continue;
            end
        end
        % c. should be the same side with SP to central Line
        %    otherwise continue without filling
        %             if(sign(cenl_a*xr-yr+cenl_b)~=cenl_sign)
        %                 gma(yr,xr)=1;
        %                 continue;
        %             end
        % d. Should be in Fill Circle
        %    otherwise continue without filling
        %             if(sqrt((xr-x)^2+(yr-y)^2)>FillRadius)
        %                 gma(yr,xr)=1;
        %                 continue;
        %             end
        %             greyimg(yr,xr)=0;   %all condition satisfied
        gma(yr,xr)=0;
    end
end
greyimg(gma>0)=255;
greyimg(gma==0)=0;

%     outfma=sprintf('%s%s.mat',SBMaskOutDir,filename);
%     outputf=sprintf('%s%s',testfillImgDir,filename);
%     imwrite(greyimg,outputf,'jpg');
%             outputma(ii,:,:)=gma;

%     save(maoutf,'gma');
clear SXArray SYArray B_UpArray B_DownArray LPUpArray LPDownArray aa id tmpx tmpy tmpx1 tmpy1 greyma greyimg inputimg;
sbma(:,:)=gma(:,:);
save(outfma,'sbma');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Test for result put the result to TestDir
%   Uncomment to use
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if btestfill
    totalmaskma=~(gma);
    tmcolor=zeros(ORIH,ORIW);
    for j=1:3
        tmcolor(:,:)=imma(:,:,j);
        tmcolor(totalmaskma)=0;
        tmcolor(sbedgema)=255;
        imma(:,:,j)=tmcolor;
    end
    outputf=sprintf('%s%s',testfillImgDir,filename);
    imwrite(imma,outputf,'jpg');
    logger.debug(loggerID,['bTestFill is true. Plot test filling result to ' outputf]);
end

end