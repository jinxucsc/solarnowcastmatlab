% Main function to run TSI3 filling
%VERSION 1.0 2014-05-19
function realtime_mtsipipe_TSI3fill_endless(configfpath)
if nargin==0
    configfpath='mtsi_startup';
end
run(configfpath);
TO_setRealtimeModuleStatus(TO_getmodulename(),TO_str2modstate('normal'),datevec(now),ModuleStatusRecFile);
if bWriteDiary
    diary(TSI3FillDiaryFile);
end
if TSIStiMatPoolSize>1
    bUseParfor=true;
    if matlabpool('size')~=0
        matlabpool('close');
    end
    matlabpool(TSIStiMatPoolSize);
else
    bUseParfor=false;
end

undistDir1 = utsi1Dir;
undistDir2 = utsi2Dir;
undistDir3 = utsi3Dir;
TSI3filledDir = TSI3FilledDir;
HRefDir=CldLayerRefDir;
TSI3filledDir =FormatDirName(TSI3filledDir);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Logger setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
logfile=TSI3FillLogFile;
logger=log4m.getLogger(logfile);
loggerID='realtime_stich';
logger.setLogLevel(logger.DEBUG);
logger.setCommandWindowLevel(logger.DEBUG);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DNDIFF = datenum([10 0 0 0 10 0]); % consider only 15 minutes ahead
set_dn=java.util.HashSet;
%bMulticore=false;       % multicore or single core
%servertype=ServerType;
%maxmatlabinst=28;     % not used if single core
DNDIFF = datenum([0 0 0 0 10 0]); % consider only 15 minutes ahead



try
    while(1)
        files3= dir(undistDir3);
        %files= dir(TSI3filledDir);
        m=size(files3,1);
        utc_time = java.lang.System.currentTimeMillis;
        a=utc_time/1000; % conver to UNIX TIMESTAMP
        nowdn=(a/86400 + datenum(1970,1,1)); % UTC now datenum
        startdn=nowdn-DNDIFF;
        tmdns=[];
        for i = 1:m
            filename=files3(i).name;
            tmdv = GetDVFromImg(filename);
            tmdn=datenum(tmdv);
            if (tmdv==0)
                continue;
            end
            filledname=[TSI3filledDir GetImgFromDV_BNL(tmdv,'tsi3') '_shifttest.jpg'];
            if(~set_dn.contains(tmdn) && tmdn >=startdn && ~exist(filledname,'file'))
                tmdns=[tmdns tmdn];
            end
        end
        tmdns=sort(tmdns,'descend');
        l = length(tmdns);
        if l==0
            logger.debug(loggerID,['No new files available at ' datestr(now)]);
            pause(5);   % there is no available tmdn for TSI3 fill. Then sleep for 1 second and continue
            continue;
        end
        if bUseParfor
            N=min([TSIStiMatPoolSize,l]);
            parfor i = 1:N
                %filename=files3(i).name;
                tmdn=tmdns(i);
                tmdv=datevec(tmdn);
                filename=GetImgFromDV_BNL(tmdv,'tsi3');
                filename1=GetImgFromDV_BNL(tmdv,'tsi1');
                filename2=GetImgFromDV_BNL(tmdv,'tsi2');
                filename=[filename(1:end-4) '.bmp'];
                filename1=[filename1(1:end-4) '.bmp'];
                filename2=[filename2(1:end-4) '.bmp'];
                inputf1=[undistDir1 filename1];
                inputf2=[undistDir2 filename2];
                inputf3=[undistDir3 filename];
                if(~exist(inputf1,'file') || ~exist(inputf2,'file') || ~exist(inputf3,'file'))
                    logger.debug(loggerID,['[WARNING]: ' datestr(tmdv) 'is skipped due to incomplete undistortion files!']);
                    continue; %no need to do filling since other images are not ready
                end
                %filledname=[TSI3filledDir GetImgFromDV_BNL(tmdv,'tsi3') '_shifttest.jpg'];
                logger.debug(loggerID,['Start TSI3 filling for ts = ' datestr(tmdv)]);
                set_dn.add(tmdn);
                
                %                 %realtime_mtsipipe_TSI3fill(dataDir,tmdv,'./model_rbr.mat',TSI3filledDir,'mtsi_startup','./HistRef_recheck','skyfill.bmp');
                %                 cmmd=sprintf('realtime_mtsipipe_TSI3fill(''%s'',%s,''%s'',''%s'',''%s'',''%s'',''%s'');',...
                %                     dataDir,sprintf('[%d %d %d %d %d %d]',tmdv(1),tmdv(2),tmdv(3),tmdv(4),tmdv(5),tmdv(6)),...
                %                     SVMCloudModelFile,TSI3filledDir,HRefDir,TSIDefaultSkyColorFile,configfpath);
                %                 logger.debug(loggerID,cmmd);
                %                 subprocess_matlab(cmmd,maxmatlabinst,servertype);
                %                 pause(0.5);
                
                realtime_mtsipipe_TSI3fill(tmdv,SVMCloudModelFile,TSI3filledDir,HRefDir,TSIDefaultSkyColorFile,configfpath,logger);
            end
            if ~bUseParfor
                matlabpool('close');
            end
        else
            i=1;    % choose the most recent one to do TSIFill
            %filename=files3(i).name;
            tmdn=tmdns(i);
            tmdv=datevec(tmdn);
            filename=GetImgFromDV_BNL(tmdv,'tsi3');
            filename1=GetImgFromDV_BNL(tmdv,'tsi1');
            filename2=GetImgFromDV_BNL(tmdv,'tsi2');
            filename=[filename(1:end-4) '.bmp'];
            filename1=[filename1(1:end-4) '.bmp'];
            filename2=[filename2(1:end-4) '.bmp'];
            inputf1=[undistDir1 filename1];
            inputf2=[undistDir2 filename2];
            inputf3=[undistDir3 filename];
            if(~exist(inputf1,'file') || ~exist(inputf2,'file') || ~exist(inputf3,'file'))
                logger.debug(loggerID,['[WARNING]: ' datestr(tmdv) 'is skipped due to incomplete undistortion files!']);
                continue; %no need to do filling since other images are not ready
            end
            %filledname=[TSI3filledDir GetImgFromDV_BNL(tmdv,'tsi3') '_shifttest.jpg'];
            logger.debug(loggerID,['Start TSI3 filling for ts = ' datestr(tmdv)]);
            set_dn.add(tmdn);
            realtime_mtsipipe_TSI3fill(tmdv,SVMCloudModelFile,TSI3filledDir,HRefDir,TSIDefaultSkyColorFile,configfpath,logger);
        end
    end
catch err
    logger.error(loggerID,sprintf('%s--line %d  msg:%s',err.stack(1).name,err.stack(1).line,err.message));
    if bWriteDiary
        diary off;
    end
    TO_setRealtimeModuleStatus(TO_getmodulename(),TO_str2modstate('error'),datevec(now),ModuleStatusRecFile);
    return;
end


% for i = 1:l
%     filename=files3(i).name;
%     tmdv=GetDVFromImg(filename);
%     if tmdv==0
%         continue;
%     end
%     tmdn=datenum(tmdv);
%     if set_dn.contains(tmdn)
%         continue;   % All ready processed
%     end
%     filename=GetImgFromDV_BNL(tmdv,'tsi3');
%     filename1=GetImgFromDV_BNL(tmdv,'tsi1');
%     filename2=GetImgFromDV_BNL(tmdv,'tsi2');
%     filename=[filename(1:end-4) '.bmp'];
%     filename1=[filename1(1:end-4) '.bmp'];
%     filename2=[filename2(1:end-4) '.bmp'];
%     inputf1=[undistDir1 filename1];
%     inputf2=[undistDir2 filename2];
%     inputf3=[undistDir3 filename];
%     if(~exist(inputf1,'file') || ~exist(inputf2,'file') || ~exist(inputf3,'file'))
%         logger.debug(loggerID,['[WARNING]: ' datestr(tmdv) 'is skipped due to incomplete undistortion files!']);
%         continue; %no need to do filling since other images are not ready
%     end
%     tmf=GetImgFromDV_BNL(tmdv,TSI3); tmf=[tmf(1:end-4) '.jpg'];
%     outputf=[TSI3filledDir tmf '_shifttest.jpg'];
%     if exist(outputf,'file')
%         continue;   % If there is a output file then just skip this timestamp
%     end
%
%     logger.debug(loggerID,['Start TSI3 filling for ts = ' datestr(tmdv)]);
%     set_dn.add(tmdn);
%     if bMulticore
%         %realtime_mtsipipe_TSI3fill(dataDir,tmdv,'./model_rbr.mat',TSI3filledDir,'mtsi_startup','./HistRef_recheck','skyfill.bmp');
%         cmmd=sprintf('realtime_mtsipipe_TSI3fill(''%s'',%s,''%s'',''%s'',''%s'',''%s'',''%s'');',...
%             dataDir,sprintf('[%d %d %d %d %d %d]',tmdv(1),tmdv(2),tmdv(3),tmdv(4),tmdv(5),tmdv(6)),...
%             SVMCloudModelFile,TSI3filledDir,configfpath,HRefDir,TSIDefaultSkyColorFile);
%         logger.debug(loggerID,cmmd);
%         subprocess_matlab(cmmd,maxmatlabinst,servertype);
%         pause(0.5);
%     else
%         %         if tmdn<datenum([2014 04 24 4 0 0])
%         %             continue;
%         %         end
%         realtime_mtsipipe_TSI3fill(tmdv,SVMCloudModelFile,TSI3filledDir,HRefDir,TSIDefaultSkyColorFile,configfpath,logger);
%     end
% end

end


