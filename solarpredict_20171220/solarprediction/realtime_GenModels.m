%   This function is to generate SVR models based on training data
function realtime_GenModels(timerange,cldmodelmatf,configfpath,FilledTSI3Dir,outputDir)
run(configfpath);   %mtsi_startup;
%set(0,'DefaultFigureVisible', 'off');
assert((exist(cldmodelmatf,'file')~=0),'Cloud Model mat file doen''s exist!');
outputDir=FormatDirName(outputDir);
if ~exist(outputDir,'dir')
    mkdir(outputDir);
end

BPSCSMATFILE='./bpscs.mat';

%outputf=sprintf('%s%s.mat',outputDir,filename);
% if exist(outputf,'file')
%     return;
% end

ONEDAY=60*24;   % in minutes
BPS_N=25;       % BPS number
ONEMIN_DN=datenum([0 0 0 0 1 0]);
ONESEC_DN = datenum([0 0 0 0 0 1]);
MAXRAD=950;     MINRAD=0;
TFORMAT='yyyy-mm-dd HH:MM:SS';
FFORMAT='yyyy-mm-dd_HH-MM-SS';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Logger setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
logfile='./log/realtime_genmodels.log';
logger=log4m.getLogger(logfile);
loggerID='realtime_mtsipipe_GenDataset';
logger.setLogLevel(logger.DEBUG);
logger.setCommandWindowLevel(logger.ERROR);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% Read SVM Model file for CLD fraction
t=load(cldmodelmatf);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
model=t.model;
x1_min=t.x1_min;
x1_max=t.x1_max;
x2_min=t.x2_min;
x2_max=t.x2_max;
x3_min=t.x3_min;
x3_max=t.x3_max;
x4_min=t.x4_min;
x4_max=t.x4_max;
h=t.h;
w_1=model.SVs'*model.sv_coef;b_1=-model.rho;
% predicted_labels=svmpredict_li(x_valid,w_1,b_1);
%     predicted_labels(predicted_labels==1)=0;        % cld
%     predicted_labels(predicted_labels==-1)=1;       % sky
x_min=[x1_min x2_min x3_min x4_min];
x_max=[x1_max x2_max x3_max x4_max];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

timeran=timerange;
tl=length(timeran);
meanwinsize=7;
%cldpixelwinsize=9;
meanwinsize_half=floor(meanwinsize/2);


meanwinsize2=9; % larger window
meanwinsize_half2=floor(meanwinsize2/2);

%% Generate training dataset
FilledTSI3Dir=FormatDirName(FilledTSI3Dir);
allfiles=dir(FilledTSI3Dir);
[nfiles,~]=size(allfiles);
filedns=[];
for i = 1:nfiles
    filename3=allfiles(i).name;
    tmdn=datenum(GetDVFromImg(filename3));
    if tmdn<datenum([2014 04 09 17 0 0]) || tmdn>datenum([2014 04 09 20 0 0])
        continue;
    end
    filedns=[filedns;tmdn];
end
filedns = unique(filedns);
NRECS=length(filedns);
NFEATURES=23;
xtrain_norm=zeros(tl,NRECS*BPS_N,NFEATURES);
ytrain_norm=zeros(tl,NRECS*BPS_N,1);
lastdaydn = 0;  % last time daydn used for extraction clear sky values

%if ~exist('genmodels.mat','file')
if 1

for i = 1:NRECS
    filedn=filedns(i);
    filedv=datevec(filedn);
    disp(filedv);
    filename3=GetImgFromDV_BNL(filedv,TSI3);
    imgf=sprintf('%s%s_shifttest.jpg',FilledTSI3Dir,filename3);
    parasf=sprintf('%s%s_fillparas.mat',FilledTSI3Dir,filename3);
    imma3=imread(imgf);
    xtrain3=tmp_img2traindata_rbr(imma3,h,x_min,x_max);
    pl3=svmpredict_li(xtrain3,w_1,b_1);
    pl3(pl3==1)=0;        % cld
    pl3(pl3==-1)=1;       % sky
    imma3_cldma=false(UNDISTH,UNDISTW);
    imma3_cldma(:)=~pl3;
    nTotal=UNDISTH*UNDISTW;
    nCld=length(find(imma3_cldma));
    od_cf=nCld./nTotal;
    daydv=filedv; daydv(4:6)=0; daydn=datenum(daydv);
    if lastdaydn ~= daydn
        csvals_oneday = TO_getBPSCSVal(filedv,BPSCSMATFILE,'sec');
        outputf=sprintf('%s%s_%s.mat',outputDir,datestr(daydv,FFORMAT),datestr(daydv+[0 0 1 0 0 0],FFORMAT));
        if ~exist(outputf,'file')
            realradvals=TO_getBPSRad(daydv,daydv+[0 0 1 0 0 0]);
            save(outputf,'realradvals');
        else
            t=load(outputf);
            realradvals=t.realradvals;
        end
        lastdaydn=daydn;
    end
    
    % Get normalized radiation features
    tmdv_1min=datevec(filedn-ONEMIN_DN); tmdn_1min =datenum(tmdv_1min);
    
    %[realrad,realdns]=realtime_GetRad_debug(filedv);
    %[realrad_1min,~]=realtime_GetRad_debug(tmdv_1min);
    ind=round((filedn-daydn)/ONESEC_DN)+1;
    MAXRAD=csvals_oneday(ind);
    realrad=realradvals(ind,:);
    realrad_norm=realrad./MAXRAD;
    realrad_norm(realrad_norm>1) = 1;
    realrad_norm(realrad_norm<0) = 0;
    ind=ind-60; % 1 minute ahead
    MAXRAD=csvals_oneday(ind);
    realrad_1min = realradvals(ind,:);
    realrad_1min_norm=realrad_1min./MAXRAD;
    realrad_1min_norm(realrad_1min_norm>1) = 1;
    realrad_1min_norm(realrad_1min_norm<0) = 0;
    
    % Get HRef and MV
    t=load(parasf);
    HRef=t.HRef;    MVRef=t.MVRef; %[mvx1 mvy1;mvx2 mvy2]
    H=HRef(1);
    logger.debug(loggerID,sprintf('HRef primary= %d, mv primary=[%d,%d]',H,MVRef(1,1),MVRef(1,2)));
    
    
    for ti=1:tl
        timeahead=timeran(ti);
        timeahead_dn=datenum([0 0 0 0 0 timeahead]);
        preddn=filedn+timeahead_dn;
        preddv=datevec(preddn);
        %[groundrads,realdns]=realtime_GetRad_debug(preddv);  % debug is used here to extract radiation from solar-db instead of solar
        ind=round((preddn-daydn)/ONESEC_DN)+1;
        MAXRAD=csvals_oneday(ind);
        groundrads = realradvals(ind,:);
        groundrads_norm=groundrads./MAXRAD;
        groundrads_norm(groundrads_norm>1) = 1;
        groundrads_norm(groundrads_norm<0) = 0;
        
        %         if ~isempty(find(realdns-preddn>=ONEMIN_DN,1))
        %             assert(false,'Large range of datenumber!');
        %         end
        %         pred_unix=(preddn-datenum(1970,1,1))*86400;
        %         pred_est=pred_unix-5*3600;
        
        od_r=zeros(BPS_N,3); %min,max,mean
        od_g=zeros(BPS_N,3);
        od_b=zeros(BPS_N,3);
        od_cr=zeros(BPS_N,3);
        od_cg=zeros(BPS_N,3);
        od_cb=zeros(BPS_N,3);
        
        movement_x=MVRef(1,1)*round(timeahead/10);
        movement_y=MVRef(1,2)*round(timeahead/10);
        [x,y]=GetBPSPts(preddv,TSI3,H);           % SP(t) information
        [x_r,y_r]=GetBPSPts(filedv,TSI3,H);           % OneMIN ahead of SP(t-1) information
        x_pred = x-movement_x; % trace back with motion vectors
        y_pred = y-movement_y;
        x_pred=round(x_pred);  y_pred=round(y_pred);
        x_r=round(x_r);   y_r=round(y_r);
        od_crad=realrad_norm;
        od_arad=realrad_1min_norm;
        
        for j=1:BPS_N
            station=j;
            %             if station==19
            %                 station
            %             end
            if y_pred(j)-meanwinsize_half2<1 || y_pred(j)+meanwinsize_half2>UNDISTH ||...
                    x_pred(j)-meanwinsize_half2<1 || x_pred(j)+meanwinsize_half2>UNDISTW
                continue;
            end
            if y_r(j)-meanwinsize_half2<1 || y_r(j)+meanwinsize_half2>UNDISTH ||...
                    x_r(j)-meanwinsize_half2<1 || x_r(j)+meanwinsize_half2>UNDISTW
                continue;
            end
            sj=y_pred(j)-meanwinsize_half;   ej=y_pred(j)+meanwinsize_half;
            si=x_pred(j)-meanwinsize_half;   ei=x_pred(j)+meanwinsize_half;
            rvals=imma3(sj:ej,si:ei,1);
            gvals=imma3(sj:ej,si:ei,2);
            bvals=imma3(sj:ej,si:ei,3);
            od_r(j,3)=round(mean(double(rvals(:))));
            od_g(j,3)=round(mean(double(gvals(:))));
            od_b(j,3)=round(mean(double(bvals(:))));
            od_r(j,1)=round(min(double(rvals(:))));
            od_g(j,1)=round(min(double(gvals(:))));
            od_b(j,1)=round(min(double(bvals(:))));
            od_r(j,2)=round(max(double(rvals(:))));
            od_g(j,2)=round(max(double(gvals(:))));
            od_b(j,2)=round(max(double(bvals(:))));
            
            sj=y_r(j)-meanwinsize_half;   ej=y_r(j)+meanwinsize_half;
            si=x_r(j)-meanwinsize_half;   ei=x_r(j)+meanwinsize_half;
            rvals=imma3(sj:ej,si:ei,1);
            gvals=imma3(sj:ej,si:ei,2);
            bvals=imma3(sj:ej,si:ei,3);
            od_cr(j,3)=round(mean(double(rvals(:))));
            od_cg(j,3)=round(mean(double(gvals(:))));
            od_cb(j,3)=round(mean(double(bvals(:))));
            od_cr(j,1)=round(min(double(rvals(:))));
            od_cg(j,1)=round(min(double(gvals(:))));
            od_cb(j,1)=round(min(double(bvals(:))));
            od_cr(j,2)=round(max(double(rvals(:))));
            od_cg(j,2)=round(max(double(gvals(:))));
            od_cb(j,2)=round(max(double(bvals(:))));
            od_r=od_r./255;
            od_g=od_g./255;
            od_b=od_b./255;
            od_cr=od_cr./255;
            od_cg=od_cg./255;
            od_cb=od_cb./255;
            
            % Training data
            r_m=od_r(station,3);
            g_m=od_g(station,3);
            b_m=od_b(station,3);
            r_min=od_r(station,1);
            g_min=od_g(station,1);
            b_min=od_b(station,1);
            r_max=od_r(station,2);
            g_max=od_g(station,2);
            b_max=od_b(station,2);
            cr_m=od_cr(station,3);
            cg_m=od_cg(station,3);
            cb_m=od_cb(station,3);
            cr_min=od_cr(station,1);
            cg_min=od_cg(station,1);
            cb_min=od_cb(station,1);
            cr_max=od_cr(station,2);
            cg_max=od_cg(station,2);
            cb_max=od_cb(station,2);
            arad = od_arad(station);
            crad=od_crad(station);
            rbr=r_m/b_m;
            if(isnan(rbr))
                rbr=0;
            else
                if(rbr==inf||rbr>1.5)
                    rbr=1.5;
                else
                    rbr=rbr/1.5;
                end
            end
            crbr=cr_m/cb_m;
            if(isnan(crbr))
                crbr=0;
            else
                if(crbr==inf||crbr>1.5)
                    crbr=1.5;
                else
                    crbr=crbr/1.5;
                end
            end
            cldf=od_cf;
            xtrain_norm(ti,(i-1)*BPS_N+station,:)=[ r_m r_min r_max g_m g_min g_max b_m b_min b_max ...
                cr_m cr_min cr_max cg_m cg_min cg_max cb_m cb_min cb_max ...
                crbr rbr cldf arad crad];
            ytrain_norm(ti,(i-1)*BPS_N+station,1)=groundrads_norm(station);
        end
    end
end
    save('genmodels.mat');
else
    load('genmodels.mat');
end
%% Training models
% xtrain_tm=zeros(NRECS*BPS_N,NFEATURES);
% ytrain_tm=zeros(NRECS*BPS_N,1);
nfolds=5;
c_rbf=0.5;
g_rbf=-0.5;
ep_rbf=0.021;
c_li=6;
ep_li=0.07;
opt1='-s 3 -t 0';
opt2='-s 3 -t 2';
svmoptions_li=sprintf('%s -c %d -p %f -h 0',opt1,2^c_li,ep_li);
svmoptions_rbf=sprintf('%s -c %d -g %f -p %f -h 0',opt2,10^c_rbf,10^g_rbf,ep_rbf);
models_li=cell(tl,nfolds);
models_rbf=cell(tl,nfolds);
models_a=cell(tl,1);
models_rbr=cell(tl,1);
rbr_ind=20;
prerbr_ind=19;

xtrain_tm=zeros(NRECS,NFEATURES);
ytrain_tm=zeros(NRECS,1);


MAE_svrli=zeros(tl,nfolds);
RMSE_svrli=zeros(tl,nfolds);
MAE_svrrbf=zeros(tl,nfolds);
RMSE_svrrbf=zeros(tl,nfolds);
MAE_alr=zeros(tl,nfolds);
RMSE_alr=zeros(tl,nfolds);
MAE_rbr=zeros(tl,nfolds);
RMSE_rbr=zeros(tl,nfolds);
MAE_shift=zeros(tl,nfolds);
RMSE_shift=zeros(tl,nfolds);
DataCounts=zeros(tl,3);

station=21;

for ti = 1:tl
    xtrain_tm=zeros(NRECS,NFEATURES);
    ytrain_tm=zeros(NRECS,1);
    xtrain_tm(:,:) = xtrain_norm(ti,station:25:end,:);
    ytrain_tm(:,:) = ytrain_norm(ti,station:25:end,:);
    invalidma=markInvalid(xtrain_tm,'nan+0');
    if ~isempty(find(invalidma,1))
        disp('[WARNING]: There is invalid value in training dataset, remove it');
        tmrecinvalid=false(size(xtrain_tm,1),1);
        for atti=1:size(xtrain_tm,2)
            tmrecinvalid=tmrecinvalid|invalidma(:,atti);
        end
    else
        tmrecinvalid=false(size(xtrain_tm,1),1);
    end
    ytrain_valid=ytrain_tm(~tmrecinvalid);
    xtrain_valid=xtrain_tm(~tmrecinvalid,:);
    if isempty(ytrain_valid)
        disp(['[WARNING]: No valid records for timerange = ' int2str(timerange(ti))]);
        continue;
    end
    [xtrain,ytrain,xtest,ytest]=cvGenFolds(xtrain_valid,ytrain_valid,nfolds);
    DataCounts(ti,1)=length(ytrain_valid);
    DataCounts(ti,2)=length(ytrain{1});
    DataCounts(ti,3)=length(ytest{1});
    tmli_a=[];tmrbr_a=[];
    
    alr_a_folds=[];
    rbr_a_folds=[];
    for fi=1:nfolds
        tmytrain=ytrain{fi};    tmytest = ytest{fi};
        tmxtrain=xtrain{fi};    tmxtest = xtest{fi};
        
        model_li=svmtrain(tmytrain,tmxtrain,svmoptions_li);
        model_rbf=svmtrain(tmytrain,tmxtrain,svmoptions_rbf);
        assert(~isnan(model_li.rho));
        [predicted_label, accuracy, y1]=svmpredict(tmytest,tmxtest,model_li);
        [predicted_label, accuracy, y2]=svmpredict(tmytest,tmxtest,model_rbf);
        models_li{ti,fi}=model_li;
        models_rbf{ti,fi}=model_rbf;
        y1(y1>1)=1; y1(y1<0)=0;
        y2(y2>1)=1; y2(y2<0)=0;
        MAE_svrli(ti,fi)=mean(abs(y1-tmytest));
        RMSE_svrli(ti,fi)=sqrt(mean((y1-tmytest).^2));
        MAE_svrrbf(ti,fi)=mean(abs(y2-tmytest));
        RMSE_svrrbf(ti,fi)=sqrt(mean((y2-tmytest).^2));
        
        tmlix=[ones(size(tmxtrain,1),1) tmxtrain];
        tmliy=tmytrain;
        tm_alr=tmlix\tmliy;
        pred_y=[ones(size(tmxtest,1),1) tmxtest]*tm_alr;
        MAE_alr(ti,fi)=mean(abs(pred_y-tmytest));
        RMSE_alr(ti,fi)=sqrt(mean((pred_y-tmytest).^2));
        tm_alr=tm_alr';
        alr_a_folds=[alr_a_folds;tm_alr];
        tmrbrx=tmxtrain(:,rbr_ind)-tmxtrain(:,prerbr_ind); %rbr-prerbr
        tmrbrx_test=tmxtest(:,rbr_ind)-tmxtest(:,prerbr_ind); %rbr-prerbr
        tmrbry=tmytrain-tmxtrain(:,end);    %rad - prerad
        tmrbry_test=tmytest-tmxtest(:,end);    %rad - prerad
        tmrbr_a=tmrbrx\tmrbry;  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
        pred_y=tmrbrx_test*tmrbr_a;
        pred_y=pred_y+tmxtest(:,end);       %predy = predy + prerad
        MAE_rbr(ti,fi)=mean(abs(pred_y-tmytest));
        RMSE_rbr(ti,fi)=sqrt(mean((pred_y-tmytest).^2));
        rbr_a_folds=[rbr_a_folds;tmrbr_a];
        
        pred_y=tmxtest(:,end);
        MAE_shift(ti,fi)=mean(abs(pred_y-tmytest));
        RMSE_shift(ti,fi)=sqrt(mean((pred_y-tmytest).^2));
    end
    a=mean(alr_a_folds);
    a1=mean(rbr_a_folds);
    models_a{ti}=a;
    models_rbr{ti}=a1;
    
%     % debug for BPS19 only
%     MAEs=zeros(1,5);
%     RMSEs=zeros(1,5);
%     MAEs(1) = mean(MAE_alr(:));
%     MAEs(2) = mean(MAE_rbr(:));
%     MAEs(3) = mean(MAE_svrli(i,:));
%     MAEs(4) = mean(MAE_svrrbf(i,:));
%     MAEs(5) = mean(MAE_shift);
%     MAE_all=[MAE_all;MAEs];
%     RMSEs(1) = mean(RMSE_alr(:));
%     RMSEs(2) = mean(RMSE_rbr(:));
%     RMSEs(3) = mean(RMSE_svrli(i,:));
%     RMSEs(4) = mean(RMSE_svrrbf(i,:));
%     RMSEs(5) = mean(RMSE_shift(:));   % we didn't add rshift here
%     RMSE_all=[RMSE_only19;RMSEs];
%     traincount_19=[traincount_19;size(ytrain{1},1)];
%     testcount_19=[testcount_19;size(ytest{1},1)];
end

MAE_test=[mean(MAE_alr,2) mean(MAE_rbr,2) mean(MAE_svrli,2) mean(MAE_svrrbf,2) mean(MAE_shift,2)];
RMSE_test=[mean(RMSE_alr,2) mean(RMSE_rbr,2) mean(RMSE_svrli,2) mean(RMSE_svrrbf,2) mean(RMSE_shift,2)];
save(sprintf('models%d.mat',station));




end