%Main function to run realtime mtsi preprocessing
%VERSION 1.0 2014-05-19
function realtime_preprocessingPipe_endless(configfpath)
if nargin==0
    configfpath='mtsi_startup';
end
run(configfpath);
TO_setRealtimeModuleStatus(TO_getmodulename(),TO_str2modstate('normal'),datevec(now),ModuleStatusRecFile);
if bWriteDiary
    diary(PreproDiaryFile);
end
if PreproMatPoolSize>1
    bUseParfor=true;
    if matlabpool('size')~=0
        matlabpool('close');
    end
    matlabpool(PreproMatPoolSize);
else
    bUseParfor=false;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Logger setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
logfile=PreproLogFile;
logger=log4m.getLogger(logfile);
loggerID='realtime_preprocessing';
logger.setLogLevel(logger.DEBUG);
logger.setCommandWindowLevel(logger.DEBUG);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%otsi1Dir='./data/oTSI1';
%fliptsi1Dir='./data/filpTSI1';
%utsi1Dir='./data/tsi1_undist_mat';
%otsi2Dir='./data/oTSI2';
%fliptsi2Dir='./data/filpTSI2';
%utsi2Dir='./data/tsi2_undist_mat';
%otsi3Dir='./data/oTSI3';
%fliptsi3Dir='./data/filpTSI3';
%utsi3Dir='./data/tsi3_undist_mat';
%TSIRootDir='./data';
%MASKRootDir='.';
%bMulticore=false;   % KEEP false. Switch to use subprocess lib (obsolete)
%servertype=64;
%maxmatlabinst=26;

MASKRootDir=SBHAMASKROOTDIR;
set_dn=java.util.HashSet;
ONEHOUR_DN=datenum([0 0 0 1 0 0]);

try
    while(1)
        files1=dir(otsi1Dir);
        n1=size(files1,1);
        tmdns=[];
        for i=1:n1
            tmdv=GetDVFromImg(files1(i).name);
            if (tmdv==0)
                continue;
            end
            tmdn=datenum(tmdv);
            if(now-tmdn<ONEHOUR_DN*2 && set_dn.contains(tmdn))
                continue; % searched image is in the recent preprocessed list
            end
            f2=GetImgFromDV_BNL(tmdv,TSI2);
            f3=GetImgFromDV_BNL(tmdv,TSI3);
            f2=sprintf('%s%s',otsi2Dir,f2);
            f3=sprintf('%s%s',otsi3Dir,f3);
            if ~exist(f2,'file')||~exist(f3,'file')
                continue;   % TSI2 and TSI3 images are not ready
            end
            set_dn.add(tmdn);   % add ito set to make sure of no replica
            tmdns=[tmdns tmdn];
        end
        
        N = length(tmdns);
        if bUseParfor || N>30
            parfor i=1:N
                tmdn=tmdns(i);
                tmdv=datevec(tmdn);
                disp(['Preprocess the image with timestamp = ' datestr(tmdv)]);
                realtime_preprocessingPipe2(tmdv,MASKRootDir,configfpath,logger);
            end
            if ~bUseParfor
                matlabpool('close');
            end
        else
            for i=1:N
                tmdn=tmdns(i);
                tmdv=datevec(tmdn);
                disp(['Preprocess the image with timestamp = ' datestr(tmdv)]);
                realtime_preprocessingPipe2(tmdv,MASKRootDir,configfpath,logger);
            end
        end
        pause(1);
    end
catch err
    %logger.error(loggerID,err.message);
    logger.error(loggerID,sprintf('%s--line %d  msg:%s',err.stack(1).name,err.stack(1).line,err.message));
    if bWriteDiary
        diary off;
    end
    TO_setRealtimeModuleStatus(TO_getmodulename(),TO_str2modstate('error'),datevec(now),ModuleStatusRecFile);
    return;
end

end
