%Main function to run quality control for realtime pipeline
%VERSION 1.0 2014-05-19
function realtime_bpssec_qc_endless(configfpath)
if nargin==0
    configfpath='mtsi_startup';
end
run(configfpath);
TO_setRealtimeModuleStatus(TO_getmodulename(),TO_str2modstate('normal'),datevec(now),ModuleStatusRecFile);
if bWriteDiary
    diary(GendataDiaryFile);
end
%realtime_bpssec_qc_endless
lastts=0;
WAITTIME=QualityControlInterval; % in seconds. Default is 300s
WAITTIME_dn=datenum([0 0 0 0 0 WAITTIME]);
%BPSRadiationQualityControlFile='./bpsvalid.dat';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Logger setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
logfile=QualityControlLogFile;
logger=log4m.getLogger(logfile);
loggerID='realtime_qc';
logger.setLogLevel(logger.DEBUG);
logger.setCommandWindowLevel(logger.DEBUG);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BPSN=BPS_N;

try
    while(1)
        utc_now=java.lang.System.currentTimeMillis/1000;
        tmdn=utc_now/86400+datenum(1970,1,1);
        if(tmdn-lastts)>WAITTIME_dn
            tmdv=datevec(tmdn);
            logger.debug(loggerID,['start the qc at time: ' datestr(tmdv,'yyyy-mm-dd HH:MM:SS')]);
            bStatus=realtime_bpssec_qc(QCRADFILE,BPSRadiationQualityControlFile,tmdv);
            while(bStatus==-1)
                logger.debug(loggerID,['Error in reading BPSSEC.dat file at time: ' datestr(tmdv,'yyyy-mm-dd HH:MM:SS')]);
                bStatus=realtime_bpssec_qc(QCRADFILE,BPSRadiationQualityControlFile,tmdv);
                pause(1);    % wait for another 1
            end
            [qcstatus,~]=realtime_qcvalid(tmdv,BPSRadiationQualityControlFile);
            lastts=tmdn;
            tmline='';
            for i=1:BPSN
                tmline=[tmline 'BPS' int2str(i) ': ' int2str(qcstatus(i)) ' '];
            end
            logger.debug(loggerID,['Current status of stations --- ' tmline]);
        else
            pause(10);
        end
    end
catch err
    logger.error(loggerID,sprintf('%s--line %d  msg:%s',err.stack(1).name,err.stack(1).line,err.message));
    if bWriteDiary
        diary off;
    end
    TO_setRealtimeModuleStatus(TO_getmodulename(),TO_str2modstate('error'),datevec(now),ModuleStatusRecFile);
    return;
end
end