function [qcstatus,mintmdn] =realtime_qcvalid(realtimedv,radvalidfile)
TFORMAT='yyyy-mm-dd HH:MM:SS';
BPSN=25;
realtimedn=datenum(realtimedv);
qcstatus=zeros(1,BPSN);
mintmdn=0;

fh=fopen(radvalidfile);
C_all = textscan(fh,'%s %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d');
fclose(fh);
nlines = length(C_all{1});
tstrs=[cell2mat(C_all{1}) repmat(' ',nlines,1) cell2mat(C_all{2})];
status = cell2mat(C_all(3:end));
tmdns=datenum(tstrs,TFORMAT);
if isempty(tmdns) == 1
    disp('WARNING: No quality information is available!');
    return;
end
diffdns=abs(tmdns-realtimedn);
[mindiff,mini]=min(diffdns);
qcstatus(:)=status(mini,:);
mintmdn=tmdns(mini);

% while ischar(tline)
%     C=strsplit(tline);
%     tline = fgets(fid);
%     dstr=C{1};
%     timestr=C{2};
%     tstr=[dstr ' ' timestr];
%     tmdv=datevec(tstr,TFORMAT);
%     tmdn=datenum(tmdv);
%     if(abs(tmdn-realtimedn)>=mindiff)
%         continue;
%     end
%     mindiff=abs(tmdn-realtimedn);
%     mindn=tmdn;
%     for i=1:BPSN
%         tmstr=C{2+i};
%         qcstatus(i)=str2num(tmstr);
%     end
% end
% fclose(fid);
% mintmdn=mindn;


end