function script_PIPE_UndistortionwithCSL_BNL_multicore(startdv,enddv,DataDirectory,oriimgDir,undistimgDir,mode)
% mode = 'csl + undist + azmat'
load('UndistortionParameters.mat');     % load ALL the related parameters
% load('undist2ori.mat');             % matrix that contains all undist->ori mapping
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STARTUP to get all possible parameter settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
startup;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load(tsi1undistmat);        % tsi1undistmat='tsi1_ori2undist.mat';

sbazmatf='sbazimuth.mat';
haazmatf='haazimuth.mat';

% DataDirectory
% if(DataDirectory(end)~='/')
%     DataDirectory=sprintf('%s%s',DataDirectory,'/');
% end
DataDirectory=FormatDirName(DataDirectory);
%   Data/input
%   Data/output
%   Data/sbmask
%   Data/hamask
oriimgDir=FormatDirName(oriimgDir);
inputimgDir=oriimgDir;
outputimgDir=sprintf('%soutput/',DataDirectory);
sbmaskDir=sprintf('%ssbmask/',DataDirectory);
hamaskDir=sprintf('%shamask/',DataDirectory);
sbmaskDir=GetAbspathFromFilename(sbmaskDir);
hamaskDir=GetAbspathFromFilename(hamaskDir);
inputimgDir=FormatDirName(inputimgDir);
undistoutDir=undistimgDir;
undistoutDir=FormatDirName(undistoutDir);
ONEDAY_DN=datenum([0 0 1 0 0 0]);
matlabinst=20;  %matlab inst number

%   1. Build Clear Sky Lib for SB masks
allfiles=dir(oriimgDir);
[nfiles,~]=size(allfiles);
PERIOD_CSL_D=120;
PERIOD_CSL = datenum([0 0 0 0 0 PERIOD_CSL_D]);
startdn=datenum(startdv);
enddn=datenum(enddv);

oriimgdirname=GetFilenameFromAbsPath(oriimgDir);
avail_f=sprintf('avail_%s.mat',oriimgdirname);
if ~exist(avail_f,'file')
    tmdns=zeros(nfiles,1);
    for i =1:nfiles
        if allfiles(i).isdir==1
            continue;
        end
        filename=allfiles(i).name;
        tmdv=GetDVFromImg(filename);
        tmdn=datenum(tmdv);
        tmdns(i)=tmdn;
    end
    save(avail_f,'tmdns');
end
t=load(avail_f);
tmdns=t.tmdns;
tml=length(tmdns);
if tml~=nfiles
    tmdns=zeros(nfiles,1);
    for i =1:nfiles
        if allfiles(i).isdir==1
            continue;
        end
        filename=allfiles(i).name;
        tmdv=GetDVFromImg(filename);
        tmdn=datenum(tmdv);
        tmdns(i)=tmdn;
    end
    save(avail_f,'tmdns');
end

% intersection of chosen period
maxdn=max(tmdns);
mindn=min(tmdns);
startdn=max([startdn mindn]);
enddn=min([enddn maxdn]);

odstartdv=datevec(startdn);
odstartdv(4:6)=0;
odstartdn=datenum(odstartdv);
datevectors=[
    [2012 07 21 0 0 0],
    [2012 07 27 0 0 0],
    [2012 07 30 0 0 0],
    [2012 08 05 0 0 0],
    [2012 08 09 0 0 0],
    [2012 08 26 0 0 0],
    [2012 08 28 0 0 0],
    [2012 09 16 0 0 0],
    [2012 09 24 0 0 0]];
daydns=datenum(datevectors);

if ~isempty(strfind(mode, 'csl'))
    while odstartdn<enddn
        odenddn=odstartdn+ONEDAY_DN;
        if isempty(find(daydns==odstartdn,1))
            odstartdn=odenddn;
            continue;
        end
        BuildClearSkyLib_wBMPMask(oriimgDir,hamaskDir,sbmaskDir,odstartdn,odenddn,PERIOD_CSL_D);
        cmmd=sprintf('BuildClearSkyLib_wBMPMask(''%s'',''%s'',''%s'',%f,%f,%d)',...
            oriimgDir,hamaskDir,sbmaskDir,odstartdn,odenddn,PERIOD_CSL_D);
        disp(cmmd);
        disp(datestr(odstartdn));
        %subprocess_matlab(cmmd,matlabinst);
        odstartdn=odenddn;
    end
end



% for i =1:nfiles
%     if rem(i,10000)==0
%         disp(i)
%     end
%     tmdn=tmdns(i);
%     if tmdn == 0    % invalid tmdn skip
%         continue;
%     end
%     if tmdn<startdn || tmdn>=enddn  % not in dv range
%         continue;
%     end
%     if tmdn-predn<=PERIOD_CSL  % not in CSL range
%         continue;
%     end
%     predn=tmdn;
%     tmdv=datevec(tmdn);
%     if tmdv(4)<10  % UTC 10 AM= EST 5 AM
%         continue;
%     end
%     filename=allfiles(i).name;
%     inputf=sprintf('%s%s',oriimgDir,filename);
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     %   Skip the sbmask that exists
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     outfma=sprintf('%s%s.mat',sbmaskDir,filename);
%     if exist(outfma,'file')
%         continue;
%     end
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     %     disp(filename);
%     BuildClearSkyLib_singlefile(inputf,hamaskDir,sbmaskDir,startdn,enddn,PERIOD_CSL_D);
%     cmmd=sprintf('BuildClearSkyLib(''%s'',''%s'',''%s'',%f,%f,%d)',...
%         oriimgDir,hamaskDir,sbmaskDir,startdn,enddn,PERIOD_CSL);
%     %disp(cmmd);
% %     subprocess_matlab(cmmd,matlabinst);
% end

% intersection of chosen period
% startdn=max([startdn mindn]);
% enddn=min([enddn maxdn]);
% startdv=datevec(startdn);
% enddv=datevec(enddn);

%   Re-gen the sb and ha azimuth list
if ~isempty(strfind(mode, 'azmat'))
    GenAvailAzMat_SBHA(hamaskDir,sbmaskDir,haazmatf,sbazmatf);
end
% sbazimuth=load(sbazmatf);                % load all sbmask timenumbers and azumith
% haazimuth=load(haazmatf);                % load all hamask timenumbers and azumith


starttimevec=startdv;
endtimevec=enddv;

disp(SITE);
if(exist(inputimgDir,'dir')==0)
    disp('input data file does not exist!');
end
if(exist(sbmaskDir,'dir')==0)
    disp('WARNING: sbmask does not exist');
end
if(exist(hamaskDir,'dir')==0)
    disp('WARNING: hamask does not exist');
end
if(exist('UndistortionParameters.mat','file')==0)
    disp('ERROR: UndistortionParameters.mat doesnot exist!');
end
if(exist('undist2ori.mat','file')==0)
    disp('ERROR: undist2ori.mat doesnot exist!');
end

if ~exist(undistoutDir,'dir')
    mkdir(undistoutDir);
end



%   2. Generate undistortion images with splitting into days
datevectors=[
    [2012 07 21 0 0 0],
    [2012 07 27 0 0 0],
    [2012 07 30 0 0 0],
    [2012 08 05 0 0 0],
    [2012 08 09 0 0 0],
    [2012 08 26 0 0 0],
    [2012 08 28 0 0 0],
    [2012 09 16 0 0 0],
    [2012 09 24 0 0 0]];

daydns=datenum(datevectors);

odstartdn=startdn;
if ~isempty(strfind(mode, 'undist'))
    while odstartdn<enddn
        odstartdv=datevec(odstartdn);
        odenddn=odstartdn+ONEDAY_DN;
        if isempty(find(daydns==odstartdn,1))
            odstartdn=odenddn;
            continue;
        end
        
        odenddv=datevec(odenddn);
        odstartdv(4)=11;
        odenddv=odstartdv;
        odenddv(4)=22;
        startstr=datestr(odstartdv,'[yyyy mm dd HH MM SS]');
        endstr=datestr(odenddv,'[yyyy mm dd HH MM SS]');
        PIPE_UndistortionwithCSL_BNL(odstartdv,odenddv,DataDirectory,oriimgDir,undistimgDir);
        cmmd=sprintf('PIPE_UndistortionwithCSL_BNL(%s,%s,''%s'',''%s'',''%s'')',...
            startstr,endstr,DataDirectory,oriimgDir,undistimgDir);
        disp(cmmd);
        %subprocess_matlab(cmmd,matlabinst);
        odstartdn=odenddn;
    end
end

end