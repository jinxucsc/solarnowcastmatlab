% This function is designed to get realtime img std
%   Version 1.0 2014/04/14
%   Combined clouddetector with TO_GenTSISTD_all
function [imma_std1,imma_std2,imma_std3,imma_std_invalid1,imma_std_invalid2,imma_std_invalid3]...
    =realtime_getimgstd(realtimedv,StdBlockSize,dataDir)
stdstr=int2str(StdBlockSize);
tsi1Dir_std=[dataDir 'tsi1_undist_std_' stdstr '/'];
tsi1Dir=[dataDir 'tsi1_undist_mat/'];
tsi2Dir_std=[dataDir 'tsi2_undist_std_' stdstr '/'];
tsi2Dir=[dataDir 'tsi2_undist_mat/'];
tsi3Dir_std=[dataDir 'tsi3_undist_std_' stdstr '/'];
tsi3Dir=[dataDir 'tsi3_undist_mat/'];
StdBlockHalfSize=floor(StdBlockSize/2);

f1 = GetImgFromDV_BNL(realtimedv,'tsi1');
f2 = GetImgFromDV_BNL(realtimedv,'tsi2');
f3 = GetImgFromDV_BNL(realtimedv,'tsi3');
f1=f1(1:end-4); % remove tail
f2=f2(1:end-4); % remove tail
f3=f3(1:end-4); % remove tail
inputf1=sprintf('%s%s.bmp',tsi1Dir,f1);
outputf1=sprintf('%s%s.std.mat',tsi1Dir_std,f1);
inputf2=sprintf('%s%s.bmp',tsi2Dir,f2);
outputf2=sprintf('%s%s.std.mat',tsi2Dir_std,f2);
inputf3=sprintf('%s%s.bmp',tsi3Dir,f3);
outputf3=sprintf('%s%s.std.mat',tsi3Dir_std,f3);
if exist(outputf1,'file') && exist(outputf2,'file') && exist(outputf3,'file')
    t1=load(outputf1);
    t2=load(outputf2);
    t3=load(outputf3);
    imma_std1=t1.imma_std;
    imma_std2=t2.imma_std;
    imma_std3=t3.imma_std;
    imma_std_invalid1=t1.imma_std_invalid;
    imma_std_invalid2=t1.imma_std_invalid;
    imma_std_invalid3=t1.imma_std_invalid;
    return;
end

imma1=TO_imread(inputf1);
imma2=TO_imread(inputf2);
imma3=TO_imread(inputf3);
imma_lum1=single(TO_rgb2lum(imma1));
imma_lum2=single(TO_rgb2lum(imma2));
imma_lum3=single(TO_rgb2lum(imma3));
[imma_std1,imma_std_invalid1]=imgstd(double(imma_lum1),StdBlockHalfSize);
[imma_std2,imma_std_invalid2]=imgstd(double(imma_lum2),StdBlockHalfSize);
[imma_std3,imma_std_invalid3]=imgstd(double(imma_lum3),StdBlockHalfSize);

imma_std1=uint8(round(imma_std1));
imma_std2=uint8(round(imma_std2));
imma_std3=uint8(round(imma_std3));

tmdv=realtimedv;
imma_std=imma_std1;
imma_std_invalid=imma_std_invalid1;
originalf=inputf1;
save(outputf1,'StdBlockSize','StdBlockHalfSize','imma_std','tmdv','originalf','imma_std_invalid')

imma_std=imma_std2;
imma_std_invalid=imma_std_invalid2;
originalf=inputf2;
save(outputf2,'StdBlockSize','StdBlockHalfSize','imma_std','tmdv','originalf','imma_std_invalid')

imma_std=imma_std3;
imma_std_invalid=imma_std_invalid3;
originalf=inputf3;
save(outputf3,'StdBlockSize','StdBlockHalfSize','imma_std','tmdv','originalf','imma_std_invalid')


end
