function [H,indexi]=tmp_mvcc2H(cc_vec,H_vec,cldcount_vec,mv_notvalid_rec,ccThres)
Hs=unique(H_vec);
Hn=length(Hs);
lastHi=zeros(Hn,1);
for i=1:Hn
    lastHi(i)=find(H_vec==Hs(i),1,'last');
end
cc_vec(cc_vec<ccThres|mv_notvalid_rec)=0;
cldcount_vec(cc_vec<ccThres|mv_notvalid_rec)=0;
if isempty(find(cc_vec,1))
    H=0;
    indexi=0;
    return;
end
Hcount=zeros(Hn,1);
Hsumcc=zeros(Hn,1);
for i=1:Hn 
    if i==1
        tmvals=cc_vec(1:lastHi(i));
    else
        tmvals=cc_vec(lastHi(i-1)+1:lastHi(i));
    end
    Hcount(i)=length(find(tmvals>ccThres));
    Hsumcc(i)=sum(tmvals(tmvals>ccThres));
end

[~,groupi]=max(Hcount);
if length(find(Hcount==Hcount(groupi)))>1
    [~,groupi]=max(Hsumcc);
end
if groupi==1
    tmvals=cldcount_vec(1:lastHi(groupi));
else
    tmvals=cldcount_vec(lastHi(groupi-1)+1:lastHi(groupi));
end
maxcldcount_group=max(tmvals);
indexi=find(cldcount_vec==maxcldcount_group,1);
H=H_vec(indexi);


end