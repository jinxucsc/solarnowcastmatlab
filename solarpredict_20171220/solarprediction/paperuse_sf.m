%function paperuse_sf


% f1=figure();
% imshow(imma);
% hold on;
% for j=1:BPS_N
%     plot(x(j),y(j),'+b');
%     %text(x(j),y(j),['\color{black} ' 'BPS' int2str(j)],'FontSize',18);
% end
% hold off;
% set(f1,'position',[0,0,500,500]);
% set(gca,'position',[0 0 1 1],'units','normalized')
% print(f1,'sfloc1.bmp','-dbmp');

locs1=cell(BPS_N,1);
locs2=cell(BPS_N,1);
ll1=zeros(BPS_N,1);
ll1_f=zeros(BPS_N,1);
z1=zeros(BPS_N,1);
for i=1:BPS_N
    if i==15
        i
    end
    tmvals1=od_rad_norm(showstarti:showendi,i);
    tmvals2=od_r_n(showstarti:showendi,i);
    %[pks1,loc1]=findpeaks(1-tmvals1,'THRESHOLD',0.02);
    loc1=find(tmvals1<0.82);
    loc2=find(tmvals2>0.65);
    [pks2,loc2_back]=findpeaks(tmvals2);
    loc2=[loc2;loc2_back];
    loc2=unique(loc2);
    locs1{i}=loc1;
    locs2{i}=loc2;
    ll1(i)=length(loc1);
    z1(i)=min(normxcorr2(tmvals2,tmvals1));
end


locs1_found=cell(BPS_N,1);
z=zeros(BPS_N,1);
for i=1:BPS_N
    loc1=locs1{i};
    loc2=locs2{i};    
    tml=length(loc1);
    loc1_f=zeros(tml,1);
    for j=1:tml
       if ~isempty(find(loc2==loc1(j),1))
          loc1_f(j)=find(loc2==loc1(j),1);
       end
    end
    locs1_found{i}=loc1_f;
    ll1_f(i)=length(find(loc1_f>0));
    z(i)=length(find(loc1_f>0))/length(loc1_f);
end

f1=length(find(locs1_found{1}))./length(locs1_found{1});
f1
%plot(od_rad_norm(showstarti:showendi,i),'k');