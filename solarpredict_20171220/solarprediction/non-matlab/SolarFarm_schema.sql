-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: SolarFarm
-- ------------------------------------------------------
-- Server version	5.1.73-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `BPS_MIN`
--

DROP TABLE IF EXISTS `BPS_MIN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BPS_MIN` (
  `BPS_N` int(11) NOT NULL,
  `time_stamp` int(11) NOT NULL,
  `RECORD` int(11) NOT NULL,
  `BattV_Min` float DEFAULT NULL,
  `PTemp_Avg` float DEFAULT NULL,
  `SP2A_H_Avg` float DEFAULT NULL,
  `SP2A_P_Avg` float DEFAULT NULL,
  `SP2B_H_Avg` float DEFAULT NULL,
  `SP2B_P_Avg` float DEFAULT NULL,
  `AirTemp_Avg` float DEFAULT NULL,
  `PanelT_Avg` float DEFAULT NULL,
  `AirTC_Avg` float DEFAULT NULL,
  `RH_Avg` float DEFAULT NULL,
  `T_Aisle_Avg` float DEFAULT NULL,
  `T_Array_Avg` float DEFAULT NULL,
  PRIMARY KEY (`time_stamp`,`BPS_N`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `BPS_MIN_1`
--

DROP TABLE IF EXISTS `BPS_MIN_1`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_1`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_1` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_10`
--

DROP TABLE IF EXISTS `BPS_MIN_10`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_10`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_10` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_11`
--

DROP TABLE IF EXISTS `BPS_MIN_11`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_11`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_11` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_12`
--

DROP TABLE IF EXISTS `BPS_MIN_12`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_12`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_12` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_13`
--

DROP TABLE IF EXISTS `BPS_MIN_13`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_13`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_13` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_14`
--

DROP TABLE IF EXISTS `BPS_MIN_14`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_14`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_14` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_15`
--

DROP TABLE IF EXISTS `BPS_MIN_15`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_15`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_15` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_16`
--

DROP TABLE IF EXISTS `BPS_MIN_16`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_16`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_16` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_17`
--

DROP TABLE IF EXISTS `BPS_MIN_17`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_17`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_17` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_18`
--

DROP TABLE IF EXISTS `BPS_MIN_18`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_18`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_18` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_19`
--

DROP TABLE IF EXISTS `BPS_MIN_19`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_19`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_19` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_2`
--

DROP TABLE IF EXISTS `BPS_MIN_2`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_2` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_20`
--

DROP TABLE IF EXISTS `BPS_MIN_20`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_20`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_20` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_21`
--

DROP TABLE IF EXISTS `BPS_MIN_21`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_21`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_21` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_22`
--

DROP TABLE IF EXISTS `BPS_MIN_22`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_22`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_22` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_23`
--

DROP TABLE IF EXISTS `BPS_MIN_23`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_23`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_23` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_24`
--

DROP TABLE IF EXISTS `BPS_MIN_24`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_24`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_24` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_25`
--

DROP TABLE IF EXISTS `BPS_MIN_25`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_25`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_25` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_3`
--

DROP TABLE IF EXISTS `BPS_MIN_3`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_3`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_3` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_4`
--

DROP TABLE IF EXISTS `BPS_MIN_4`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_4`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_4` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_5`
--

DROP TABLE IF EXISTS `BPS_MIN_5`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_5`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_5` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_6`
--

DROP TABLE IF EXISTS `BPS_MIN_6`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_6`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_6` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_7`
--

DROP TABLE IF EXISTS `BPS_MIN_7`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_7`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_7` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_8`
--

DROP TABLE IF EXISTS `BPS_MIN_8`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_8`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_8` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_MIN_9`
--

DROP TABLE IF EXISTS `BPS_MIN_9`;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_9`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_MIN_9` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `BPS_MIN_tmpfs`
--

DROP TABLE IF EXISTS `BPS_MIN_tmpfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BPS_MIN_tmpfs` (
  `BPS_N` int(11) NOT NULL,
  `time_stamp` int(11) NOT NULL,
  `RECORD` int(11) NOT NULL,
  `BattV_Min` float DEFAULT NULL,
  `PTemp_Avg` float DEFAULT NULL,
  `SP2A_H_Avg` float DEFAULT NULL,
  `SP2A_P_Avg` float DEFAULT NULL,
  `SP2B_H_Avg` float DEFAULT NULL,
  `SP2B_P_Avg` float DEFAULT NULL,
  `AirTemp_Avg` float DEFAULT NULL,
  `PanelT_Avg` float DEFAULT NULL,
  `AirTC_Avg` float DEFAULT NULL,
  `RH_Avg` float DEFAULT NULL,
  `T_Aisle_Avg` float DEFAULT NULL,
  `T_Array_Avg` float DEFAULT NULL,
  PRIMARY KEY (`time_stamp`,`BPS_N`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='/mnt/solar/' INDEX DIRECTORY='/mnt/solar/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BPS_Prediction_full_tmpfs`
--

DROP TABLE IF EXISTS `BPS_Prediction_full_tmpfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BPS_Prediction_full_tmpfs` (
  `BPS_N` int(11) NOT NULL,
  `time_stamp_real` int(11) NOT NULL,
  `time_stamp_pred` int(11) NOT NULL,
  `SP2A_H_Avg(SVRLi)` float DEFAULT NULL,
  `SP2A_H_Avg(SVRRBF)` float DEFAULT NULL,
  PRIMARY KEY (`time_stamp_real`,`time_stamp_pred`,`BPS_N`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='/mnt/solar/' INDEX DIRECTORY='/mnt/solar/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BPS_Prediction_lc_tmpfs`
--

DROP TABLE IF EXISTS `BPS_Prediction_lc_tmpfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BPS_Prediction_lc_tmpfs` (
  `BPS_N` int(11) NOT NULL,
  `time_stamp` int(11) NOT NULL,
  `SP2A_H_Avg` float DEFAULT NULL,
  PRIMARY KEY (`time_stamp`,`BPS_N`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='/mnt/solar/' INDEX DIRECTORY='/mnt/solar/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BPS_Prediction_tmpfs`
--

DROP TABLE IF EXISTS `BPS_Prediction_tmpfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BPS_Prediction_tmpfs` (
  `BPS_N` int(11) NOT NULL,
  `time_stamp` int(11) NOT NULL,
  `SP2A_H_Avg` float DEFAULT NULL,
  PRIMARY KEY (`time_stamp`,`BPS_N`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='/mnt/solar/' INDEX DIRECTORY='/mnt/solar/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BPS_SEC`
--

DROP TABLE IF EXISTS `BPS_SEC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BPS_SEC` (
  `BPS_N` int(11) NOT NULL,
  `time_stamp` int(11) NOT NULL,
  `RECORD` int(11) NOT NULL,
  `BattV_Min` float DEFAULT NULL,
  `PTemp_Avg` float DEFAULT NULL,
  `SP2A_H_Avg` float DEFAULT NULL,
  `SP2A_P_Avg` float DEFAULT NULL,
  `SP2B_H_Avg` float DEFAULT NULL,
  `SP2B_P_Avg` float DEFAULT NULL,
  `AirTemp_Avg` float DEFAULT NULL,
  `PanelT_Avg` float DEFAULT NULL,
  `AirTC_Avg` float DEFAULT NULL,
  `RH_Avg` float DEFAULT NULL,
  `T_Aisle_Avg` float DEFAULT NULL,
  `T_Array_Avg` float DEFAULT NULL,
  PRIMARY KEY (`time_stamp`,`BPS_N`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `BPS_SEC_1`
--

DROP TABLE IF EXISTS `BPS_SEC_1`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_1`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_1` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_10`
--

DROP TABLE IF EXISTS `BPS_SEC_10`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_10`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_10` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_11`
--

DROP TABLE IF EXISTS `BPS_SEC_11`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_11`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_11` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_12`
--

DROP TABLE IF EXISTS `BPS_SEC_12`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_12`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_12` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_13`
--

DROP TABLE IF EXISTS `BPS_SEC_13`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_13`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_13` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_14`
--

DROP TABLE IF EXISTS `BPS_SEC_14`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_14`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_14` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_15`
--

DROP TABLE IF EXISTS `BPS_SEC_15`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_15`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_15` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_16`
--

DROP TABLE IF EXISTS `BPS_SEC_16`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_16`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_16` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_17`
--

DROP TABLE IF EXISTS `BPS_SEC_17`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_17`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_17` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_18`
--

DROP TABLE IF EXISTS `BPS_SEC_18`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_18`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_18` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_19`
--

DROP TABLE IF EXISTS `BPS_SEC_19`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_19`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_19` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_2`
--

DROP TABLE IF EXISTS `BPS_SEC_2`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_2` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_20`
--

DROP TABLE IF EXISTS `BPS_SEC_20`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_20`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_20` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_21`
--

DROP TABLE IF EXISTS `BPS_SEC_21`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_21`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_21` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_22`
--

DROP TABLE IF EXISTS `BPS_SEC_22`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_22`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_22` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_23`
--

DROP TABLE IF EXISTS `BPS_SEC_23`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_23`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_23` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_24`
--

DROP TABLE IF EXISTS `BPS_SEC_24`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_24`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_24` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_25`
--

DROP TABLE IF EXISTS `BPS_SEC_25`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_25`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_25` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_3`
--

DROP TABLE IF EXISTS `BPS_SEC_3`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_3`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_3` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_4`
--

DROP TABLE IF EXISTS `BPS_SEC_4`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_4`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_4` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_5`
--

DROP TABLE IF EXISTS `BPS_SEC_5`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_5`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_5` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_6`
--

DROP TABLE IF EXISTS `BPS_SEC_6`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_6`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_6` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_7`
--

DROP TABLE IF EXISTS `BPS_SEC_7`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_7`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_7` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_8`
--

DROP TABLE IF EXISTS `BPS_SEC_8`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_8`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_8` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `BPS_SEC_9`
--

DROP TABLE IF EXISTS `BPS_SEC_9`;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_9`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BPS_SEC_9` (
 `BPS_N` tinyint NOT NULL,
  `time_stamp` tinyint NOT NULL,
  `RECORD` tinyint NOT NULL,
  `BattV_Min` tinyint NOT NULL,
  `PTemp_Avg` tinyint NOT NULL,
  `SP2A_H_Avg` tinyint NOT NULL,
  `SP2A_P_Avg` tinyint NOT NULL,
  `SP2B_H_Avg` tinyint NOT NULL,
  `SP2B_P_Avg` tinyint NOT NULL,
  `AirTemp_Avg` tinyint NOT NULL,
  `PanelT_Avg` tinyint NOT NULL,
  `AirTC_Avg` tinyint NOT NULL,
  `RH_Avg` tinyint NOT NULL,
  `T_Aisle_Avg` tinyint NOT NULL,
  `T_Array_Avg` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `BPS_SEC_tmpfs`
--

DROP TABLE IF EXISTS `BPS_SEC_tmpfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BPS_SEC_tmpfs` (
  `BPS_N` int(11) NOT NULL,
  `time_stamp` int(11) NOT NULL,
  `RECORD` int(11) NOT NULL,
  `BattV_Min` float DEFAULT NULL,
  `PTemp_Avg` float DEFAULT NULL,
  `SP2A_H_Avg` float DEFAULT NULL,
  `SP2A_P_Avg` float DEFAULT NULL,
  `SP2B_H_Avg` float DEFAULT NULL,
  `SP2B_P_Avg` float DEFAULT NULL,
  `AirTemp_Avg` float DEFAULT NULL,
  `PanelT_Avg` float DEFAULT NULL,
  `AirTC_Avg` float DEFAULT NULL,
  `RH_Avg` float DEFAULT NULL,
  `T_Aisle_Avg` float DEFAULT NULL,
  `T_Array_Avg` float DEFAULT NULL,
  PRIMARY KEY (`time_stamp`,`BPS_N`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='/mnt/solar/' INDEX DIRECTORY='/mnt/solar/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LISF_PA_5min`
--

DROP TABLE IF EXISTS `LISF_PA_5min`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LISF_PA_5min` (
  `time_stamp` datetime NOT NULL,
  `power` float DEFAULT '0',
  PRIMARY KEY (`time_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `String_MIN`
--

DROP TABLE IF EXISTS `String_MIN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `String_MIN` (
  `String_N` int(11) NOT NULL,
  `time_stamp` datetime NOT NULL,
  `RECORD` int(11) NOT NULL,
  `Batt` float DEFAULT NULL,
  `DLT` float DEFAULT NULL,
  `A1` float DEFAULT NULL,
  `A2` float DEFAULT NULL,
  `A3` float DEFAULT NULL,
  `A4` float DEFAULT NULL,
  `A5` float DEFAULT NULL,
  `A6` float DEFAULT NULL,
  `A7` float DEFAULT NULL,
  `A8` float DEFAULT NULL,
  `A9` float DEFAULT NULL,
  `A10` float DEFAULT NULL,
  `A11` float DEFAULT NULL,
  `A12` float DEFAULT NULL,
  `A13` float DEFAULT NULL,
  `A14` float DEFAULT NULL,
  `A15` float DEFAULT NULL,
  `VTot` float DEFAULT NULL,
  PRIMARY KEY (`time_stamp`,`String_N`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `String_MIN_tmpfs`
--

DROP TABLE IF EXISTS `String_MIN_tmpfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `String_MIN_tmpfs` (
  `String_N` int(11) NOT NULL,
  `time_stamp` datetime NOT NULL,
  `RECORD` int(11) NOT NULL,
  `Batt` float DEFAULT NULL,
  `DLT` float DEFAULT NULL,
  `A1` float DEFAULT NULL,
  `A2` float DEFAULT NULL,
  `A3` float DEFAULT NULL,
  `A4` float DEFAULT NULL,
  `A5` float DEFAULT NULL,
  `A6` float DEFAULT NULL,
  `A7` float DEFAULT NULL,
  `A8` float DEFAULT NULL,
  `A9` float DEFAULT NULL,
  `A10` float DEFAULT NULL,
  `A11` float DEFAULT NULL,
  `A12` float DEFAULT NULL,
  `A13` float DEFAULT NULL,
  `A14` float DEFAULT NULL,
  `A15` float DEFAULT NULL,
  `VTot` float DEFAULT NULL,
  PRIMARY KEY (`time_stamp`,`String_N`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='/mnt/solar/' INDEX DIRECTORY='/mnt/solar/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `String_SEC`
--

DROP TABLE IF EXISTS `String_SEC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `String_SEC` (
  `String_N` int(11) NOT NULL,
  `time_stamp` datetime NOT NULL,
  `RECORD` int(11) NOT NULL,
  `Batt` float DEFAULT NULL,
  `DLT` float DEFAULT NULL,
  `A1` float DEFAULT NULL,
  `A2` float DEFAULT NULL,
  `A3` float DEFAULT NULL,
  `A4` float DEFAULT NULL,
  `A5` float DEFAULT NULL,
  `A6` float DEFAULT NULL,
  `A7` float DEFAULT NULL,
  `A8` float DEFAULT NULL,
  `A9` float DEFAULT NULL,
  `A10` float DEFAULT NULL,
  `A11` float DEFAULT NULL,
  `A12` float DEFAULT NULL,
  `A13` float DEFAULT NULL,
  `A14` float DEFAULT NULL,
  `A15` float DEFAULT NULL,
  `VTot` float DEFAULT NULL,
  PRIMARY KEY (`time_stamp`,`String_N`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `String_SEC_tmpfs`
--

DROP TABLE IF EXISTS `String_SEC_tmpfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `String_SEC_tmpfs` (
  `String_N` int(11) NOT NULL,
  `time_stamp` datetime NOT NULL,
  `RECORD` int(11) NOT NULL,
  `Batt` float DEFAULT NULL,
  `DLT` float DEFAULT NULL,
  `A1` float DEFAULT NULL,
  `A2` float DEFAULT NULL,
  `A3` float DEFAULT NULL,
  `A4` float DEFAULT NULL,
  `A5` float DEFAULT NULL,
  `A6` float DEFAULT NULL,
  `A7` float DEFAULT NULL,
  `A8` float DEFAULT NULL,
  `A9` float DEFAULT NULL,
  `A10` float DEFAULT NULL,
  `A11` float DEFAULT NULL,
  `A12` float DEFAULT NULL,
  `A13` float DEFAULT NULL,
  `A14` float DEFAULT NULL,
  `A15` float DEFAULT NULL,
  `VTot` float DEFAULT NULL,
  PRIMARY KEY (`time_stamp`,`String_N`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='/mnt/solar/' INDEX DIRECTORY='/mnt/solar/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `BPS_MIN_1`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_1`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_1`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_1` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_10`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_10`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_10`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_10` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 10) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_11`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_11`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_11`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_11` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 11) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_12`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_12`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_12`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_12` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 12) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_13`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_13`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_13`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_13` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 13) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_14`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_14`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_14`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_14` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 14) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_15`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_15`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_15`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_15` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 15) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_16`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_16`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_16`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_16` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 16) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_17`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_17`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_17`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_17` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 17) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_18`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_18`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_18`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_18` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 18) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_19`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_19`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_19`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_19` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 19) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_2`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_2`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_2` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 2) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_20`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_20`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_20`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_20` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 20) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_21`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_21`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_21`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_21` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 21) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_22`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_22`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_22`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_22` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 22) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_23`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_23`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_23`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_23` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 23) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_24`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_24`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_24`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_24` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 24) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_25`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_25`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_25`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_25` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 25) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_3`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_3`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_3`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_3` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 3) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_4`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_4`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_4`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_4` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 4) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_5`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_5`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_5`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_5` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 5) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_6`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_6`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_6`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_6` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 6) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_7`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_7`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_7`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_7` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 7) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_8`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_8`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_8`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_8` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 8) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_MIN_9`
--

/*!50001 DROP TABLE IF EXISTS `BPS_MIN_9`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_MIN_9`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_MIN_9` AS select `BPS_MIN`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_MIN`.`time_stamp`) AS `time_stamp`,`BPS_MIN`.`RECORD` AS `RECORD`,`BPS_MIN`.`BattV_Min` AS `BattV_Min`,`BPS_MIN`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_MIN`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_MIN`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_MIN`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_MIN`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_MIN`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_MIN`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_MIN`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_MIN`.`RH_Avg` AS `RH_Avg`,`BPS_MIN`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_MIN`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_MIN` where (`BPS_MIN`.`BPS_N` = 9) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_1`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_1`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_1`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_1` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_10`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_10`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_10`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_10` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 10) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_11`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_11`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_11`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_11` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 11) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_12`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_12`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_12`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_12` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 12) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_13`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_13`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_13`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_13` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 13) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_14`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_14`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_14`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_14` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 14) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_15`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_15`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_15`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_15` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 15) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_16`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_16`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_16`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_16` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 16) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_17`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_17`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_17`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_17` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 17) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_18`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_18`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_18`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_18` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 18) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_19`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_19`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_19`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_19` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 19) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_2`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_2`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_2` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 2) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_20`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_20`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_20`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_20` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 20) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_21`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_21`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_21`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_21` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 21) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_22`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_22`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_22`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_22` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 22) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_23`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_23`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_23`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_23` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 23) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_24`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_24`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_24`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_24` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 24) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_25`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_25`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_25`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_25` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 25) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_3`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_3`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_3`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_3` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 3) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_4`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_4`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_4`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_4` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 4) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_5`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_5`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_5`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_5` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 5) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_6`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_6`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_6`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_6` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 6) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_7`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_7`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_7`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_7` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 7) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_8`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_8`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_8`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_8` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 8) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `BPS_SEC_9`
--

/*!50001 DROP TABLE IF EXISTS `BPS_SEC_9`*/;
/*!50001 DROP VIEW IF EXISTS `BPS_SEC_9`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BPS_SEC_9` AS select `BPS_SEC`.`BPS_N` AS `BPS_N`,from_unixtime(`BPS_SEC`.`time_stamp`) AS `time_stamp`,`BPS_SEC`.`RECORD` AS `RECORD`,`BPS_SEC`.`BattV_Min` AS `BattV_Min`,`BPS_SEC`.`PTemp_Avg` AS `PTemp_Avg`,`BPS_SEC`.`SP2A_H_Avg` AS `SP2A_H_Avg`,`BPS_SEC`.`SP2A_P_Avg` AS `SP2A_P_Avg`,`BPS_SEC`.`SP2B_H_Avg` AS `SP2B_H_Avg`,`BPS_SEC`.`SP2B_P_Avg` AS `SP2B_P_Avg`,`BPS_SEC`.`AirTemp_Avg` AS `AirTemp_Avg`,`BPS_SEC`.`PanelT_Avg` AS `PanelT_Avg`,`BPS_SEC`.`AirTC_Avg` AS `AirTC_Avg`,`BPS_SEC`.`RH_Avg` AS `RH_Avg`,`BPS_SEC`.`T_Aisle_Avg` AS `T_Aisle_Avg`,`BPS_SEC`.`T_Array_Avg` AS `T_Array_Avg` from `BPS_SEC` where (`BPS_SEC`.`BPS_N` = 9) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-23 13:33:51
