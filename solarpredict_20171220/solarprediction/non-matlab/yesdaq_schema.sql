-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: yesdaq
-- ------------------------------------------------------
-- Server version	5.1.73-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tsi_skyimage`
--

DROP TABLE IF EXISTS `tsi_skyimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tsi_skyimage` (
  `iInstallationID` int(11) DEFAULT NULL,
  `dtUTC` datetime NOT NULL,
  `blSkyImage` mediumblob,
  PRIMARY KEY (`dtUTC`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tsi_skyimage_1`
--

DROP TABLE IF EXISTS `tsi_skyimage_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tsi_skyimage_1` (
  `iInstallationID` int(11) DEFAULT NULL,
  `dtUTC` datetime NOT NULL,
  `blSkyImage` mediumblob,
  PRIMARY KEY (`dtUTC`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tsi_skyimage_2`
--

DROP TABLE IF EXISTS `tsi_skyimage_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tsi_skyimage_2` (
  `iInstallationID` int(11) DEFAULT NULL,
  `dtUTC` datetime NOT NULL,
  `blSkyImage` mediumblob,
  PRIMARY KEY (`dtUTC`),
  KEY `instid` (`iInstallationID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tsi_skyimage_2_tmpfs`
--

DROP TABLE IF EXISTS `tsi_skyimage_2_tmpfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tsi_skyimage_2_tmpfs` (
  `iInstallationID` int(11) DEFAULT NULL,
  `dtUTC` datetime NOT NULL,
  `blSkyImage` mediumblob,
  PRIMARY KEY (`dtUTC`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='/mnt/solar/' INDEX DIRECTORY='/mnt/solar/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tsi_skyimage_3`
--

DROP TABLE IF EXISTS `tsi_skyimage_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tsi_skyimage_3` (
  `iInstallationID` int(11) DEFAULT NULL,
  `dtUTC` datetime NOT NULL,
  `blSkyImage` mediumblob,
  PRIMARY KEY (`dtUTC`),
  KEY `instid` (`iInstallationID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tsi_skyimage_3_tmpfs`
--

DROP TABLE IF EXISTS `tsi_skyimage_3_tmpfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tsi_skyimage_3_tmpfs` (
  `iInstallationID` int(11) DEFAULT NULL,
  `dtUTC` datetime NOT NULL,
  `blSkyImage` mediumblob,
  PRIMARY KEY (`dtUTC`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='/mnt/solar/' INDEX DIRECTORY='/mnt/solar/';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tsi_skyimage_tmpfs`
--

DROP TABLE IF EXISTS `tsi_skyimage_tmpfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tsi_skyimage_tmpfs` (
  `iInstallationID` int(11) DEFAULT NULL,
  `dtUTC` datetime NOT NULL,
  `blSkyImage` mediumblob,
  PRIMARY KEY (`dtUTC`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='/mnt/solar/' INDEX DIRECTORY='/mnt/solar/';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-23 13:33:28
