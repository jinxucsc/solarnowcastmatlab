'''
mktmpfs
    This script is to make tmpfs and create database for the usage of real-time data 
Created on Feb 17, 2014

@author: mikegrup
@version: 1.0    2014/03/03 add BPS_MIN and BPS_SEC 

'''

import MySQLdb,logging,os,subprocess,sys
from optparse import OptionParser


# define a function to check the existence of dbtables
def checkTableExists(dbcon, tablename):
    c = dbcon.cursor()
    c.execute("""
        SELECT COUNT(*)
        FROM information_schema.tables
        WHERE table_name = '{0}'
        """.format(tablename.replace('\'', '\'\'')))
    if c.fetchone()[0] == 1:
        c.close()
        return True
    c.close()
    return False





#####################################################################################
#    MAIN
#####################################################################################

#####################################################################################
logger = logging.getLogger('getimg_logger')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('mktmpfs.log')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')# create formatter
fh.setFormatter(formatter)  # add formatter to ch
logger.addHandler(fh)
#####################################################################################  
parser = OptionParser()
parser.add_option("-t", "--tmpfs", dest="tmpfspath", default='/mnt/solar', help="tmpfs root path", type="string")
(options, args) = parser.parse_args()
tmpfspath = options.tmpfspath #default is: '/mnt/solar/'

# 1. Check the tmpfs
# 1.1. Check the path, if there is no such directory then create one using SUDO
logger.debug("Start to check tmpfs directory.."+tmpfspath)
if (False==os.path.isdir(tmpfspath)):
    logger.debug("tmpfs directory is not found, create a new one: "+tmpfspath)
    os.system("sudo mkdir "+tmpfspath)
    

# 1.2. Check tmpfs Filesystem, if there is no such file system create one
p = subprocess.Popen(['df', '-lh'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out,err=p.communicate();
# tmpfspath last char is '/'
logger.debug("Start to check tmpfs mounting status.."+tmpfspath)
if(-1==out.find(tmpfspath[:-1])):
    cmmd="sudo mount -t tmpfs -o size=2048m tmpfs "+tmpfspath
    logger.debug("tmpfs entry is in df list create a new one: "+cmmd)
    os.system(cmmd)
    cmmd="sudo chown -R mysql:mysql "+tmpfspath
    logger.debug("change ownership of tmpfs to be mysql: "+cmmd)
    os.system(cmmd)


# 2. Check the real-time tables under this directory
dbhost='localhost'
dbuser='root'
dbpasswd='1234567'
dbname='yesdaq'     # yesdaq is realtime database for TSI
dbname1='SolarFarm' # SolarFarm is the realtime for solar farm radiation and energy output
dbt_t1='tsi_skyimage_tmpfs'
dbt_t2='tsi_skyimage_2_tmpfs'
dbt_t3='tsi_skyimage_3_tmpfs'
dbt_bpsmin='BPS_MIN_tmpfs';
dbt_bpssec='BPS_SEC_tmpfs';
dbt_strmin='String_MIN';
dbt_strsec='String_SEC';
dbt_rad='BPS_MIN_tmpfs';

# 2.1. Check connection to database
try:
    conn=MySQLdb.connect(dbhost,dbuser,dbpasswd,dbname)
    conn1=MySQLdb.connect(dbhost,dbuser,dbpasswd,dbname1)
except MySQLdb.Error, e:
    errorinfo="Error %d: %s" % (e.args[0], e.args[1])
    print(errorinfo) 
    logger.error(errorinfo)
    sys.exit()
except:
    print ("Error")
logger.debug("Connection to DB is normal.")

# 2.2. Check db tables existence. If there is no table on tmpfs, then create one
ctsicmmd="CREATE TABLE `{0}` ("+ \
        "`iInstallationID` int(11) DEFAULT NULL, " + \
        "`dtUTC` datetime NOT NULL, " + \
        "`blSkyImage` mediumblob, "+ \
        "PRIMARY KEY (`dtUTC`) "+ \
        ")  DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='{1}' INDEX DIRECTORY='{2}'"


cbpscmmd="CREATE TABLE `{0}` ("+ \
  "`BPS_N` int(11) NOT NULL,"+ \
  "`time_stamp` int(11) NOT NULL,"+ \
  "`RECORD` int(11) NOT NULL,"+ \
  "`BattV_Min` float DEFAULT NULL,"+ \
  "`PTemp_Avg` float DEFAULT NULL,"+ \
  "`SP2A_H_Avg` float DEFAULT NULL,"+ \
  "`SP2A_P_Avg` float DEFAULT NULL,"+ \
  "`SP2B_H_Avg` float DEFAULT NULL,"+ \
  "`SP2B_P_Avg` float DEFAULT NULL,"+ \
  "`AirTemp_Avg` float DEFAULT NULL,"+ \
  "`PanelT_Avg` float DEFAULT NULL,"+ \
  "`AirTC_Avg` float DEFAULT NULL,"+ \
  "`RH_Avg` float DEFAULT NULL,"+ \
  "`T_Aisle_Avg` float DEFAULT NULL,"+ \
  "`T_Array_Avg` float DEFAULT NULL,"+ \
  "PRIMARY KEY (`time_stamp`,`BPS_N`)"+ \
  ") DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='{1}' INDEX DIRECTORY='{2}'"

cstrcmmd="CREATE TABLE `{0}` ( "+ \
  "`String_N` int(11) NOT NULL, "+ \
  "`time_stamp` datetime NOT NULL, "+ \
  "`RECORD` int(11) NOT NULL,"+ \
  "`Batt` float DEFAULT NULL,"+ \
  "`DLT` float DEFAULT NULL,"+ \
  "`A1` float DEFAULT NULL,"+ \
  "`A2` float DEFAULT NULL,"+ \
  "`A3` float DEFAULT NULL,"+ \
  "`A4` float DEFAULT NULL,"+ \
  "`A5` float DEFAULT NULL,"+ \
  "`A6` float DEFAULT NULL,"+ \
  "`A7` float DEFAULT NULL,"+ \
  "`A8` float DEFAULT NULL,"+ \
  "`A9` float DEFAULT NULL,"+ \
  "`A10` float DEFAULT NULL,"+ \
  "`A11` float DEFAULT NULL,"+ \
  "`A12` float DEFAULT NULL,"+ \
  "`A13` float DEFAULT NULL,"+ \
  "`A14` float DEFAULT NULL,"+ \
  "`A15` float DEFAULT NULL,"+ \
  "`VTot` float DEFAULT NULL,"+ \
  "PRIMARY KEY (`time_stamp`,`String_N`)"+ \
") ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin DATA DIRECTORY='{1}' INDEX DIRECTORY='{2}'"


try:
    if (True==checkTableExists(conn, dbt_t1) and True==checkTableExists(conn, dbt_t2) and True==checkTableExists(conn, dbt_t3)):
        logger.debug("All TSI tables exist in the database.")
    else:
        logger.debug("There is one or more tables to be created!")
        c=conn.cursor()
        if (False==checkTableExists(conn, dbt_t1)):
            cmmd=ctsicmmd.format(dbt_t1,tmpfspath,tmpfspath)
            c.execute(cmmd);
            logger.debug("TSI1 table has been created!")
        if (False==checkTableExists(conn, dbt_t2)):
            cmmd=ctsicmmd.format(dbt_t2,tmpfspath,tmpfspath)
            c.execute(cmmd);
            logger.debug("TSI2 table has been created!")
        if (False==checkTableExists(conn, dbt_t3)):
            cmmd=ctsicmmd.format(dbt_t3,tmpfspath,tmpfspath)
            c.execute(cmmd);
            logger.debug("TSI3 table has been created!")
    if (True==checkTableExists(conn1,dbt_bpsmin) and True==checkTableExists(conn1,dbt_bpssec)):
        logger.debug("BPS_MIN and BPS_SEC exists in the database")
    else:
        logger.debug("There is one or more tables to be created!")
        c1=conn1.cursor()
        if (False==checkTableExists(conn1, dbt_bpsmin)):
            cmmd=cbpscmmd.format(dbt_bpsmin,tmpfspath,tmpfspath)
            c1.execute(cmmd);
            logger.debug("BPS_MIN table has been created!")
        if (False==checkTableExists(conn1, dbt_bpssec)):
            cmmd=cbpscmmd.format(dbt_bpssec,tmpfspath,tmpfspath)
            c1.execute(cmmd);
            logger.debug("BPS_SEC table has been created!")
    if (True==checkTableExists(conn1,dbt_strmin) and True==checkTableExists(conn1,dbt_strsec)):
        logger.debug("String_MIN and String_SEC exists in the database")
    else:
        logger.debug("There is one or more tables to be created!")
        c1=conn1.cursor()
        if (False==checkTableExists(conn1, dbt_strmin)):
            cmmd=cstrcmmd.format(dbt_strmin,tmpfspath,tmpfspath)
            c1.execute(cmmd);
            logger.debug("String_MIN table has been created!")
        if (False==checkTableExists(conn1, dbt_strsec)):
            cmmd=cstrcmmd.format(dbt_strsec,tmpfspath,tmpfspath)
            c1.execute(cmmd);
            logger.debug("String_SEC table has been created!")
except MySQLdb.Error, e:
    errorinfo="Error %d: %s" % (e.args[0], e.args[1])
    print(errorinfo) 
    logger.error(errorinfo)
    sys.exit()
except:
    print ("Error") 
    

# close the connection
conn.close()
conn1.close()
