'''
getimg.py
    is to collect TSI1, TSI2, TSI3 images from jpg links using urllib.
    Different from previous newgetimg.py, new script is writing to a tmpfs which created in the morning
    
@version: 1.0    2014-02-17 from newgetimg.py
@version: 1.1    2014-02-18
    a. Add frequency control. Add -f input option for sampling rate
    b. Modify the TSICollectThread to change sampling rate based on input
    c. Change TSIDBThread to remove last inserted ts
@prerequisite:
    1. createtmpfs.py
'''

import datetime,urllib2,MySQLdb,time,threading,Queue,logging,smtplib
from email.MIMEText import MIMEText
from optparse import OptionParser

dbhost='localhost'
dbuser='radandtsi'
dbpasswd='radandtsi'
dbname='yesdaq'
dbtable1='tsi_skyimage_tmpfs'
dbtable2='tsi_skyimage_2_tmpfs'
dbtable3='tsi_skyimage_3_tmpfs'

def sendemail(msg):
    fromadd='tsiserver@gmail.com'
    toadd='mikegrup@hotmail.com'
    username = 'tsiserver'
    password = 'yourpassword'
    
    msgall = MIMEText(msg) # Create a Message object with the body text
    # Now add the headers
    msgall['Subject'] = '[REPORT]Solar.bnl.gov hourly check report'
    msgall['From'] = fromadd
    msgall['To'] = toadd
    
    server=smtplib.SMTP('smtp.gmail.com:587')
    server.starttls();
    server.login(username,password)
    server.sendmail(fromadd, toadd, msgall.as_string())
    server.quit()

def sendemail_all(subject,msg,fromadd,toadd,username,password):
    #fromadd='tsiserver@gmail.com'
    #toadd='mikegrup@hotmail.com'
    #username = 'tsiserver'
    #password = 'yourpassword'
    
    msgall = MIMEText(msg) # Create a Message object with the body text
    # Now add the headers
    msgall['Subject'] = subject
    msgall['From'] = fromadd
    msgall['To'] = toadd
    
    server=smtplib.SMTP('smtp.gmail.com:587')
    server.starttls();
    server.login(username,password)
    server.sendmail(fromadd, toadd, msgall.as_string())
    server.quit()


class TSICollectThread(threading.Thread):
    def __init__(self,url,station,imgqueue,tsqueue,logger):
        threading.Thread.__init__(self)  
        self.url=url
        self.station=station
        if station==1:
            self.table=dbtable1
        elif station==2:
            self.table=dbtable2
        elif station ==3:
            self.table=dbtable3
        self.status=''      #Initial status is valid 
        self.imgqueue=imgqueue
        self.tsqueue=tsqueue
        self.logger=logger
        self.freq=freq
        
    def stop(self):
        self.logger.debug('Stop TSI collecting thread')
        self._stop.set()
    
    def GetStatus(self):
        return self.status
           
    def run(self):
        self.status=''
        self.logger.debug('Start TSI collecting thread')
        freq=self.freq
        exptime=time.time();
        while(True):
            startclick=time.time()
            if(startclick<exptime):
                time.sleep(exptime-startclick) # main thread sleeps to reach the next sampling ts
                exptime=exptime+freq # update next expected timstamp
            else: #(startclick>=exptime)
                if(startclick-exptime>1): # current timestamp is at least 1s ahead from exptime
                    self.logger.error('Gathering may have a gap %f'%(startclick-exptime))
                    while(startclick-exptime>0):
                        exptime=exptime+freq # find the next expected ts and skip the middle ts
                else:
                    exptime=exptime+freq # update next expected timstamp
            
            nowdt=datetime.datetime.utcnow()
            tstamp=nowdt.strftime("%Y-%m-%d %H:%M:%S")
            URL=self.url
            connst=time.time()
            try:
                filehandle=urllib2.urlopen(URL,timeout=1)
                #f=open('/var/zpeng/%s.jpg'%(tstamp),'wb')
                contents=filehandle.read()
                filehandle.close()
                self.imgqueue.put(contents)
                self.tsqueue.put(tstamp)
            except urllib2.HTTPError, e:
                errorinfo="Error %d: %s" % (e.code, e.read())
                self.status=errorinfo
                self.logger.error(errorinfo)
            except urllib2.URLError, e:
                errorinfo="urllib2 URLError: %s" % (e.args)
                self.status=errorinfo
                self.logger.error(errorinfo)
            except:
                self.logger.error('unknown error!')
            else:
                self.status=''
            connet=time.time()
            
            if connet-connst>freq:
                self.logger.error('[WARNING]: Operation lasts %f'%(connet-connst))
            
            
class TSIDBThread(threading.Thread):
    def __init__(self,url,station,imgqueue,tsqueue,logger):
        threading.Thread.__init__(self)  
        self.url=url
        self.station=station
        if station==1:
            self.table=dbtable1
        elif station==2:
            self.table=dbtable2
        elif station ==3:
            self.table=dbtable3
        self.status=''      #Initial status is valid 
        self.imgqueue=imgqueue  # image queue to hold images
        self.tsqueue=tsqueue    # timestamp queue
        self.logger=logger      # logger instance
        
    def stop(self):
        self.logger.debug('Stop TSI DB thread')
        self._stop.set()
    
    def GetStatus(self):
        return self.status
           
    def run(self):
        self.logger.debug('Start TSI DB thread')        
        conn=MySQLdb.connect(dbhost,dbuser,dbpasswd,dbname)
        c=conn.cursor()
        bRestartMySQL=False
        self.status=''
        tslist=[]
        lastts=0; # initial value 
        tslist.append(lastts)
        while(True):
            if(self.imgqueue.qsize()>0):
                if True==bRestartMySQL:
                    conn=MySQLdb.connect(dbhost,dbuser,dbpasswd,dbname)
                    c=conn.cursor()
                try:
                    # Get frame and timestamp
                    contents=self.imgqueue.get()
                    tstamp=self.tsqueue.get()
                    self.imgqueue.task_done()
                    self.tsqueue.task_done()
                    #    tstamp is in tslist
                    if tstamp in tslist:    continue
                    tslist.append(tstamp)
                    cmmd="INSERT INTO %s VALUES(%d"%(self.table,int(self.station))+",%s,%s)"
                    params=(tstamp,contents)
                    c.execute(cmmd,params) #Execute mysql command
                    tslist.remove(lastts) # remove the last and update with current insert ts 
                    lastts=tstamp       
                except MySQLdb.Error, e:
                    if e.args[0]==1062: # Duplicated entry is handled here
                        pass
                    else:
                        errorinfo="Error %d: %s" % (e.args[0], e.args[1])
                        self.status=errorinfo
                        bRestartMySQL=True
                        print(errorinfo)
                        self.logger.error(errorinfo)
                except:
                    print('unknown error!')
                    self.logger.error('unknown error!')
                else:
                    self.status=''
                    bRestartMySQL=False
            else:
                time.sleep(1) # main thread sleep for one second



#END OF CLASS



#####################################################################################
#    Main entrance
#####################################################################################
parser = OptionParser()
parser.add_option("-s", "--station", dest="station", default='1', help="TSI station #", type="int")
parser.add_option("-l", "--logger", dest="loggerfile", default='newgetimg.log', help="logger file location", type="string")
parser.add_option("-f", "--frequency", dest="freq", default='2', help="sampling rate in one second", type="int")
(options, args) = parser.parse_args()
station = options.station
loggerfile = options.loggerfile
freq=options.freq


#####################################################################################
logger = logging.getLogger('getimg_logger')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(loggerfile)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')# create formatter
fh.setFormatter(formatter)  # add formatter to ch
logger.addHandler(fh)
#####################################################################################  


URL1='http://130.199.208.131:2000/axis-cgi/jpg/image.cgi?resolution=640x480'
URL2='http://192.168.198.51:2000/axis-cgi/jpg/image.cgi?resolution=640x480'
URL3='http://192.168.198.52:2000/axis-cgi/jpg/image.cgi?resolution=640x480'

URL11='http://130.199.208.26/image1?res=full&x0=0&y0=0&x1=3648&y1=2752&quality=21&doublescan=0'
URL12='http://130.199.208.26/image2?res=full&x0=0&y0=0&x1=3648&y1=2752&quality=21&doublescan=0'
URL13='http://130.199.208.26/image3?res=full&x0=0&y0=0&x1=3648&y1=2752&quality=21&doublescan=0'
URL14='http://130.199.208.26/image4?res=full&x0=0&y0=0&x1=3648&y1=2752&quality=21&doublescan=0'


if 1==station:
    URL=URL1
elif 2==station:
    URL=URL2
elif 3==station:
    URL=URL3


logger.debug('Main Fuction starts with station = %d and loggerfile = %s'%(station,loggerfile))
# Initialize Queues
imgqueue=Queue.Queue()
tsqueue=Queue.Queue()


logger.debug('Start TSI collecting and DB thread')
t1 = TSICollectThread(URL,station,imgqueue,tsqueue,logger)  # start TSI collecting thread
t2 = TSIDBThread(URL,station,imgqueue,tsqueue,logger)  # start TSI db thread
t1.setDaemon(True)
t1.start() # main thread sleep for one second
t2.setDaemon(True)
t2.start()

    
while (True):
    nowdt=datetime.datetime.now()
    if nowdt.hour>=21 or nowdt.hour<5:
        t1.stop()
        t2.stop()
    if True==bool(t1.GetStatus()):
        msg=t1.GetStatus()
        logger.error(msg)
        sendemail(msg)
    time.sleep(600) # main thread sleep for one second

