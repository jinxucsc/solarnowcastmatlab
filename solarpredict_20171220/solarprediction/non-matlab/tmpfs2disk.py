'''
tmpfs2disk
    This script is to output the tmpfs result to disk storage. This script is similiar with tsidbsync.py
     
Created on Feb 17, 2014

@author: mikegrup
@version: 1.1    2014/03/24
@prerequisite:    
    mktmpfs.py --- make tmpfs file system
    getimg.py    --- store the TSI image into tmpfs file system
'''
import MySQLdb,logging,os,subprocess,sys,smtplib
from optparse import OptionParser
from email.MIMEText import MIMEText

def sendemail_all(subject,msg,fromadd,toadd,username,password):
    #fromadd='tsiserver@gmail.com'
    #toadd='mikegrup@hotmail.com'
    #username = 'tsiserver'
    #password = 'yourpassword'
    
    msgall = MIMEText(msg) # Create a Message object with the body text
    # Now add the headers
    msgall['Subject'] = subject
    msgall['From'] = fromadd
    msgall['To'] = toadd
    
    server=smtplib.SMTP('smtp.gmail.com:587')
    server.starttls();
    server.login(username,password)
    server.sendmail(fromadd, toadd, msgall.as_string())
    server.quit()



#####################################################################################
#    MAIN
#####################################################################################

#####################################################################################
logger = logging.getLogger('getimg_logger')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('tmpfs2disk.log')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')# create formatter
fh.setFormatter(formatter)  # add formatter to ch
logger.addHandler(fh)
#####################################################################################

dbhost='localhost'
dbuser='radandtsi'
dbpasswd='radandtsi'
dbname='yesdaq'
dbname1='SolarFarm'
dbtable1_t='tsi_skyimage_tmpfs'
dbtable2_t='tsi_skyimage_2_tmpfs'
dbtable3_t='tsi_skyimage_3_tmpfs'
dbt_bpsmin_t='BPS_MIN_tmpfs'
dbt_bpssec_t='BPS_SEC_tmpfs'
dbt_strmin_t='String_MIN_tmpfs'
dbt_strsec_t='String_SEC_tmpfs'

dbtable1='tsi_skyimage'
dbtable2='tsi_skyimage_2'
dbtable3='tsi_skyimage_3'
dbt_bpsmin='BPS_MIN'
dbt_bpssec='BPS_SEC'
dbt_strmin='String_MIN'
dbt_strsec='String_SEC'


fromadd='tsiserver@gmail.com'
toadd='mikegrup@hotmail.com'
username = 'tsiserver'
password = 'yourpassword'

# 1. insert all records in tsi_tmpfs to tsi
# INSERT IGNORE INTO tsi_skyimage SELECT * FROM tsi_skyimage_tmpfs;
t2dcmmd="INSERT IGNORE INTO {0} SELECT * FROM {1};"
dropcmmd="delete from {0} where {1}<='{2}';" # for TSI table int which ts is in '2014-03-05 00:00:00' 
dropcmmd1="delete from {0} where {1}<={2};" # drop for BPS_MIN and BPS_SEC in which the ts is in UNIX TIME
maxcmmd="SELECT max({0}) from {1};"
logger.debug("Initilize the DB connection..")
try:
    conn=MySQLdb.connect(dbhost,dbuser,dbpasswd,dbname)
    conn1=MySQLdb.connect(dbhost,dbuser,dbpasswd,dbname1)
    c=conn.cursor()
    c1=conn1.cursor()
except:
    print('[ERROR]: Local MySQL connection cannot be established!')
    logger.error('[ERROR]: Local MySQL connection cannot be established!')
    sendemail_all("[REPORT]tmpfs2disk error report!",'[ERROR]: Local MySQL connection cannot be established!',fromadd,toadd,username,password)
    
logger.debug(' local MySQL connection has been established!')

logger.debug("Copy entries on tmpfs into disk tables..")
bDelete=True;
try:
    # TSI1 
    logger.debug("Start copying from "+dbtable1_t + " to "+dbtable1)
    cmmd=maxcmmd.format("dtUTC",dbtable1_t)
    c.execute(cmmd)
    row=c.fetchone()
    ts=row[0]
    if(ts==None):
        logger.debug("Table is Empty")
    else:
        logger.debug("Get max TS: "+ts.strftime("%Y-%m-%d %H:%M:%S"))
        cmmd=t2dcmmd.format(dbtable1,dbtable1_t)
        c.execute(cmmd)
        logger.debug("Remove records from tmpfs.. ")
        cmmd=dropcmmd.format(dbtable1_t,"dtUTC",ts.strftime("%Y-%m-%d %H:%M:%S"))
        c.execute(cmmd)
    #TSI2
    logger.debug("Start copying from "+dbtable2_t + " to "+dbtable2)
    cmmd=maxcmmd.format("dtUTC",dbtable2_t)
    c.execute(cmmd)
    row=c.fetchone()
    ts=row[0]
    if(ts==None):
        logger.debug("Table is Empty")
    else:
        logger.debug("Get max TS: "+ts.strftime("%Y-%m-%d %H:%M:%S"))
        cmmd=t2dcmmd.format(dbtable2,dbtable2_t)
        c.execute(cmmd)
        logger.debug("Remove records from tmpfs.. ")
        cmmd=dropcmmd.format(dbtable2_t,"dtUTC",ts.strftime("%Y-%m-%d %H:%M:%S"))
        c.execute(cmmd)
    #TSI3
    logger.debug("Start copying from "+dbtable3_t + " to "+dbtable3)
    cmmd=maxcmmd.format("dtUTC",dbtable3_t)
    c.execute(cmmd)
    row=c.fetchone()
    ts=row[0]
    if(ts==None):
        logger.debug("Table is Empty")
    else:
        logger.debug("Get max TS: "+ts.strftime("%Y-%m-%d %H:%M:%S"))
        cmmd=t2dcmmd.format(dbtable3,dbtable3_t)
        c.execute(cmmd)
        logger.debug("Remove records from tmpfs.. ")
        cmmd=dropcmmd.format(dbtable3_t,"dtUTC",ts.strftime("%Y-%m-%d %H:%M:%S"))
        c.execute(cmmd)
    
    #BPS SEC
    logger.debug("Start copying from "+dbt_bpssec_t + " to "+dbt_bpssec)
    cmmd=maxcmmd.format("time_stamp",dbt_bpssec_t)
    c1.execute(cmmd)
    row=c1.fetchone()
    ts=row[0]
    if(ts==None):
        logger.debug("Table is Empty")
    else:
        logger.debug("Get max TS: "+str(ts))
        cmmd=t2dcmmd.format(dbt_bpssec,dbt_bpssec_t)
        logger.debug("cmmd =  "+cmmd)
        c1.execute(cmmd)
        logger.debug("Remove records from tmpfs.. ")
        cmmd=dropcmmd1.format(dbt_bpssec_t,"time_stamp",ts)
        c1.execute(cmmd)

    #BPS MIN
    logger.debug("Start copying from "+dbt_bpsmin_t + " to "+dbt_bpsmin)
    cmmd=maxcmmd.format("time_stamp",dbt_bpsmin_t)
    c1.execute(cmmd)
    row=c1.fetchone()
    ts=row[0]
    if(ts==None):
        logger.debug("Table is Empty")
    else:
        logger.debug("Get max TS: "+str(ts))
        cmmd=t2dcmmd.format(dbt_bpsmin,dbt_bpsmin_t)
        
        c1.execute(cmmd)
        logger.debug("Remove records from tmpfs.. ")
        cmmd=dropcmmd1.format(dbt_bpsmin_t,"time_stamp",ts)
        c1.execute(cmmd)
    
    #String SEC
    logger.debug("Start copying from "+dbt_strsec_t + " to "+dbt_strsec)
    cmmd=maxcmmd.format("time_stamp",dbt_strsec_t)
    c1.execute(cmmd)
    row=c1.fetchone()
    ts=row[0]
    if(ts==None):
        logger.debug("Table is Empty")
    else:
        logger.debug("Get max TS: "+str(ts))
        cmmd=t2dcmmd.format(dbt_strsec,dbt_strsec_t)
        c1.execute(cmmd)
        logger.debug("Remove records from tmpfs.. ")
        cmmd=dropcmmd.format(dbt_strsec_t,"time_stamp",ts)
        c1.execute(cmmd)
    
    #String MIN
    logger.debug("Start copying from "+dbt_strmin_t + " to "+dbt_strmin)
    cmmd=maxcmmd.format("time_stamp",dbt_strmin_t)
    c1.execute(cmmd)
    row=c1.fetchone()
    ts=row[0]
    if(ts==None):
        logger.debug("Table is Empty")
    else:
        logger.debug("Get max TS: "+str(ts))
        cmmd=t2dcmmd.format(dbt_strmin,dbt_strmin_t)
        c1.execute(cmmd)
        logger.debug("Remove records from tmpfs.. ")
        cmmd=dropcmmd.format(dbt_strmin_t,"time_stamp",ts)
        c1.execute(cmmd)
    
except MySQLdb.Error,e:
    errinfo= "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
    print errinfo
    logger.error('DB error while executing the query!Error msg = %s'%(errinfo))
    bDelete=False;
    sendemail_all("[REPORT]tmpfs2disk error report!",errinfo,fromadd,toadd,username,password)

# 2. synchronize the records from solar.bnl.gov to solar-db.bnl.gov
# It is a synchronization call using tsidbsync.py. This part is for database synchronization NOT NECESSARY in real-time pipeline.  
if bDelete == True: #if something wrong with copying data from tmpfs to disk, no sync will be intiated 
    logger.debug("Synchronize to solar-db.bnl.gov..")
    #os.system('python tsidbsync.py') # DEBUG current solardb is down, donot sync the data
else:
    logger.error('No synchronization and DELETE will be executed due to the error occured in the step of copying data from tmpfs to disk.')


# close the db connection
logger.debug("Close the db connection..")
conn.commit();
conn1.commit();
conn.close()
conn1.close()


 
 
 # 3. After Synchronization, drop tmp tables
#logger.debug("DELETE ALL records of tables on tmpfs..")
#try:
#         cmmd=dropcmmd.format(dbtable1_t);
#         c.execute(cmmd)
#         cmmd=dropcmmd.format(dbtable2_t);
#         c.execute(cmmd)
#         cmmd=dropcmmd.format(dbtable3_t);
#         c.execute(cmmd)
#         cmmd=dropcmmd.format(dbt_bpsmin_t);
#         c1.execute(cmmd)
#         cmmd=dropcmmd.format(dbt_bpssec_t);
#         c1.execute(cmmd)
#     except MySQLdb.Error,e:
#         errinfo= "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
#         logger.error('DB error while executing the query!Error msg = %s'%(errinfo))
 
