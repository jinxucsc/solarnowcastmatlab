'''
killpy.py
    is to designed to kill a process containing a name string. You may need to have the pemission to perform this kill.
    
@version: 1.0    2014-02-18
@prerequisite:
'''
import os
import signal
import time
import datetime
from optparse import OptionParser


parser = OptionParser()
parser.add_option("-p", "--process", dest="processname", default='getimg.py', help="The process containing this name will be killed", type="string")
(options, args) = parser.parse_args()
processname=options.processname
now=time.gmtime()
tstamp="./killpylog/%(0)s-%(1)s-%(2)s_%(3)s-%(4)s-%(5)s.log"%{'0':now.tm_year,'1':now.tm_mon,'2':now.tm_mday,'3':now.tm_hour,'4':now.tm_min,'5':now.tm_sec}
FILE = open(tstamp,"w")
tlines=''
for line in os.popen("ps xa|grep python"):
    fields = line.split()
    print line
    if len(fields)<=5:
        continue
    pid = fields[0]
    process = fields[4]
    process1=fields[5]
    process=process+' '+process1
    print len(fields)
    tlines=tlines+line
    if process.find(processname) >0:
        os.kill(int(pid),signal.SIGHUP)
        print "sample.py is killed"
        tlines=tlines+'kill'+pid+'\n'
FILE.writelines(tlines)
