% PIPE_Undistortion: Main Entrance of the pipeline of preprocessing. It
% needs to ClearSkyLibrary to do shadow band masking and holding arm
% masking.
%   IN:
%       realtimedv  --   Datevector, current timestamp to do preprocessing
%       DataDirectory   --  Directory where to export sb/ha masks
%           DataDirectory/sbmask
%           DataDirectory/hamask
%       oriimgDir       --  Original Images(Not flipped)
%       FlipimgDir      --  Flipped Image(Flipped)
%       undistimgDir    --  Undistortion images
%       UndistortionParameters.mat(in)  --  related parameters
%
%   OUT:
%       undistimgDir/*     --  all the undistorted images
%
%   VERSION 1.0     2014-03-06
%   previous ver is PIPE_UndistortionwithCSL_BNL.
%   VERSIOn 2.0     2014-04-28
function realtime_preprocessingPipe (realtimedv,DataDirectory,oriimgDir,undistimgDir,site,configfpath,logger)
% Set up loggerID
[tasks,~]=dbstack;
loggerID = tasks(1).name;

%load('UndistortionParameters.mat');     % load ALL the related parameters
% load('undist2ori.mat');             % matrix that contains all undist->ori mapping
DataDirectory=FormatDirName(DataDirectory);
realtimedn=datenum(realtimedv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STARTUP to get all possible parameter settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run(configfpath); %mtsi_startup;
nsite =length(SITES);
for i=1:nsite
    if site==SITES{i};
        location=locations(i);
        azshift=AzumithShifts(i);
        zeshift=ZenithShifts(i);
        %rcentrep=rcentreps(i);
        %cw=cws(i);
        %ch=chs(i);
        %crx=crxs(i);
        %cry=crys(i);
        %fo=fos(i);
        %h=hs(i);
        %r=rs(i);
        %bmpmaskorif=BMPMASKORIFS{i};    % cell of image name
        t=load(UNDISTMATS{i});
        % set SBMASK dir and HAMASK dir
        sbmaskDir=sprintf('%ssbmask%d/',DataDirectory,i);
        hamaskDir=sprintf('%shamask%d/',DataDirectory,i);
        sbazmatf=sprintf('sbazimuth_tsi%d.mat',i);
        haazmatf=sprintf('haazimuth_tsi%d.mat',i);
    end
end
u2oma=t.u2oma;
undist2orima=u2oma;
UNDISTWndist=UNDISTW;
UNDISTHndist=UNDISTH;
alt=location.altitude;
lon=location.longitude;
lat=location.latitude;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Regulate Directories
oriimgDir=FormatDirName(oriimgDir);
sbmaskDir=GetAbspathFromFilename(sbmaskDir);
hamaskDir=GetAbspathFromFilename(hamaskDir);
undistoutDir=undistimgDir;
undistoutDir=FormatDirName(undistoutDir);
if ~exist(hamaskDir,'dir')
    mkdir(hamaskDir);
end
if ~exist(sbmaskDir,'dir')
    mkdir(sbmaskDir);
end
if ~exist(undistoutDir,'dir')
    mkdir(undistoutDir);
end

% flip the original image
filename=GetImgFromDV_BNL(realtimedv,site);
fullinputp=sprintf('%s%s',oriimgDir,filename);
img1=imread(fullinputp);
img1(:,:,1)=flipud(img1(:,:,1));
img1(:,:,2)=flipud(img1(:,:,2));
img1(:,:,3)=flipud(img1(:,:,3));
%column upside down
img1(:,:,1)=fliplr(img1(:,:,1));
img1(:,:,2)=fliplr(img1(:,:,2));
img1(:,:,3)=fliplr(img1(:,:,3));
delete(fullinputp); % remove the original image!!


%   Build Clear Sky Lib for SB masks
%startdn=datenum(realtimedv);
%enddn=datenum(realtimedv+[0 0 0 0 1 0]);
%BuildClearSkyLib_wBMPMask(inputimgDir,hamaskDir,sbmaskDir,site,startdn,enddn,60);
timespan=MASKTIMESPAN;
realtime_BuildClearSkyLib_wBMPMask(realtimedv,img1,hamaskDir,sbmaskDir,site,timespan,configfpath,logger);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   This code is to generate available avail MAT of SB and HA masks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Re-gen the sb and ha azimuth list
%GenAvailAzMat_SBHA(hamaskDir,sbmaskDir,haazmatf,sbazmatf,site);
if ~exist(sbazmatf,'file') ||  ~exist(haazmatf,'file')
    logger.debug(loggerID,'Call GenAvailAzMat_SBHA to generate availa mat files for SB/HA masks');
    GenAvailAzMat_SBHA(hamaskDir,sbmaskDir,haazmatf,sbazmatf,site,configfpath);
end
haazimuth=load(haazmatf);                % load all hamask timenumbers and azumith

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Output file skipping(uncomment to skip)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
outputf=sprintf('%s%s.bmp',undistoutDir,filename(1:end-4));
% if exist(outp,'file')~=0
%     return;
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sun=INNER_GETAandZ(realtimedv,lat,lon,alt);
az=sun.azimuth/180*pi+azshift/180*pi;
ze=sun.zenith/180*pi+zeshift/180*pi;
%  Get HA index and mask file
haindexi=GetAZIndex(haazimuth.azs,haazimuth.zes,az,ze);
hatmdv=datevec(haazimuth.tsdn(haindexi));
hamaskfilename=GetImgFromDV_BNL(hatmdv,site);
hamaskfilename=sprintf('%s%s',hamaskfilename,'.mat');
hamaskf=sprintf('%s%s',hamaskDir,hamaskfilename);

%  Get SB index and mask file
%  1. Check whether its sbmask file exists or not
%  2. if NOT, use sbindex
sbmaskfilename=GetImgFromDV_BNL(realtimedv,site);
sbmaskfilename=sprintf('%s%s',sbmaskfilename,'.mat');
sbmaskf=sprintf('%s%s',sbmaskDir,sbmaskfilename);
if (~exist(sbmaskf,'file'))
    logger.debug(loggerID,['sbmaskf = ' sbmaskf ' doesn''t exist, try to load the nearest one.']);
    sbazimuth=load(sbazmatf);                % load all sbmask timenumbers and azumith
    sbindexi=GetAZIndex_wThres(sbazimuth.tsdn,sbazimuth.azs,sbazimuth.zes,realtimedn,az,ze);
    if(sbindexi==-1)
        logger.error(loggerID,['Cannot find nearest SB Mask for' datestr(realtimedv)]);
        return;
    end
    sbtmdv=datevec(sbazimuth.tsdn(sbindexi));
    sbmaskfilename=GetImgFromDV_BNL(sbtmdv,site);
    sbmaskfilename=sprintf('%s%s',sbmaskfilename,'.mat');
    sbmaskf=sprintf('%s%s',sbmaskDir,sbmaskfilename);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
s=load(hamaskf, '-mat', 'hama');
holdma=s.hama;  %   tmma is the final name of holding arm
s=load(sbmaskf, '-mat', 'sbma');
shadowma=s.sbma;
totalmaskma=holdma&shadowma;
totalmaskma=~totalmaskma;
img1=INNER_MASK(img1,totalmaskma);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Undistortion Mapping here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
unimg=uint8(INNER_Undist(img1,undist2orima,UNDISTWndist,UNDISTHndist));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Output images. BMP files are generated here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%outp=sprintf('%s%s.bmp',undistoutDir,imgfilename(1:end-4));
imwrite(unimg,outputf,'bmp');

end
