function [imma_cld cldmask]=test_GenCldMask_2r(imma,imma_avg)
r1=imma(:,:,1);
g1=imma(:,:,2);
b1=imma(:,:,3);

ra=imma_avg(:,:,1);
ga=imma_avg(:,:,2);
ba=imma_avg(:,:,3);

ind=(r1>ra+15|r1>b1+10);

r2=r1;
g2=g1;
b2=b1;
imma2=imma;
r2(ind)=0;
g2(ind)=0;
b2(ind)=0;

imma2(:,:,1)=r2;
imma2(:,:,2)=g2;
imma2(:,:,3)=b2;

cldmask=ind;
imma_cld=imma2;

end