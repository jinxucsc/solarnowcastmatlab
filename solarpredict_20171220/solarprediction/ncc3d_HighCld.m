% This function is trying to find the best high altitude cloud using
%   It starts from startp on TSI3, gets recSize rectangle area as reference area. It will search the
%   whole area to find the best H using nccsumall
%   Previous version test_ncc3d_v2.m
%       fomula:     similarity = ncc12+ncc23+ncc13
%       
%   INPUT:
%       datevector      --- Datevector, time of image
%       startp          --- 2x1 double, [starty startx]
%       recSize         --- 2x1 double, [recH recW]
%       TSIDir          --- String, TSI mat Root directory
%       H_range         --- H range for test
%       ncc_thres       --- Minimum ncc for matching
%   OUTPUT:
%       H               --- Most possible height
%       maxcc           --- max ncc value
%       HMV_x           --- Height --> TSI3->TSI1 displacement 
%                           HMV_x=[HMV31_x(ind) HMV32_x(ind)];
%       HMV_y           --- Height --> TSI3->TSI2 displacement 
%                           HMV_y=[HMV31_y(ind) HMV32_y(ind)];
%
%   Version 1.0:        2013-07-29
    
function [H,maxcc,HMV_x,HMV_y]=ncc3d_HighCld(datevector,startp,recSize,TSIDir,H_range,ncc_thres,tsi3BlkMask)
mtsi_startup;

if ~exist('ncc_thres','var')
    ncc_thres=0;
end
if ~exist('tsi3BlkMask','var')
    tsi3BlkMask=true(UNDISTH,UNDISTW);
end

t=load(HeightMatrixValidMatf);
hma31_valid=t.H_valid_31;
hma32_valid=t.H_valid_32;
HMV31_x=t.HMVx_valid_31;
HMV31_y=t.HMVy_valid_31;
HMV32_x=t.HMVx_valid_32;
HMV32_y=t.HMVy_valid_32;
H_range_full=t.H_range_full;
H_range_full=round(H_range_full);
H_l=length(H_range);
full_valid=zeros(H_l,1);
H_range_full=int32(H_range_full);
H_range=int32(H_range);
for tmi=1:H_l
    ind=find(H_range_full==H_range(tmi),1);
    full_valid(tmi)=ind;
end
hma31_valid=hma31_valid(full_valid);
hma32_valid=hma32_valid(full_valid);
HMV31_x=HMV31_x(full_valid);
HMV31_y=HMV31_y(full_valid);
HMV32_x=HMV32_x(full_valid);
HMV32_y=HMV32_y(full_valid);


imma11=TO_rgb2lum(tmp_GetImgMatFromDataDir(datevector,TSI1,TSIDir));
imma21=TO_rgb2lum(tmp_GetImgMatFromDataDir(datevector,TSI2,TSIDir));
imma31=TO_rgb2lum(tmp_GetImgMatFromDataDir(datevector,TSI3,TSIDir));

recH=recSize(1);
recW=recSize(2);
undistp31=startp;
uy31=undistp31(1);
ux31=undistp31(2);

blk31=single(TO_GetImgVal_Pad([uy31 ux31],[recH recW],imma31,20));
blk31_mask=TO_GetImgVal_Pad([uy31 ux31],[recH recW],tsi3BlkMask,20);
blk31(~blk31_mask)=0;  % invalid false=0



% 2. For each height in range do similarity check
sim31=zeros(H_l,1);
sim32=zeros(H_l,1);
sim21=zeros(H_l,1);
for i=1:H_l
    H=H_range(i);
    H31=hma31_valid(i);
    H32=hma32_valid(i);
    if abs(H31-H)/H>0.1 || abs(H32-H)/H>0.1
        continue;
    end
    tmHMV_x=HMV31_x(i);
    tmHMV_y=HMV31_y(i);
    ux11=round(ux31+tmHMV_x);
    uy11=round(uy31+tmHMV_y);
    tmHMV_x=HMV32_x(i);
    tmHMV_y=HMV32_y(i);
    ux21=round(ux31+tmHMV_x);
    uy21=round(uy31+tmHMV_y);
    if ux11<=-recW || uy11<=-recH || ux21<=-recW || uy21<=-recH ||...
        ux11>=UNDISTW+recW || uy11>=UNDISTH+recH || ux21>=UNDISTW+recW || uy21>=UNDISTH+recH
        % the total rec area will be all black for TSI1 or TSI2. Then just skip it and keep sim_all
        % = 0 in this case
        continue;
    end
    

    blk11=single(TO_GetImgVal_Pad([uy11 ux11],[recH recW],imma11,max(recH,recW)));
    blk21=single(TO_GetImgVal_Pad([uy21 ux21],[recH recW],imma21,max(recH,recW)));
    
    tmcc=normxcorr2_blk(blk31,blk11);
    sim31(i)=tmcc(recH,recW);
    if sim31(i)<ncc_thres
        sim31(i)=0;
        sim32(i)=0;
        sim21(i)=0;
        continue;
    end
    tmcc=normxcorr2_blk(blk31,blk21);
    sim32(i)=tmcc(recH,recW);
    if sim32(i)<ncc_thres
        sim31(i)=0;
        sim32(i)=0;
        sim21(i)=0;
        continue;
    end
    tmcc=normxcorr2_blk(blk11,blk21);
    sim21(i)=tmcc(recH,recW);
end
sim_all=sim31+sim32+sim21;
% sim_all=sim31+sim32;


% 3. Get most similar Height from similiar matrix
[maxcc, ind]=max(sim_all);
assert(~isnan(maxcc),'maxcc=NAN ! Please debug ncc3d_HighCld!');
assert(maxcc~=inf,'maxcc=inf exists! Please debug ncc3d_HighCld!');
% [maxcc, ind]=min(sim_all);
H=H_range(ind);
HMV_x=[HMV31_x(ind) HMV32_x(ind)];
HMV_y=[HMV31_y(ind) HMV32_y(ind)];


end