%   BMP2SBMask_BNL is to read the HA mask BMP file and generate a MAT file for BuildClearSkyLib_wBMPMask
%   INPUT:
%       outputf         --  Output file name (default name should be the same as bmpmaskorif, but with subfix to be .mat)
%       site            --  Currently supported TSI1,TSI2,TSI3 in mtsi_startup
%       configfpath     --  mtsi_startup;
%   OUTPUT:
%       outputf         --  Containing the bmpmask matrix
%   VERSION 1.0 2013-06-04  for ALL TSI sites(1,2,3)
%       From BMP2SBMask_BNLC1 which is designed for C1 only.
%   Version 1.1 2013-09-02 remove inputf as input. inputf is replaced with bmpmaskorif which is
%   defined in this function
%   VERSION 2.0 2014-04-28 Add configfpath
function BMP2SBMask_BNL(outputf,site,configfpath)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STARTUP to get all possible parameter settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run(configfpath);   %mtsi_startup;

nsite =length(SITES);
for i=1:nsite
    if site==SITES{i};
        location=locations(i);
        AzumithShift=AzumithShifts(i);
        ZenithShift=ZenithShifts(i);
%         rcentrep=rcentreps(i);
%         cw=cws(i);
%         ch=chs(i);
%         crx=crxs(i);
%         cry=crys(i);
%         fo=fos(i);
%         h=hs(i);
%         r=rs(i);
        bmpmaskorif=BMPMASKORIFS{i};    % cell of image name
    end
end
lon=location.longitude;
lat=location.latitude;
alt=location.altitude;

filename=GetFilenameFromAbsPath(bmpmaskorif);
tmdv=GetDVFromImg(filename);
sun=INNER_GETAandZ(tmdv,lat,lon,alt);
az=sun.azimuth/180*pi+AzumithShift/180*pi;
ze=sun.zenith/180*pi+ZenithShift/180*pi;
ma1=rgb2gray(imread(bmpmaskorif));
bmpmask=ma1>10;
inputf=bmpmaskorif;
% site='c1';
save(outputf,'az','ze','site','inputf','outputf','bmpmask');

end