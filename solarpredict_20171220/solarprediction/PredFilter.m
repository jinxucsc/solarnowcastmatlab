% PredFilter is based on previous prediction results automatically add fix current results
% Version 1.0 2014-04-01
%   1. Add qc control inside

function newpredrad = PredFilter(predrad,realtimedv,localcorrectionDir)


%% parameter settings
NMIN=10;
NRECS=NMIN*6;
BPSN=25;
NMIN_dn=datenum([0 0 0 0 NMIN 0]);

localcorrectionDir=FormatDirName(localcorrectionDir);
if ~exist(localcorrectionDir,'dir')
    mkdir(localcorrectionDir);
end
allfiles=dir(localcorrectionDir);
[m,~]=size(allfiles);
realtimedn=datenum(realtimedv);
startdn=realtimedn-NMIN_dn;
for i=1:m
    filename=allfiles(i).name;
    tmdv=GetDVFromImg(filename);
    tmdn=datenum(tmdv);
    if(tmdn<startdn)
       continue; 
    end
    % Within range of local correction, load the lc file and return corrected prediction value
    absf=sprintf('%s%s',localcorrectionDir,filename);
    t=load(absf);
    newpredrad=t.offset+predrad;
    if (newpredrad>950) newpredrad=950;
    elseif (newpredrad<50) newpredrad=50;
    end
    return;
end

% Must generate local correction file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Database related
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REPLACE INTO %s values (%d,%s,%s);";
RealTBName='BPS_SEC_tmpfs';
TBName='BPS_Prediction_tmpfs';
TBName1='BPS_Prediction_full_tmpfs';
replacecmmd='REPLACE INTO %s values (%d,%s,%6.2f);';
insertcmmd='INSERT INTO %s values(%d,%s,%s,%6.2f,%6.2f);'; % this is to save all possible prediction
MySQLServerURL='solar.bnl.gov';
username='realtime_matlab';
password='realtime_matlab';
DBName='SolarFarm';
MySQLBinPath='';
Outputf='./data/insertprediction.log';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% MySQLcmmd is string format which requires cmmd and output file as inputs
MySQLcmmd=sprintf(...
    '"%s"mysql -P 3306 -h %s -u %s -p"%s" %s -e"%s" > %s',...
    MySQLBinPath,MySQLServerURL,username,password,DBName,'%s','%s');

%selectcmmd='SELECT BPS_N,time_stamp,SP2A_H_Avg FROM %s where time_stamp >= %s;';

selectcmmd= ['select BPS_Prediction_tmpfs.SP2A_H_Avg as predrad,BPS_SEC_tmpfs.SP2A_H_Avg as ' ...
'realrad,BPS_Prediction_tmpfs.time_stamp, BPS_Prediction_tmpfs.BPS_N from BPS_Prediction_tmpfs' ...
' join BPS_SEC_tmpfs on BPS_Prediction_tmpfs.time_stamp=BPS_SEC_tmpfs.time_stamp and ' ...
'BPS_Prediction_tmpfs.BPS_N=BPS_SEC_tmpfs.BPS_N where BPS_SEC_tmpfs.time_stamp>%s order by BPS_Prediction_tmpfs.time_stamp and BPS_Prediction_tmpfs.BPS_N;'];






%% Main
% Get last 10 minutes predrad/realrad

realtimedn=datenum(realtimedv);
startdn=realtimedn-NMIN_dn;
start_unix=(startdn-datenum(1970,1,1))*86400;
startdbts=start_unix-5*3600;

realcmmd=sprintf(selectcmmd,int2str(startdbts));
realoutf='r.dat';
cmmd=sprintf(MySQLcmmd,realcmmd,realoutf);
disp(cmmd);
dos(cmmd);



%File format
%   predrad	realrad	time_stamp	BPS_N
%   732.31	747.4	1396345915	1
%   724.53	19.02	1396345915	2
%   723.08	736.2	1396345915	3
format='%f	%f	%d	%d';

realrads=zeros(BPSN,NRECS); 
predrads=zeros(BPSN,NRECS); 
tss=zeros(1,NRECS); 
fh = fopen(realoutf,'r');
tline=fgets(fh); % skip first description row
tline=fgets(fh);

i=0;

% Get status of current stations
RADVALIDFILE='./bpsvalid.dat';
[qcstatus,tmdn] = realtime_qcvalid(realtimedv,RADVALIDFILE);

while(ischar(tline))
    A = sscanf(tline, format);
    if i==0||A(3)~=tss(i)
        i=i+1;
    end
    predrads(A(4),i)=A(1);
    realrads(A(4),i)=A(2);
    tss(i)=A(3);
    if(qcstatus(A(4))<=0)
        tss(i)=0;   % set as not valid since qcstatus of this station is not normal
    end
    tline=fgets(fh);
end
fclose(fh);
validindma=tss~=0;
predrads=predrads(:,validindma);
realrads=realrads(:,validindma);
tss=tss(validindma);
predrads_med=zeros(length(tss),1);
realrads_med=zeros(length(tss),1);
for i=1:length(tss)
    valsp=predrads(:,i);
    valsr=realrads(:,i);
    indma=(valsp>0&valsr>0);
    predrads_med(i)=mean(valsp(indma));
    realrads_med(i)=mean(valsr(indma));
end

raddiff=(realrads_med-predrads_med)';
offset=mean(raddiff(~isnan(raddiff)));
if isnan(offset)
    disp('[WARNING]: Training on local correction is not trusted: offset = 0!');
    offset=0;
end
%p = polyfit(predrads_med,realrads_med,1);
filename=GetImgFromDV_BNL(realtimedv,'tsi1');
outputf=sprintf('%s%s_localcorrect.mat',localcorrectionDir,filename(1:end-4));
save(outputf,'offset');
% use the latest to do local correction
%newpredrad=polyval(p,predrad);
newpredrad=predrad+offset;
if (newpredrad>950) newpredrad=950;
elseif (newpredrad<50) newpredrad=50;
end
end