% This function is to do qc control based on current BPSSEC.dat file
%   The BPSSEC.dat is in GMT/UTC format
% QCcode
%   1  normal
%   0   no rad available in this time range
%   -1 All the records are missing
%   -2 There some gaps/holes
%   -3 Not regular pattern(for instance )
%   Version 1.0 2014/04/09
%       Add qcdebugDir to store the default value
%   Version 1.1 2014/04/10
%       10 minutes for qc control
%   Version 1.2 2014/04/28
%       Add bStatus for error of conflict of reading radiation file or file doesn't exist
function bStatus=realtime_bpssec_qc(radfile,outputf,realtimedv)
qcdebugDir='./qc';
qcdebugDir=FormatDirName(qcdebugDir);
if(~exist(qcdebugDir,'dir'))
    mkdir(qcdebugDir);
end

RADFILE=radfile;
NORMRANGE=100;
realtimedn=datenum(realtimedv);
TFORMAT='yyyy-mm-dd HH:MM:SS';
BPSN=25;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% InsertPrediction()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REPLACE INTO %s values (%d,%s,%s);";
%TBName='BPS_SEC_tmpfs';
%MySQLServerURL='solar.bnl.gov';
%username='realtime_matlab';
%password='realtime_matlab';
%DBName='SolarFarm';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%TWOMINDN=datenum([0 0 0 0 2 0]);
DIFFDN=datenum([0 0 0 0 10 0]);    % scan last 10 minutes to do qc control
dns=[];
rads=[];

% selectcmmd_bps='select SP2A_H_Avg,time_stamp,BPS_N from %s where time_stamp >= %d and time_stamp < %d;';
% ts_gmt=TO_dn2unix(realtimedn);
% ts_est=ts_gmt-5*3600;
% ts_est_start=ts_est-DIFFDN;
% dbtable=TBName;
% 
% if (0~=mysql('status'))
%     mysql('open',MySQLServerURL,username,password);
%     mysql(['use ' DBName]);
% end
% cmmd=sprintf(selectcmmd_bps,dbtable,ts_est_start,ts_est);
% [allrads,allts,allstn]=mysql(cmmd);

try
    if ~exist(RADFILE,'file')
        bStatus = -1;
        return;
    end
    fid = fopen(RADFILE);
    tline = fgets(fid);
    while ischar(tline)
        C=strsplit(tline);
        tline = fgets(fid);
        dstr=C{1};
        timestr=C{2};
        tstr=[dstr ' ' timestr];
        tmdv=datevec(tstr,TFORMAT);
        tmdn=datenum(tmdv);
        if(abs(tmdn-realtimedn)<=DIFFDN)
            dns=[dns;tmdn];
            for i=1:BPSN
                rad=C{2+i};
                minrads(i)=str2double(rad);
            end
            rads=[rads;minrads];
        end
    end
    fclose(fid);
catch err
    disp(err.message);
    bStatus = -1;
    return;
end



% QCcode
%   1  normal
%   0   no rad available in this time range
%   -1 All the records are missing
%   -2 There some gaps/holes
%   -3 Not regular pattern(for instance )

qccodes=ones(1,25); %normal

meanvals=zeros(1,25);
if isempty(rads)
   qccodes(:) =0; % no rad available in this time range
else
    for i=1:BPSN
        tmrads=rads(:,i);
        if ~isempty(find(tmrads<=0,1))
            if isempty(find(tmrads>0,1))
                qccodes(i)=-1;
            else
                qccodes(i)=-2;
            end
        else
            meanvals(i)=mean(tmrads);
        end
    end
end

% There is one or more stations are working properly
% Use these values to pick outliters
if ~isempty(find(meanvals~=0,1))
    maxval=max(meanvals(meanvals~=0));
    minval=min(meanvals(meanvals~=0));
    if(maxval-minval>NORMRANGE) % if min - max is within 100 range, then treat it as normal    
        avgval=mean(meanvals(meanvals~=0));
        stdval=std(meanvals(meanvals~=0));
        ind= abs(meanvals-avgval)>3*stdval & meanvals~=0;
        qccodes(ind)=-3;
    end
end

% outlier detection by comparison on difference on mean with 3*std values
if ~isempty(find(qccodes<=0,1))
    tmf=GetImgFromDV_BNL(realtimedv,'tsi1');
    debugf=sprintf('%s%s.dat',qcdebugDir,tmf(1:end-4));
    fid = fopen(debugf,'a');    
    tmline=[datestr(realtimedv,TFORMAT) ' '];
    for i=1:BPSN
        tmline=sprintf('%s%f ',tmline,meanvals(i));
    end
    tmline=tmline(1:end-1);
    fprintf(fid,'%s\n',tmline);
    fclose(fid);
end


fid = fopen(outputf,'a');
tmline=[datestr(realtimedv,TFORMAT) ' '];
for i=1:BPSN
    tmline=[tmline int2str(qccodes(i)) ' '];
end
tmline=tmline(1:end-1);
fprintf(fid,'%s\n',tmline);
fclose(fid);
bStatus=1;  % normal status



end