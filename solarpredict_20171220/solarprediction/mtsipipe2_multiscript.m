%mtsipipe2_multiscript

% startdv=[2013 05 01 0 0 0];
% enddv=[2013 07 01 0 0 0];
% startdn=datenum(startdv);
% enddn=datenum(enddv);
odstartdv=[0 0 0 14 0 0];       % EST 9am
odenddv=[0 0 0 20 0 0];         % EST 3pm
odstartdn=datenum(odstartdv);
odenddn=datenum(odenddv);

availdvs=[
    2013 05 07 0 0 0;
    2013 05 14 0 0 0;
    2013 05 17 0 0 0;
    2013 05 20 0 0 0;
    2013 05 21 0 0 0;
    2013 05 22 0 0 0;
    2013 05 23 0 0 0;
    2013 05 24 0 0 0;
    2013 05 25 0 0 0;
    2013 05 26 0 0 0;
    2013 05 27 0 0 0;
    2013 05 28 0 0 0;
    2013 05 29 0 0 0;
    2013 05 30 0 0 0;
    2013 06 01 0 0 0;
    2013 06 02 0 0 0;
    2013 06 03 0 0 0;
    2013 06 04 0 0 0;
    2013 06 05 0 0 0;
    2013 06 06 0 0 0;
    2013 06 09 0 0 0;
    2013 06 14 0 0 0;
    2013 06 17 0 0 0;
    2013 06 20 0 0 0;
    2013 06 21 0 0 0;
    2013 06 22 0 0 0;
    2013 06 23 0 0 0;
    2013 06 24 0 0 0;
    ];

ONEDAY_DN=datenum([0 0 1 0 0 0]);
availdns=datenum(availdvs);
ndays=length(availdns);



configfpath='mtsi_startup'; % Configration file/function name
mode='server+skip';              % run on server or laptop
                            % add mode options here
cldmodelmatf='model_rbr.mat';   %cloud detection SVM model file
%bMulticore=false;          % multicore switcher
bMulticore=true;
matlabinstn=12;
servertype=64;


for i=1:ndays
    oddn=availdns(i);
    startdn=oddn+odstartdn;
    enddn=oddn+ONEDAY_DN;
    startdv=datevec(startdn);
    enddv=datevec(enddn);
    if bMulticore
        %mtsipipe2(startdv,enddv,mode,cldmodelmatf,configfpath)
        cmmd=sprintf('mtsipipe2(%s,%s,''%s'',''%s'',''%s'');',...
            TO_datevec2inputstr(startdv),...
            TO_datevec2inputstr(enddv),...
            mode,cldmodelmatf,configfpath);
        disp(cmmd);
        subprocess_matlab(cmmd,matlabinstn,servertype);
        pause(2);
    else
        mtsipipe2(startdv,enddv,mode,cldmodelmatf,configfpath);
    end
end














