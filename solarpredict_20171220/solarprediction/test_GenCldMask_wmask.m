function [imma_bg,bgind]=test_GenCldMask_wmask(imma,imma_mask)
r1=imma(:,:,1);
g1=imma(:,:,2);
b1=imma(:,:,3);
r2=imma_mask(:,:,1);
g2=imma_mask(:,:,2);
b2=imma_mask(:,:,3);
rbr1=double(r1)./double(b1);
rbr2=double(r2)./double(b2);
cldind=(rbr1>1.2*rbr2);
bgind=~cldind&~isnan(rbr1)&~isnan(rbr2);
r1(~bgind)=0;
g1(~bgind)=0;
b1(~bgind)=0;
imma_bg=imma;
imma_bg(:,:,1)=r1(:,:);
imma_bg(:,:,2)=g1(:,:);
imma_bg(:,:,3)=b1(:,:);

end