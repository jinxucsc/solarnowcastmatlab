% PIPE_Undistortion: Main Entrance of the pipeline of preprocessing. It
% needs to ClearSkyLibrary to do shadow band masking and holding arm
% masking.
%   IN:
%       startv  --   start date vector(2010,10,0,x,x,x)
%       endv    --   end date vector(2010,10,0,x,x,x)
%       DataDirectory   --  Directory where you put input data
%           data/input
%           data/output
%           data/sbmask
%           data/hamask
%       UndistortionParameters.mat(in)  --  related parameters
%       azimuth.mat(in) --  for clear sky library.IF NOT 'CL' mode
%               tsdn(clear sky timestamp),azs(sun azimuth),zes(sun zenith)
%       sbmaskDir(in)   --  'sbmask/' which contains all sb masks related
%                           to tsdn items
%       hamaskDir(in)   --  'hamask/' which contains all ha masks related
%                           to tsdn items
%   OUT:
%       outputDir/*     --  all the undistorted images
%
%   VERSION 1.1     2012-04-01
%   1.  Add new input freq(in seconds)

function PIPE_UndistortionwithCSL_BNL(startv,endv,DataDirectory,Freq)
load('UndistortionParameters.mat');     % load ALL the related parameters
sbazimuth=load('sbazimuth.mat');                % load all sbmask timenumbers and azumith
haazimuth=load('haazimuth.mat');                % load all hamask timenumbers and azumith
load('undist2ori.mat');             % matrix that contains all undist->ori mapping

% DataDirectory
if(DataDirectory(end)~='/')
    DataDirectory=sprintf('%s%s',DataDirectory,'/');
end
%   Data/input
%   Data/output
%   Data/sbmask
%   Data/hamask
inputimgDir=sprintf('%sinput/',DataDirectory);
outputimgDir=sprintf('%soutput/',DataDirectory);
sbmaskDir=sprintf('%ssbmask/',DataDirectory);
hamaskDir=sprintf('%shamask/',DataDirectory);
undistoutDir=outputimgDir;
undistoutDir='C:/TDDOWNLOAD/dataout/';
%strcmpi(PRO_MODE,'REMOVE')==1
%   Set start time vector and end time vector
starttimevec=startv;
% starttimevec(4)=0;
% starttimevec(5)=0;
% starttimevec(6)=0;
endtimevec=endv;
% endtimevec(4)=0;
% endtimevec(5)=0;
% endtimevec(6)=0;

%   Basic parameters and prerequisites checking
% hamaskDir='hamask/';
% sbmaskDir='sbmask/';
disp(SITE);
if(exist(inputimgDir,'dir')==0)
    disp('input data file does not exist!');
end
if(exist(sbmaskDir,'dir')==0)
    disp('WARNING: sbmask does not exist');
end
if(exist(hamaskDir,'dir')==0)
    disp('WARNING: hamask does not exist');
end
if(exist('azimuth.mat','file')==0)
    disp('WARNING: azimuth does not exist');
end
if(exist('UndistortionParameters.mat','file')==0)
    disp('ERROR: UndistortionParameters.mat doesnot exist!');
end
if(exist('undist2ori.mat','file')==0)
    disp('ERROR: undist2ori.mat doesnot exist!');
end
if(inputimgDir(end)~='/')
    inputimgDir=sprintf('%s%s',inputimgDir,'/');
end
if(undistoutDir(end)~='/')
    undistoutDir=sprintf('%s%s',undistoutDir,'/');
end
mkdir(undistoutDir);
holdma=zeros(Hi,Wi);
shadowma=zeros(Hi,Wi);
totalmaskma=zeros(Hi,Wi);
totalmaskma=logical(totalmaskma);
tmimgma=zeros(Hi,Wi);
tmimgma=uint8(tmimgma);
unimg=zeros(Hu,Wu,3);
unimg=uint8(unimg);
AZThres=4/180*pi;  % Inner Control parameter to consider within Azimuth Threshold masks only.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FreqTn=datenum([0 0 0 0 0 Freq]);
% tmstep=datenum([0,0,0,0,0,30]);       %tmstep -- time step of consecutive images.
tmstep=FreqTn;
%which is 30s by default Here can be changed to 1s for BNL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nfiles=0;
tmvec=starttimevec;
while(tmvec~=endtimevec)
    nfiles=nfiles+1;
    tmvec=tmvec+tmstep;
end
tmdn=datenum(starttimevec);
startdn=datenum(starttimevec);
enddn=datenum(endtimevec);
% testdn=datenum([2010 10 26 00 15 00]);
iter_i=1;       % for timestep increment
allfiles=dir(inputimgDir);
nfiles=1;
[NFILES,~]=size(allfiles);

imgfilename=allfiles(3).name;
tmfiledn=GetDVFromImg(imgfilename);
if(tmfiledn>=enddn)
    disp('NO match in startvec to endvec range');
    return;
else
    %     startdn=tmfiledn;
end
tmvec=GetDVFromImg(imgfilename);

lon=location.longitude;
lat=location.latitude;
alt=location.altitude;
sun=INNER_GETAandZ(tmvec,lat,lon,alt);
azshift=AzumithShift;
zeshift=ZenithShift;
tmimgma=[];
undist2orima=orim;
Wundist=Wu;
Hundist=Hu;
parfor i=3:NFILES  %remove first two directory name
    imgfilename=allfiles(i).name;
    tmvec=GetDVFromImg(imgfilename);
    tmfiledn=datenum(tmvec);
    if(tmfiledn<startdn||tmfiledn>enddn)
        %   Not in the range of requested time
        continue;
    end
%     disp('Debug: input valid');
    inputfilep=sprintf('%s%s',inputimgDir,imgfilename);
    outp=sprintf('%s%s.bmp',undistoutDir,imgfilename(1:end-4));
    %     if exist(outp,'file')~=0
    %         continue;
    %     end
    oriimg=imread(inputfilep);
    %     time.year    =  tmvec(1);
    %     time.month   =  tmvec(2);
    %     time.day     =  tmvec(3);
    %     time.hour    =  tmvec(4);
    %     time.min    =   tmvec(5);
    %     time.sec    =   tmvec(6);
    %     time.UTC    =   0;
    %     sun = sun_position(time, location);
    sun=INNER_GETAandZ(tmvec,lat,lon,alt);
    az=sun.azimuth/180*pi+azshift/180*pi;
    ze=sun.zenith/180*pi+zeshift/180*pi;
    %     az=sun.azimuth/180*pi
    %     ze=sun.zenith/180*pi;
    haindexi=GetAZIndex(haazimuth.azs,haazimuth.zes,az,ze);
    sbindexi=GetAZIndex_wThres(sbazimuth.azs,sbazimuth.zes,az,ze,AZThres);
    if(sbindexi==-1)
        disp(datestr(tmvec));
        continue;
    end
%     disp('Debug: sbmask and hamask valid');
    hatmdv=datevec(haazimuth.tsdn(haindexi));
    sbtmdv=datevec(sbazimuth.tsdn(sbindexi));
    az1=sbazimuth.azs(sbindexi);
    ze1=sbazimuth.zes(sbindexi);
    %         ii=indexi+2;    %   indexi=i+2 to avoid directory
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%
    % for TWP C1 format only. SGP, BNL may needs different img format
    hamaskfilename=GetImgFromDV_BNLC1(hatmdv);
    hamaskfilename=sprintf('%s%s',hamaskfilename,'.mat');
    sbmaskfilename=GetImgFromDV_BNLC1(sbtmdv);
    sbmaskfilename=sprintf('%s%s',sbmaskfilename,'.mat');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    hamaskf=sprintf('%s%s',hamaskDir,hamaskfilename);
    sbmaskf=sprintf('%s%s',sbmaskDir,sbmaskfilename);
    s=load(hamaskf, '-mat', 'hama');
    holdma=s.hama;  %   tmma is the final name of holding arm
    s=load(sbmaskf, '-mat', 'sbma');
    shadowma=s.sbma;
    totalmaskma=holdma&shadowma;
    totalmaskma=~totalmaskma;
    %     for j=1:3
    %         tmimgma=oriimg(:,:,j);
    %         tmimgma(totalmaskma)=0;
    %         oriimg(:,:,j)=tmimgma(:,:);
    %     end
    oriimg=INNER_MASK(oriimg,totalmaskma);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     [ix,iy]=CalImageCoorFrom_ALL(az,ze);
%     ix=round(ix);iy=round(iy);
%     if(iy<480&&ix<480&&ix>0&&iy>0)
%         oriimg(iy,ix,1)=255;
%         oriimg(iy,ix,2)=255;
%         oriimg(iy,ix,3)=255;
%         oriimg(iy-1,ix,1)=255;
%         oriimg(iy-1,ix,2)=255;
%         oriimg(iy-1,ix,3)=255;
%         oriimg(iy+1,ix,1)=255;
%         oriimg(iy+1,ix,2)=255;
%         oriimg(iy+1,ix,3)=255;
%         oriimg(iy,ix-1,1)=255;
%         oriimg(iy,ix-1,2)=255;
%         oriimg(iy,ix-1,3)=255;
%         oriimg(iy,ix+1,1)=255;
%         oriimg(iy,ix+1,2)=255;
%         oriimg(iy,ix+1,3)=255;
%     end
    
%     [ix,iy]=CalImageCoorFrom_ALL(az1,ze1);
%     ix=round(ix);iy=round(iy);
%     if(iy<480&&ix<480&&ix>0&&iy>0)
%         oriimg(iy,ix,1)=255;
%         oriimg(iy,ix,2)=0;
%         oriimg(iy,ix,3)=0;
%         oriimg(iy-1,ix,1)=255;
%         oriimg(iy-1,ix,2)=0;
%         oriimg(iy-1,ix,3)=0;
%         oriimg(iy+1,ix,1)=255;
%         oriimg(iy+1,ix,2)=0;
%         oriimg(iy+1,ix,3)=0;
%         oriimg(iy,ix-1,1)=255;
%         oriimg(iy,ix-1,2)=0;
%         oriimg(iy,ix-1,3)=0;
%         oriimg(iy,ix+1,1)=255;
%         oriimg(iy,ix+1,2)=0;
%         oriimg(iy,ix+1,3)=0;
%     end
    
    
    INNER_GETAandZ
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Undistortion Mapping here
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    unimg=uint8(INNER_Undist(oriimg,undist2orima,Wundist,Hundist));
    %     for Xu=1:Wu
    %         for Yu=1:Hu
    %             %                 if(Xu==240&&Yu==240)
    %             %                     Xu
    %             %                 end
    %             yo=round(orim(Yu,Xu,1));
    %             xo=round(orim(Yu,Xu,2));
    %             if(xo==0&&yo==0)
    %                 continue;
    %                 %                     unimg(Yu,Xu,1)=0;%black
    %                 %                     unimg(Yu,Xu,2)=0;
    %                 %                     unimg(Yu,Xu,3)=0;
    %             else
    %                 unimg(Yu,Xu,1)=oriimg(yo,xo,1);
    %                 unimg(Yu,Xu,2)=oriimg(yo,xo,2);
    %                 unimg(Yu,Xu,3)=oriimg(yo,xo,3);
    %             end
    %         end
    %     end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Output images. BMP files are generated here
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    outp=sprintf('%s%s.bmp',undistoutDir,imgfilename(1:end-4));
    imwrite(unimg,outp,'bmp');
end
end
