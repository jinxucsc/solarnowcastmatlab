% previous version is test_cldHdection
%   New version is based on modules
%   VERSION 2.0     2013-11-01
function mtsipipe2(startdv,enddv,mode,cldmodelmatf,configfpath)
run(configfpath);
% InnerBlockSize=10;
% ConnMinSize=10;
% ConnMaxSize=60;
% NextFrameMVSearchWinSize=15;
% H_sran_low=1000:100:5000;
% H_sran_high=5500:500:20000;
% H_sran_simple=[2000:500:5000 5500:1000:20000];
% H_sran=[H_sran_low H_sran_high];
% ncc_thres=0.6;
% ncc_thres_tsi32=2.1;        % ncc31,32 + ncc31,32 + ncc21,22
% ncc_cloudfield_trust=0.7;   % ncc value which consider the movement is reasonable for valid cloud field

% LOWCLDHTHRES = 5000;        % Treat as low altittude
% HREF_DIFF_TOLERANCE=500;    % difference to reference height will be tolerated or considered the same as reference
% MAXNUMOFLAYER=2;            % maximum layers for extraction
%%
bMulti=modepaser(mode,'multicore');
bServer=modepaser(mode,'server');
bPlot=modepaser(mode,'plot');
bSkip=modepaser(mode,'skip');
bRun=modepaser(mode,'run');
%bCldMatMod=modepaser(mode,'cldmatmod');
bPlotVisible=modepaser(mode,'visible');
if bPlotVisible
    set(0,'DefaultFigureVisible', 'on');
else
    set(0,'DefaultFigureVisible', 'off');
end
startdv_all=startdv;
enddv_all=enddv;
startdn_all=datenum(startdv_all);
enddn_all=datenum(enddv_all);

startdv(4:6)=0;
if isempty(find(enddv(4:6)~=0,1))
    enddv(3)=enddv(3)+1;
    enddv(4:6)=0;
    enddv=datevec(datenum(enddv));
else
    
end
startdn=datenum(startdv);
enddn=datenum(enddv);
%NCCFSOutMatDir='ncc_fs_all';
HRefDir_recheck='HistRef_recheck_1/';
HRefDir=HRefDir_recheck;
HRefDir_recheck=FormatDirName(HRefDir_recheck);
if ~exist(HRefDir,'dir')
    mkdir(HRefDir);
end
if ~exist(HRefDir_recheck,'dir')
    mkdir(HRefDir_recheck);
end


%NCCFSOutMatDir='ncc_fs_all_server/ncc_fs_all/'; % ncc full search for ncc3d_32 results using multicore
%RefOutMatDir='HRef_matout/'; % ncc full search for ncc3d_32 results using multicore
%NCCFSOutMatDir=FormatDirName(NCCFSOutMatDir);
% if ~exist(NCCFSOutMatDir,'dir')
%     mkdir(NCCFSOutMatDir);
% end
% RefOutMatDir=FormatDirName(RefOutMatDir);
% if ~exist(RefOutMatDir,'dir')
%     mkdir(RefOutMatDir);
% end








if bServer && bMulti
    nMatlabinst=32;
    severtype=64;
elseif  ~bServer && bMulti
    nMatlabinst=8;
    severtype=64;
end
if bServer
    dataDir='~/workdata/mtsi_stich/';
else
    dataDir='/data/workdata/mtsi_stich/';
end
dataDir=FormatDirName(dataDir);

% Get Avail mat files for database
mtsi_tsiavailf='mtsi_tsiavail.mat';
if ~exist(mtsi_tsiavailf,'file')
    tmdv=[2013 05 14 18 0 0];   % sample for extraction of directory
    f1=tmp_IsImgMatInDataDir(tmdv,TSI1,dataDir);    % only used for extraction of parent directory path
    tsi1Dir=GetDirPathFromPath(f1);
    [TSIavail,TSIavail_days]=TO_CheckTSIAvail(tsi1Dir);
    save('mtsi_tsiavail.mat','TSIavail','TSIavail_days');
else
    t=load(mtsi_tsiavailf);
    TSIavail=t.TSIavail;
    TSIavail_days=t.TSIavail_days;
end


% Generate the available/searching datenumbers
ndays=length(TSIavail_days);
ONEDAY_DN=datenum([0 0 1 0 0 0]);
ONEMIN_DN=datenum([0 0 0 0 1 0]);
ODSTART_DN=datenum([0 0 0 14 0 0]);
ODEND_DN=datenum([0 0 0 20 0 0]);
NSLOTS=round((ODEND_DN-ODSTART_DN)/ONEMIN_DN)+1;
oddns=zeros(ndays,NSLOTS);
for i=1:ndays
    odstartdn=datenum(TSIavail_days(i,:))+ODSTART_DN;
    for j=1:NSLOTS
        oddns(i,j)=odstartdn+(j-1)*ONEMIN_DN;
    end
end
% tmdn=startdn;
% oddns=[];
% i=0;
% while(tmdn<enddn)
%     %   tmdn=startdn+i*TENSECS_DN; %10s
%     tmdn=startdn+i*ONEMIN_DN;   % 1 minute
%     oddns=[oddns;tmdn];
%     i=i+1;
% end
% oddvs=datevec(oddns);
% oddns_n=length(oddns);



%HREF_PREF=[1200 3700];
for dayi=1:ndays
    tmdn=oddns(dayi,1);
    if tmdn<startdn || tmdn>enddn
        continue;
    end
    for i =1:NSLOTS-1
        tmdn=oddns(dayi,i);
        if tmdn<startdn_all || tmdn>enddn_all
            continue;
        end
        tmdv=datevec(tmdn);
        %tmdv_1faf=oddvs(i+1,:);
        tmdv_1faf=datevec(tmdn+TENSECS_DN);  % ten seconds is the next frame
        
        %tmdn_1faf=oddns(i+1);
        %if tmdn<datenum([2013 05 14 19 37 0])
        %if tmdn<datenum([2013 06 17 15 29 40])
        %if tmdn<datenum([2013 06 17 14 12 40]) % choose multilay cldmask
        %if tmdn<datenum([2013 06 09 17 13 5]) %choose for multi cld mask
        %if tmdn<datenum([2013 06 17 19 59 30]) % choose for exteme cloudy cldmask
        %if tmdn<datenum([2013 06 17 14 13 10]) %used for HRef output mat unittest
        %     if tmdn<datenum([2013 06 17 15 11 50]) % used for build skyfill.bmp
        %             continue;
        %         end
        %if tmdn<datenum([2013 06 17 14 12 00]) %for cldtrack
        %if tmdn<datenum([2013 06 17 17 34 50]) % for single multilayer filling
        %     if tmdn<datenum([2013 05 14 15 09 50]) % for multiple multilayer filling
        %         continue;
        %     end
        if 0~=rem(i,5)  % one minute
            continue;
        end
        
        
        
        
        f1=tmp_IsImgMatInDataDir(tmdv,TSI1,dataDir);
        f2=tmp_IsImgMatInDataDir(tmdv,TSI2,dataDir);
        f3=tmp_IsImgMatInDataDir(tmdv,TSI3,dataDir);
        f4=tmp_IsImgMatInDataDir(tmdv_1faf,TSI1,dataDir);
        f5=tmp_IsImgMatInDataDir(tmdv_1faf,TSI2,dataDir);
        f6=tmp_IsImgMatInDataDir(tmdv_1faf,TSI3,dataDir);
        
        if isempty(f1)||isempty(f2)||isempty(f3)||isempty(f4)||isempty(f5)||isempty(f6)
            dispstr=sprintf('%s is skipped since input mats (6 images) donot exist!',datestr(tmdv));
            disp(dispstr);
            continue;
        end
        
        tmf=GetImgFromDV_BNL(tmdv,TSI3);
        cldtrkmatoutf=sprintf('%s%s.cldtrk.mat',HRefDir,tmf(1:end-4));
        cldtrkmatoutf_full=sprintf('%s%s.cldtrk.full.mat',HRefDir,tmf(1:end-4));
        cldtrkmatoutf_2=sprintf('%s%s.cldtrk2.mat',HRefDir_recheck,tmf(1:end-4));
        cldtrkmatoutf_reg=sprintf('%s%s.cldtrkreg.mat',HRefDir_recheck,tmf(1:end-4));
        bSkipModules=false;
        if bSkip    % if need to skip
            if (exist(cldtrkmatoutf,'file') || exist(cldtrkmatoutf_full,'file')) && exist(cldtrkmatoutf_2,'file') && exist(cldtrkmatoutf_reg,'file')
                bSkipModules=true;
            end
        end
        disp(tmdv);
        
        %---------------------------------------------------------------------------------%
        % Module 1: Cloud Detection
        %---------------------------------------------------------------------------------%
        if ~bSkipModules
            [imma1_cldma_n,imma2_cldma_n,imma3_cldma_n]=...
                clouddetector(tmdv,cldmodelmatf,dataDir,configfpath);
            imma1_ori=tmp_GetImgMatFromDataDir(tmdv,TSI1,dataDir);
            imma2_ori=tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir);
            imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
            Red1 = imma3(:, :, 1);
            Green1 = imma3(:, :, 2);
            Blue1 = imma3(:, :, 3);
            HnRed1=imhist(Red1);
            HnGreen1=imhist(Green1);
            HnBlue1=imhist(Blue1);
            HnRed1(1)=0;HnGreen1(1)=0;HnBlue1(1)=0;     % 0 means not valid
            ref_sky_hist=[HnRed1 HnGreen1 HnBlue1];
            
            imma1=TO_histeq_TSISky(imma1_ori,ref_sky_hist);
            imma2=TO_histeq_TSISky(imma2_ori,ref_sky_hist);
            imma1_lum=single(TO_rgb2lum(imma1));
            imma2_lum=single(TO_rgb2lum(imma2));
            imma3_lum=single(TO_rgb2lum(imma3));
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % SkyColorTestOutDir plot for histeq based on imma3
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %     f1=figure();subplot(2,3,1);imshow(imma1);
        %     subplot(2,3,2);imshow(imma3);
        %     subplot(2,3,3);imshow(imma2);
        %     subplot(2,3,4);imshow(imma1_cldma);
        %     subplot(2,3,5);imshow(imma3_cldma);
        %     subplot(2,3,6);imshow(imma2_cldma);
        %     filename=GetImgFromDV_BNL(tmdv,TSI1);
        %     outputf=sprintf('%s%s',SkyColorTestOutDir,filename);
        %     print(f1,outputf,'-djpeg');
        %     continue;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % New Cloud Mask for imma using std
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %     f1=figure();subplot(2,3,1);imshow(imma1);
        %     subplot(2,3,2);imshow(imma3);
        %     subplot(2,3,3);imshow(imma2);
        %     subplot(2,3,4);imshow(imma1_cldma_n);
        %     subplot(2,3,5);imshow(imma3_cldma_n);
        %     subplot(2,3,6);imshow(imma2_cldma_n);
        %     filename=GetImgFromDV_BNL(tmdv,TSI1);
        %     outputf=sprintf('%s%s',SkyColorTestOutDir,filename);
        %     %print(f1,outputf,'-djpeg');
        %     continue;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %---------------------------------------------------------------------------------%
        % Module 2. Cloud fields extraction
        %---------------------------------------------------------------------------------%
        if ~bSkipModules
            [CFMaskMa,nCF]=cloudfieldext(imma3_cldma_n,configfpath);
        end
        
        
        
        %---------------------------------------------------------------------------------%
        % Module 3. History Reference
        %---------------------------------------------------------------------------------%
        %tmf=GetImgFromDV_BNL(tmdv,TSI1);
        if ~bSkipModules
            %[HRef,MVRef]=GetHistRefLayers(HRefDir,tmdv,configfpath);
            HRef=[1400 10000];
            MVRef=[];
        end
        
        %---------------------------------------------------------------------------------%
        % Module 4. Cloud Tracking
        %   If There is H reference, then do hist check else full search
        %---------------------------------------------------------------------------------%
        if ~bSkipModules
           CldTrack_2(HRef,MVRef,CFMaskMa,tmdv,dataDir,cldtrkmatoutf,configfpath)
        end
        return;
        %     if ~isempty(HRef)
        %         tmf=GetImgFromDV_BNL(tmdv,TSI3);
        %         cldtrkmatoutf=sprintf('%s%s.cldtrk.mat',HRefDir,tmf(1:end-4));
        %     else
        %         tmf=GetImgFromDV_BNL(tmdv,TSI3);
        %         cldtrkmatoutf=sprintf('%s%s.cldtrk.full.mat',HRefDir,tmf(1:end-4));
        %     end
        %     if ~exist(cldtrkmatoutf,'file')
        %     %if 1
        %         CldTrack(HRef,MVRef,CFMaskMa,tmdv,dataDir,cldtrkmatoutf,configfpath)
        %     end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Show cloud field full search results
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        t=load(cldtrkmatoutf);
        %t=load('cldtrk2_test.mat');
        H=t.H;
        Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
        Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
        HMV_x=t.HMV_x; HMV_y=t.HMV_y;
        MV_x=t.MV_x;    MV_y=t.MV_y;
        MAXCC=t.MAXCC;
        nConn=t.nCF;
        tmoutDir=sprintf('CldTrackTestout_2/');
        if ~exist(tmoutDir,'dir');
            mkdir(tmoutDir);
        end
        f1=figure();
        subplot(1,3,2);
        imshow(imma3);
        hold on;
        for tmi=1:nConn
            %         if MAXCC(tmi)==0
            %             continue;
            %         end
            
            minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
            recH=Conn_H(tmi); recW=Conn_W(tmi);
            if MAXCC(tmi)==0
                rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
            elseif H(tmi)<3000
                rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
                quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
            elseif H(tmi)>=3000
                rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
                quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
                quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
            end
            %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
            %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
            %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
            %text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H(tmi))],'FontSize',18);
        end
        hold off;
        subplot(1,3,3);
        imshow(imma2);
        hold on;
        for tmi=1:nConn
            if MAXCC(tmi)==0
                continue;
            end
            minsx=Conn_sx(tmi)+HMV_x(tmi,1); minsy=Conn_sy(tmi)+HMV_y(tmi,1);
            recH=Conn_H(tmi); recW=Conn_W(tmi);
            if H(tmi)<3000
                rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
            else
                rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
            end
            %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
            %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
        end
        hold off;
        subplot(1,3,1);
        imshow(imma1);
        hold on;
        for tmi=1:nConn
            if MAXCC(tmi)==0
                continue;
            end
            minsx=Conn_sx(tmi)+HMV_x(tmi,2); minsy=Conn_sy(tmi)+HMV_y(tmi,2);
            recH=Conn_H(tmi); recW=Conn_W(tmi);
            if H(tmi)<3000
                rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
            else
                rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
            end
            %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
            %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
        end
        hold off;
        tmoutf=sprintf('%s%s',tmoutDir,GetImgFromDV_BNL(tmdv,TSI1));
        print(f1,tmoutf,'-djpeg');
        continue;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %---------------------------------------------------------------------------------%
        % Module 5. Re-check CF based on neighbors on TSI3
        %---------------------------------------------------------------------------------%
        %cldtrkmatoutf=sprintf('%s%s.cldtrk.mat',HRefDir,tmf(1:end-4));
        if ~bSkipModules
            tsi3invalidma=imma3_lum==0;
            CldTrack_recheck(cldtrkmatoutf,cldtrkmatoutf_2,tsi3invalidma,configfpath);
        end
        %     if ~exist(cldtrkmatoutf_2,'file')
        %     %if 1
        %         CldTrack_recheck(cldtrkmatoutf,cldtrkmatoutf_2,tsi3invalidma,configfpath);
        %     end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Show cloud field recheck results
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %         t=load(cldtrkmatoutf_2);
        %     H=t.H;
        %     Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
        %     Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
        %     HMV_x=t.HMV_x; HMV_y=t.HMV_y;
        %     MAXCC=t.MAXCC;
        %     nConn=t.nCF;
        %     tmoutDir=sprintf('CldTrackTestout/');
        %     if ~exist(tmoutDir,'dir');
        %         mkdir(tmoutDir);
        %     end
        %     f1=figure();
        %     subplot(1,3,2);
        %     imshow(imma3);
        %     hold on;
        %     for tmi=1:nConn
        %                 if MAXCC(tmi)==0
        %                     continue;
        %                 end
        %
        %         minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
        %         recH=Conn_H(tmi); recW=Conn_W(tmi);
        %         if MAXCC(tmi)==0
        %             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
        %         elseif H(tmi)<3000
        %             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
        %             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
        %         elseif H(tmi)>=3000
        %             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
        %             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
        %             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
        %         end
        %         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
        %         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
        %         text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H(tmi))],'FontSize',18);
        %     end
        %     hold off;
        %     subplot(1,3,3);
        %     imshow(imma2);
        %     hold on;
        %     for tmi=1:nConn
        %         if MAXCC(tmi)==6
        %             continue;
        %         end
        %         minsx=Conn_sx(tmi)+HMV_x(tmi,1); minsy=Conn_sy(tmi)+HMV_y(tmi,1);
        %         recH=Conn_H(tmi); recW=Conn_W(tmi);
        %         if H(tmi)<3000
        %             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %         else
        %             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
        %         end
        %         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
        %     end
        %     hold off;
        %     subplot(1,3,1);
        %     imshow(imma1);
        %     hold on;
        %     for tmi=1:nConn
        %         if MAXCC(tmi)==0
        %             continue;
        %         end
        %         minsx=Conn_sx(tmi)+HMV_x(tmi,2); minsy=Conn_sy(tmi)+HMV_y(tmi,2);
        %         recH=Conn_H(tmi); recW=Conn_W(tmi);
        %         if H(tmi)<3000
        %             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %         else
        %             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
        %         end
        %         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
        %     end
        %     hold off;
        %     tmfilename=GetImgFromDV_BNL(tmdv,TSI1);
        %     tmoutf=sprintf('%s%s.recheck.jpg',tmoutDir,tmfilename(1:end-4));
        %     print(f1,tmoutf,'-djpeg');
        %     %continue;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %---------------------------------------------------------------------------------%
        % Module 6. Generate Layers of CF based on current results
        %       1. Use kmeans to find centoid of MAXNUMOFLAYER
        %       2. For each centroid area heights, vote for H and MV
        %       3. Aggregation of layers
        %---------------------------------------------------------------------------------%
        if ~bSkipModules
            CldTrack_Regulate(cldtrkmatoutf_2,cldtrkmatoutf_reg,tmdv,cldmodelmatf,dataDir,configfpath);
        end
        %     %if 1
        %     if ~exist(cldtrkmatoutf_reg,'file')
        %         CldTrack_Regulate(cldtrkmatoutf_2,cldtrkmatoutf_reg,tmdv,cldmodelmatf,dataDir,configfpath);
        %     end
        
        
        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Show cloud field recheck results
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if bPlot
            t=load(cldtrkmatoutf_2);
            H=t.H;
            Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
            Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
            HMV_x=t.HMV_x; HMV_y=t.HMV_y;
            MAXCC=t.MAXCC;
            nConn=t.nCF;
            t=load(cldtrkmatoutf_reg);
            H_Reg=t.H_Reg;  Conn_Reg=t.Conn_Reg;    CldC_Reg=t.CldC_Reg;
            MV_x_Reg=t.MV_x_Reg;    MV_y_Reg=t.MV_y_Reg;
            HMV_x_Reg=t.HMV_x_Reg;    HMV_y_Reg=t.HMV_y_Reg;
            RegH=t.RegH;    RegHMV_x=t.RegHMV_x;    RegHMV_y=t.RegHMV_y;
            RegMV_x=t.RegMV_x;  RegMV_y=t.RegMV_y;
            tmoutDir=sprintf('CldTrackLayers/');
            if ~exist(tmoutDir,'dir');
                mkdir(tmoutDir);
            end
            
            imma1_ori=tmp_GetImgMatFromDataDir(tmdv,TSI1,dataDir);
            imma2_ori=tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir);
            imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
            Red1 = imma3(:, :, 1);
            Green1 = imma3(:, :, 2);
            Blue1 = imma3(:, :, 3);
            HnRed1=imhist(Red1);
            HnGreen1=imhist(Green1);
            HnBlue1=imhist(Blue1);
            HnRed1(1)=0;HnGreen1(1)=0;HnBlue1(1)=0;     % 0 means not valid
            ref_sky_hist=[HnRed1 HnGreen1 HnBlue1];
            imma1=TO_histeq_TSISky(imma1_ori,ref_sky_hist);
            imma2=TO_histeq_TSISky(imma2_ori,ref_sky_hist);
            f1=figure();
            subplot(1,3,2);
            imshow(imma3);
            hold on;
            bShowText1=false;   bShowText2=false;
            for tmi=1:nConn
                %         if MAXCC(tmi)==0
                %             continue;
                %         end
                minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
                recH=Conn_H(tmi); recW=Conn_W(tmi);
                if RegH(tmi)==0
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
                elseif RegH(tmi)==H_Reg(1)
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                    quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
                    quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
                    if bShowText1==false
                        text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
                        bShowText1=true;
                    end
                elseif RegH(tmi)==H_Reg(2)
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
                    quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
                    quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
                    if bShowText2==false
                        text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(RegH(tmi))],'FontSize',18);
                        bShowText2=true;
                    end
                end
                %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
                %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
                
            end
            hold off;
            subplot(1,3,3);
            imshow(imma2);
            hold on;
            for tmi=1:nConn
                if RegH(tmi)==0
                    continue;
                end
                minsx=Conn_sx(tmi)+RegHMV_x(tmi,1); minsy=Conn_sy(tmi)+RegHMV_y(tmi,1);
                recH=Conn_H(tmi); recW=Conn_W(tmi);
                if RegH(tmi)==H_Reg(1)
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                else
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
                end
                %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
            end
            hold off;
            subplot(1,3,1);
            imshow(imma1);
            hold on;
            for tmi=1:nConn
                if RegH(tmi)==0
                    continue;
                end
                minsx=Conn_sx(tmi)+RegHMV_x(tmi,2); minsy=Conn_sy(tmi)+RegHMV_y(tmi,2);
                recH=Conn_H(tmi); recW=Conn_W(tmi);
                if RegH(tmi)==H_Reg(1)
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                else
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
                end
                %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
            end
            hold off;
            tmfilename=GetImgFromDV_BNL(tmdv,TSI1);
            tmoutf=sprintf('%s%s.jpg',tmoutDir,tmfilename(1:end-4));
            print(f1,tmoutf,'-djpeg');
        end
        %continue;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %continue;   %no cld check on TSI1 and TSI2
        
        
        
        
        
        
        %---------------------------------------------------------------------------------%
        % Module 7. Generate CF on TSI1, TSI2, and assign layers info to them
        %       1. Divide into CFs for the rest of cloud masks on TSI1 and TSI2
        %       2. Sim check only for motion vectors
        %       3. If no-match, follow the major layer
        %---------------------------------------------------------------------------------%
        cldtrk13matoutf=sprintf('%s%s.cldtrk13.mat',HRefDir_recheck,tmf(1:end-4));
        if tmdn<datenum([2013 05 14 19 30 0])
            continue;
        end
        if 1
        %if ~bSkipModules
            cloudtrack_otherTSI(cldtrkmatoutf_2,cldtrkmatoutf_reg,cldtrk13matoutf,tmdv,cldmodelmatf,dataDir,configfpath);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Show cloud field recheck results
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %if bPlot
            if 1
                % load 3 results
                [imma1_cldma_n,imma2_cldma_n,imma3_cldma_n]= clouddetector(tmdv,cldmodelmatf,dataDir,configfpath);
                imma1_ori=tmp_GetImgMatFromDataDir(tmdv,TSI1,dataDir);
                imma2_ori=tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir);
                imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
                Red1 = imma3(:, :, 1);
                Green1 = imma3(:, :, 2);
                Blue1 = imma3(:, :, 3);
                HnRed1=imhist(Red1);
                HnGreen1=imhist(Green1);
                HnBlue1=imhist(Blue1);
                HnRed1(1)=0;HnGreen1(1)=0;HnBlue1(1)=0;     % 0 means not valid
                ref_sky_hist=[HnRed1 HnGreen1 HnBlue1];
                imma1=TO_histeq_TSISky(imma1_ori,ref_sky_hist);
                imma2=TO_histeq_TSISky(imma2_ori,ref_sky_hist);
                imma1_lum=single(TO_rgb2lum(imma1));
                imma2_lum=single(TO_rgb2lum(imma2));
                imma3_lum=single(TO_rgb2lum(imma3));
                
                t=load(cldtrkmatoutf_2);
                H=t.H;
                Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
                Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
                HMV_x=t.HMV_x; HMV_y=t.HMV_y;
                MAXCC=t.MAXCC;
                nConn=t.nCF;
                t=load(cldtrkmatoutf_reg);
                H_Reg=t.H_Reg;  Conn_Reg=t.Conn_Reg;    CldC_Reg=t.CldC_Reg;
                MV_x_Reg=t.MV_x_Reg;    MV_y_Reg=t.MV_y_Reg;
                HMV_x_Reg=t.HMV_x_Reg;    HMV_y_Reg=t.HMV_y_Reg;
                RegH=t.RegH;    RegHMV_x=t.RegHMV_x;    RegHMV_y=t.RegHMV_y;
                RegMV_x=t.RegMV_x;  RegMV_y=t.RegMV_y;
                t=load(cldtrk13matoutf);
                MV_x1=t.MV_x1;  MV_x2=t.MV_x2;
                MV_y1=t.MV_x1;  MV_y1=t.MV_y1;
                nCF1=t.nCF1;    nCF2=t.nCF2;
                Conn_sx1=t.Conn_sx1; Conn_sx2=t.Conn_sx2;
                Conn_sy1=t.Conn_sy1; Conn_sy2=t.Conn_sy2;
                Conn_H1=t.Conn_H1; Conn_H2=t.Conn_H2;
                Conn_W1=t.Conn_W1; Conn_W2=t.Conn_W2;
                HMV_x1=t.HMV_x1; HMV_x2=t.HMV_x2;
                HMV_y1=t.HMV_x1; HMV_y2=t.HMV_y2;
                H1=t.H1;    H2=t.H2;
                
                imma1_ori_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI1,dataDir);
                imma2_ori_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI2,dataDir);
                %imma3_ori_3faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI3,dataDir);
                
                imma1_1faf=TO_histeq_TSISky(imma1_ori_1faf,ref_sky_hist);
                imma2_1faf=TO_histeq_TSISky(imma2_ori_1faf,ref_sky_hist);
                %imma3_1faf=TO_histeq_TSISky(imma3_ori_1faf,ref_sky_hist);
                
                imma1_lum_1faf=single(TO_rgb2lum(imma1_1faf));
                imma2_lum_1faf=single(TO_rgb2lum(imma2_1faf));
                %imma3_lum=single(TO_rgb2lum(imma3));
                
                
                
                
                tmoutDir=sprintf('CldTrackLayers_12/');
                if ~exist(tmoutDir,'dir');
                    mkdir(tmoutDir);
                end
                f1=figure();
                subplot(1,3,2);
                imshow(imma3);
                hold on;
                bShowText1=false;   bShowText2=false;
                for tmi=1:nConn
                    minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
                    recH=Conn_H(tmi); recW=Conn_W(tmi);
                    if RegH(tmi)==0
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','c');
                    elseif RegH(tmi)==H_Reg(1)
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','r');
                        quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
                        quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
                        if bShowText1==false
                            text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
                            bShowText1=true;
                        end
                    elseif RegH(tmi)==H_Reg(2)
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
                        quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
                        quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
                        if bShowText2==false
                            text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(RegH(tmi))],'FontSize',18);
                            bShowText2=true;
                        end
                    end
                end
                
                for tmi=1:nCF1
                    minsx=Conn_sx1(tmi)-HMV_x1(tmi,2); minsy=Conn_sy1(tmi)-HMV_y1(tmi,2);
                    recH=Conn_H1(tmi); recW=Conn_W1(tmi);
                    if H1(tmi)==0
                        rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','c');
                    elseif H1(tmi)==H_Reg(1)
                        rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','r');
                        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x1(tmi,1),HMV_y1(tmi,1));
                        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x1(tmi,2),HMV_y1(tmi,2));
                        if bShowText1==false
                            text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
                            bShowText1=true;
                        end
                    elseif H1(tmi)==H_Reg(2)
                        rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','y');
                        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x1(tmi,1),HMV_y1(tmi,1));
                        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x1(tmi,2),HMV_y1(tmi,2));
                        if bShowText2==false
                            text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(H1(tmi))],'FontSize',18);
                            bShowText2=true;
                        end
                    end
                end
                for tmi=1:nCF2
                    minsx=Conn_sx2(tmi)-HMV_x2(tmi,1); minsy=Conn_sy2(tmi)-HMV_y2(tmi,1);
                    recH=Conn_H2(tmi); recW=Conn_W2(tmi);
                    if H2(tmi)==0
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
                    elseif H2(tmi)==H_Reg(1)
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x2(tmi,1),HMV_y2(tmi,1));
                        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x2(tmi,2),HMV_y2(tmi,2));
                        if bShowText1==false
                            text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
                            bShowText1=true;
                        end
                    elseif H2(tmi)==H_Reg(2)
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
                        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x2(tmi,1),HMV_y2(tmi,1));
                        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x2(tmi,2),HMV_y2(tmi,2));
                        if bShowText2==false
                            text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(H2(tmi))],'FontSize',18);
                            bShowText2=true;
                        end
                    end
                end
                hold off;
                
                
                subplot(1,3,3);
                imshow(imma2);
                hold on;
                for tmi=1:nConn
                    if RegH(tmi)==0
                        continue;
                    end
                    minsx=Conn_sx(tmi)+RegHMV_x(tmi,1); minsy=Conn_sy(tmi)+RegHMV_y(tmi,1);
                    recH=Conn_H(tmi); recW=Conn_W(tmi);
                    if RegH(tmi)==H_Reg(1)
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','r');
                    else
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','y');
                    end
                    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
                end
                for tmi=1:nCF1
                    if H1(tmi)==0
                        continue;
                    end
                    minsx=Conn_sx1(tmi)-HMV_x1(tmi,2)+HMV_x1(tmi,1); minsy=Conn_sy1(tmi)-HMV_y1(tmi,2)+HMV_y1(tmi,1);
                    recH=Conn_H1(tmi); recW=Conn_W1(tmi);
                    if H1(tmi)==H_Reg(1)
                        rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','r');
                    else
                        rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','y');
                    end
                    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
                end
                for tmi=1:nCF2
                    if H2(tmi)==0
                        continue;
                    end
                    minsx=Conn_sx2(tmi); minsy=Conn_sy2(tmi);
                    recH=Conn_H2(tmi); recW=Conn_W2(tmi);
                    if H2(tmi)==H_Reg(1)
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                    else
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
                    end
                    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
                end
                hold off;
                subplot(1,3,1);
                imshow(imma1);
                hold on;
                for tmi=1:nConn
                    if RegH(tmi)==0
                        continue;
                    end
                    minsx=Conn_sx(tmi)+RegHMV_x(tmi,2); minsy=Conn_sy(tmi)+RegHMV_y(tmi,2);
                    recH=Conn_H(tmi); recW=Conn_W(tmi);
                    if RegH(tmi)==H_Reg(1)
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','r');
                    else
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','y');
                    end
                    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
                end
                
                for tmi=1:nCF1
                    if H1(tmi)==0
                        continue;
                    end
                    minsx=Conn_sx1(tmi); minsy=Conn_sy1(tmi);
                    recH=Conn_H1(tmi); recW=Conn_W1(tmi);
                    if H1(tmi)==H_Reg(1)
                        rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','r');
                    else
                        rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','y');
                    end
                    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
                end
                for tmi=1:nCF2
                    if H2(tmi)==0
                        continue;
                    end
                    minsx=Conn_sx2(tmi)-HMV_x2(tmi,1)+HMV_x2(tmi,2); minsy=Conn_sy2(tmi)-HMV_y2(tmi,1)+HMV_y2(tmi,2);
                    recH=Conn_H2(tmi); recW=Conn_W2(tmi);
                    if H2(tmi)==H_Reg(1)
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                    else
                        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
                    end
                    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
                end
                hold off;
                tmfilename=GetImgFromDV_BNL(tmdv,TSI1);
                tmoutf=sprintf('%s%s.jpg',tmoutDir,tmfilename(1:end-4));
                print(f1,tmoutf,'-djpeg');
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end
        
        continue;   % next timestamp
    end
end
