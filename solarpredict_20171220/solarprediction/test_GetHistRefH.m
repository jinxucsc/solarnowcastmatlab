function [MV_Href,MVx_class,MVy_class,MV_classcount]=test_GetHistRefH(dataDir,cldmodelmatf,tmdvs,NCCFSOutMatDir)
HRef=0; MV_Href=0;MVx_class=0; MVy_class=0; MV_classcount=0;
% StdBlockSize=11;
% StdBlockHalfSize=floor(StdBlockSize/2);
% StdCldThres=5;
% ConnMinSize=10;
% ConnMaxSize=60;
% NextFrameMVSearchWinSize=15;
% LOWCLDHTHRES = 5000;        % Treat as low altittude
% ncc_cloudfield_trust=0.7;
% H_sran_low=1000:100:5000;
% H_sran_high=5500:500:20000;
% %H_sran_simple=[2000:500:5000 5500:1000:20000];
% H_sran=[H_sran_low H_sran_high];
% H_range=H_sran;
InnerBlockSize=10;
StdBlockSize=11;
StdBlockHalfSize=floor(StdBlockSize/2);
StdCldThres=5;
ConnMinSize=10;
ConnMaxSize=60;
NextFrameMVSearchWinSize=15;
H_sran_low=1000:100:5000;
H_sran_high=5500:500:20000;
H_sran_simple=[2000:500:5000 5500:1000:20000];
H_sran=[H_sran_low H_sran_high];
ncc_thres=0.5;
ncc_thres_tsi32=2.1;        % ncc31,32 + ncc31,32 + ncc21,22
ncc_cloudfield_trust=0.7;   % ncc value which consider the movement is reasonable for valid cloud field
LOWCLDHTHRES = 5000;        % Treat as low altittude
HREF_DIFF_TOLERANCE=500;    % difference to reference height will be tolerated or considered the same as reference

H_range=H_sran;
mtsi_startup_mini;

t=load(cldmodelmatf);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
model=t.model;
x1_min=t.x1_min;
x1_max=t.x1_max;
x2_min=t.x2_min;
x2_max=t.x2_max;
x3_min=t.x3_min;
x3_max=t.x3_max;
x4_min=t.x4_min;
x4_max=t.x4_max;
h=t.h;
w_1=model.SVs'*model.sv_coef;b_1=-model.rho;
% predicted_labels=svmpredict_li(x_valid,w_1,b_1);
%     predicted_labels(predicted_labels==1)=0;        % cld
%     predicted_labels(predicted_labels==-1)=1;       % sky
x_min=[x1_min x2_min x3_min x4_min];
x_max=[x1_max x2_max x3_max x4_max];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tmdns=datenum(tmdvs);
FRAME_N=length(tmdns);
nConn_MAX=100;
MV_x=zeros(nConn_MAX,FRAME_N); MV_y=zeros(nConn_MAX,FRAME_N);
HMV_x=zeros(nConn_MAX,FRAME_N); HMV_y=zeros(nConn_MAX,FRAME_N);
H32=zeros(nConn_MAX,FRAME_N);     MAXCC=zeros(nConn_MAX,FRAME_N);
Conn_H=zeros(nConn_MAX,FRAME_N); Conn_W=zeros(nConn_MAX,FRAME_N);
Conn_sx=zeros(nConn_MAX,FRAME_N);Conn_sy=zeros(nConn_MAX,FRAME_N);
Conn_cldcount=zeros(nConn_MAX,FRAME_N);
bhistframe=true;
cc_all=cell(nConn_MAX,FRAME_N);
for fi=1:FRAME_N
    tmdv=tmdvs(fi,:);
    try
        imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
    catch
        bhistframe=false;
        break;
    end
    xtrain3=tmp_img2traindata_rbr(imma3,h,x_min,x_max);
    pl3=svmpredict_li(xtrain3,w_1,b_1);
    pl3(pl3==1)=0;        % cld
    pl3(pl3==-1)=1;       % sky
    imma3_cldma=false(UNDISTH,UNDISTW);
    imma3_cldma(:)=~pl3;
    imma3_lum=single(TO_rgb2lum(imma3));
    [imma3_std,imma3_std_invalid]=tmp_GetImgStdMatFromDataDir(tmdv,TSI3,StdBlockSize,dataDir);
    imma3_std(~imma3_std_invalid)=0;
    imma3_cldma_n=(imma3_std>StdCldThres)|(imma3_cldma&imma3_std_invalid);
    [ConnLabelma1,nConn1]=bwlabel(imma3_cldma_n);
    [ConnLabelma,nConn]=bwlabel_adjust(ConnLabelma1,ConnMinSize,ConnMaxSize);
    [ConnLabelma,nConn]=bwlabel_adjust(ConnLabelma,ConnMinSize,ConnMaxSize);
    
    for tmi=1:nConn
        tmf=GetImgFromDV_BNL(tmdv,TSI1);
        tmind=find(ConnLabelma==tmi);
        Conn_cldcount(tmi,fi)=length(tmind);
        [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
        minsy=min(y);
        minsx=min(x);
        recH=max(y)-min(y)+1;
        recW=max(x)-min(x)+1;
        Conn_sx(tmi,fi)=minsx;Conn_sy(tmi,fi)=minsy;
        Conn_H(tmi,fi)=recH;Conn_W(tmi,fi)=recW;
        tmoutf=sprintf('%s_%d.mat',tmf(1:end-4),tmi);
        outmatf=sprintf('%s%s',NCCFSOutMatDir,tmoutf);
        t=load(outmatf);
        H32(tmi,fi)=t.H;
        MV_x(tmi,fi)=t.mv_x; MV_y(tmi,fi)=t.mv_y;
        HMV_x(tmi,fi)=t.HMV_x; HMV_y(tmi,fi)=t.HMV_y;
        MAXCC(tmi,fi)=t.maxcc;
        cc_all{tmi,fi}=t.cc_all;
    end
end

if bhistframe==false
    tmf=GetImgFromDV_BNL(tmdv,TSI3);
    dispstr=sprintf('Can not get image file %s under %s.',tmf,dataDir);
    disp(dispstr);
    return;
end

% Motion vector is 0, don't trust them as reference
invalidma=MV_x==0&MV_y==0;
H32(invalidma)=0;
H_w=H32.*Conn_cldcount;
H_h_indma=H32>LOWCLDHTHRES;
H_l_indma=H32<=LOWCLDHTHRES;
Href_h=sum(H_w(H_h_indma))/sum(Conn_cldcount(H_h_indma));   % high cloud just choose average H

tmhvals=H32(H_l_indma);
tmhcounts=Conn_cldcount(H_l_indma);
x=[];
for i=1:length(tmhvals);
   tmx=zeros(Conn_cldcount(i),1); 
   tmx(:)=tmhvals(i);
   x=[x;tmx];
end
x_nonzero=sort(x(x~=0));

% classlabel=kmeans(x_nonzero,2,'start',[1100;3700]);
% cenp(1)=sum(x_nonzero(classlabel==1))/length(find(classlabel==1));
% cenp(2)=sum(x_nonzero(classlabel==2))/length(find(classlabel==2));
[cenp, ~, ~] =   WCSSKmeans(x_nonzero, 2, 500, 500);
cenp=sort(cenp);

Href_l1=cenp(1);
Href_l2=cenp(2);

if abs(Href_l1-Href_l2)<1000
    disp('Two low layers can be merged into one layer');
end

HRef=[Href_l1 Href_l2 Href_h];
HRef=tmp_FindMostSimHRan(HRef,H_range);
%HRef=round(HRef/100)*100;
Href_l1=HRef(1);    Href_l2=HRef(2);    Href_h=HRef(3);
H_major=zeros(nConn_MAX,FRAME_N);
MV_y_major=zeros(nConn_MAX,FRAME_N);   MV_x_major=zeros(nConn_MAX,FRAME_N);
for fi=1:FRAME_N
    for tmi=1:nConn
        if H32(tmi,fi)==0
            continue;
        end
        cc_ma=cc_all{tmi,fi};
        tempSize=[Conn_H(tmi,fi) Conn_W(tmi,fi)];
        ASize=[2*NextFrameMVSearchWinSize+Conn_H(tmi,fi) 2*NextFrameMVSearchWinSize+Conn_W(tmi,fi)];
        if H_h_indma(tmi,fi)
            [MV_ref,maxcc,~]=tmp_FindMVfromCC(cc_ma,tempSize,ASize,H_range,Href_h);
            H_major(tmi,fi)=Href_h;
        elseif abs(H32(tmi,fi)-Href_l1)>abs(H32(tmi,fi)-Href_l2)
            [MV_ref,maxcc,~]=tmp_FindMVfromCC(cc_ma,tempSize,ASize,H_range,Href_l2);
            H_major(tmi,fi)=Href_l2;
        else
            [MV_ref,maxcc,~]=tmp_FindMVfromCC(cc_ma,tempSize,ASize,H_range,Href_l1);
            H_major(tmi,fi)=Href_l1;
        end
        if maxcc>ncc_thres_tsi32
            MV_y_major(tmi,fi)=MV_ref(1); MV_x_major(tmi,fi)=MV_ref(2);
        else
            H_major(tmi,fi)=0;
        end
    end
end

Href_l1_ind=find(H_major==Href_l1);
Href_l2_ind=find(H_major==Href_l2);
Href_h_ind=find(H_major==Href_h);
[MVx_l1_class,MVy_l1_class,MV_l1_classcount,MV_l1_nclass]=...
    findConnMVClass(MV_x_major,MV_y_major,Href_l1_ind,Conn_cldcount);
[MVx_l2_class,MVy_l2_class,MV_l2_classcount,MV_l2_nclass]=...
    findConnMVClass(MV_x_major,MV_y_major,Href_l2_ind,Conn_cldcount);
[MVx_H_class,MVy_H_class,MV_H_classcount,MV_H_nclass]=...
    findConnMVClass(MV_x_major,MV_y_major,Href_h_ind,Conn_cldcount);
MVx_class=[MVx_l1_class;MVx_l2_class;MVx_H_class];
MVy_class=[MVy_l1_class;MVy_l2_class;MVy_H_class];
MV_classcount=[MV_l1_classcount;MV_l2_classcount;MV_H_classcount];

MV_Href=[repmat(Href_l1,MV_l1_nclass,1);repmat(Href_l2,MV_l2_nclass,1);repmat(Href_h,MV_H_nclass,1)];







end