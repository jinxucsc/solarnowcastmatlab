% Main function to run forecasting
% VERSION 1.0 2014-05-19
function realtime_mtsipipe_GenDataset_endless(configfpath)
% check the filled image and use filled image to do prediction
%configfpath='mtsi_startup';
if nargin==0
    configfpath='mtsi_startup';
end
run(configfpath);
TO_setRealtimeModuleStatus(TO_getmodulename(),TO_str2modstate('normal'),datevec(now),ModuleStatusRecFile);
if bWriteDiary
    diary(GendataDiaryFile);
end
DNDIFF = datenum([0 0 0 0 10 0]); % consider only 15 minutes ahead
set_dn=java.util.HashSet;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Logger setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
logfile=GendataLogFile;
logger=log4m.getLogger(logfile);
loggerID='realtime_gendata';
logger.setLogLevel(logger.DEBUG);
logger.setCommandWindowLevel(logger.DEBUG);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
TSI3FilledDir=FormatDirName(TSI3FilledDir);
SVRmodelObj=load(SVRModelFile);
daydn_pre = 0;
try
%     while(1)
%         files=dir(TSI3FilledDir);
%         N=size(files,1);
%         utc_time = java.lang.System.currentTimeMillis;
%         a=utc_time/1000; % conver to UNIX TIMESTAMP
%         nowdn=(a/86400 + datenum(1970,1,1)); % UTC now datenum
%         daydv=datevec(nowdn); daydv(4:6)=0; daydn = datenum(daydv);
%         if daydn_pre ~= daydn
%             daydn_pre = daydn;  % UTC day has changed then update clear sky values
%             csvals_oneday = TO_getBPSCSVal(daydv,BPSClearSkyMatFile,'sec');
%         end
%         startdn=nowdn-DNDIFF;
%         tmdns=zeros(N,1);
%         indice=zeros(N,1);
%         for i=1:N
%             filename=files(i).name;
%             tmdv=GetDVFromImg(filename);
%             if tmdv==0
%                 continue;
%             end
%             tmdn=datenum(tmdv);
%             
%             if tmdn<startdn || set_dn.contains(tmdn)
%                 continue; % in range then start the
%             end
%             tmdns(i)=tmdn;
%             indice(i)=i;
%         end
%         tmind=tmdns~=0;
%         tmdns=tmdns(tmind);
%         indice=indice(tmind);
%         tmdns=sort(unique(tmdns),'descend');
%         l=length(tmdns);
%         if l==0
%             warning(sprintf('No new file at %s',datestr(nowdn)));
%             pause(2);
%             continue;
%         end
%         
%         tmdn=tmdns(1);
%         tmdv=datevec(tmdn);
%         logger.debug(loggerID,sprintf('Start Forecast at timestamp = %s',datestr(tmdv)));
%         realtime_mtsipipe_GenDataset(tmdv,ForecastTimeRange,csvals_oneday,SVRmodelObj,configfpath,logger);
%         set_dn.add(tmdn);
%     end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Compatable ver. starts here
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    while(1)
        utc_time = java.lang.System.currentTimeMillis;
        a=utc_time/1000; % conver to UNIX TIMESTAMP
        nowdn=(a/86400 + datenum(1970,1,1)); % UTC now datenum
        tmdv = datevec(nowdn - TENSECS_DN); % Real forecast starts from 10 seconds ahead
        daydv=datevec(nowdn); daydv(4:6)=0; daydn = datenum(daydv);
        if daydn_pre ~= daydn
            daydn_pre = daydn;  % UTC day has changed then update clear sky values
            csvals_oneday = TO_getBPSCSVal(daydv,BPSClearSkyMatFile,'sec');
        end
        tmdn=datenum(tmdv);
        logger.debug(loggerID,sprintf('Start Compatable Forecast at timestamp = %s',datestr(tmdv)));
        realtime_mtsipipe_GenDataset(tmdv,ForecastTimeRange,csvals_oneday,SVRmodelObj,configfpath,logger);
        set_dn.add(tmdn);
        pause(10);
    end
catch err
    logger.error(loggerID,sprintf('%s--line %d  msg:%s',err.stack(1).name,err.stack(1).line,err.message));    
    if bWriteDiary
        diary off;
    end
    TO_setRealtimeModuleStatus(TO_getmodulename(),TO_str2modstate('error'),datevec(now),ModuleStatusRecFile);
    return;
end


end