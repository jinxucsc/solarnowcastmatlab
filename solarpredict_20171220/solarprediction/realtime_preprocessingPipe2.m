% realtime_preprocessingPipe2 is to do preprocessing for 3 TSIs
%   Current settings for otsidir and utsidir are set in mtsi_startup;
%   VERSION 1.0 2014/04/28
function realtime_preprocessingPipe2(realtimedv,SBHAROOTDir,configfpath,logger)
% Set up loggerID
run(configfpath);   %mtsi_startup;
loggerID=TO_getloggerID();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Logger setup for multicore
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% if ~exist('logger','var');
%     logfile='./log/realtime_preprocessing.log';
%     logger=log4m.getLogger(logfile);
%     logger.setLogLevel(logger.DEBUG);
%     logger.setCommandWindowLevel(logger.DEBUG);
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nsite =length(SITES);
for i=1:nsite
    site=SITES{i};
    otsiDir=otsiDirs{i};
    utsiDir=utsiDirs{i};
    %logger.debug(loggerID,['Start preprocessing of site = ' site]);
    realtime_preprocessingPipe(realtimedv,SBHAROOTDir,otsiDir,utsiDir,site,configfpath,logger);
end


% oTSIRootDir=FormatDirName(oTSIRootDir);
% otsi1Dir=[oTSIRootDir 'oTSI1'];
% fliptsi1Dir=[oTSIRootDir 'filpTSI1'];
% utsi1Dir=[oTSIRootDir 'tsi1_undist_mat'];
% otsi2Dir=[oTSIRootDir 'oTSI2'];
% fliptsi2Dir=[oTSIRootDir 'filpTSI2'];
% utsi2Dir=[oTSIRootDir 'tsi2_undist_mat'];    
% otsi3Dir=[oTSIRootDir 'oTSI3'];
% fliptsi3Dir=[oTSIRootDir 'filpTSI3'];
% utsi3Dir=[oTSIRootDir 'tsi3_undist_mat'];    
% 
% 
% realtime_preprocessingPipe(realtimedv,DataDirectory,otsi2Dir,fliptsi2Dir,utsi2Dir,'tsi2');
% realtime_preprocessingPipe(realtimedv,DataDirectory,otsi3Dir,fliptsi3Dir,utsi3Dir,'tsi3');

end