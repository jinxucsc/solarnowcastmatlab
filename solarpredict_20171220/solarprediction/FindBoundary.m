function [b_uparray,b_lowarray]=...
    FindBoundary(sxarray,syarray,bSearchRange,bthres,brightthres,a,b,imgin) %#ok<INUSL>
[loopmax,~]=size(sxarray);
b_uparray=zeros(loopmax,2);
b_lowarray=zeros(loopmax,2);

if(a==-9999)
    a_verti=0;
else
    if(a==0)
        a_verti=-9999;      %stands for infinite
    else
        a_verti=-1/a;
    end
end

[m,n]=size(imgin);
mindim=min([m,n]);
vertistartrange=bSearchRange-20;
for loopi=1:loopmax
    tmpx=sxarray(loopi,1);
    tmpy=syarray(loopi,1);
    b_verti=tmpy-a_verti*tmpx;  %b=y-a*x
    [vertiranx,vertirany]=...
        FindVertiSArray(a_verti,b_verti,tmpx,tmpy,bSearchRange,vertistartrange);
    if (vertiranx(1)<mindim)        &&(vertiranx(1)>0)  &&...
            (vertiranx(end)<mindim) &&(vertiranx(end)>0)&&...
            (vertirany(1)<mindim)   &&(vertirany(1)>0)  &&...
            (vertirany(end)<mindim) &&(vertirany(end)>0)
        %   if start and end points are in minimum dimension range no need
        %   to regenerate
    else
        [vertiranx,vertirany]=ReGenVertiArrayWithDim(vertiranx,vertirany,mindim);
    end
    [tmpup,tmpdown]=...
        GetBoundInVeriArray(vertiranx,vertirany,bthres,brightthres,imgin);
    b_uparray(loopi,:)=tmpup;
    b_lowarray(loopi,:)=tmpdown;
end

end
