% mtsipipe2_realtime is looking at realtimedv and try to calculate height/layers based on history
% data
%   New version is based on modules
%   VERSION 1.0     2014-05-07
%   Previous Ver. is mtsipipe2_realtime.m
function bStatus = realtime_cldtrk(realtimedv,HRefDir,cldmodelmatf,configfpath,logger)
run(configfpath);
loggerID=TO_getloggerID();


%%
bStatus=false;  % default value
%set(0,'DefaultFigureVisible', 'on');
realtimedn=datenum(realtimedv);
HRefDir=FormatDirName(HRefDir);
if ~exist(HRefDir,'dir')
    mkdir(HRefDir);
end
HRefDir_recheck=HRefDir;
dataDir=RealtimePipeDataDir;
dataDir=FormatDirName(dataDir);

% Check hist sequences of realtimedv.
HISTN=NCLDTRKFRAMS;  % N of History frames to look up
histdns=repmat(realtimedn,[1 HISTN]);
histdns=histdns+(HISTN-1:-1:0).*-TENSECS_DN;
tmdn1=histdns(1);
tmdn2=histdns(2);
tmdn3=histdns(3);
tmdv1=datevec(tmdn1);
tmdv2=datevec(tmdn2);
tmdv3=datevec(tmdn3);

% Currently we are using bmp for realtime pipeline
f1=tmp_IsImgBMPInDataDir(tmdv1,TSI1,dataDir);
f2=tmp_IsImgBMPInDataDir(tmdv1,TSI2,dataDir);
f3=tmp_IsImgBMPInDataDir(tmdv1,TSI3,dataDir);
f4=tmp_IsImgBMPInDataDir(tmdv2,TSI1,dataDir);
f5=tmp_IsImgBMPInDataDir(tmdv2,TSI2,dataDir);
f6=tmp_IsImgBMPInDataDir(tmdv2,TSI3,dataDir);
f7=tmp_IsImgBMPInDataDir(tmdv3,TSI1,dataDir);
f8=tmp_IsImgBMPInDataDir(tmdv3,TSI2,dataDir);
f9=tmp_IsImgBMPInDataDir(tmdv3,TSI3,dataDir);

if isempty(f1)
    dispstr=sprintf('%s doens''t exist!realtime_cldtrk return.',GetImgFromDV_BNL(tmdv1,TSI1));
    logger.debug(loggerID,dispstr);
    %disp(dispstr);
    return;
end
if isempty(f2)
    dispstr=sprintf('%s doens''t exist!realtime_cldtrk return.',GetImgFromDV_BNL(tmdv1,TSI2));
    logger.debug(loggerID,dispstr);
    %disp(dispstr);
    return;
end
if isempty(f3)
    dispstr=sprintf('%s doens''t exist!realtime_cldtrk return.',GetImgFromDV_BNL(tmdv1,TSI3));
    logger.debug(loggerID,dispstr);
    %disp(dispstr);
    return;
end
if isempty(f4)
    dispstr=sprintf('%s doens''t exist!realtime_cldtrk return.',GetImgFromDV_BNL(tmdv2,TSI1));
    logger.debug(loggerID,dispstr);
    %disp(dispstr);
    return;
end
if isempty(f5)
    dispstr=sprintf('%s doens''t exist!realtime_cldtrk return.',GetImgFromDV_BNL(tmdv2,TSI2));
    logger.debug(loggerID,dispstr);
    %disp(dispstr);
    return;
end
if isempty(f6)
    dispstr=sprintf('%s doens''t exist!realtime_cldtrk return.',GetImgFromDV_BNL(tmdv2,TSI3));
    logger.debug(loggerID,dispstr);
    %disp(dispstr);
    return;
end
if isempty(f7)
    dispstr=sprintf('%s doens''t exist!realtime_cldtrk return.',GetImgFromDV_BNL(tmdv3,TSI1));
    logger.debug(loggerID,dispstr);
    %disp(dispstr);
    return;
end
if isempty(f8)
    dispstr=sprintf('%s doens''t exist!realtime_cldtrk return.',GetImgFromDV_BNL(tmdv3,TSI2));
    logger.debug(loggerID,dispstr);
    %disp(dispstr);
    return;
end
if isempty(f9)
    dispstr=sprintf('%s doens''t exist!realtime_cldtrk return.',GetImgFromDV_BNL(tmdv3,TSI3));
    logger.debug(loggerID,dispstr);
    %disp(dispstr);
    return;
end

dispstr=sprintf('Start simple cldtrk at %s.',datestr(realtimedn));
logger.debug(loggerID,dispstr);

tmf=GetImgFromDV_BNL(tmdv3,TSI3);
cldtrkmatoutf=sprintf('%s%s.cldtrk.mat',HRefDir,tmf(1:end-4));
cldtrklayersmatoutf=sprintf('%s%s.cldtrk.layers.mat',HRefDir,tmf(1:end-4));
cldtrkmatoutf_full=sprintf('%s%s.cldtrk.full.mat',HRefDir,tmf(1:end-4));
cldtrkmatoutf_2=sprintf('%s%s.cldtrk2.mat',HRefDir_recheck,tmf(1:end-4));
cldtrkmatoutf_reg=sprintf('%s%s.cldtrkreg.mat',HRefDir_recheck,tmf(1:end-4));

% %Suppose existence has been considered in previous step: set.contains(tmdn)?
% if exist(cldtrkmatoutf_reg,'file') % skip for debug only
%     bStatus=false;
%     %logger.debug(loggerID,sprintf('%s is skipped due to existence of final mat file.',datestr(realtimedn),cldtrkmatoutf_reg));
%     logger.debug(loggerID,sprintf('%s is skipped due to existence of final mat file.',datestr(realtimedn)));
%     return;
% end


%---------------------------------------------------------------------------------%
% Module 1: Simple Tracking
%---------------------------------------------------------------------------------%
[H_simple,cldsimtrkStatus]=cldtrk_simple(realtimedv,configfpath);





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Switch to open the mtsipipeline
bSkipClddet=false; 
bSkipCldfie=false;
bSkipHisref=false;
bSkipCldtrk=false;
bSkipCldclu=false;
bSkipCldrec=false;

if cldsimtrkStatus == 1
    % Simplt tracking is good. skip:
    bSkipHisref=true;   %Module3: history reference
    bSkipCldtrk=true;   %Module4: Full cloud tracking
    bSkipCldclu=true;   %Module5: Cloud layers clustering after full cloud tracking
    C_l = [H_simple,0,0;0,0,0];   % Centroid: No preference for MV
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%---------------------------------------------------------------------------------%
% Module 2: Cloud Detection
%---------------------------------------------------------------------------------%
if ~bSkipClddet
    [imma1_cldma_n,imma2_cldma_n,imma3_cldma_n]=...
        clouddetector(tmdv1,cldmodelmatf,configfpath);
    %%debug test for plotting
    %     imma1_ori=tmp_GetImgMatFromDataDir(tmdv1,TSI1,dataDir);
    %     imma2_ori=tmp_GetImgMatFromDataDir(tmdv1,TSI2,dataDir);
    %     imma3=tmp_GetImgMatFromDataDir(tmdv1,TSI3,dataDir);
    %     Red1 = imma3(:, :, 1);
    %     Green1 = imma3(:, :, 2);
    %     Blue1 = imma3(:, :, 3);
    %     HnRed1=imhist(Red1);
    %     HnGreen1=imhist(Green1);
    %     HnBlue1=imhist(Blue1);
    %     HnRed1(1)=0;HnGreen1(1)=0;HnBlue1(1)=0;     % 0 means not valid
    %     ref_sky_hist=[HnRed1 HnGreen1 HnBlue1];
    %     
    %     imma1=TO_histeq_TSISky(imma1_ori,ref_sky_hist);
    %     imma2=TO_histeq_TSISky(imma2_ori,ref_sky_hist);
    %     imma1_lum=single(TO_rgb2lum(imma1));
    %     imma2_lum=single(TO_rgb2lum(imma2));
    %     imma3_lum=single(TO_rgb2lum(imma3));
end

        
%---------------------------------------------------------------------------------%
% Module 3. Cloud fields extraction
%---------------------------------------------------------------------------------%
if ~bSkipCldfie
    [CFMaskMa,nCF]=cloudfieldext(imma3_cldma_n,configfpath);
end
if (nCF==0)
    logger.debug(loggerID,'Cloud is not detected or cloud field extraction failutre.');
    return;
end


%---------------------------------------------------------------------------------%
% Module 4. History Reference
%---------------------------------------------------------------------------------%
%tmf=GetImgFromDV_BNL(tmdv,TSI1);
if ~bSkipHisref
    [HRef,MVRef]=GetHistRefLayers(HRefDir,tmdv1,configfpath);
    if (true==isempty(HRef))
        HRef=[3400 10000];
        MVRef=[0,0];
    end
end


%---------------------------------------------------------------------------------%
% Module 5. Cloud Tracking
%   If There is H reference, then do hist check else full search
%---------------------------------------------------------------------------------%
if ~bSkipCldtrk
    bMulticore=true;
    %CldTrack_2(HRef,MVRef,CFMaskMa,tmdv,dataDir,cldtrkmatoutf,configfpath,bMulticore);
    if ~exist(cldtrkmatoutf,'file')
        CldTrack_2(HRef,MVRef,CFMaskMa,tmdv1,dataDir,cldtrkmatoutf,configfpath,bMulticore)
    end
end


%---------------------------------------------------------------------------------%
% Module 6. Clustering based on Cloud Tracking results
%   If There is H reference, then do hist check else full search
%---------------------------------------------------------------------------------%\
if ~bSkipCldclu
    t=load(cldtrkmatoutf);
    %t=load('cldtrk2_test.mat');
    H=t.H;
    Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
    Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
    HMV_x=t.HMV_x; HMV_y=t.HMV_y;
    MV_x=t.MV_x;    MV_y=t.MV_y;
    MAXCC=t.MAXCC;
    nConn=t.nCF;
    
    H_cldtrk=[];
    MV_x_cldtrk=[];
    MV_y_cldtrk=[];
    for tmi=1:nConn
        if isempty(H{tmi})
            continue;
        end
        H_cldtrk=[H_cldtrk;H{tmi}];
        MV_x_cldtrk=[MV_x_cldtrk;MV_x{tmi}];
        MV_y_cldtrk=[MV_y_cldtrk;MV_y{tmi}];
    end
    if isempty(H_cldtrk)
        dispstr=sprintf('nConn = %d, but there is no trusted height detected. Default setting will ingore this case.',nConn);
        logger.debug(loggerID,dispstr);
        bStatus=true;
        return;
    end
    
    if  length(H_cldtrk)~=1
        [IDX_l,C_l]=kmeans([double(H_cldtrk) MV_x_cldtrk MV_y_cldtrk],MAXNUMOFLAYER); % current consider two layers
    else
        C_l=[double(H_cldtrk) MV_x_cldtrk MV_y_cldtrk;0 0 0]; % current consider two layers
    end
    if abs(C_l(1,1)-C_l(2,1))<SingleLayerHDiffThreshold || C_l(2,1)==0
        % We treat two layers as one layer, use mean value of them
        C_l=[mean(H_cldtrk) mean(MV_x_cldtrk) mean(MV_y_cldtrk); 0 0 0];
        logger.debug(loggerID,['Single layer has been detected with H = ' int2str(C_l(1,1))]);
    end
end


%---------------------------------------------------------------------------------%
% Module 7. Use clustering result to get most possible MV and H
%   Use previous calculation result to recheck the tracking results. Save the final results as a
%   cldtrk matfile
%---------------------------------------------------------------------------------%
if ~bSkipCldrec
    if(C_l(1,1)<C_l(2,1) || C_l(2,1)==0)
        lowerind=1;
    else
        lowerind=2;
    end
    %logger.debug(loggerID,['Current setting will use referebce H = ' int2str(C_l(lowerind,1))]);
    HRef=C_l(lowerind,1);
    MVRef=[C_l(lowerind,2) C_l(lowerind,3)];
    
    if (cldsimtrkStatus == 1)
        bCldTrkSimple=true;
    else
        bCldTrkSimple=false;
    end
    CldTrack_recheck_2(HRef,MVRef,CFMaskMa,tmdv1,cldtrkmatoutf_reg,bCldTrkSimple,configfpath,logger);
    
    t=load(cldtrkmatoutf_reg);
    C_l=[t.H(1) t.MV(1) t.MV(2);0 0 0];
    dispstr=sprintf('HRef = %d,MVRefx = %d, MVRefy=%d. H = %d, MVx=%d, MVy=%d.',...
       HRef,MVRef(1),MVRef(2),C_l(1,1),C_l(1,2),C_l(1,3));
    logger.debug(loggerID,dispstr);
    save(cldtrklayersmatoutf,'C_l');
    bStatus=true;
end



return; % to plot temp result comment return out

%% DEBUG for Plotting and visualization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Show cloud field full search results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t=load(cldtrkmatoutf);
%t=load('cldtrk2_test.mat');
H=t.H;
Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
HMV_x=t.HMV_x; HMV_y=t.HMV_y;
MV_x=t.MV_x;    MV_y=t.MV_y;
MAXCC=t.MAXCC;
nConn=t.nCF;
tmoutDir=sprintf('CldTrackTestout_2/');
if ~exist(tmoutDir,'dir');
    mkdir(tmoutDir);
end

f1=figure();
subplot(1,3,2);
imshow(imma3);
hold on;
for tmi=1:nConn
    %         if MAXCC(tmi)==0
    %             continue;
    %         end
    
    minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
    recH=Conn_H(tmi); recW=Conn_W(tmi);
    [mcc,mi]=max(MAXCC{tmi});
    tmh=H{tmi}(mi);
    if isempty(MAXCC{tmi})
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
    elseif tmh<3000
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        [HMV_x,HMV_y]=TO_H2HMV(tmh,TSI3,configfpath);
        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(1,1),HMV_x(1,1));
        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(1,2),HMV_y(1,2));
        text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(tmh)],'FontSize',18);
    elseif tmh>=3000
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
        [HMV_x,HMV_y]=TO_H2HMV(tmh,TSI3,configfpath);
        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(1,1),HMV_x(1,1));
        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(1,2),HMV_y(1,2));
        text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(tmh)],'FontSize',18);
    end
    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
    %text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H(tmi))],'FontSize',18);
end
hold off;
subplot(1,3,3);
imshow(imma2);
hold on;
for tmi=1:nConn
    if isempty(MAXCC{tmi})
        continue;
    end
    [mcc,mi]=max(MAXCC{tmi});
    tmh=H{tmi}(mi);
    [HMV_x,HMV_y]=TO_H2HMV(tmh,TSI3,configfpath);
    minsx=Conn_sx(tmi)+HMV_x(1,2); minsy=Conn_sy(tmi)+HMV_y(1,2);
    recH=Conn_H(tmi); recW=Conn_W(tmi);
    if tmh<3000
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    else
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
    end
    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
end
hold off;
subplot(1,3,1);
imshow(imma1);
hold on;
for tmi=1:nConn
    if isempty(MAXCC{tmi})
        continue;
    end
    [mcc,mi]=max(MAXCC{tmi});
    tmh=H{tmi}(mi);
    [HMV_x,HMV_y]=TO_H2HMV(tmh,TSI3,configfpath);
    minsx=Conn_sx(tmi)+HMV_x(1,1); minsy=Conn_sy(tmi)+HMV_y(1,1);
    recH=Conn_H(tmi); recW=Conn_W(tmi);
    if tmh<3000
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    else
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
    end
    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
end
hold off;
tmoutf=sprintf('%s%s',tmoutDir,GetImgFromDV_BNL(tmdv,TSI1));
print(f1,tmoutf,'-djpeg');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%---------------------------------------------------------------------------------%
% Module 5. Re-check CF based on neighbors on TSI3
%---------------------------------------------------------------------------------%
%cldtrkmatoutf=sprintf('%s%s.cldtrk.mat',HRefDir,tmf(1:end-4));
if ~bSkipModules
    tsi3invalidma=imma3_lum==0;
    CldTrack_recheck_2(cldtrkmatoutf,cldtrkmatoutf_2,tsi3invalidma,configfpath);
end
%     if ~exist(cldtrkmatoutf_2,'file')
%     %if 1
%         CldTrack_recheck(cldtrkmatoutf,cldtrkmatoutf_2,tsi3invalidma,configfpath);
%     end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Show cloud field recheck results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         t=load(cldtrkmatoutf_2);
%     H=t.H;
%     Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
%     Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
%     HMV_x=t.HMV_x; HMV_y=t.HMV_y;
%     MAXCC=t.MAXCC;
%     nConn=t.nCF;
%     tmoutDir=sprintf('CldTrackTestout/');
%     if ~exist(tmoutDir,'dir');
%         mkdir(tmoutDir);
%     end
%     f1=figure();
%     subplot(1,3,2);
%     imshow(imma3);
%     hold on;
%     for tmi=1:nConn
%                 if MAXCC(tmi)==0
%                     continue;
%                 end
%
%         minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         if MAXCC(tmi)==0
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
%         elseif H(tmi)<3000
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
%             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
%         elseif H(tmi)>=3000
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
%             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
%         end
%         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
%         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
%         text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H(tmi))],'FontSize',18);
%     end
%     hold off;
%     subplot(1,3,3);
%     imshow(imma2);
%     hold on;
%     for tmi=1:nConn
%         if MAXCC(tmi)==6
%             continue;
%         end
%         minsx=Conn_sx(tmi)+HMV_x(tmi,1); minsy=Conn_sy(tmi)+HMV_y(tmi,1);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         if H(tmi)<3000
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         else
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%         end
%         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%     end
%     hold off;
%     subplot(1,3,1);
%     imshow(imma1);
%     hold on;
%     for tmi=1:nConn
%         if MAXCC(tmi)==0
%             continue;
%         end
%         minsx=Conn_sx(tmi)+HMV_x(tmi,2); minsy=Conn_sy(tmi)+HMV_y(tmi,2);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         if H(tmi)<3000
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         else
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%         end
%         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%     end
%     hold off;
%     tmfilename=GetImgFromDV_BNL(tmdv,TSI1);
%     tmoutf=sprintf('%s%s.recheck.jpg',tmoutDir,tmfilename(1:end-4));
%     print(f1,tmoutf,'-djpeg');
%     %continue;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%---------------------------------------------------------------------------------%
% Module 6. Generate Layers of CF based on current results
%       1. Use kmeans to find centoid of MAXNUMOFLAYER
%       2. For each centroid area heights, vote for H and MV
%       3. Aggregation of layers
%---------------------------------------------------------------------------------%
if ~bSkipModules
    if (cldsimtrkStatus == 1)
        bCldTrkSimple=true;
    else
        bCldTrkSimple=false;
    end
    CldTrack_Regulate(cldtrkmatoutf_2,cldtrkmatoutf_reg,tmdv,cldmodelmatf,dataDir,configfpath);
end
%     %if 1
%     if ~exist(cldtrkmatoutf_reg,'file')
%         CldTrack_Regulate(cldtrkmatoutf_2,cldtrkmatoutf_reg,tmdv,cldmodelmatf,dataDir,configfpath);
%     end


return;



end
