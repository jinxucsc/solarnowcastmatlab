lisfpbs_startup;
imma = imread('./multitest_fill.bmp');

[x,y]=GetBPSPts([2013 05 14 17 30 40],'tsi3',1500);
f1=figure();
imshow(imma);
hold on;
for j=1:25
    plot(x(j),y(j),'+r', 'MarkerSize',10);
    %text(x(j),y(j),['\color{black} ' 'BPS' int2str(j)],'FontSize',18);
end
hold off;
set(f1,'position',[0,0,500,500]);
set(gca,'position',[0 0 1 1],'units','normalized')
print(f1,'sfloc1.bmp','-dbmp');