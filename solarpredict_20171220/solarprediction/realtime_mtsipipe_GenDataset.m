% mtsipipe_GenDataset
%   This function is called to generate mtsi dataset for radiation forecast. Previous version is
%       test_multicld_sfplot_20131203, test_multicld_sfplot. The function will take filled images
%       and radiation values into training/test dataset based on different timerange defination
% INPUT:
%       datevecobj              --- [startdv;enddv], See datevectorparser.m for details.
%       timerangege             --- Array, prediction time/times.
%       ClearSkyValues_UTC      --- 86400 x 1 Vector, One day clear sky values in seconds
%       cldmodelmatf            --- MAT file, SVM model for cloud detection
%       configfpath             --- String, mtsi_startup filepath
%       SolarFarmRadRootDir     --- Directory, which is the root of solar radiation data (See
%                                   /home/mikegrup/D/workspace/SolarFarmDataProcessing/BPS_SP2A_H_Avg_MIN_days_dat/
%       FilledTSI3Dir           --- Directory, containing all the filled TSI3 images
%       datasetDir              --- Directory, containing all output dataset files
%       testplotDir             --- Directory, for UNIT TEST of plots
% OUTPUT:
%       datasetDir/*.mat        --- MAT files, output file
%       testplotDir/*           --- Plots for test
%
%
% PREREQUISITE:
%       mtsipipe2 ---  Get layers information and tracking results
%       mtsipipe_fill --- Generate the filling results
% VERSION: 1.0      2014/03/06
%   Previously mtsipipe_GenDataset
% VERSION: 1.1      2014/04/09  Add logger
% VERSION: 1.2      2014/04/24  Handle cloud conditions. cldcondt=clear or overcast use previous
% prediction
% VERSION: Compatable ver. for persistent output
%   
%   
% USAGE:
%     SolarFarmRadRootDir='/home/mikegrup/D/workspace/SolarFarmDataProcessing/BPS_SP2A_H_Avg_MIN_days_dat/';
%     FilledTSI3Dir='./testfill';
%     datasetDir='./dataset_20131203';
%     testplotDir='./sftestplot_20131203/';
%     mtsipipe_GenDataset([2013 05 14 0 0 0;2013 05 15 0 0 0],0,'laptop','model_rbr.mat','mtsi_startup',SolarFarmRadRootDir,FilledTSI3Dir,datasetDir,testplotDir);
%
function realtime_mtsipipe_GenDataset(realtimedv,timerange,ClearSkyValues_UTC,SVRmodelObj,configfpath,logger)
%mtsi_startup;
run(configfpath);
loggerID=TO_getloggerID();
% set(0,'DefaultFigureVisible', 'off');
cldmodelmatf=SVMCloudModelFile;
assert((exist(cldmodelmatf,'file')~=0),'Cloud Model mat file doen''s exist!');
localcorrectionDir=LocalCorrectionDir;
FilledTSI3Dir=FormatDirName(TSI3FilledDir);
%ONEDAY=60*24;   % in minutes
%BPS_N=25;       % BPS number
%ONEMIN_DN=datenum([0 0 0 0 1 0]);
%MAXRAD=950;     MINRAD=0;
TFORMAT=DATETIMEFORMAT;
daydv=realtimedv; daydv(4:6)=0; daydn=datenum(daydv);
csvals_oneday = ClearSkyValues_UTC;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MySQL Settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REPLACE INTO %s values (%d,%s,%s);";
TBName=BPSPredictionTable;
TBName1=BPSPredictionTable_full;
TBName_lc=BPSPredictionTable_lc;
%replacecmmd='REPLACE INTO %s values (%d,%s,%6.2f);';
%insertcmmd='INSERT INTO %s values(%d,%s,%s,%6.2f,%6.2f);'; % this is to save all possible prediction
%MySQLServerURL='solar.bnl.gov';
%username='realtime_matlab';
%password='realtime_matlab';
%DBName='SolarFarm';
%MySQLBinPath=MySQLBinPath;  %MySQLBinPath='';
Outputf=MySQLExeTempOutputFile;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% 1. Get the image of datevecobj and do the extraction from filled image and radiation
filename3=GetImgFromDV_BNL(realtimedv,TSI3);
% Fill image is like
% bnltsiskyimageC3.a1.20130514.144750.jpg.20130514144750_fill.bmp
f3=[filename3 '_shifttest.jpg'];
inputf3 = sprintf('%s%s',FilledTSI3Dir,f3);
imageNotExists = false;
if(~exist(inputf3,'file'))
    disp(['[WARNING]: There is no such file like: ' inputf3]);
    %return; % Don't skip to make sure it has persistent results
    imageNotExists = true;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   for current version of mtsipipe_TSI3fill.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correction: groundtruth t-1,  pred: t,
if (~imageNotExists) 
    paraspath=sprintf('%s%s_fillparas.mat',FilledTSI3Dir,filename3);
    t=load(paraspath);
    HRef=t.HRef;    MVRef=t.MVRef; %[mvx1 mvy1;mvx2 mvy2]
    cldcondt=t.cldcondt;
    H=HRef(1);
    logger.debug(loggerID,sprintf('HRef primary= %d, mv primary=[%d,%d]',H,MVRef(1,1),MVRef(1,2)));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cmmd='';
cmmd1='';
cmmd_lc='';
realtimedn=datenum(realtimedv);
tmdn_1min=realtimedn-ONEMIN_DN;
tmdv_1min=datevec(tmdn_1min);

[realrad,~]=realtime_GetRad(realtimedv,configfpath);
[realrad_1min,~]=realtime_GetRad(tmdv_1min,configfpath);

realrad_valid=realrad>0;
ONESEC_DN=datenum([0 0 0 0 0 1]);

ind=round((realtimedn-daydn)/ONESEC_DN)+1;
if ind>length(csvals_oneday) || ind<1
    MAXRAD=inf;
else
    MAXRAD=csvals_oneday(ind);
end

realrad_norm=realrad./MAXRAD;
realrad_norm(realrad_norm>1)=1;
realrad_norm(realrad_norm<0)=0;

ind=round((tmdn_1min-daydn)/ONESEC_DN)+1;
if ind>length(csvals_oneday) || ind<1
    MAXRAD=inf;
else
    MAXRAD=csvals_oneday(ind);
end
realrad_1min_norm=realrad_1min./MAXRAD;
realrad_1min_norm(realrad_1min_norm>1)=1;
realrad_1min_norm(realrad_1min_norm<0)=0;


tmline=datestr(realtimedv,TFORMAT);
for i=1:BPS_N
    tmline=[tmline ' ' num2str(realrad(i))];
end
logger.debug(loggerID,['rads from realtime_GetRad: ' tmline]);
tl=length(timerange);
% Get latest radiation value as backup forecast
utc_time = java.lang.System.currentTimeMillis;
a=utc_time/1000; % conver to UNIX TIMESTAMP
nowdn=(a/86400 + datenum(1970,1,1)); % UTC now datenum
nowdv=datevec(nowdn);
latestrad=realtime_GetRad(nowdv,configfpath);

if imageNotExists || cldcondt==1 || cldcondt==2
    if imageNotExists
        logger.debug(loggerID,'Stitched TSI3 cannot be found. Use persistent model.')
    elseif cldcondt==1
        logger.debug(loggerID,'Cloud condition is clear sky case. Use persistent model.');
    else
        logger.debug(loggerID,'Cloud condition is overcast case. Use persistent model.');
    end

    for ti=1:tl
        timeahead=timerange(ti);
        timeahead_dn=datenum([0 0 0 0 0 timeahead]);
        preddn=realtimedn+timeahead_dn;
        preddv=datevec(preddn);
        pred_unix=(preddn-datenum(1970,1,1))*86400;
        pred_est=pred_unix-5*3600;
        for j=1:BPS_N
            station=j;
            MySQLcmmd=sprintf(...
                replacecmmd,...
                TBName,station,int2str(pred_est),latestrad(station));
            cmmd=[cmmd ' ' MySQLcmmd];
            MySQLcmmd=sprintf(...
                replacecmmd,...
                TBName_lc,station,int2str(pred_est),latestrad(station));
            cmmd_lc=[cmmd_lc ' ' MySQLcmmd];
            continue;
        end
    end
    logger.debug(loggerID,'start to store the predictions into DB.');
    totalcmmd=sprintf(...
        '"%s"mysql -P 3306 -h %s -u %s -p"%s" %s -e"%s" > %s',...
        MySQLBinPath,MySQLServerURL,username,password,DBName,cmmd,Outputf...
        );
    %disp(totalcmmd);
    %logger.info(loggerID,totalcmmd);
    dos(totalcmmd);
    
    %     totalcmmd=sprintf(...
    %         '"%s"mysql -P 3306 -h %s -u %s -p"%s" %s -e"%s" > %s',...
    %         MySQLBinPath,MySQLServerURL,username,password,DBName,cmmd1,Outputf...
    %         );
    %disp(totalcmmd);
    %logger.info(loggerID,totalcmmd);
    %dos(totalcmmd);
    
    totalcmmd=sprintf(...
        '"%s"mysql -P 3306 -h %s -u %s -p"%s" %s -e"%s" > %s',...
        MySQLBinPath,MySQLServerURL,username,password,DBName,cmmd_lc,Outputf...
        );
    %disp(totalcmmd);
    %logger.info(loggerID,totalcmmd);
    dos(totalcmmd);
    return;
end



imma3=imread(inputf3);






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Read SVM Model file for CLD fraction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t=load(cldmodelmatf);
model=t.model;
x1_min=t.x1_min;
x1_max=t.x1_max;
x2_min=t.x2_min;
x2_max=t.x2_max;
x3_min=t.x3_min;
x3_max=t.x3_max;
x4_min=t.x4_min;
x4_max=t.x4_max;
h=t.h;
w_1=model.SVs'*model.sv_coef;b_1=-model.rho;
% predicted_labels=svmpredict_li(x_valid,w_1,b_1);
%     predicted_labels(predicted_labels==1)=0;        % cld
%     predicted_labels(predicted_labels==-1)=1;       % sky
x_min=[x1_min x2_min x3_min x4_min];
x_max=[x1_max x2_max x3_max x4_max];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xtrain3=tmp_img2traindata_rbr(imma3,h,x_min,x_max);
pl3=svmpredict_li(xtrain3,w_1,b_1);
pl3(pl3==1)=0;        % cld
pl3(pl3==-1)=1;       % sky
imma3_cldma=false(UNDISTH,UNDISTW);
imma3_cldma(:)=~pl3;
nTotal=UNDISTH*UNDISTW;
nCld=length(find(imma3_cldma));
%[imma3_std,imma3_std_invalid]=tmp_GetImgStdMatFromDataDir(tmdv_a,TSI3,StdBlockSize,dataDir);
%imma3_cldma_n=(imma3_std>StdCldThres|imma3_cldma)&imma3_std_invalid;
%nTotal=length(find(imma3_std_invalid));
%nCld=length(find(imma3_cldma_n));
od_cf=nCld./nTotal;
TSIFeatureWindowSize=3;
%cldpixelwinsize=9;
meanwinsize_half=floor(TSIFeatureWindowSize/2);
%cldpixelwinsize_half=floor(meanwinsize/2);
% obpoints=[122 124 125 144 147 152 162 182 184];
% obpoints=obpoints+600;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Read model file 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% modelsDir='./models/';
% startdv=[2013 05 07 0 0 0]; % startdv
% enddv=[2013 05 08 0 0 0];   % enddv
% str1=datestr(startdv,'yyyymmdd.HHMMSS');
% str2=datestr(enddv,'yyyymmdd.HHMMSS');
% tmfilename=[str1 '-' str2];
% modelfile=sprintf('%s%s_models_no19.mat',modelsDir,tmfilename);     % OUTPUT model file

% filerbf = fopen('./data/prediction_rbf.dat','w+');
% fileli = fopen('./data/prediction_li.dat','w+');



realtime_unix=(realtimedn-datenum(1970,1,1))*86400;
realtime_dbts=realtime_unix-5*3600;
modelt=SVRmodelObj;
for ti=1:tl
    timeahead=timerange(ti);
    timeahead_dn=datenum([0 0 0 0 0 timeahead]);
    preddn=realtimedn+timeahead_dn;
    preddv=datevec(preddn);
    pred_unix=(preddn-datenum(1970,1,1))*86400;
    pred_est=pred_unix-5*3600;
    ind=round((preddn-daydn)/ONESEC_DN)+1;
    if ind>length(csvals_oneday) || ind<1
        MAXRAD=inf;
    else
        MAXRAD=csvals_oneday(ind);
    end
    
    od_r=zeros(BPS_N,3); %min,max,mean
    od_g=zeros(BPS_N,3);
    od_b=zeros(BPS_N,3);
    od_prer=zeros(BPS_N,3);
    od_preg=zeros(BPS_N,3);
    od_preb=zeros(BPS_N,3);
    
    movement_x=MVRef(1,1)*round(timeahead/10);
    movement_y=MVRef(1,2)*round(timeahead/10);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    [x,y]=GetBPSPts(preddv,TSI3,H);           % SP(t) information
    [x_r,y_r]=GetBPSPts(tmdv_1min,TSI3,H);           % OneMIN ahead of SP(t-1) information
    %[x_a,y_a]=GetBPSPts(tmdv_a,TSI3,H);
    x_pred = x-movement_x; % trace back with motion vectors
    y_pred = y-movement_y;
    x_pred=round(x_pred);  y_pred=round(y_pred);
    x_r=round(x_r);   y_r=round(y_r);
    od_arad=realrad_norm;
    od_prerad=realrad_1min_norm;
    predicted_y=zeros(BPS_N,2);

    
    
    for j=1:BPS_N
        station=j;
        if y_pred(j)-meanwinsize_half<1 || y_pred(j)+meanwinsize_half>UNDISTH ||...
                x_pred(j)-meanwinsize_half<1 || x_pred(j)+meanwinsize_half>UNDISTW
            %logger.debug(loggerID,['Out of range! Use default value=' num2str(realrad_nonorm(station))]);
            MySQLcmmd=sprintf(...
                replacecmmd,...
                TBName,station,int2str(pred_est),latestrad(station));
            cmmd=[cmmd ' ' MySQLcmmd];
                        
            MySQLcmmd=sprintf(...
                replacecmmd,...
                TBName_lc,station,int2str(pred_est),latestrad(station));
            cmmd_lc=[cmmd_lc ' ' MySQLcmmd];
            continue;
        end
        if y_r(j)-meanwinsize_half<1 || y_r(j)+meanwinsize_half>UNDISTH ||...
                x_r(j)-meanwinsize_half<1 || x_r(j)+meanwinsize_half>UNDISTW
            MySQLcmmd=sprintf(...
                replacecmmd,...
                TBName,station,int2str(pred_est),latestrad(station));
            cmmd=[cmmd ' ' MySQLcmmd];
            MySQLcmmd=sprintf(...
                replacecmmd,...
                TBName_lc,station,int2str(pred_est),latestrad(station));
            cmmd_lc=[cmmd_lc ' ' MySQLcmmd];
            continue;
        end
        if ~realrad_valid(station)
            MySQLcmmd=sprintf(...
                replacecmmd,...
                TBName,station,int2str(pred_est),0);
            cmmd=[cmmd ' ' MySQLcmmd];
            MySQLcmmd=sprintf(...
                replacecmmd,...
                TBName_lc,station,int2str(pred_est),0);
            cmmd_lc=[cmmd_lc ' ' MySQLcmmd];
            continue;
        end
        
        sj=y_pred(j)-meanwinsize_half;   ej=y_pred(j)+meanwinsize_half;
        si=x_pred(j)-meanwinsize_half;   ei=x_pred(j)+meanwinsize_half;
        rvals=imma3(sj:ej,si:ei,1);
        gvals=imma3(sj:ej,si:ei,2);
        bvals=imma3(sj:ej,si:ei,3);
        od_r(j,3)=round(mean(double(rvals(:))));
        od_g(j,3)=round(mean(double(gvals(:))));
        od_b(j,3)=round(mean(double(bvals(:))));
        od_r(j,1)=round(min(double(rvals(:))));
        od_g(j,1)=round(min(double(gvals(:))));
        od_b(j,1)=round(min(double(bvals(:))));
        od_r(j,2)=round(max(double(rvals(:))));
        od_g(j,2)=round(max(double(gvals(:))));
        od_b(j,2)=round(max(double(bvals(:))));
        
        sj=y_r(j)-meanwinsize_half;   ej=y_r(j)+meanwinsize_half;
        si=x_r(j)-meanwinsize_half;   ei=x_r(j)+meanwinsize_half;
        rvals=imma3(sj:ej,si:ei,1);
        gvals=imma3(sj:ej,si:ei,2);
        bvals=imma3(sj:ej,si:ei,3);
        od_prer(j,3)=round(mean(double(rvals(:))));
        od_preg(j,3)=round(mean(double(gvals(:))));
        od_preb(j,3)=round(mean(double(bvals(:))));
        od_prer(j,1)=round(min(double(rvals(:))));
        od_preg(j,1)=round(min(double(gvals(:))));
        od_preb(j,1)=round(min(double(bvals(:))));
        od_prer(j,2)=round(max(double(rvals(:))));
        od_preg(j,2)=round(max(double(gvals(:))));
        od_preb(j,2)=round(max(double(bvals(:))));
        od_r=od_r./255;
        od_g=od_g./255;
        od_b=od_b./255;
        od_prer=od_prer./255;
        od_preg=od_preg./255;
        od_preb=od_preb./255;
        
        % Training data
        r_m=od_r(station,3);
        g_m=od_g(station,3);
        b_m=od_b(station,3);
        r_min=od_r(station,1);
        g_min=od_g(station,1);
        b_min=od_b(station,1);
        r_max=od_r(station,2);
        g_max=od_g(station,2);
        b_max=od_b(station,2);
        prer_m=od_prer(station,3);
        preg_m=od_preg(station,3);
        preb_m=od_preb(station,3);
        prer_min=od_prer(station,1);
        preg_min=od_preg(station,1);
        preb_min=od_preb(station,1);
        prer_max=od_prer(station,2);
        preg_max=od_preg(station,2);
        preb_max=od_preb(station,2);
        arad = od_arad(station);
        prerad=od_prerad(station);
        rbr=r_m/b_m;
        if(isnan(rbr))
            rbr=0;
        else
            if(rbr==inf||rbr>1.5)
                rbr=1.5;
            else
                rbr=rbr/1.5;
            end
        end
        prerbr=prer_m/preb_m;
        if(isnan(prerbr))
            prerbr=0;
        else
            if(prerbr==inf||prerbr>1.5)
                prerbr=1.5;
            else
                prerbr=prerbr/1.5;
            end
        end
        cldf=od_cf;
        xtrain_norm=...
            [r_m r_min r_max g_m g_min g_max b_m b_min b_max ...
            prer_m prer_min prer_max preg_m preg_min preg_max preb_m preb_min preb_max ...
            prerbr rbr cldf arad prerad];
        fit_y1=zeros(size(xtrain_norm,1),1);
        nfolds=5;
        y_li=zeros(nfolds,1);
        y_rbf=zeros(nfolds,1);
        for fi=1:nfolds
            model_li=modelt.models_li_allbps{ti,fi};
            model_rbf=modelt.models_rbf_allbps{ti,fi};
            %[predicted_label, accuracy, y1]=svmpredict(fit_y1,xtrain_norm,model_li);
            %[predicted_label, accuracy, y2]=svmpredict(fit_y1,xtrain_norm,model_rbf);
            [~, ~, y1]=svmpredict(fit_y1,xtrain_norm,model_li);
            [~, ~, y2]=svmpredict(fit_y1,xtrain_norm,model_rbf);
            y1(y1>1)=1; y1(y1<0)=0;
            y2(y2>1)=1; y2(y2<0)=0;
            y_li(fi)=y1;
            y_rbf(fi)=y2;
        end
        
        predicted_y(station,1)=mean(y_li)*MAXRAD;
        predicted_y(station,2)=mean(y_rbf)*MAXRAD;
%         pred_unix=(preddn-datenum(1970,1,1))*86400;
%         pred_est=pred_unix-5*3600;
        predy_ori=predicted_y(station,1); % use RBF_Li
        predy_lc=PredFilter(predy_ori,realtimedv,localcorrectionDir);

        
        
        MySQLcmmd=sprintf(...
            replacecmmd,...
            TBName,station,int2str(pred_est),predy_ori);
        cmmd=[cmmd ' ' MySQLcmmd];
        MySQLcmmd=sprintf(...
            replacecmmd,...
            TBName_lc,station,int2str(pred_est),predy_lc);
        cmmd_lc=[cmmd_lc ' ' MySQLcmmd];
        MySQLcmmd=sprintf(...
            insertcmmd,...
            TBName1,station,int2str(realtime_dbts),int2str(pred_est),predicted_y(station,1),predicted_y(station,2));
        cmmd1=[cmmd1 ' ' MySQLcmmd];
    end
    
    %fprintf(fileli,'%s %6.2f\n',datestr(preddv,TFORMAT),predicted_y(14,1));
    %fprintf(filerbf,'%s %6.2f\n',datestr(preddv,TFORMAT),predicted_y(14,2));
end

% if (0~=mysql('status'))
%     mysql('open',MySQLServerURL,username,password);
%     mysql(['use ' DBName]);
% end
% disp(cmmd);
% mysql(cmmd);
% mysql(cmmd1);
% mysql(cmmd_lc);
logger.debug(loggerID,'start to store the predictions into DB.');
totalcmmd=sprintf(...
    '"%s"mysql -P 3306 -h %s -u %s -p"%s" %s -e"%s" > %s',...
    MySQLBinPath,MySQLServerURL,username,password,DBName,cmmd,Outputf...
    );
%disp(totalcmmd);
%logger.info(loggerID,totalcmmd);
dos(totalcmmd);

totalcmmd=sprintf(...
    '"%s"mysql -P 3306 -h %s -u %s -p"%s" %s -e"%s" > %s',...
    MySQLBinPath,MySQLServerURL,username,password,DBName,cmmd1,Outputf...
    );
%disp(totalcmmd);
%logger.info(loggerID,totalcmmd);
dos(totalcmmd);

totalcmmd=sprintf(...
    '"%s"mysql -P 3306 -h %s -u %s -p"%s" %s -e"%s" > %s',...
    MySQLBinPath,MySQLServerURL,username,password,DBName,cmmd_lc,Outputf...
    );
%disp(totalcmmd);
%logger.info(loggerID,totalcmmd);
dos(totalcmmd);

% fclose(filerbf);
% fclose(fileli);


end
