function CldTrack_Regulate(cldtrkmatout_recheck,cldtrkmatout_reg,currentdatevec,cldmodelmatf,dataDir,configfpath)
run(configfpath);
%mtsi_startup_mini;
%MAXNUMOFLAYER=2;            % maximum layers for extraction
%NextFrameMVSearchWinSize=15;

t=load(cldtrkmatout_recheck);
H=t.H;
Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
HMV_x=t.HMV_x; HMV_y=t.HMV_y;
MAXCC=t.MAXCC;
nConn=t.nCF;
MV_x=t.MV_x;    MV_y=t.MV_y;
H_Reg=zeros(MAXNUMOFLAYER,1);
MV_x_Reg=zeros(MAXNUMOFLAYER,1);    MV_y_Reg=zeros(MAXNUMOFLAYER,1);
HMV_x_Reg=zeros(MAXNUMOFLAYER,2);   HMV_y_Reg=zeros(MAXNUMOFLAYER,2);
CldC_Reg=zeros(MAXNUMOFLAYER,1);
Conn_Reg=zeros(nConn,1);




%   Rule 1: valid Heights are less than MAXNUMOFLAYER which means that matching failure in this
%   case. Simply return with all 0 indicating not to use at this time
Hvalidvals=H(MAXCC>0);
if length(Hvalidvals)<=MAXNUMOFLAYER
    %     tmdv=GetDVFromImg(cldtrkmatout_recheck);
    %     HRefDir=GetDirPathFromPath(cldtrkmatout_recheck);
    %     HRefDir=FormatDirName(HRefDir);
    %     [HRef,~]=GetHistRefLayers(HRefDir,tmdv,configfpath);
    %     if isempty(HRef)
    %         return;
    %     else
    %         HRef
    %     end
    RegH=H;
    RegH(:)=0;
    RegMV_x=MV_x;   RegMV_y=MV_y;
    RegHMV_x=HMV_x;   RegHMV_y=HMV_y;
    save(cldtrkmatout_reg,'RegH','RegHMV_x','RegHMV_y','RegMV_x','RegMV_y',...
    'Conn_Reg','MV_x_Reg','MV_y_Reg','HMV_x_Reg','HMV_y_Reg','H_Reg','CldC_Reg');
    return;
end

[~,~,imma3_cldma_n]=clouddetector(currentdatevec,cldmodelmatf,dataDir,configfpath);
cldcs=Conn_H;   cldcs(:)=0;
for tmi=1:nConn
    ind=tmi;
    tmsy=Conn_sy(ind);  tmsx=Conn_sx(ind);
    tmH=Conn_H(ind);    tmW=Conn_W(ind);
    %             tmmask=false(UNDISTH,UNDISTW);
    %             tmmask(tmsy:tmsy+tmH-1,tmsx:tmsx+tmW-1)=true;
    %             tmmask=tmmask&imma3_cldma_n;
    tmblk_cld=logical(TO_GetImgVal_Pad([tmsy tmsx],[tmH tmW],imma3_cldma_n));
    cldcs(ind)=length(find(tmblk_cld));
end

H_arr=H(MAXCC>0);
cldc_arr=cldcs(MAXCC>0);
tml=length(cldc_arr);
tmcs=round(cldc_arr./100);
tmHs=[];
for li=1:tml
    tmH=zeros(tmcs(li),1);
    tmH(:)=H_arr(li);
    tmHs=[tmHs;tmH];
end



if 1==length(unique(tmHs))
    bSinglelayered=true;
else
    bValid=false;
    while bValid==false
        try
            [IDX,C]=kmeans(tmHs,MAXNUMOFLAYER);
            bValid=true;
        catch
            bValid=false;
        end
    end
    if abs(C(1)-C(2))<1000
        bSinglelayered=true;
    else
        bSinglelayered=false;
    end
    C=round(C./100)*100;
    C=sort(C);
end

RegH=H;
RegH(:)=0;
RegMV_x=MV_x;   RegMV_y=MV_y;
RegHMV_x=HMV_x;   RegHMV_y=HMV_y;
tmdn=datenum(currentdatevec);
[~,~,imma3_cldma_n]=clouddetector(currentdatevec,cldmodelmatf,dataDir,configfpath);
bFirstLayerEmpty=false; bSecondLayerEmpty=false;
if false==bSinglelayered
    indma2=abs(H-C(1))>abs(H-C(2)) & MAXCC>0;
    indma1=abs(H-C(1))<=abs(H-C(2)) & MAXCC>0;
    H_Reg(1)=C(1);  H_Reg(2)=C(2);
    if isempty(find(indma1,1))
        bFirstLayerEmpty=true;
    end
    if isempty(find(indma2,1))
        bSecondLayerEmpty=true;
    end
    % For layer1 sim check
    tmH=H_Reg(1);
    tmHsran=TO_GenHSearchRange(tmH);
    tml=length(indma1);
    cc_allsum=[];
    cldc_all=0;
    tmdns=[tmdn tmdn+TENSECS_DN];
    tmmask=true(UNDISTH,UNDISTW);
    for tmi=1:tml
        if indma1(tmi)==0
            continue;
        end
        ind=tmi;
        tmsy=Conn_sy(ind);  tmsx=Conn_sx(ind);
        tmH=Conn_H(ind);    tmW=Conn_W(ind);
        %             tmmask=false(UNDISTH,UNDISTW);
        %             tmmask(tmsy:tmsy+tmH-1,tmsx:tmsx+tmW-1)=true;
        %             tmmask=tmmask&imma3_cldma_n;
        tmblk_cld=logical(TO_GetImgVal_Pad([tmsy tmsx],[tmH tmW],imma3_cldma_n));
        cldc=length(find(tmblk_cld));
        [~,~,~,cc_all,~]=nccsim(...
            tmdns,[tmsy tmsx],[tmH tmW],NextFrameMVSearchWinSize,...
            dataDir,tmHsran,0,tmmask,CLDFRACTHRES,configfpath);
        MVstartx=-NextFrameMVSearchWinSize; MVendx=NextFrameMVSearchWinSize;
        MVstarty=MVstartx;  MVendy=MVendx;
        MVstartx=MVstartx+tmW+NextFrameMVSearchWinSize;
        MVendx=MVendx+tmW+NextFrameMVSearchWinSize;
        MVstarty=MVstarty+tmH+NextFrameMVSearchWinSize;
        MVendy=MVendy+tmH+NextFrameMVSearchWinSize;
        tmccall=cc_all(MVstarty:MVendy,MVstartx:MVendx,:);
        if isempty(cc_allsum)
            cc_allsum=tmccall;
        else
            cc_allsum=cc_allsum+tmccall;
        end
        cldc_all=cldc_all+cldc;
    end
    MVVec=-NextFrameMVSearchWinSize:NextFrameMVSearchWinSize; % same as y direction
    [tmmaxcc,ind]=max(cc_allsum(:));
    [tmy,tmx,hind]=ind2sub(size(cc_allsum),ind);
    tmH=tmHsran(hind);
    tmmvy=MVVec(tmy);
    tmmvx=MVVec(tmx);   MV=[tmmvy tmmvx];
    %if ~isempty(find(MV~=0,1))  % if MV=[0,0] treat it as noise
    MV_y_Reg(1)=MV(1);
    MV_x_Reg(1)=MV(2);
    [HMVx,HMVy]=TO_H2HMV(tmH,TSI3);
    HMV_x_Reg(1,1)=HMVx(2);    HMV_x_Reg(1,2)=HMVx(1);
    HMV_y_Reg(1,1)=HMVy(2);    HMV_y_Reg(1,2)=HMVy(1);
    H_Reg(1)=tmH;
    CldC_Reg(1)=cldc_all;
    %end
    
    % For layer2 sim check
    tmH=H_Reg(2);
    tmHsran=TO_GenHSearchRange(tmH);
    tml=length(indma2);
    cc_allsum=[];
    cldc_all=0;
    tmdns=[tmdn tmdn+TENSECS_DN];
    for tmi=1:tml
        if indma2(tmi)==0
            continue;
        end
        ind=tmi;
        tmsy=Conn_sy(ind);  tmsx=Conn_sx(ind);
        tmH=Conn_H(ind);    tmW=Conn_W(ind);
        %             tmmask=false(UNDISTH,UNDISTW);
        %             tmmask(tmsy:tmsy+tmH-1,tmsx:tmsx+tmW-1)=true;
        %             tmmask=tmmask&imma3_cldma_n;
        tmblk_cld=logical(TO_GetImgVal_Pad([tmsy tmsx],[tmH tmW],imma3_cldma_n));
        cldc=length(find(tmblk_cld));
        [~,~,~,cc_all,~]=nccsim(...
            tmdns,[tmsy tmsx],[tmH tmW],NextFrameMVSearchWinSize,...
            dataDir,tmHsran,0,tmmask,1,configfpath);
        MVstartx=-NextFrameMVSearchWinSize; MVendx=NextFrameMVSearchWinSize;
        MVstarty=MVstartx;  MVendy=MVendx;
        MVstartx=MVstartx+tmW+NextFrameMVSearchWinSize;
        MVendx=MVendx+tmW+NextFrameMVSearchWinSize;
        MVstarty=MVstarty+tmH+NextFrameMVSearchWinSize;
        MVendy=MVendy+tmH+NextFrameMVSearchWinSize;
        tmccall=cc_all(MVstarty:MVendy,MVstartx:MVendx,:);
        if isempty(cc_allsum)
            cc_allsum=tmccall;
        else
            cc_allsum=cc_allsum+tmccall;
        end
        cldc_all=cldc_all+cldc;
    end
    MVVec=-NextFrameMVSearchWinSize:NextFrameMVSearchWinSize; % same as y direction
    [tmmaxcc,ind]=max(cc_allsum(:));
    [tmy,tmx,hind]=ind2sub(size(cc_allsum),ind);
    tmH=tmHsran(hind);
    tmmvy=MVVec(tmy);
    tmmvx=MVVec(tmx);   MV=[tmmvy tmmvx];
    if ~isempty(find(MV~=0,1))  % if MV=[0,0] treat it as noise
        MV_y_Reg(2)=MV(1);
        MV_x_Reg(2)=MV(2);
        [HMVx,HMVy]=TO_H2HMV(tmH,TSI3);
        HMV_x_Reg(2,1)=HMVx(2);    HMV_x_Reg(2,2)=HMVx(1);
        HMV_y_Reg(2,1)=HMVy(2);    HMV_y_Reg(2,2)=HMVy(1);
        H_Reg(2)=tmH;
        CldC_Reg(2)=cldc_all;
    end
    
    Conn_Reg(indma1)=1;
    Conn_Reg(indma2)=2;
    RegH(indma1)=H_Reg(1);
    RegH(indma2)=H_Reg(2);
    RegMV_x(indma1)=MV_x_Reg(1);
    RegMV_y(indma1)=MV_y_Reg(1);
    RegMV_x(indma2)=MV_x_Reg(2);
    RegMV_y(indma2)=MV_y_Reg(2);
    RegHMV_x(indma1,1)=HMV_x_Reg(1,1);
    RegHMV_y(indma1,1)=HMV_y_Reg(1,1);
    RegHMV_x(indma1,2)=HMV_x_Reg(1,2);
    RegHMV_y(indma1,2)=HMV_y_Reg(1,2);
    RegHMV_x(indma2,1)=HMV_x_Reg(2,1);
    RegHMV_y(indma2,1)=HMV_y_Reg(2,1);
    RegHMV_x(indma2,2)=HMV_x_Reg(2,2);
    RegHMV_y(indma2,2)=HMV_y_Reg(2,2);
    
    % Similarity check for invalid Cloud Fields
    %   Current version is simply choose the most frequent cloud layer
    for tmi=1:nConn
        if MAXCC(tmi)>0
            continue;
        end
        [~,indexi]=max(CldC_Reg);
        RegH(tmi)=H_Reg(indexi);
        RegMV_x(tmi)=MV_x_Reg(indexi);
        RegMV_y(tmi)=MV_y_Reg(indexi);
        RegHMV_x(tmi,1)=HMV_x_Reg(indexi,1);
        RegHMV_y(tmi,1)=HMV_y_Reg(indexi,1);
        RegHMV_x(tmi,2)=HMV_x_Reg(indexi,2);
        RegHMV_y(tmi,2)=HMV_y_Reg(indexi,2);
    end
    
else
    indma1= MAXCC>0;
    H_Reg(1)=round(mean(H(indma1)));
    H_Reg(1)=round(H_Reg(1)./100)*100;
    % For layer1 sim check
    tmH=H_Reg(1);
    tmHsran=TO_GenHSearchRange(tmH);
    tml=length(indma1);
    cc_allsum=[];
    cldc_all=0;
    tmdns=[tmdn tmdn+TENSECS_DN];
    tmmask=true(UNDISTH,UNDISTW);
    for tmi=1:tml
        if indma1(tmi)==0
            continue;
        end
        ind=tmi;
        tmsy=Conn_sy(ind);  tmsx=Conn_sx(ind);
        tmH=Conn_H(ind);    tmW=Conn_W(ind);
        %             tmmask=false(UNDISTH,UNDISTW);
        %             tmmask(tmsy:tmsy+tmH-1,tmsx:tmsx+tmW-1)=true;
        %             tmmask=tmmask&imma3_cldma_n;
        tmblk_cld=logical(TO_GetImgVal_Pad([tmsy tmsx],[tmH tmW],imma3_cldma_n));
        cldc=length(find(tmblk_cld));
        [~,~,~,cc_all,~]=nccsim(...
            tmdns,[tmsy tmsx],[tmH tmW],NextFrameMVSearchWinSize,...
            dataDir,tmHsran,0,tmmask,CLDFRACTHRES,configfpath);
        MVstartx=-NextFrameMVSearchWinSize; MVendx=NextFrameMVSearchWinSize;
        MVstarty=MVstartx;  MVendy=MVendx;
        MVstartx=MVstartx+tmW+NextFrameMVSearchWinSize;
        MVendx=MVendx+tmW+NextFrameMVSearchWinSize;
        MVstarty=MVstarty+tmH+NextFrameMVSearchWinSize;
        MVendy=MVendy+tmH+NextFrameMVSearchWinSize;
        tmccall=cc_all(MVstarty:MVendy,MVstartx:MVendx,:);
        if isempty(cc_allsum)
            cc_allsum=tmccall;
        else
            cc_allsum=cc_allsum+tmccall;
        end
        cldc_all=cldc_all+cldc;
    end
    MVVec=-NextFrameMVSearchWinSize:NextFrameMVSearchWinSize; % same as y direction
    [tmmaxcc,ind]=max(cc_allsum(:));
    [tmy,tmx,hind]=ind2sub(size(cc_allsum),ind);
    tmH=tmHsran(hind);
    tmmvy=MVVec(tmy);
    tmmvx=MVVec(tmx);   MV=[tmmvy tmmvx];
    if ~isempty(find(MV~=0,1))  % if MV=[0,0] treat it as noise
        MV_y_Reg(1)=MV(1);
        MV_x_Reg(1)=MV(2);
        [HMVx,HMVy]=TO_H2HMV(tmH,TSI3);
        HMV_x_Reg(1,1)=HMVx(2);    HMV_x_Reg(1,2)=HMVx(1);
        HMV_y_Reg(1,1)=HMVy(2);    HMV_y_Reg(1,2)=HMVy(1);
        H_Reg(1)=tmH;
        CldC_Reg(1)=cldc_all;
    end
    
    RegH(indma1)=H_Reg(1);
    RegMV_x(indma1)=MV_x_Reg(1);
    RegMV_y(indma1)=MV_y_Reg(1);
    RegHMV_x(indma1,1)=HMV_x_Reg(1,1);
    RegHMV_y(indma1,1)=HMV_y_Reg(1,1);
    RegHMV_x(indma1,2)=HMV_x_Reg(1,2);
    RegHMV_y(indma1,2)=HMV_y_Reg(1,2);
    
    % Similarity check for invalid Cloud Fields
    %   Current version is simply choose the most frequent cloud layer
    for tmi=1:nConn
        if MAXCC(tmi)>0
            continue;
        end
        indexi=1;
        RegH(tmi)=H_Reg(indexi);
        RegMV_x(tmi)=MV_x_Reg(indexi);
        RegMV_y(tmi)=MV_y_Reg(indexi);
        RegHMV_x(tmi,1)=HMV_x_Reg(indexi,1);
        RegHMV_y(tmi,1)=HMV_y_Reg(indexi,1);
        RegHMV_x(tmi,2)=HMV_x_Reg(indexi,2);
        RegHMV_y(tmi,2)=HMV_y_Reg(indexi,2);
    end
end

save(cldtrkmatout_reg,'RegH','RegHMV_x','RegHMV_y','RegMV_x','RegMV_y',...
    'Conn_Reg','MV_x_Reg','MV_y_Reg','HMV_x_Reg','HMV_y_Reg','H_Reg','CldC_Reg');


end