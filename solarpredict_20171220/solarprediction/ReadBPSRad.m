function od_rad=ReadBPSRad(sfradDir,tmdv)
lisfpbs_startup;
tmdn=datenum(tmdv);
ONEDAY=60*24;
ONEMINDN=datenum([0 0 0 0 1 0]);
od_rad=zeros(ONEDAY,NPBS);
od_ts=zeros(ONEDAY,NPBS);
sfradDir=FormatDirName(sfradDir);
for i=1:NPBS
    bpssubDir=sprintf('%sBPS_%d',sfradDir,i);
    bpssubDir=FormatDirName(bpssubDir);
    filenamestr=datestr(tmdv,BPSDATTIMEFORMAT);
    inputf=sprintf('%s%s.dat',bpssubDir,filenamestr);
    fid = fopen(inputf);
    data = textscan(fid,'%d\t%f','HeaderLines',1,'CollectOutput',1);
    data1 = data{1}; data2 = data{2};
    fclose(fid);
    if length(data1)==ONEDAY
        od_rad(:,i)=data2(:);
        od_ts(:,i)= unixts2datenum(data1);
    else
        tmdns=unixts2datenum(data1);
        tminds=round((tmdns-tmdn)./ONEMINDN)+1;
        od_rad(tminds,i)=data2(:);
    end
end



end



