% realtime_rad is to poll radiation values from remote database
%   VERSION 1.0 2014-05-19
function [realtime_rad,realtime_dns]=realtime_GetRad(realtimedv,configfpath)
run(configfpath);   % mtsi_startup
%RADFILE='./data/BPSSEC.dat';
[qcstatus,tmdn] = realtime_qcvalid(realtimedv,BPSRadiationQualityControlFile);
invalidbpslist=qcstatus<=0;
realtimedn=datenum(realtimedv);
BPSN=BPS_N;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% InsertPrediction()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REPLACE INTO %s values (%d,%s,%s);";
TBName=BPSRealtimeTable;
%MySQLServerURL='solar.bnl.gov';
%username='realtime_matlab';
%password='realtime_matlab';
%DBName='SolarFarm';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% select least dist
if (0~=mysql('status'))
    mysql('open',MySQLServerURL,username,password);
    mysql(['use ' DBName]);
end
leastcmmd_bps= 'select abs(time_stamp-%d) as d,time_stamp,BPS_N,SP2A_H_Avg from %s where BPS_N=%d order by d limit 0,1;';
%selectcmmd_bps='select SP2A_H_Avg from %s where time_stamp = %d and BPS_N=%d;';
selectcmmd_bps='select SP2A_H_Avg,BPS_N from %s where time_stamp = %d;';
ts_gmt=int32(TO_dn2unix(realtimedn));
ts_est=int32(ts_gmt-5*3600);
dbtable=TBName;
realtime_rad=zeros(BPSN,1);
realtime_dns=zeros(BPSN,1);


cmmd=sprintf(selectcmmd_bps,dbtable,ts_est);
[rad,stns]=mysql(cmmd);
realtime_rad(stns)=rad;
ind = find(~invalidbpslist' & realtime_rad==0);
for tmi=1:length(ind)
    i=ind(tmi);
    cmmd=sprintf(leastcmmd_bps,ts_est,dbtable,i);
    [d,time_stamp,~,rad]=mysql(cmmd);
    if (~isempty(d)) 
        realtime_dns(i)=TO_unix2dn(time_stamp+3600*5);
        realtime_rad(i)=rad;
    end
end

mysql('close');




% for i=1:BPSN
%     if invalidbpslist(i)
%         continue;
%     end
%     cmmd=sprintf(selectcmmd_bps,dbtable,ts_est,i);
%     rad=mysql(cmmd);
%     time_stamp=ts_est;
%     if isempty(rad)
%         cmmd=sprintf(leastcmmd_bps,ts_est,dbtable,i);
%         [d,time_stamp,~,rad]=mysql(cmmd);
%     end
%     realtime_dns(i)=TO_unix2dn(time_stamp+3600*5);
%     realtime_rad(i)=rad;
% end
% fid = fopen(RADFILE);
% tline = fgets(fid);
% TFORMAT='yyyy-mm-dd HH:MM:SS';
% BPSN=25;
% %TWOMINDN=datenum([0 0 0 0 2 0]);
% mindiff=realtimedn;
% mindn=0;
% minrads=zeros(1,BPSN);
% while ischar(tline)
%     C=strsplit(tline);
%     tline = fgets(fid);
%     dstr=C{1};
%     timestr=C{2};
%     tstr=[dstr ' ' timestr];
%     tmdv=datevec(tstr,TFORMAT);
%     tmdn=datenum(tmdv);
%     if(abs(tmdn-realtimedn)>=mindiff)
%         continue;
%     end
%     mindiff=abs(tmdn-realtimedn);
%     mindn=tmdn;
%     for i=1:BPSN
%         if (~invalidbpslist(i))
%             rad=C{2+i};
%             minrads(i)=str2double(rad);
%         end
%     end
% end
% fclose(fid);
% realtime_rad=minrads';
% realtime_dns=zeros(BPSN,1);
% realtime_dns(:)=mindn;


% % find the nearest one
% TWOMINDN=datenum([0 0 0 0 2 0]);
%
% for i=1:25
%     ind=Ns==i;
%     dnss=tmdns(ind);
%     radss=rads(ind);
%     [minval,mini]=min(abs(dnss-realtimedn));
%     if(minval<TWOMINDN)
%         realtime_rad(i)=radss(mini);
%         realtime_dns(i)=dnss(mini);
%     else
%         realtime_rad(i)=0;
%         realtime_dns(i)=-1;
%     end
% end


end