%2012-04-12 Version 1.1
%   add controlling area code of vertisran
function [sranx,srany]=FindVertiSArray(a,b,x0,y0,vertirange,vertisran)
        sranx=zeros(2*vertirange+1,1);
        srany=zeros(2*vertirange+1,1);
        if(a==-9999)
            sranx(:)=x0;
            srany(:)=y0-vertirange:y0+vertirange;
            return;
        end
        %stepdist=1;
        verdist=2*vertirange+1;
        [tmpsx,tmpsy]=GetEndPoint(x0,y0,a,b,vertisran);
        %control area that should avoid searching
        [minsx,~]=min(tmpsx);
%         minsy=tmpsy(1,tmpindex);
        [minex,~]=max(tmpsx);
%         miney=tmpsy(1,tmpindex);
        
        [tmpx,tmpy]=GetEndPoint(x0,y0,a,b,vertirange);
        [sranx(1),tmpindex]=min(tmpx);
        srany(1)=tmpy(1,tmpindex);
        
        for tmpj=2:verdist
            [looptmx,looptmy]=GetEndPoint(sranx(1),srany(1),a,b,tmpj-1);
            [sranx(tmpj),tmpindex]=max(looptmx);
            srany(tmpj)=looptmy(1,tmpindex);
            if(sranx(tmpj)>minsx&&sranx(tmpj)<minex)
                sranx(tmpj)=-1; %   in the control area
                srany(tmpj)=-1; %   -1 means no searching
            end
        end
        sranx=round(sranx);
        srany=round(srany);
    end


