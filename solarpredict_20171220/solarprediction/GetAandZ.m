%   This function is to calculate the azimuth and zenith based on time
%   vector and location information
%   VERSION 1.0     2012-04-18

function [A,Z]=GetAandZ(datevector,location)
    time.year    =  datevector(1);
    time.month   =  datevector(2);
    time.day     =  datevector(3);
    time.hour    =  datevector(4);
    time.min    =   datevector(5);
    time.sec    =   datevector(6);
    time.UTC    =   0;
%     location.longitude = 147.42500;
%     location.latitude = -2.0610001;
%     location.altitude = 4;        %metre
    sun = sun_position(time, location);  % compute the sun position
    Z=sun.zenith;
    Z=Z/180*pi;
    A=sun.azimuth;
    A=A/180*pi;
    %A=A/180*pi;
end