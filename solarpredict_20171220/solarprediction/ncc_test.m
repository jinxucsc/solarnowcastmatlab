function R=ncc_test(template1,image_whole)
nan_ma=isnan(template1);
if isempty(find(nan_ma,1))
    disp('Use normxcorr2');
    R=normxcorr2(template1,image_whole);
    return;
end
%   DEBUG
% figure();
% subplot(1,2,1);
% imshow(~nan_ma);
% subplot(1,2,2);
% imshow(template1);


notnan_ind=find(~nan_ma);
x=template1(notnan_ind);
x_s=x(:);
x_a=mean(x_s);
x_d=std(x_s);
calX=x-x_a;
x_norm=calX/x_d;

[tH,tW]=size(template1);
[iH,iW]=size(image_whole);
image_patch=zeros(tH+iH+tH,tW+iW+tW);     
image_patch(tH+1:tH+iH,tW+1:tW+iW)=image_whole(:,:);

nSearchH=tH+iH-1;
nSearchW=tW+iW-1;
R=zeros(nSearchH,nSearchW);
tmN=tH*tW;
y_d_ma=zeros(nSearchH,nSearchW);
%y_ma=zeros(length(notnan_ind),nSearchH*nSearchW);
for i =1:nSearchH
    for j=1:nSearchW
        tmblk=image_patch(i:i+tH-1,j:j+tW-1);
        y=tmblk(notnan_ind);
        y_a=mean(y(:));
        y_d=std(y(:));
        calY=y-y_a;
        %y_norm=(y-y_a)./y_d;
        %y_ma(:,(j-1)*SearchH+i)=y_norm;
        %c=sum(calX.*calY)/(x_d*y_d)/tmN;
        c=sum(calX.*calY);
        R(i,j)=c;
        y_d_ma(i,j)=y_d;
    end
end
% R_norm=conv2(x_norm,y_norm,'same');
% R_t=R_norm(1,:);
% R(:,:)=R_t(:);

R=R./y_d_ma./x_d/tmN;

end