% Estimate height of cloud layer
function HEstimate(datevector,outputmatf,configfpath)
run(configfpath);

tmdv=datevector;
tmdn=datenum(tmdv);

% Current verion is using TSI2 and TSI3 pair ONLY
t=load(HeightMatrixValidMatf);
hma32_valid=t.H_valid_32;
HSearchRange=unique(hma32_valid);   % Total H search range
L=length(HSearchRange); 
[tmHMV_x,tmHMV_y]=TO_H2HMV(HSearchRange,TSI3,configfpath);
nccvals=zeros(L,1);
cldmatchfraction=zeros(L,1);

% Read TSI2 and TSI3 images
imma2=TO_imread(tmdv,utsi2Dir,TSI2);
imma3=TO_imread(tmdv,utsi3Dir,TSI3);
[~,imma2_cldma,imma3_cldma]=clouddetector(tmdv,SVMCloudModelFile,configfpath); % get cloud mask
imma2_lum=TO_rgb2lum(imma2);
imma3_lum=TO_rgb2lum(imma3);
invalid3=imma3_lum<10;
invalid2=imma2_lum<10;
imma3(TO_3DMaskfrom2D(invalid3))=nan;
imma3_skyma=~imma3_cldma & ~invalid3;
imma2_skyma=~imma2_cldma & ~invalid2;

parfor i=1:L
    tmHMV_x31=tmHMV_x(i,1);tmHMV_x32=tmHMV_x(i,2);
    tmHMV_y31=tmHMV_y(i,1);tmHMV_y32=tmHMV_y(i,2);
    imma2_s=TO_imshift(imma2,[-tmHMV_y32 -tmHMV_x32]);
    valid2_s=logical(TO_imshift(~invalid2,[-tmHMV_y32 -tmHMV_x32]));
    imma2_cldma_s=logical(TO_imshift(imma2_cldma,[-tmHMV_y32 -tmHMV_x32]));
    imma2_skyma_s=logical(TO_imshift(imma2_skyma,[-tmHMV_y32 -tmHMV_x32]));
    
    validall=valid2_s&~invalid3;    % Both TSI2 and TSI3 should be valid
    r2=double(imma2_s(:,:,1));   b2=double(imma2_s(:,:,3));
    r3=double(imma3(:,:,1));    b3=double(imma3(:,:,3));
    rbr2=r2./(r2+b2);    rbr3=r3./(r3+b3);
    rbr2(imma2_skyma_s)=0.1;
    rbr3(imma3_skyma)=0.1;
    vals2=double(rbr2(validall));
    vals3=double(rbr3(validall));
    m2=mean(vals2); m3=mean(vals3);
    stdval2=std(vals2); stdval3=std(vals3);
    z =(vals2-m2).*(vals3-m3);
    nccvals(i)=sum(z)/length(vals2)/(stdval2*stdval3); % minimize the -val = maximize val
    
    cldall3_n=length(find(imma3_cldma & validall));
    cldmatch3_n=length(find(imma3_cldma & validall & imma2_cldma_s));
    cldmatchfraction(i)=cldmatch3_n/cldall3_n;
end

save(outputmatf,'nccvals','cldmatchfraction','tmdv');


end