%   function is to get cross validation data set, including traing and test
%   data.
function [trainma,testma]=CVInstDataExt(cvinstance)
NumTestSets=cvinstance.NumTestSets; % Total folds 
N=cvinstance.N;
% TestSize=cvinstance.TestSize;
% TrainSize=cvinstance.TrainSize;
trainma=false(N,NumTestSets);
testma=false(N,NumTestSets);
for i=1:NumTestSets
    trainma(:,i)=training(cvinstance,i);
    testma(:,i)=test(cvinstance,i);
end

end
